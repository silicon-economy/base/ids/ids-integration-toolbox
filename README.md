> This project has been archived. It is no longer maintained or updated. If you have any questions about the project, please contact info@openlogisticsfoundation.org.


<div align="center">
  <h2 align="center">IDS Integration Toolbox</h2>
  Java framework for configuring and managing the <a href="https://github.com/International-Data-Spaces-Association/DataspaceConnector">Dataspace Connector (DSC)</a>.
</div>



## Table of Contents

- [About](#about)
  - [Background](#background)
- [Getting Started](#getting-started)
  - [Prerequirements](#prerequirements)
  - [Quick Start](#quick-start)
- [Contributing](#contributing)
- [License](#license)
- [Developers](#developers)


## About

The *IDS Integration Toolbox* (formerly known as *Wrapper Toolbox*) is a Java framework that aims to help developers to
configure and manage the
[Dataspace Connector (DSC)](https://github.com/International-Data-Spaces-Association/DataspaceConnector)
on code level. It provides a bunch of useful tools and methods to easily integrate new or
existing systems into the IDS ecosystem. Therefore, deeper knowledge in the IDS concepts or DSC
API enpoints aren't needed.

### Background
The [International Data Spaces (IDS)](https://internationaldataspaces.org/) initiative, formed
by different research and industry partners across the globe, aims to create a decentralized
ecosystem to enable secure data exchange under the consideration of data sovereignty and
interoperability. One core component that is needed to participate is an IDS Connector, for
example the [Dataspace Connector (DSC)](https://github.com/International-Data-Spaces-Association/DataspaceConnector)
that follows the [IDS Reference Architecture Model (RAM)](https://www.internationaldataspaces.org/wp-content/uploads/2019/03/IDS-Reference-Architecture-Model-3.0.pdf).


## Getting Started

### Prerequirements
The following things are nedded to use the IDS Integration Toolbox:

- A Java Runtime Environment or an equivalent Docker setup
- Maven
- A running Dataspace Connector instance
  - More information can be found in the [DSC documentation](https://international-data-spaces-association.github.io/DataspaceConnector/GettingStarted)


### Quick Start
1. Create an empty Java Spring project.
2. Add the IDS Integration Toolbox as a maven dependency in your `pom.xml` (should be available via [Maven Central](https://search.maven.org/)):
```
<dependency>
  <groupId>org.siliconeconomy</groupId>
  <artifactId>ids-integration-toolbox</artifactId>
  <version>${version.you.desire}</version>
</dependency>
```
3. Add the packages to your Spring application:
```
@SpringBootApplication(scanBasePackages = {"org.siliconeconomy.idsintegrationtoolbox"})
```
4. Integrate all necessary properties to your `application.properties`. They can be found in
   `src/main/resources/example.properties`.
5. You are ready to use all the provided tools and functions!

More and detailed information can be found in our documentation.

### In-Depth
For additional information, check out the [documentation](https://git.openlogisticsfoundation.org/silicon-economy/base/ids/ids-integration-toolbox/-/tree/main/docs) and especially the Getting-Started Guide
for an in-depth overview of the library-usage and integration.

## Contributing
Contributions to this project are greatly appreciated! Code changes are handled over pull requests.
Detailed information can be found in the `CONTRIBUTING.md` guidelines and the `CODE_OF_CONDUCT.md`.


## License
Copyright © Open Logistics Foundation. The project is licensed under the [Open Logistics Foundation License 1.3](https://openlogisticsfoundation.org/licenses/). For details on the licensing terms, see the `LICENSE` file.

## Developers

### Active
- Ronja Quensel ([Fraunhofer ISST](https://www.isst.fraunhofer.de/en.html))
- Haydar Qarawlus ([Fraunhofer ISST](https://www.isst.fraunhofer.de/en.html))
- Malte Hellmeier ([Fraunhofer ISST](https://www.isst.fraunhofer.de/en.html))

### Inactive
- Johannes Pieperbeck ([Fraunhofer ISST](https://www.isst.fraunhofer.de/en.html))
- Steffen Biehs ([Fraunhofer ISST](https://www.isst.fraunhofer.de/en.html))
- Marc Peschke ([Fraunhofer IML](https://www.iml.fraunhofer.de/en.html))
- Florian Zimmer ([Fraunhofer ISST](https://www.isst.fraunhofer.de/en.html))
