/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.manual;

import java.net.URI;
import java.util.List;

import de.fraunhofer.iais.eis.Action;
import de.fraunhofer.iais.eis.BaseConnector;
import de.fraunhofer.iais.eis.PermissionBuilder;
import de.fraunhofer.iais.eis.Resource;
import de.fraunhofer.iais.eis.Rule;
import de.fraunhofer.iais.eis.util.TypedLiteral;
import de.fraunhofer.iais.eis.util.Util;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.siliconeconomy.idsintegrationtoolbox.core.ids.MessagesApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.model.input.SubscriptionInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * MessagesApiOperatorManualTest
 *
 * @author Florian Zimmer
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
@ContextConfiguration(classes = {IntegrationTestConfiguration.class})
@TestPropertySource(locations = "classpath:example.properties")
@Disabled("For manual testing purposes only")
class MessagesApiOperatorManualTest {

    @Autowired
    MessagesApiOperator messagesApiOperator;

    String resourceId = "http://localhost:8080/api/offers/d89313ff-14b6-467e-bf70-c24f2bde14ca";
    String artifactId_ofResource = "http://localhost:8080/api/artifacts/7e934a20-9edf-4545-bdee" +
        "-81582ce7ea45";

    @Test
    void sendDownloadAppMessage_shouldDownloadApp() {
        final var appStore = URI.create("https://binac.fit.fraunhofer.de/");

        assertDoesNotThrow(() -> messagesApiOperator.appDownload(appStore, URI.create(
            "appId")));
    }

    @Test
    void sendContractRequest_shouldReturnContract() throws Exception {
        assertTrue(messagesApiOperator.contractNegotiation(
            URI.create("http://localhost:8080/api/ids/data"), List.of(URI.create(resourceId)),
            List.of(URI.create(artifactId_ofResource)), false,
            List.of(getRule())).getRemoteId().toString().contains("http://localhost:8080/api" +
            "/agreements/"));
    }

    @Test
    void sendDescriptionRequest_shouldReturnBaseConnector() throws Exception {
        assertTrue(messagesApiOperator.descriptionRequest(
            URI.create("http://localhost:8080/api/ids/data")) instanceof BaseConnector);
    }

    @Test
    void sendDescriptionRequestWithElementId_shouldReturnResource() throws Exception {
        assertTrue(messagesApiOperator.descriptionRequest(
            URI.create("http://localhost:8080/api/ids/data"),
            URI.create(resourceId)) instanceof Resource);
    }

    /**
     * This methods runs several test methods directly at the below specified broker.
     * They are meaningfully sorted in order to prevent producing garbage at the broker, but
     * rather cleaning the created resources afterwards.
     */
    @Test
    @SneakyThrows
    void sendBrokerMessages() {
        final var broker = URI.create("https://broker.ids.isst.fraunhofer.de/infrastructure");

        //1. Connector Update
        assertDoesNotThrow(() -> messagesApiOperator.connectorUpdate(broker));

        //2. Resource Update
        assertDoesNotThrow(() -> messagesApiOperator.resourceUpdate(broker,
            URI.create(resourceId)));

        //3. Ids Search
        assertThat(messagesApiOperator.search(broker, "ISST", 50, 0))
            .contains("?resultUri")
            .contains("?res")
            .contains("?type")
            .contains("?accessUrl")
            .contains("?internalUri")
            .contains("ISST");

        //4. Resource Unavailable
        assertDoesNotThrow(() -> messagesApiOperator.resourceUnavailable(broker,
            URI.create(resourceId)));

        //5. Connector Unavailable
        assertDoesNotThrow(() -> messagesApiOperator.connectorUnavailable(broker));
    }

    @Test
    void sendSubscribeRequest_shouldReturnSuccess() throws Exception {
        assertThat(messagesApiOperator.subscribe(URI.create("http://localhost:8080/api" +
            "/ids/data"), getSubscriptionMessage()))
            .isEqualTo("Successfully subscribed to " + artifactId_ofResource + ".");
    }

    @Test
    void sendUnsubscribeRequestWithElementId_shouldReturnSuccess() throws Exception {
        messagesApiOperator.subscribe(URI.create("http://localhost:8080/api" +
            "/ids/data"), getSubscriptionMessage());

        assertThat(messagesApiOperator.unsubscribe(URI.create("http://localhost:8080" +
            "/api" +
            "/ids/data"), URI.create(artifactId_ofResource)))
            .isEqualTo("Successfully unsubscribed from " + artifactId_ofResource + ".");
    }

    @Test
    void sendNotifyMessageWithElementId() {
        assertDoesNotThrow(() -> messagesApiOperator.notify(URI.create(artifactId_ofResource)));
    }

    /**
     * In order for the following test to succeed, the DSC first needs to log messages to the
     * Clearing House:
     *      - enable property clearing.house.url (commented out by default)
     *      - set deploy mode to PRODUCTIVE_DEPLOYMENT in config.json
     *      - set keystore in config.json to valid IDS certificate
     *      - start 1 or 2 DSCs
     *      - run one of the DSC's test scripts that performs a data transfer
     *  Afterwards, replace the processId in this test with the agreement's UUID (provider side).
     */
//    @Test
//    @SneakyThrows
//    void queryClearingHouse() {
//        final var clearingHouseUrl = URI.create("https://ch-ids.aisec.fraunhofer.de");
//        final var processId = "ed8efa0c-163b-4690-801a-0f76fb082093";
//
//        final var response = messagesApiOperator.queryClearingHouse(clearingHouseUrl, processId);
//
//        assertThat(response).isNotEmpty();
//
//        // Should print the agreement once, the ArtifactRequestMessage twice and the
//        // ArtifactResponseMessage twice
//        response.forEach(e -> System.out.println(e.getDecodedPayload()));
//    }

    private Rule getRule() {
        return new PermissionBuilder(URI.create("https://w3id.org/idsa/autogen/permission" +
            "/591467af-9633-4a4e-8bcf-47ba4e6679ea"))
            ._title_(Util.asList(new TypedLiteral("Example Usage Policy")))
            ._description_(Util.asList(new TypedLiteral("usage-logging")))
            ._action_(Util.asList(Action.USE))
            ._target_(URI.create(artifactId_ofResource))
            .build();
    }

    private SubscriptionInput getSubscriptionMessage() {
        return new SubscriptionInput(URI.create(artifactId_ofResource),
            URI.create("http://localhost:8080/api/ids/data"),
            URI.create("http://localhost:8080/api/ids/data"),
            true);
    }
}
