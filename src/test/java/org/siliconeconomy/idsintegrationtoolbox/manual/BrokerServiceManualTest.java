/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.manual;

import java.net.URI;
import java.time.ZonedDateTime;
import java.util.Collections;

import de.fraunhofer.iais.eis.ids.jsonld.Serializer;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.OfferedResourceCatalogsApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.model.input.ArtifactInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.CatalogInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.ContractInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.OfferedResourceInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.RepresentationInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.RuleInput;
import org.siliconeconomy.idsintegrationtoolbox.utils.PolicyUtils;
import org.siliconeconomy.idsintegrationtoolbox.utils.UuidUtils;
import org.siliconeconomy.idsintegrationtoolbox.workflows.BrokerService;
import org.siliconeconomy.idsintegrationtoolbox.workflows.ResourceCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * BrokerServiceManualTest
 *
 * @author Ronja Quensel
 */
@SpringBootTest
@ContextConfiguration(classes = {IntegrationTestConfiguration.class})
@TestPropertySource(locations = "classpath:example.properties")
@Disabled("For manual testing purposes only")
class BrokerServiceManualTest {

    @Autowired
    private BrokerService brokerService;

    @Autowired
    private ResourceCreator resourceCreator;

    @Autowired
    private OfferedResourceCatalogsApiOperator resourceCatalogsApiOperator;

    private final URI brokerUrl = URI.create("https://broker1.mds-demo.dataspace.sovity.de/infrastructure");

    private final String searchTerm = "SELE Toolbox test";

    /**
     * Tests registering, querying and unregistering the connector and its resources at the broker.
     * The resources are created by this test and do not have to be created beforehand.
     *
     * As the broker is queried to assert that the connector and resources have been correctly
     * registered/unregistered, the DSC used for running this test should include
     * "SELE Toolbox test" in its title (can be changed in config.json), so that it can be found
     * with the same search term as the resources created in this test.
     */
    @Test
    @SneakyThrows
    void registerAndUnregister() {
        // Create a complete resource
        var resource1 = resourceCreator.createResource(getResource1(),
            getCatalog(), getRepresentation(), getArtifact1(), getContract(), getRule());

        // Register connector with all resources
        brokerService.registerAllResources(brokerUrl);

        // Query the broker -> should show connector, resource 1 and artifact 1
        System.out.println();
        System.out.println(brokerService.search(brokerUrl, searchTerm));
        System.out.println();

        // Get the ID of the created catalog
        var resource1Uuid = UuidUtils.getUuidFromOutput(resource1);
        var catalog = resourceCatalogsApiOperator.getAll(resource1Uuid)
            .getEmbedded().getEntries().get(0);
        var catalogId = URI.create(catalog.getLinks().getSelf().getHref());

        // Create another resource
        var resource2 = resourceCreator.createResource(getResource2(),
            catalogId, getRepresentation(), getArtifact2(), getContract(), getRule());
        var resource2Id = URI.create(resource2.getLinks().getSelf().getHref());

        // Register only that resource
        brokerService.registerResources(brokerUrl, Collections.singletonList(resource2Id));

        // Query the broker -> should show connector, both resources and both artifacts
        System.out.println();
        System.out.println(brokerService.search(brokerUrl, searchTerm));
        System.out.println();

        // Unregister second resource
        brokerService.unregisterResources(brokerUrl, Collections.singletonList(resource2Id));

        // Query the broker -> should show connector, resource 1 and artifact 1
        System.out.println();
        System.out.println(brokerService.search(brokerUrl, searchTerm));
        System.out.println();

        // Unregister connector
        brokerService.unregister(brokerUrl);

        // Query the broker -> should not show any results
        final var lastQueryResult = brokerService.search(brokerUrl, searchTerm);

        System.out.println();
        System.out.println(lastQueryResult);
        System.out.println();

        assertThat(lastQueryResult).doesNotContain(searchTerm);
    }

    private CatalogInput getCatalog() {
        var input = new CatalogInput();
        input.setTitle("SELE Toolbox test catalog");
        return input;
    }

    private OfferedResourceInput getResource1() {
        var input = new OfferedResourceInput();
        input.setTitle("SELE Toolbox test resource 1");
        return input;
    }

    private OfferedResourceInput getResource2() {
        var input = new OfferedResourceInput();
        input.setTitle("SELE Toolbox test resource 2");
        return input;
    }

    private RepresentationInput getRepresentation() {
        var input = new RepresentationInput();
        input.setTitle("SELE Toolbox test representation");
        input.setMediaType("json");
        return input;
    }

    private ArtifactInput getArtifact1() {
        var input = new ArtifactInput();
        input.setTitle("SELE Toolbox test artifact 1");
        input.setValue("test data");
        return input;
    }

    private ArtifactInput getArtifact2() {
        var input = new ArtifactInput();
        input.setTitle("SELE Toolbox test artifact 2");
        input.setValue("test data");
        return input;
    }

    private ContractInput getContract() {
        return new ContractInput("SELE Toolbox test contract", ZonedDateTime.now(),
            ZonedDateTime.now().plusHours(6));
    }

    @SneakyThrows
    private RuleInput getRule() {
        var rule = PolicyUtils.buildProvideAccessRule("SELE Toolbox test policy");
        var value = new Serializer().serialize(rule);
        var input = new RuleInput(value);
        input.setTitle("SELE Toolbox test rule");
        return input;
    }

}
