/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.manual;

import java.net.URI;
import java.util.UUID;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.idsintegrationtoolbox.core.base.AppApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.ids.MessagesApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.ActionType;
import org.siliconeconomy.idsintegrationtoolbox.utils.UuidUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

/**
 * Manual Tests for testing app and appstore functionality.
 *
 * ****IMPORTANT: Valid IDS Certificate needed for this test****
 * Works with Portainer v2.6.2
 * Please follow this deployment example
 * https://github.com/International-Data-Spaces-Association/IDS-Deployment-Examples/tree/main/dataspace-connector/full
 *
 * @author Haydar Qarawlus
 */
@SpringBootTest
@ContextConfiguration(classes = {IntegrationTestConfiguration.class})
@TestPropertySource(locations = "classpath:example.properties")
@Disabled("For manual testing purposes only")
class AppApiOperatorManualTest {

    @Autowired
    MessagesApiOperator messagesApiOperator;
    @Autowired
    AppApiOperator appApiOperator;

    String appStoreUri = "https://binac.fit.fraunhofer.de/api/ids/data";
    String appUri = "https://binac.fit.fraunhofer.de/api/resources/0f950c19-21b7-4657-98ac-28a6079914c2";


    @Test
    @SneakyThrows
    void downloadAndStartApp_shouldStartApp() {
        // Step 1: Download the app from the Appstore
        var downloadedApp = messagesApiOperator.appDownload(URI.create(appStoreUri),
            URI.create(appUri));

        // Step 2: Extract app UUID
        UUID appUuid =
            UuidUtils.getUuidFromUri(URI.create(downloadedApp.getLinks().getSelf().getHref()));

        // Step 3: Start the app.
        Assertions.assertDoesNotThrow(() -> appApiOperator.applyAction(appUuid, ActionType.START));

        // Step 4: Check Portainer to see if app is running.
        System.out.println("App start action applied. Check Portainer to see if app is running!");
    }
}
