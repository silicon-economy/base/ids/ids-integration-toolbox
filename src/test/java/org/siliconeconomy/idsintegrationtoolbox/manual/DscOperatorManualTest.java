/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.manual;

import java.net.URI;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;

import de.fraunhofer.iais.eis.ids.jsonld.Serializer;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.idsintegrationtoolbox.DscOperator;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.DeployMethod;
import org.siliconeconomy.idsintegrationtoolbox.model.input.ArtifactInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.AuthenticationInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.CatalogInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.ContractInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.DataSourceInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.EndpointInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.GenericEndpointInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.OfferedResourceInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.RepresentationInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.RestDataSourceInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.RouteInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.RuleInput;
import org.siliconeconomy.idsintegrationtoolbox.utils.PolicyUtils;
import org.siliconeconomy.idsintegrationtoolbox.utils.UuidUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * DscOperatorManualTest
 *
 * @author Ronja Quensel
 */
@SpringBootTest
@ContextConfiguration(classes = {IntegrationTestConfiguration.class})
@TestPropertySource("classpath:example.properties")
@Disabled("For manual testing purposes only.")
class DscOperatorManualTest {

    @Value("${connector.url:https://localhost:8080}")
    protected String connectorBaseUrl;

    @Value("${connector.username:admin}")
    protected String username;

    @Value("${connector.password:password}")
    protected String password;

    @Autowired
    private DscOperator dsc;

    private final String testData = "test data";

    @Test
    @SneakyThrows
    void createAndRequestResource() {
        // Create complete resource
        var resource = dsc
            .entities()
            .workflows()
            .resourceCreation()
            .createResource(getResource(), getCatalog(), getRepresentation(), getArtifact(),
                getContract(), getRule());

        // Get contract that was created
        var resourceUuid = UuidUtils.getUuidFromOutput(resource);
        var contracts = dsc
            .entities()
            .resources()
            .offers()
            .contracts()
            .getAll(resourceUuid);
        assertThat(contracts.getEmbedded().getEntries()).isNotEmpty().hasSize(1);

        // Get URIs of resource and contract (required for negotiation)
        var contractUri = URI.create(contracts.getEmbedded().getEntries().get(0)
            .getLinks().getSelf().getHref());
        var resourceUri = URI.create(resource.getLinks().getSelf().getHref());

        // Negotiate a contract
        final var recipient = URI.create(connectorBaseUrl + "/api/ids/data");
        var agreement = dsc
            .ids()
            .workflows()
            .negotiation()
            .negotiateContract(recipient, resourceUri, contractUri, true);

        // Find the requested artifact
        var agreementUuid = UuidUtils.getUuidFromOutput(agreement);
        var artifacts = dsc
            .entities()
            .agreements()
            .artifacts()
            .getAll(agreementUuid);
        assertThat(artifacts.getEmbedded().getEntries()).isNotEmpty().hasSize(1);

        // Get the requested artifact's data
        var artifactUuid = UuidUtils
            .getUuidFromOutput(artifacts.getEmbedded().getEntries().get(0));
        var data = dsc
            .entities()
            .artifacts()
            .data()
            .get(artifactUuid, false, null, new ArrayList<>(),
                new HashMap<>(), new HashMap<>(), null);

        // Assert that same data received as initially used for artifact creation
        var dataString = new String(data.getRawData());
        assertThat(dataString).isNotNull().isEqualTo(testData);
    }

    @Test
    @SneakyThrows
    void getAndSetSettings() {
        // Negotiation status (true by default)
        var negotiationStatus = dsc.connector().settings().negotiation();
        assertThat(negotiationStatus).isTrue();

        negotiationStatus = dsc.connector().settings().negotiation(false);
        assertThat(negotiationStatus).isFalse();

        // Unsupported patterns status (false by default)
        var unsupportedPatternsStatus = dsc.connector().settings().unsupportedPatterns();
        assertThat(unsupportedPatternsStatus).isFalse();

        unsupportedPatternsStatus = dsc.connector().settings().unsupportedPatterns(true);
        assertThat(unsupportedPatternsStatus).isTrue();

        // Reset both
        dsc.connector().settings().negotiation(true);
        dsc.connector().settings().unsupportedPatterns(false);
    }

    @Test
    @SneakyThrows
    void createArtifactWithRouteReference() {
        // Create data source, generic endpoint and route
        var dataSource = dsc.entities()
            .dataSources()
            .create(getDataSource());
        var dataSourceUuid = UuidUtils.getUuidFromOutput(dataSource);

        var endpoint = dsc.entities()
            .endpoints()
            .create(getGenericEndpoint());
        var endpointUri = URI.create(endpoint.getLinks().getSelf().getHref());
        var endpointUuid = UuidUtils.getUuidFromOutput(endpoint);

        var route = dsc.entities()
            .routes()
            .create(getRoute());
        var routeUri = URI.create(route.getLinks().getSelf().getHref());
        var routeUuid = UuidUtils.getUuidFromOutput(route);

        // Link data source to endpoint & endpoint to route
        dsc.entities()
            .endpoints()
            .dataSources()
            .add(endpointUuid, dataSourceUuid);
        dsc.entities()
            .routes()
            .endpoints()
            .start(routeUuid, endpointUri);

        // Create artifact with route reference
        var artifact = dsc.entities()
            .artifacts()
            .create(getArtifactWithRouteReference(routeUri));
        var artifactUuid = UuidUtils.getUuidFromOutput(artifact);

        // Get artifact data -> should be DSC's API entrypoint
        var data = dsc.entities()
            .artifacts()
            .data()
            .get(artifactUuid, false, null, new ArrayList<>(),
                new HashMap<>(), new HashMap<>(), null);
        var dataString = new String(data.getRawData());

        System.out.println();
        System.out.println(dataString);
        System.out.println();

        assertThat(dataString).isNotNull().isNotEmpty().isNotBlank().contains("_links");
    }

    private CatalogInput getCatalog() {
        var input = new CatalogInput();
        input.setTitle("SELE Toolbox test catalog");
        return input;
    }

    private OfferedResourceInput getResource() {
        var input = new OfferedResourceInput();
        input.setTitle("SELE Toolbox test resource");
        return input;
    }

    private RepresentationInput getRepresentation() {
        var input = new RepresentationInput();
        input.setTitle("SELE Toolbox test representation");
        input.setMediaType("json");
        return input;
    }

    private ArtifactInput getArtifact() {
        var input = new ArtifactInput();
        input.setTitle("SELE Toolbox test artifact");
        input.setValue(testData);
        return input;
    }

    @SneakyThrows
    private ArtifactInput getArtifactWithRouteReference(final URI route) {
        var input = new ArtifactInput();
        input.setTitle("SELE Toolbox test artifact with route");
        input.setAccessUrl(route.toURL());
        return input;
    }

    private ContractInput getContract() {
        return new ContractInput("SELE Toolbox test contract", ZonedDateTime.now(),
            ZonedDateTime.now().plusHours(6));
    }

    @SneakyThrows
    private RuleInput getRule() {
        var rule = PolicyUtils.buildProvideAccessRule("SELE Toolbox test policy");
        var value = new Serializer().serialize(rule);
        var input = new RuleInput(value);
        input.setTitle("SELE Toolbox test rule");
        return input;
    }

    private EndpointInput getGenericEndpoint() {
        var input = new GenericEndpointInput();
        input.setLocation(connectorBaseUrl + "/api");
        return input;
    }

    private DataSourceInput getDataSource() {
        var input = new RestDataSourceInput();
        input.setBasicAuth(new AuthenticationInput(username, password));
        return input;
    }

    private RouteInput getRoute() {
        var input = new RouteInput();
        input.setTitle("SELE Toolbox test route");
        input.setDeploy(DeployMethod.CAMEL);
        return input;
    }

}
