/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.manual;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.idsintegrationtoolbox.workflows.ContractNegotiator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ContextConfiguration(classes = {IntegrationTestConfiguration.class})
@TestPropertySource("classpath:example.properties")
@Disabled("For manual testing purposes only.")
class ContractNegotiatorManualTest {

    @Autowired
    private ContractNegotiator negotiator;

    private final URI recipient = URI.create("https://localhost:8080/api/ids/data");
    private final URI resourceId = URI.create("https://localhost:8080/api/offers/704dd884-3bb8-4bcc-a878-b55aa16a04a5");
    private final URI contractId = URI.create("https://localhost:8080/api/contracts/fbe9e3ba-7def-4238-ace5-142d5cc8a9de");
    private final URI artifactId = URI.create("https://localhost:8080/api/artifacts/18e699a1-e342-4cc4-93f8-e47db0a48df9");

    /**
     * Before running this test, a complete resource needs to be present in the connector.
     * Therefore, run one of the DSC's scripts
     * (e.g. scripts/tests/runner/scripts/contract_negotiation_allow_access.py) after setting
     * providerUrl and consumerUrl to point to your local connector. After the script has
     * successfully run, copy the resource's, the contract's and the artifact's ID into the
     * respective fields of this test class.
     */
    @Test
    @SneakyThrows
    void negotiateContract0_invalidArtifactId_logWarning() {
        // Arrange
        var artifactIds = new ArrayList<URI>();
        artifactIds.add(artifactId);
        artifactIds.add(URI.create("https://invalid-artifact"));

        // Act
        var result = negotiator.negotiateContract(recipient,
            Collections.singletonList(resourceId),
            artifactIds,
            Collections.singletonList(contractId),
            false);

        // Assert
        assertThat(result).isNotNull();

        // As we supplied an invalid artifactId that can not be matched to the resource, a warning
        // should have been logged.
    }

}
