/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.manual;

import lombok.SneakyThrows;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.siliconeconomy.idsintegrationtoolbox.core.camel.BeanApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.camel.CamelRouteApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.utils.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

@SpringBootTest
@ContextConfiguration(classes = {IntegrationTestConfiguration.class})
@TestPropertySource(locations = "classpath:example.properties")
@Disabled("For manual testing purposes only")
class FileUploadTest {

    @Autowired
    private CamelRouteApiOperator camelRouteApiOperator;

    @Autowired
    private BeanApiOperator beanApiOperator;

    private final MockedStatic<FileUtils> fileUtilsMock = Mockito.mockStatic(FileUtils.class);

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @AfterEach
    public void closeMock() {
        fileUtilsMock.close();
    }

    @Test
    @SneakyThrows
    void uploadRoute() {
        final var fileSystemResource = getRouteFile();
        fileUtilsMock.when(() -> FileUtils.readFile(Mockito.any())).thenReturn(fileSystemResource);

        assertDoesNotThrow(() -> camelRouteApiOperator.upload("apache-camel/test-route.xml"));
    }

    @Test
    @SneakyThrows
    void deleteRoute() {
        assertDoesNotThrow(() -> camelRouteApiOperator.delete("api-test-route"));
    }

    @Test
    @SneakyThrows
    void uploadBean() {
        final var fileSystemResource = getBeanFile();
        fileUtilsMock.when(() -> FileUtils.readFile(Mockito.any())).thenReturn(fileSystemResource);

        assertDoesNotThrow(() -> beanApiOperator.upload("apache-camel/test-bean.xml"));
    }

    @Test
    @SneakyThrows
    void deleteBean() {
        assertDoesNotThrow(() -> beanApiOperator.delete("testDataSource"));
    }

    @SneakyThrows
    private FileSystemResource getRouteFile() {
        final var file = new ClassPathResource("apache-camel/test-route.xml").getFile();
        return new FileSystemResource(file);
    }

    @SneakyThrows
    private FileSystemResource getBeanFile() {
        final var file = new ClassPathResource("apache-camel/test-bean.xml").getFile();
        return new FileSystemResource(file);
    }

}
