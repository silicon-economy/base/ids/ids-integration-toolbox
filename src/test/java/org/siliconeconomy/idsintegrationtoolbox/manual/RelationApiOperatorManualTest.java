/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.manual;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import org.siliconeconomy.idsintegrationtoolbox.core.base.ArtifactApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.base.BaseApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.base.CatalogApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.base.ContractApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.base.OfferedResourceApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.base.RepresentationApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.base.RuleApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.base.SubscriptionApiOperator;

import org.siliconeconomy.idsintegrationtoolbox.core.relation.ArtifactRepresentationsApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.CatalogOfferedResourcesApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.ContractRulesApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.OfferedResourceRepresentationsApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.OfferedResourceSubscriptionsApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.RelationApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.RuleContractsApiOperator;

import org.siliconeconomy.idsintegrationtoolbox.model.dsc.AbstractEntity;
import org.siliconeconomy.idsintegrationtoolbox.model.input.ArtifactInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.CatalogInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.ContractInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.EntityInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.OfferedResourceInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.RepresentationInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.RuleInput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.Embedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.Links;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.EntityMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.EntityOutput;

import org.siliconeconomy.idsintegrationtoolbox.utils.PolicyUtils;
import org.siliconeconomy.idsintegrationtoolbox.utils.UuidUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

import java.net.URI;
import java.net.URL;
import java.time.ZonedDateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Stream;

/**
 * RelationApiOperatorManualTest
 *
 * @author Cem Bana
 */


@ContextConfiguration(classes = {IntegrationTestConfiguration.class})
@TestPropertySource("classpath:example.properties")
@Disabled("For manual testing purposes only.")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.MethodName.class)
@SpringBootTest(classes = {ArtifactRepresentationsApiOperator.class})
class RelationApiOperatorManualTest {
    @Autowired
    ArtifactApiOperator artifactApiOperator;

    @Autowired
    RuleApiOperator ruleApiOperator;

    @Autowired
    ContractApiOperator contractApiOperator;

    @Autowired
    CatalogApiOperator catalogApiOperator;

    @Autowired
    SubscriptionApiOperator subscriptionApiOperator;

    @Autowired
    OfferedResourceSubscriptionsApiOperator offeredResourceSubscriptionsApiOperator;

    @Autowired
    OfferedResourceApiOperator offeredResourceApiOperator;

    @Autowired
    RepresentationApiOperator representationApiOperator;

    @Autowired
    CatalogOfferedResourcesApiOperator catalogOfferedResourcesApiOperator;

    @Autowired
    ArtifactRepresentationsApiOperator artifactRepresentationsApiOperator;

    @Autowired
    ContractRulesApiOperator contractRulesApiOperator;

    @Autowired
    RuleContractsApiOperator ruleContractsApiOperator;

    @Autowired
    OfferedResourceRepresentationsApiOperator offeredResourceRepresentationsApiOperator;

    @Autowired
    ObjectMapper objectMapper;

    private Map<UUID, List<URI>> uuidLinkMap;
    private Map<UUID, RelationApiOperator<?, ?, ?, ?, ?>> uuidRelationMap;

    /**
     * Creates two entities for the owner API Operator and the related API Operator
     * Then we test if a relation between those can be made, if yes we test the page size of the output
     *
     * @param ownerApiOperator    The owner API Operator
     * @param relatedApiOperator  The related API Operator
     * @param relationApiOperator The relation API Operator
     * @param owningEntity        The owning entity
     * @param relatedEntity       The related entity
     */

    @ParameterizedTest
    @SneakyThrows
    @MethodSource("provideCreateArguments")
    <T1 extends AbstractEntity, O1 extends EntityOutput<T1>, E1 extends Embedded<T1, O1>, L1 extends Links<T1>, M1 extends EntityMultiOutput<T1, O1, E1, L1>,
        T2 extends AbstractEntity, O2 extends EntityOutput<T2>, E2 extends Embedded<T2, O2>, L2 extends Links<T2>, M2 extends EntityMultiOutput<T2, O2, E2, L2>>
    void testCreate(BaseApiOperator<T1, O1, E1, L1, M1> ownerApiOperator,
                    BaseApiOperator<T2, O2, E2, L2, M2> relatedApiOperator,
                    RelationApiOperator<T1, L1, T2, O2, E2> relationApiOperator,
                    EntityInput<T1> owningEntity,
                    EntityInput<T2> relatedEntity) {

        var ownerOutput = ownerApiOperator.create(owningEntity);
        var relatedOutput = relatedApiOperator.create(relatedEntity);

        var relationOutput = relationApiOperator.add(UuidUtils.getUuidFromOutput(ownerOutput), Collections.singletonList(URI.create(relatedOutput.getLinks().getSelf().getHref())));

        Assertions.assertEquals(1, relationOutput.getPage().getTotalElements());

        uuidLinkMap.put(UuidUtils.getUuidFromOutput(ownerOutput), Collections.singletonList(URI.create(relatedOutput.getLinks().getSelf().getHref())));
        uuidRelationMap.put(UuidUtils.getUuidFromOutput(ownerOutput), relationApiOperator);
    }

    /**
     * We try to receive all entries from the newly created Relation and test if its entries are equal to 1
     *
     * @param relationApiOperator The Relation API Operator
     * @param outputUUID          The UUID of the newly created relation output
     */

    @ParameterizedTest
    @SneakyThrows
    @MethodSource("provideUUIDArguments")
    <T1 extends AbstractEntity, L1 extends Links<T1>,
        T2 extends AbstractEntity, O2 extends EntityOutput<T2>, E2 extends Embedded<T2, O2>>
    void testGetAll(RelationApiOperator<T1, L1, T2, O2, E2> relationApiOperator, UUID outputUUID) {
        Assertions.assertEquals(1, relationApiOperator.getAll(outputUUID).getEmbedded().getEntries().size());
    }

    /**
     * Tries to remove the relation, with the given UUID and URIs
     * Tests if it does not throw any exception
     *
     * @param relationApiOperator The relation API Operator
     * @param outputUUID          The UUID of the output created by testCreate()
     * @param linkList            A list of UUID's
     */

    @ParameterizedTest
    @SneakyThrows
    @MethodSource("provideUUIDArguments")
    <T1 extends AbstractEntity, L1 extends Links<T1>,
        T2 extends AbstractEntity, O2 extends EntityOutput<T2>, E2 extends Embedded<T2, O2>>
    void testRemove(RelationApiOperator<T1, L1, T2, O2, E2> relationApiOperator, UUID outputUUID, List<URI> linkList) {
        Assertions.assertDoesNotThrow(() -> relationApiOperator.remove(outputUUID, linkList));
    }

    /**
     * Provides the arguments for the getAll and remove methods, so we can run the test class as one test
     * These arguments are gathered from the testCreate method
     *
     * @return Stream of arguments
     */

    private Stream<Arguments> provideUUIDArguments() {
        List<Arguments> argumentsList = new ArrayList<>();

        uuidLinkMap.forEach((uuid, link) -> argumentsList.add(Arguments.of(uuidRelationMap.get(uuid), uuid, link)));

        return argumentsList.stream();
    }

    /**
     * Creates a stream of arguments and feeds them to the testCreate method
     *
     * @return Stream of Arguments
     */

    private Stream<Arguments> provideCreateArguments() {
        uuidLinkMap = new HashMap<>();
        uuidRelationMap = new HashMap<>();

        return Stream.of(
            Arguments.of(artifactApiOperator, representationApiOperator, artifactRepresentationsApiOperator, createArtifactInput(), createRepresentationInput()),
            Arguments.of(contractApiOperator, ruleApiOperator, contractRulesApiOperator, createContractInput(), createRuleInput()),
            Arguments.of(ruleApiOperator, contractApiOperator, ruleContractsApiOperator, createRuleInput(), createContractInput()),
            Arguments.of(catalogApiOperator, offeredResourceApiOperator, catalogOfferedResourcesApiOperator, createCatalogInput(), createOfferedResourceInput()),
            Arguments.of(offeredResourceApiOperator, representationApiOperator, offeredResourceRepresentationsApiOperator, createOfferedResourceInput(), createRepresentationInput())
        );
    }

    @SneakyThrows
    private ArtifactInput createArtifactInput() {
        final var accessUrlObject = new URL("http://localhost");

        return new ArtifactInput("", accessUrlObject, null,
            null, false);
    }

    @SneakyThrows
    private RepresentationInput createRepresentationInput() {
        final var dataRepresentation = new RepresentationInput();
        dataRepresentation.setMediaType("application/json");
        dataRepresentation.setTitle("title");
        dataRepresentation.setLanguage("en");

        return dataRepresentation;
    }

    @SneakyThrows
    private RuleInput createRuleInput() {
        final var ruleInput = new RuleInput();
        ruleInput.setTitle("Test Title");
        ruleInput.setValue(objectMapper.writeValueAsString(PolicyUtils.buildProvideAccessRule("Test Title")));

        return ruleInput;
    }

    @SneakyThrows
    private CatalogInput createCatalogInput() {
        final var catalogInput = new CatalogInput();

        catalogInput.setTitle("Test");
        catalogInput.setDescription("Test desc");

        return catalogInput;
    }

    @SneakyThrows
    private ContractInput createContractInput() {
        return new ContractInput("Test",
            ZonedDateTime.parse("2021-08-06T13:33:44.995+02:00"),
            ZonedDateTime.parse("2021-09-06T13:33:44.995+02:00"));
    }

    @SneakyThrows
    private OfferedResourceInput createOfferedResourceInput() {
        final var offeredResourceInput = new OfferedResourceInput();
        offeredResourceInput.setTitle("Test");
        offeredResourceInput.setLanguage("EN");

        return offeredResourceInput;
    }
}
