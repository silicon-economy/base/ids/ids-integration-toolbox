/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.manual;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Map;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.siliconeconomy.idsintegrationtoolbox.core.base.DataSourceApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.model.input.AuthenticationInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.DatabaseDataSourceInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.RestDataSourceInput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.DataSourceMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.DatabaseDataSourceOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.RestDataSourceOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.UuidUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 * DataSourceApiManualTest
 *
 * @author Florian Zimmer
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
@ContextConfiguration(classes = {IntegrationTestConfiguration.class})
@TestPropertySource("classpath:example.properties")
@Disabled("For manual testing purposes only.")
class DataSourceApiManualTest {

    /** Class under test */
    @Autowired
    private DataSourceApiOperator operator;

    @Test
    @SneakyThrows
    void getOneDatabase_shouldParseCorrectly() {
        // Assert
        var input = new DatabaseDataSourceInput("http://abc.de/", "driver");
        input.setAdditional(Map.of());
        input.setBasicAuth(new AuthenticationInput("key", "value"));

        // Act
        var datasource = operator.create(input);

        final var result = operator.getOne(UuidUtils.getUuidFromOutput(datasource));

        // Assert
        assertThat(result.getClass()).isEqualTo(DatabaseDataSourceOutput.class);
    }

    @Test
    @SneakyThrows
    void getOneRest_shouldParseCorrectly() {
        // Assert
        var input = new RestDataSourceInput();
        input.setAdditional(Map.of());
        input.setBasicAuth(new AuthenticationInput("key", "value"));

        // Act
        var datasource = operator.create(input);

        final var result = operator.getOne(UuidUtils.getUuidFromOutput(datasource));

        // Assert
        assertThat(result.getClass()).isEqualTo(RestDataSourceOutput.class);
    }

    @Test
    @SneakyThrows
    void getAll_shouldParseCorrectly() {
        // Assert
        var databaseInput = new DatabaseDataSourceInput("http://abc.de/", "driver");
        databaseInput.setAdditional(Map.of());
        databaseInput.setBasicAuth(new AuthenticationInput("key", "value"));

        var restInput = new RestDataSourceInput();
        restInput.setAdditional(Map.of());
        restInput.setBasicAuth(new AuthenticationInput("key", "value"));

        operator.create(databaseInput);
        operator.create(restInput);

        // Act
        final var result = operator.getAll();

        // Assert
        assertThat(result.getClass()).isEqualTo(DataSourceMultiOutput.class);
    }
}
