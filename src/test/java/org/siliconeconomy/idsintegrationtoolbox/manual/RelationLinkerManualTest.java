/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.manual;

import java.time.ZonedDateTime;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.siliconeconomy.idsintegrationtoolbox.core.base.ArtifactApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.base.BrokerApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.base.ContractApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.base.OfferedResourceApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.base.RuleApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.ContractOfferedResourcesApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.ContractRulesApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.model.input.ArtifactInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.BrokerInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.ContractInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.OfferedResourceInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.RuleInput;
import org.siliconeconomy.idsintegrationtoolbox.utils.UuidUtils;
import org.siliconeconomy.idsintegrationtoolbox.workflows.RelationLinker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * RelationLinkerManualTest
 *
 * @author Florian Zimmer
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
@ContextConfiguration(classes = {IntegrationTestConfiguration.class})
@TestPropertySource("classpath:example.properties")
@Disabled("For manual testing purposes only.")
class RelationLinkerManualTest {

    /** Class under test */
    @Autowired
    private RelationLinker relationLinker;

    @Autowired
    private ContractApiOperator contractApiOperator;

    @Autowired
    private RuleApiOperator ruleApiOperator;

    @Autowired
    private ArtifactApiOperator artifactApiOperator;

    @Autowired
    private BrokerApiOperator brokerApiOperator;

    @Autowired
    private OfferedResourceApiOperator offeredResourceApiOperator;

    @Autowired
    private ObjectMapper objectMapper;

    @SpyBean
    private ContractRulesApiOperator contractRulesApiOperator;

    @SpyBean
    private ContractOfferedResourcesApiOperator contractOfferedResourcesApiOperator;

    @BeforeEach
    @SneakyThrows
    void init() {
        objectMapper.registerModule(new JavaTimeModule());
    }

    @Test
    @SneakyThrows
    void linkEntities_linkableEntities_shouldGetLinked() {
        // Arrange
        final var contractInput = new ContractInput(ZonedDateTime.now());
        contractInput.setTitle("ManualTest-Contract");
        contractInput.setAdditional(Map.of("additional", "field"));
        final var contract = contractApiOperator.create(contractInput);

        final var ruleInput = new RuleInput();
        ruleInput.setTitle("ManualTest-Rule");
        ruleInput.setAdditional(Map.of("additional", "field"));
        final var rule = ruleApiOperator.create(ruleInput);

        final var offeredResourceInput = new OfferedResourceInput();
        offeredResourceInput.setTitle("ManualTest-OfferedResource");
        offeredResourceInput.setAdditional(Map.of("additional", "field"));
        final var offeredResource = offeredResourceApiOperator.create(offeredResourceInput);

        // Act
        relationLinker.linkEntities(contract, rule, offeredResource);

        // Assert
        assertThat(contractRulesApiOperator.getAll(UuidUtils.getUuidFromOutput(contract)).getPage().getTotalElements()).isEqualTo(1);
        assertThat(contractOfferedResourcesApiOperator.getAll(UuidUtils.getUuidFromOutput(contract)).getPage().getTotalElements()).isEqualTo(1);

        verify(contractRulesApiOperator, times(1)).add(any(), any());
        verify(contractOfferedResourcesApiOperator, times(1)).add(any(), any());
    }

    @Test
    @SneakyThrows
    void linkEntities_unlinkableEntities_shouldGetIgnored() {
        // Arrange
        final var artifactInput = new ArtifactInput();
        artifactInput.setTitle("ManualTest-Artifact");
        artifactInput.setAdditional(Map.of("additional", "field"));
        final var artifact = artifactApiOperator.create(artifactInput);

        final var offeredResourceInput = new OfferedResourceInput();
        offeredResourceInput.setTitle("ManualTest-OfferedResource");
        offeredResourceInput.setAdditional(Map.of("additional", "field"));
        final var offeredResource = offeredResourceApiOperator.create(offeredResourceInput);

        final var brokerInput = new BrokerInput();
        brokerInput.setTitle("ManualTest-Subscription");
        brokerInput.setAdditional(Map.of("additional", "field"));
        final var subscription = brokerApiOperator.create(brokerInput);

        // Act
        relationLinker.linkEntities(artifact, offeredResource, subscription);

        // Assert
        verify(contractRulesApiOperator, times(0)).add(any(), any());
        verify(contractOfferedResourcesApiOperator, times(0)).add(any(), any());

        // As we supplied entities that can not be linked, warnings should have been logged
    }
}
