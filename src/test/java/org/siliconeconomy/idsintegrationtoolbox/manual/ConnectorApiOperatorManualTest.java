/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.manual;

import de.fraunhofer.iais.eis.BaseConnector;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.siliconeconomy.idsintegrationtoolbox.core.ConnectorApiOperator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * ConnectorManualTest
 *
 * @author Florian Zimmer
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
@ContextConfiguration(classes = {IntegrationTestConfiguration.class})
@TestPropertySource(locations = "classpath:example.properties")
@Disabled("For manual testing purposes only")
class ConnectorApiOperatorManualTest {

    @Autowired
    ConnectorApiOperator connectorApiOperator;

    @Test
    void getPublicSelfDescription_ShouldReturnSelfDescription() throws Exception {
        assertTrue(connectorApiOperator.getPublic() instanceof BaseConnector);
    }

    @Test
    void getSelfDescription_ShouldReturnSelfDescription() throws Exception {
        assertTrue(connectorApiOperator.getPrivate() instanceof BaseConnector);
    }

    @Test
    void setNegotiationStatus_shouldReturnTrue() throws Exception {
        assertTrue(connectorApiOperator.negotiation(true));
    }

    @Test
    void setNegotiationStatus_shouldReturnFalse() throws Exception {
        assertFalse(connectorApiOperator.negotiation(false));
    }

    @Test
    void getNegotiationStatus_shouldReturnTrue() throws Exception {
        connectorApiOperator.negotiation(true);

        assertTrue(connectorApiOperator.negotiation());
    }

    @Test
    void setUnsupportedPatterns_shouldReturnFalse() throws Exception {
        assertFalse(connectorApiOperator.unsupportedPatterns(false));
    }

    @Test
    void setUnsupportedPatterns_shouldReturnTrue() throws Exception {
        assertTrue(connectorApiOperator.unsupportedPatterns(true));
    }

    @Test
    void getUnsupportedPatterns_shouldReturnTrue() throws Exception {
        connectorApiOperator.unsupportedPatterns(true);

        assertTrue(connectorApiOperator.unsupportedPatterns());
    }
}
