/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.manual;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Application context configuration for manual tests, that loads the complete context.
 */
@SpringBootApplication(scanBasePackages = {"org.siliconeconomy.idsintegrationtoolbox"})
public class IntegrationTestConfiguration {
    public static void main(String[] args) {
        SpringApplication.run(IntegrationTestConfiguration.class, args);
    }
}
