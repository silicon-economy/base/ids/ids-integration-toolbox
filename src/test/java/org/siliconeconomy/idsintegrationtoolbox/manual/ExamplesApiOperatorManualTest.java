/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.manual;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import de.fraunhofer.iais.eis.Permission;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.siliconeconomy.idsintegrationtoolbox.core.ExamplesApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.PolicyPattern;
import org.siliconeconomy.idsintegrationtoolbox.model.input.policy.ProvideAccessPolicyInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 * UsageControlManualTest
 *
 * @author Florian Zimmer
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
@ContextConfiguration(classes = {IntegrationTestConfiguration.class})
@TestPropertySource("classpath:example.properties")
@Disabled("For manual testing purposes only")
class ExamplesApiOperatorManualTest {

    @Autowired
    ExamplesApiOperator examplesApiOperator;

    @Test
    void getExamplePolicy_shouldReturnPolicy() throws Exception {
        final var policyInput = new ProvideAccessPolicyInput.Builder().build();

        var response = examplesApiOperator.policy(policyInput);

        assertTrue(response instanceof Permission);
    }

    @Test
    void getExampleValidation() throws Exception {
        assertEquals(PolicyPattern.PROVIDE_ACCESS,
            examplesApiOperator.policyValidation(examplePolicy()));
    }

    private String examplePolicy() {
        return "{\n" +
            "  \"@context\" : {\n" +
            "    \"ids\" : \"https://w3id.org/idsa/core/\",\n" +
            "    \"idsc\" : \"https://w3id.org/idsa/code/\"\n" +
            "  },\n" +
            "  \"@type\" : \"ids:Permission\",\n" +
            "  \"@id\" : \"https://w3id.org/idsa/autogen/permission/756626d6-5c43-4065-8def" +
            "-3e5713dfc788\",\n" +
            "  \"ids:constraint\" : [ ],\n" +
            "  \"ids:action\" : [ {\n" +
            "    \"@id\" : \"https://w3id.org/idsa/code/USE\"\n" +
            "  } ],\n" +
            "  \"ids:assignee\" : [ ],\n" +
            "  \"ids:assigner\" : [ ],\n" +
            "  \"ids:description\" : [ {\n" +
            "    \"@value\" : \"provide-access\",\n" +
            "    \"@type\" : \"http://www.w3.org/2001/XMLSchema#string\"\n" +
            "  } ],\n" +
            "  \"ids:title\" : [ {\n" +
            "    \"@value\" : \"Allow Data Usage\",\n" +
            "    \"@type\" : \"http://www.w3.org/2001/XMLSchema#string\"\n" +
            "  } ],\n" +
            "  \"ids:postDuty\" : [ ],\n" +
            "  \"ids:preDuty\" : [ ]\n" +
            "}";
    }
}
