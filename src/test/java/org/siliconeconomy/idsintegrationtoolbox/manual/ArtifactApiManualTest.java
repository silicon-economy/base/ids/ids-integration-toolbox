/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.manual;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.ArtifactAgreementsApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.base.ArtifactApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.base.ArtifactDataApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.ArtifactRepresentationsApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.OperationNotAllowedException;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.QueryInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.ArtifactInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * ArtifactApiManualTest
 *
 * @author Ronja Quensel
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
@ContextConfiguration(classes = {IntegrationTestConfiguration.class})
@TestPropertySource("classpath:example.properties")
@Disabled("For manual testing purposes only.")
class ArtifactApiManualTest {

    final UUID id = UUID.fromString("f31af5bb-3da4-4027-9503-2c2c00cb3358");
    final URI representation1 = URI.create("http://localhost:8080/api/representations/e7f01ba7" +
            "-0d2a-4709-88ce-c2e3e9a1cc83");
    final URI representation2 = URI.create("http://localhost:8080/api/representations/e6db9b8b" +
            "-2110-499a-ad91-e817cfe78aa5");
    final URI routeId = URI.create("http://localhost:8080/api/routes/1549ecfb-e086-4513-97b1-5b73dd5bc9bc");

    @Autowired
    private ArtifactApiOperator operator;
    @Autowired
    private ArtifactAgreementsApiOperator agreementOperator;
    @Autowired
    private ArtifactRepresentationsApiOperator representationOperator;
    @Autowired
    private ArtifactDataApiOperator dataOperator;

    /*
        BASE API
     */

    @Test
    @SneakyThrows
    void getArtifacts() {
        final var result = operator.getAll();

        assertNotNull(result);
        System.out.println(result);
    }

    @Test
    @SneakyThrows
    void getOne() {
        final var result = operator.getOne(id);

        assertNotNull(result);
        System.out.println(result);
    }

    @Test
    @SneakyThrows
    void create() {
        final var input = new ArtifactInput();
        input.setTitle("my artifact");
        input.setValue("my value");
        input.setAutomatedDownload(true);

        final var result = operator.create(input);

        assertNotNull(result);
        System.out.println(result);
    }

    @Test
    @SneakyThrows
    void update() {
        final var input = new ArtifactInput();
        input.setTitle("my other artifact");
        input.setValue("my value");
        input.setAutomatedDownload(true);

        assertDoesNotThrow(() -> operator.update(id, input));
    }

    @Test
    @SneakyThrows
    void delete() {
        assertDoesNotThrow(() -> operator.delete(id));
    }

    /*
        AGREEMENTS API
     */

    @Test
    @SneakyThrows
    void getAgreements() {
        final var result = agreementOperator.getAll(id);

        assertNotNull(result);
        System.out.println(result);
    }

    @Test
    void addNotAllowed() {
        // Arrange
        var list = new ArrayList<URI>();

        // Act & Assert
        assertThrows(OperationNotAllowedException.class,
                () -> agreementOperator.add(id, list));
    }

    @Test
    void updateNotAllowed() {
        // Arrange
        var list = new ArrayList<URI>();

        // Act & Assert
        assertThrows(OperationNotAllowedException.class,
                () -> agreementOperator.replace(id, list));
    }

    @Test
    void removeNotAllowed() {
        // Arrange
        var list = new ArrayList<URI>();

        // Act & Assert
        assertThrows(OperationNotAllowedException.class,
                () -> agreementOperator.remove(id, list));
    }

    /*
        REPRESENTATIONS API
     */

    @Test
    @SneakyThrows
    void getRepresentations() {
        final var result = representationOperator.getAll(id);

        assertNotNull(result);
        System.out.println(result);
    }

    @Test
    @SneakyThrows
    void addRepresentations() {
        final var result = representationOperator.add(id, List.of(representation1));

        assertNotNull(result);
        System.out.println(result);
    }

    @Test
    @SneakyThrows
    void updateRepresentations() {
        assertDoesNotThrow(() -> representationOperator.replace(id, List.of(representation2)));
    }

    @Test
    @SneakyThrows
    void removeRepresentations() {
        assertDoesNotThrow(() -> representationOperator.remove(id, List.of(representation1)));
    }

    /*
        DATA API
     */

    @Test
    @SneakyThrows
    void upload() {
        assertDoesNotThrow(() -> dataOperator.upload(id, "NEW DATA"));
    }

    @Test
    @SneakyThrows
    void getData_POST() {
        final var result = dataOperator.get(id, new QueryInput(), List.of(routeId));

        assertNotNull(result);
        System.out.println(result);
    }

    @Test
    @SneakyThrows
    void getData_GET() {
        final var result = dataOperator.get(id, true, null, List.of(routeId), null
            , null, null);

        assertNotNull(result);
        System.out.println(result);
    }

}
