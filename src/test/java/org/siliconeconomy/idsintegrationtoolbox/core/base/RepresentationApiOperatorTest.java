/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.base;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RepresentationArtifactsApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RepresentationResourcesApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RepresentationSubscriptionsApi;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.RepresentationMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.RepresentationOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * RepresentationApiOperatorTest
 *
 * @author Florian Zimmer
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {RepresentationApiOperator.class})
class RepresentationApiOperatorTest {

    /** Class under test */
    @Autowired
    private RepresentationApiOperator operator;

    @MockBean
    private RepresentationArtifactsApi artifacts;

    @MockBean
    private RepresentationResourcesApi resources;

    @MockBean
    private RepresentationSubscriptionsApi subscriptions;

    @MockBean
    private ObjectMapper objectMapper;

    @MockBean
    private RestRequestHandler requestHandler;

    @Test
    void getEntitySingleOutputClass_returnClass() {
        // Act
        final var result = operator.getEntitySingleOutputClass();

        // Assert
        assertThat(result).isEqualTo(RepresentationOutput.class);
    }

    @Test
    void getEntityMultiOutputClass_returnClass() {
        // Act
        final var result = operator.getEntityMultiOutputClass();

        // Assert
        assertThat(result).isEqualTo(RepresentationMultiOutput.class);
    }
}
