/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.base;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RequestedResourceCatalogsApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RequestedResourceContractsApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RequestedResourceRepresentationsApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RequestedResourceSubscriptionsApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.RequestedResource;
import org.siliconeconomy.idsintegrationtoolbox.model.input.EntityInput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.RequestedResourceMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.RequestedResourceOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.OperationNotAllowedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

/**
 * RequestedResourceApiOperatorTest
 *
 * @author Florian Zimmer
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {RequestedResourceApiOperator.class})
class RequestedResourceApiOperatorTest {

    /** Class under test */
    @Autowired
    private RequestedResourceApiOperator operator;

    @MockBean
    private RequestedResourceCatalogsApi catalogs;

    @MockBean
    private RequestedResourceContractsApi contracts;

    @MockBean
    private RequestedResourceRepresentationsApi representations;

    @MockBean
    private RequestedResourceSubscriptionsApi subscriptions;

    @MockBean
    private ObjectMapper objectMapper;

    @MockBean
    private RestRequestHandler requestHandler;

    @Test
    void create_notAllowed_throwOperationNotAllowedException() {
        // Arrange
        final var input = new RequestedResourceInput();

        // Act & Assert
        assertThatThrownBy(() -> operator.create(input))
            .isInstanceOf(OperationNotAllowedException.class);
    }

    @Test
    void getEntitySingleOutputClass_returnClass() {
        // Act
        final var result = operator.getEntitySingleOutputClass();

        // Assert
        assertThat(result).isEqualTo(RequestedResourceOutput.class);
    }

    @Test
    void getEntityMultiOutputClass_returnClass() {
        // Act
        final var result = operator.getEntityMultiOutputClass();

        // Assert
        assertThat(result).isEqualTo(RequestedResourceMultiOutput.class);
    }

    private static class RequestedResourceInput extends EntityInput<RequestedResource> {
    }
}
