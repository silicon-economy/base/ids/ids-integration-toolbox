/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.base;

import java.net.URL;
import java.util.Map;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.ArtifactAgreementsApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.ArtifactRepresentationsApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.ArtifactSubscriptionsApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.DataApi;
import org.siliconeconomy.idsintegrationtoolbox.model.RestRequest;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Artifact;
import org.siliconeconomy.idsintegrationtoolbox.model.input.ArtifactInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.AuthenticationInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.EntityInput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.Link;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.RouteLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.ArtifactMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ArtifactOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.RouteOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.client.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

/**
 * ArtifactApiOperatorTest
 *
 * @author Florian Zimmer
 */
@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest(classes = ArtifactApiOperator.class)
class ArtifactApiOperatorTest {

    /** Class under test */
    @Autowired
    private ArtifactApiOperator operator;

    @MockBean
    private DataApi data;

    @MockBean
    private ArtifactAgreementsApi agreements;

    @MockBean
    private ArtifactRepresentationsApi representations;

    @MockBean
    private ArtifactSubscriptionsApi subscriptions;

    @MockBean
    private RestRequestHandler restRequestHandler;

    @MockBean
    private ObjectMapper objectMapper;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    @SneakyThrows
    void getAll_successfulRequest_returnResponse() {
        // Arrange
        final var dummyString = "String";

        when(restRequestHandler.sendRequest(any(RestRequest.class))).thenReturn(dummyString);
        when(objectMapper.readValue(anyString(), eq(ArtifactMultiOutput.class)))
            .thenReturn(getTestMultiOutput());

        // Act
        final var result = (TestMultiOutput) operator.getAll();

        // Assert
        assertThat(result.testField).isEqualTo("testContent");
    }

    @Test
    @SneakyThrows
    void getAll_unsuccessfulRequest_throwApiInteractionUnsuccessfulException() {
        // Arrange
        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenThrow(new ApiInteractionUnsuccessfulException("error", new NotFoundException()));

        // Act & Assert
        assertThatThrownBy(() -> operator.getAll())
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());
    }

    @Test
    @SneakyThrows
    void getOne_successfulRequest_returnResponse() {
        // Arrange
        final var uuid = UUID.randomUUID();

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenReturn(getArtifactSingleOutputExample());
        when(objectMapper.readValue(anyString(), any(Class.class)))
            .thenReturn(getSingleOutput());

        // Act
        final var result = (TestOutput) operator.getOne(uuid);

        // Assert
        assertThat(result.testField).isEqualTo("testContent");
    }

    @Test
    @SneakyThrows
    void getOne_unsuccessfulRequest_throwApiInteractionUnsuccessfulException() {
        // Arrange
        final var uuid = UUID.randomUUID();

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenThrow(new ApiInteractionUnsuccessfulException("error", new NotFoundException()));

        // Act & Assert
        assertThatThrownBy(() -> operator.getOne(uuid))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());
    }

    @Test
    @SneakyThrows
    void create_successfulRequest_returnResponse() {
        // Arrange
        final var input = getSampleInput();

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenReturn(getArtifactSingleOutputExample());
        when(objectMapper.writeValueAsString(input))
            .thenReturn("{\"dummy\":\"JSON\"}");
        when(objectMapper.readValue(anyString(), any(Class.class))).thenReturn(getSingleOutput());

        // Act
        final var result = (TestOutput) operator.create(input);

        // Assert
        assertThat(result.testField).isEqualTo("testContent");
    }

    @Test
    @SneakyThrows
    void create_localArtifactSuccessfulRequest_returnResponse() {
        // Arrange
        final var input = new ArtifactInput("title", "MY DATA VALUE", true);
        input.setAdditional(Map.of("key", "value"));

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenReturn(getArtifactSingleOutputExample());
        when(objectMapper.writeValueAsString(input))
            .thenReturn("{\"dummy\":\"JSON\"}");
        when(objectMapper.readValue(anyString(), any(Class.class))).thenReturn(getSingleOutput());

        // Act
        final var result = (TestOutput) operator.create(input);

        // Assert
        assertThat(result.testField).isEqualTo("testContent");
    }

    @Test
    @SneakyThrows
    void create_remoteArtifactSuccessfulRequest_returnResponse() {
        // Arrange
        final var input = new ArtifactInput("title", new URL("https://my-backend.com"),
            new AuthenticationInput("username", "password"), new AuthenticationInput("apiKey",
            "apiKey"), true);
        input.setAdditional(Map.of("key", "value"));

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenReturn(getArtifactSingleOutputExample());
        when(objectMapper.writeValueAsString(input))
            .thenReturn("{\"dummy\":\"JSON\"}");
        when(objectMapper.readValue(anyString(), any(Class.class))).thenReturn(getSingleOutput());

        // Act
        final var result = (TestOutput) operator.create(input);

        // Assert
        assertThat(result.testField).isEqualTo("testContent");
    }


    @Test
    @SneakyThrows
    void create_validArtifactWithRoute_returnResponse() {
        // Arrange
        final var location = "https://location";

        final var route = new RouteOutput();
        final var routeLinks = new RouteLinks();
        final var routeLink = new Link();

        ReflectionTestUtils.setField(routeLink, "href", location);
        ReflectionTestUtils.setField(routeLinks, "self", routeLink);
        ReflectionTestUtils.setField(route, "links", routeLinks);

        final var input = new ArtifactInput("title", route,
            new AuthenticationInput("username", "password"), new AuthenticationInput("apiKey",
            "apiKey"), true);
        input.setAdditional(Map.of("key", "value"));

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenReturn(getArtifactSingleOutputExample());
        when(objectMapper.writeValueAsString(input))
            .thenReturn("{\"dummy\":\"JSON\"}");
        when(objectMapper.readValue(anyString(), any(Class.class))).thenReturn(getSingleOutput());

        // Act
        final var result = (TestOutput) operator.create(input);

        // Assert
        assertThat(result.testField).isEqualTo("testContent");
    }

    @Test
    @SneakyThrows
    void create_unsuccessfulRequest_throwApiInteractionUnsuccessfulException() {
        // Arrange
        final var input = getSampleInput();

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenThrow(new ApiInteractionUnsuccessfulException("error", new NotFoundException()));
        when(objectMapper.writeValueAsString(input))
            .thenReturn("{\"dummy\":\"JSON\"}");

        // Act & Assert
        assertThatThrownBy(() -> operator.create(input))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());
    }

    @Test
    @SneakyThrows
    void update_successfulRequest_throwNoException() {
        // Arrange
        final var input = getSampleInput();
        final var uuid = UUID.randomUUID();

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenReturn("Dummy String");
        when(objectMapper.writeValueAsString(input))
            .thenReturn("{\"dummy\":\"JSON\"}");

        // Act & Assert
        assertDoesNotThrow(() -> operator.update(uuid, input));
    }

    @Test
    @SneakyThrows
    void update_unsuccessfulRequest_throwApiInteractionUnsuccessfulException() {
        final var input = getSampleInput();
        final var uuid = UUID.randomUUID();

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenThrow(new ApiInteractionUnsuccessfulException("error", new NotFoundException()));
        when(objectMapper.writeValueAsString(input)).thenReturn("{\"dummy\":\"JSON\"}");

        // Act & Assert
        assertThatThrownBy(() -> operator.update(uuid, input))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());
    }

    @Test
    @SneakyThrows
    void delete_successfulRequest_throwNoException() {
        // Arrange
        final var uuid = UUID.randomUUID();

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenReturn("Dummy String");

        // Act & Assert
        assertDoesNotThrow(() -> operator.delete(uuid));
    }

    @Test
    @SneakyThrows
    void delete_unsuccessfulRequest_throwApiInteractionUnsuccessfulException() {
        // Arrange
        final var uuid = UUID.randomUUID();

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenThrow(new ApiInteractionUnsuccessfulException("error", new NotFoundException()));

        // Act & Assert
        assertThatThrownBy(() -> operator.delete(uuid))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());
    }

    /**********************************************************************************************
        Utilities
     **********************************************************************************************/

    private EntityInput<Artifact> getSampleInput() {
        final var input = new ArtifactInput();
        input.setTitle("ArtifactInputTestTitle");
        input.setAdditional(Map.of("key", "value"));
        return input;
    }

    private String getArtifactSingleOutputExample() {
        return "{\n" +
                "        \"creationDate\": \"2021-06-14T11:30:33.280+0200\",\n" +
                "        \"modificationDate\": \"2021-06-14T11:30:33.280+0200\",\n" +
                "        \"remoteId\": \"genesis\",\n" +
                "        \"title\": \"ArtifactTestTitle\",\n" +
                "        \"numAccessed\": 0,\n" +
                "        \"byteSize\": 32,\n" +
                "        \"checkSum\": 3110206735,\n" +
                "        \"additional\": {},\n" +
                "        \"_links\": {\n" +
                "          \"self\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/4e9c5c7b-2808-470d" +
                "-b2d5-9a8f3ea04d6a\"\n" +
                "          },\n" +
                "          \"data\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/4e9c5c7b-2808-470d" +
                "-b2d5-9a8f3ea04d6a/data\"\n" +
                "          },\n" +
                "          \"representations\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/4e9c5c7b-2808-470d" +
                "-b2d5-9a8f3ea04d6a/representations{?page,size}\",\n" +
                "            \"templated\": true\n" +
                "          },\n" +
                "          \"agreements\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/4e9c5c7b-2808-470d" +
                "-b2d5-9a8f3ea04d6a/agreements{?page,size}\",\n" +
                "            \"templated\": true\n" +
                "          }\n" +
                "        }\n" +
                "      }";
    }

    private String getArtifactMultiOutputExample() {
        return "{\n" +
                "  \"_embedded\": {\n" +
                "    \"artifacts\": [\n" +
                "      {\n" +
                "        \"creationDate\": \"2021-06-14T11:30:33.280+0200\",\n" +
                "        \"modificationDate\": \"2021-06-14T11:30:33.280+0200\",\n" +
                "        \"remoteId\": \"genesis\",\n" +
                "        \"title\": \"ArtifactTestTitle\",\n" +
                "        \"numAccessed\": 0,\n" +
                "        \"byteSize\": 32,\n" +
                "        \"checkSum\": 3110206735,\n" +
                "        \"additional\": {},\n" +
                "        \"_links\": {\n" +
                "          \"self\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/4e9c5c7b-2808-470d" +
                "-b2d5-9a8f3ea04d6a\"\n" +
                "          },\n" +
                "          \"data\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/4e9c5c7b-2808-470d" +
                "-b2d5-9a8f3ea04d6a/data\"\n" +
                "          },\n" +
                "          \"representations\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/4e9c5c7b-2808-470d" +
                "-b2d5-9a8f3ea04d6a/representations{?page,size}\",\n" +
                "            \"templated\": true\n" +
                "          },\n" +
                "          \"agreements\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/4e9c5c7b-2808-470d" +
                "-b2d5-9a8f3ea04d6a/agreements{?page,size}\",\n" +
                "            \"templated\": true\n" +
                "          }\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"creationDate\": \"2021-06-14T11:30:34.865+0200\",\n" +
                "        \"modificationDate\": \"2021-06-14T13:01:19.892+0200\",\n" +
                "        \"remoteId\": \"http://localhost:8080/api/artifacts/4e9c5c7b-2808-470d" +
                "-b2d5-9a8f3ea04d6a\",\n" +
                "        \"title\": \"\",\n" +
                "        \"numAccessed\": 0,\n" +
                "        \"byteSize\": 0,\n" +
                "        \"checkSum\": 0,\n" +
                "        \"additional\": {\n" +
                "          \"ids:byteSize\": \"32\",\n" +
                "          \"ids:creationDate\": \"2021-06-14T09:30:33.280Z\",\n" +
                "          \"ids:checkSum\": \"3110206735\"\n" +
                "        },\n" +
                "        \"_links\": {\n" +
                "          \"self\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/8dd358d9-c3b8-4a58" +
                "-a998-1365ea80f47b\"\n" +
                "          },\n" +
                "          \"data\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/8dd358d9-c3b8-4a58" +
                "-a998-1365ea80f47b/data\"\n" +
                "          },\n" +
                "          \"representations\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/8dd358d9-c3b8-4a58" +
                "-a998-1365ea80f47b/representations{?page,size}\",\n" +
                "            \"templated\": true\n" +
                "          },\n" +
                "          \"agreements\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/8dd358d9-c3b8-4a58" +
                "-a998-1365ea80f47b/agreements{?page,size}\",\n" +
                "            \"templated\": true\n" +
                "          }\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"creationDate\": \"2021-07-05T14:11:28.766+0200\",\n" +
                "        \"modificationDate\": \"2021-07-05T14:11:28.766+0200\",\n" +
                "        \"remoteId\": \"genesis\",\n" +
                "        \"title\": \"\",\n" +
                "        \"numAccessed\": 0,\n" +
                "        \"byteSize\": 32,\n" +
                "        \"checkSum\": 3110206735,\n" +
                "        \"additional\": {},\n" +
                "        \"_links\": {\n" +
                "          \"self\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/74d5af17-3c6d-4511" +
                "-9d73-1e50857d98db\"\n" +
                "          },\n" +
                "          \"data\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/74d5af17-3c6d-4511" +
                "-9d73-1e50857d98db/data\"\n" +
                "          },\n" +
                "          \"representations\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/74d5af17-3c6d-4511" +
                "-9d73-1e50857d98db/representations{?page,size}\",\n" +
                "            \"templated\": true\n" +
                "          },\n" +
                "          \"agreements\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/74d5af17-3c6d-4511" +
                "-9d73-1e50857d98db/agreements{?page,size}\",\n" +
                "            \"templated\": true\n" +
                "          }\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"creationDate\": \"2021-07-05T14:11:30.378+0200\",\n" +
                "        \"modificationDate\": \"2021-07-05T14:11:30.378+0200\",\n" +
                "        \"remoteId\": \"http://localhost:8080/api/artifacts/74d5af17-3c6d-4511" +
                "-9d73-1e50857d98db\",\n" +
                "        \"title\": \"\",\n" +
                "        \"numAccessed\": 0,\n" +
                "        \"byteSize\": 0,\n" +
                "        \"checkSum\": 0,\n" +
                "        \"additional\": {\n" +
                "          \"ids:byteSize\": \"32\",\n" +
                "          \"ids:creationDate\": \"2021-07-05T12:11:28.766Z\",\n" +
                "          \"ids:checkSum\": \"3110206735\"\n" +
                "        },\n" +
                "        \"_links\": {\n" +
                "          \"self\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/802c9f4c-0419-41b8" +
                "-9934-933488e23699\"\n" +
                "          },\n" +
                "          \"data\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/802c9f4c-0419-41b8" +
                "-9934-933488e23699/data\"\n" +
                "          },\n" +
                "          \"representations\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/802c9f4c-0419-41b8" +
                "-9934-933488e23699/representations{?page,size}\",\n" +
                "            \"templated\": true\n" +
                "          },\n" +
                "          \"agreements\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/802c9f4c-0419-41b8" +
                "-9934-933488e23699/agreements{?page,size}\",\n" +
                "            \"templated\": true\n" +
                "          }\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"creationDate\": \"2021-07-07T09:45:29.630+0200\",\n" +
                "        \"modificationDate\": \"2021-07-07T09:45:29.630+0200\",\n" +
                "        \"remoteId\": \"genesis\",\n" +
                "        \"title\": \"\",\n" +
                "        \"numAccessed\": 0,\n" +
                "        \"byteSize\": 32,\n" +
                "        \"checkSum\": 3110206735,\n" +
                "        \"additional\": {},\n" +
                "        \"_links\": {\n" +
                "          \"self\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/c5f2f589-a892-4cbd" +
                "-8047-fbacc02ec539\"\n" +
                "          },\n" +
                "          \"data\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/c5f2f589-a892-4cbd" +
                "-8047-fbacc02ec539/data\"\n" +
                "          },\n" +
                "          \"representations\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/c5f2f589-a892-4cbd" +
                "-8047-fbacc02ec539/representations{?page,size}\",\n" +
                "            \"templated\": true\n" +
                "          },\n" +
                "          \"agreements\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/c5f2f589-a892-4cbd" +
                "-8047-fbacc02ec539/agreements{?page,size}\",\n" +
                "            \"templated\": true\n" +
                "          }\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"creationDate\": \"2021-07-07T09:45:31.217+0200\",\n" +
                "        \"modificationDate\": \"2021-07-07T09:45:31.217+0200\",\n" +
                "        \"remoteId\": \"http://localhost:8080/api/artifacts/c5f2f589-a892-4cbd" +
                "-8047-fbacc02ec539\",\n" +
                "        \"title\": \"\",\n" +
                "        \"numAccessed\": 0,\n" +
                "        \"byteSize\": 0,\n" +
                "        \"checkSum\": 0,\n" +
                "        \"additional\": {\n" +
                "          \"ids:byteSize\": \"32\",\n" +
                "          \"ids:creationDate\": \"2021-07-07T07:45:29.630Z\",\n" +
                "          \"ids:checkSum\": \"3110206735\"\n" +
                "        },\n" +
                "        \"_links\": {\n" +
                "          \"self\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/848077ba-bb54-4a08" +
                "-9dd4-f473c999cb89\"\n" +
                "          },\n" +
                "          \"data\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/848077ba-bb54-4a08" +
                "-9dd4-f473c999cb89/data\"\n" +
                "          },\n" +
                "          \"representations\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/848077ba-bb54-4a08" +
                "-9dd4-f473c999cb89/representations{?page,size}\",\n" +
                "            \"templated\": true\n" +
                "          },\n" +
                "          \"agreements\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/848077ba-bb54-4a08" +
                "-9dd4-f473c999cb89/agreements{?page,size}\",\n" +
                "            \"templated\": true\n" +
                "          }\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"creationDate\": \"2021-07-12T10:46:07.976+0200\",\n" +
                "        \"modificationDate\": \"2021-07-12T10:46:07.976+0200\",\n" +
                "        \"remoteId\": \"genesis\",\n" +
                "        \"title\": \"\",\n" +
                "        \"numAccessed\": 0,\n" +
                "        \"byteSize\": 32,\n" +
                "        \"checkSum\": 3110206735,\n" +
                "        \"additional\": {},\n" +
                "        \"_links\": {\n" +
                "          \"self\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/12d5f106-7eb1-4b15" +
                "-9a28-9428a262455c\"\n" +
                "          },\n" +
                "          \"data\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/12d5f106-7eb1-4b15" +
                "-9a28-9428a262455c/data\"\n" +
                "          },\n" +
                "          \"representations\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/12d5f106-7eb1-4b15" +
                "-9a28-9428a262455c/representations{?page,size}\",\n" +
                "            \"templated\": true\n" +
                "          },\n" +
                "          \"agreements\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/12d5f106-7eb1-4b15" +
                "-9a28-9428a262455c/agreements{?page,size}\",\n" +
                "            \"templated\": true\n" +
                "          }\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"creationDate\": \"2021-07-12T10:46:09.559+0200\",\n" +
                "        \"modificationDate\": \"2021-07-12T10:46:09.559+0200\",\n" +
                "        \"remoteId\": \"http://localhost:8080/api/artifacts/12d5f106-7eb1-4b15" +
                "-9a28-9428a262455c\",\n" +
                "        \"title\": \"\",\n" +
                "        \"numAccessed\": 0,\n" +
                "        \"byteSize\": 0,\n" +
                "        \"checkSum\": 0,\n" +
                "        \"additional\": {\n" +
                "          \"ids:byteSize\": \"32\",\n" +
                "          \"ids:creationDate\": \"2021-07-12T08:46:07.976Z\",\n" +
                "          \"ids:checkSum\": \"3110206735\"\n" +
                "        },\n" +
                "        \"_links\": {\n" +
                "          \"self\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/9c5ab0c0-2c5e-40a0" +
                "-9257-2ba05062570e\"\n" +
                "          },\n" +
                "          \"data\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/9c5ab0c0-2c5e-40a0" +
                "-9257-2ba05062570e/data\"\n" +
                "          },\n" +
                "          \"representations\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/9c5ab0c0-2c5e-40a0" +
                "-9257-2ba05062570e/representations{?page,size}\",\n" +
                "            \"templated\": true\n" +
                "          },\n" +
                "          \"agreements\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/9c5ab0c0-2c5e-40a0" +
                "-9257-2ba05062570e/agreements{?page,size}\",\n" +
                "            \"templated\": true\n" +
                "          }\n" +
                "        }\n" +
                "      }\n" +
                "    ]\n" +
                "  },\n" +
                "  \"_links\": {\n" +
                "    \"self\": {\n" +
                "      \"href\": \"http://localhost:8080/api/artifacts?page=0&size=30\"\n" +
                "    }\n" +
                "  },\n" +
                "  \"page\": {\n" +
                "    \"size\": 30,\n" +
                "    \"totalElements\": 8,\n" +
                "    \"totalPages\": 1,\n" +
                "    \"number\": 0\n" +
                "  }\n" +
                "}";
    }

    private TestMultiOutput getTestMultiOutput() {
        final var output = new TestMultiOutput();
        output.testField = "testContent";
        return output;
    }

    private TestOutput getSingleOutput() {
        final var output = new TestOutput();
        output.testField = "testContent";
        return output;
    }

    static class TestMultiOutput extends ArtifactMultiOutput {
        public String testField;
    }

    static class TestOutput extends ArtifactOutput {
        public String testField;
    }
}
