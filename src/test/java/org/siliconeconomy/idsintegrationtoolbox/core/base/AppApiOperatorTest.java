/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.base;


import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.AppEndpointsApi;
import org.siliconeconomy.idsintegrationtoolbox.model.RestRequest;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.ActionType;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.AppStore;
import org.siliconeconomy.idsintegrationtoolbox.model.input.AppInput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.AppMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.AppOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.OperationNotAllowedException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.client.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 * AppApiOperatorTest
 *
 * @author Steffen Biehs
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {AppApiOperator.class})
class AppApiOperatorTest {

    /**
     * Class under test
     */
    @Autowired
    private AppApiOperator operator;

    @MockBean
    private AppEndpointsApi endpoints;

    @MockBean
    private ObjectMapper objectMapper;

    @MockBean
    private RestRequestHandler requestHandler;

    @Captor
    private ArgumentCaptor<RestRequest> requestArgumentCaptor;

    @Test
    void update_notAllowed_throwOperationNotAllowedException() {
        // Arrange
        final var uuid = UUID.randomUUID();
        final var input = new AppInput();

        // Act & Assert
        assertThatThrownBy(() -> operator.update(uuid, input))
            .isInstanceOf(OperationNotAllowedException.class);
    }

    @Test
    @SneakyThrows
    void applyAction_successfulRequest_doNotThrowException() {
        // Arrange
        final var input = ActionType.START;
        final var uuid = UUID.randomUUID();

        when(requestHandler.sendRequest(requestArgumentCaptor.capture())).thenReturn("Dummy String");
        when(objectMapper.writeValueAsString(input)).thenReturn("{\"dummy\":\"JSON\"}");

        // Act & Assert
        assertDoesNotThrow(() -> operator.applyAction(uuid, input));
        assertThat(requestArgumentCaptor.getValue().getQueryParameters().get("type").get(0)).isEqualTo(input.toString());
    }

    @Test
    @SneakyThrows
    void applyAction_unsuccessfulRequest_throwApiInteractionUnsuccessfulException() {
        // Arrange
        final var input = ActionType.START;
        final var uuid = UUID.randomUUID();

        when(requestHandler.sendRequest(any(RestRequest.class)))
            .thenThrow(new ApiInteractionUnsuccessfulException("error", new NotFoundException()));
        when(objectMapper.writeValueAsString(input)).thenReturn("{\"dummy\":\"JSON\"}");

        // Act & Assert
        assertThatThrownBy(() -> operator.applyAction(uuid, input))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());

    }

    @Test
    @SneakyThrows
    void getAppstoreById_successfulRequest_doNotThrowException() {
        // Arrange
        final var input = new AppStore();
        final var uuid = UUID.randomUUID();

        when(requestHandler.sendRequest(requestArgumentCaptor.capture())).thenReturn("Dummy String");
        when(objectMapper.writeValueAsString(input)).thenReturn("{\"dummy\":\"JSON\"}");

        // Act & Assert
        assertDoesNotThrow(() -> operator.appstore(uuid));
        assertTrue(requestArgumentCaptor.getValue().getPathVariables().containsValue(uuid.toString()));
    }

    @Test
    @SneakyThrows
    void getAppstoreById_unsuccessfulRequest_throwApiInteractionUnsuccessfulException() {
        // Arrange
        final var input = new AppStore();
        final var uuid = UUID.randomUUID();

        when(requestHandler.sendRequest(any(RestRequest.class)))
            .thenThrow(new ApiInteractionUnsuccessfulException("error", new NotFoundException()));
        when(objectMapper.writeValueAsString(input)).thenReturn("{\"dummy\":\"JSON\"}");

        // Act & Assert
        assertThatThrownBy(() -> operator.appstore(uuid))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());

    }

    @Test
    void getEntitySingleOutputClass_returnClass() {
        // Act
        final var result = operator.getEntitySingleOutputClass();

        // Assert
        assertThat(result).isEqualTo(AppOutput.class);
    }

    @Test
    void getEntityMultiOutputClass_returnClass() {
        // Act
        final var result = operator.getEntityMultiOutputClass();

        // Assert
        assertThat(result).isEqualTo(AppMultiOutput.class);
    }
}
