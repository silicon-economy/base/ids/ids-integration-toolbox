/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.base;

import java.util.Map;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.siliconeconomy.idsintegrationtoolbox.model.RestRequest;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Configuration;
import org.siliconeconomy.idsintegrationtoolbox.model.input.ConfigurationInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.EntityInput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.ConfigurationMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ConfigurationOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.client.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

/**
 * ConfigurationApiOperatorTest
 *
 * @author Florian Zimmer
 */
@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest(classes = ConfigurationApiOperator.class)
class ConfigurationApiOperatorTest {

    /** Class under test */
    @Autowired
    private ConfigurationApiOperator operator;

    @MockBean
    private RestRequestHandler restRequestHandler;

    @MockBean
    private ObjectMapper objectMapper;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    @SneakyThrows
    void getAll_successfulRequest_returnResponse() {
        // Arrange
        final var dummyString = "String";

        when(restRequestHandler.sendRequest(any(RestRequest.class))).thenReturn(dummyString);
        when(objectMapper.readValue(anyString(), eq(ConfigurationMultiOutput.class)))
            .thenReturn(getTestMultiOutput());

        // Act
        final var result = (TestMultiOutput) operator.getAll();

        // Assert
        assertThat(result.testField).isEqualTo("testContent");
    }

    @Test
    @SneakyThrows
    void getAll_unsuccessfulRequest_throwApiInteractionUnsuccessfulException() {
        // Arrange
        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenThrow(new ApiInteractionUnsuccessfulException("error", new NotFoundException()));

        // Act & Assert
        assertThatThrownBy(() -> operator.getAll())
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());
    }

    @Test
    @SneakyThrows
    void getOne_successfulRequest_returnResponse() {
        // Arrange
        final var uuid = UUID.randomUUID();

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenReturn(getConfigurationSingleOutputExample());
        when(objectMapper.readValue(anyString(), any(Class.class)))
            .thenReturn(getSingleOutput());

        // Act
        final var result = (TestOutput) operator.getOne(uuid);

        // Assert
        assertThat(result.testField).isEqualTo("testContent");
    }

    @Test
    @SneakyThrows
    void getOne_unsuccessfulRequest_throwApiInteractionUnsuccessfulException() {
        // Arrange
        final var uuid = UUID.randomUUID();

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenThrow(new ApiInteractionUnsuccessfulException("error", new NotFoundException()));

        // Act & Assert
        assertThatThrownBy(() -> operator.getOne(uuid))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());
    }

    @Test
    @SneakyThrows
    void getActive_successfulRequest_returnResponse() {
        // Arrange
        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenReturn(getConfigurationSingleOutputExample());
        when(objectMapper.readValue(anyString(), any(Class.class)))
            .thenReturn(getSingleOutput());

        // Act
        final var result = (TestOutput) operator.active();

        // Assert
        assertThat(result.testField).isEqualTo("testContent");
    }

    @Test
    @SneakyThrows
    void getActive_unsuccessfulRequest_throwApiInteractionUnsuccessfulException() {
        // Arrange
        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenThrow(new ApiInteractionUnsuccessfulException("error", new NotFoundException()));

        // Act & Assert
        assertThatThrownBy(() -> operator.active())
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());
    }

    @Test
    @SneakyThrows
    void create_successfulRequest_returnResponse() {
        // Arrange
        final var input = getSampleInput();

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenReturn(getConfigurationSingleOutputExample());
        when(objectMapper.writeValueAsString(input))
            .thenReturn("{\"dummy\":\"JSON\"}");
        when(objectMapper.readValue(anyString(), any(Class.class))).thenReturn(getSingleOutput());

        // Act
        final var result = (TestOutput) operator.create(input);

        // Assert
        assertThat(result.testField).isEqualTo("testContent");
    }

    @Test
    @SneakyThrows
    void create_localConfigurationSuccessfulRequest_returnResponse() {
        // Arrange
        final var input = new ConfigurationInput();
        input.setAdditional(Map.of("key", "value"));

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenReturn(getConfigurationSingleOutputExample());
        when(objectMapper.writeValueAsString(input))
            .thenReturn("{\"dummy\":\"JSON\"}");
        when(objectMapper.readValue(anyString(), any(Class.class))).thenReturn(getSingleOutput());

        // Act
        final var result = (TestOutput) operator.create(input);

        // Assert
        assertThat(result.testField).isEqualTo("testContent");
    }

    @Test
    @SneakyThrows
    void create_remoteConfigurationSuccessfulRequest_returnResponse() {
        // Arrange
        final var input = new ConfigurationInput();
        input.setAdditional(Map.of("key", "value"));

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenReturn(getConfigurationSingleOutputExample());
        when(objectMapper.writeValueAsString(input))
            .thenReturn("{\"dummy\":\"JSON\"}");
        when(objectMapper.readValue(anyString(), any(Class.class))).thenReturn(getSingleOutput());

        // Act
        final var result = (TestOutput) operator.create(input);

        // Assert
        assertThat(result.testField).isEqualTo("testContent");
    }

    @Test
    @SneakyThrows
    void create_unsuccessfulRequest_throwApiInteractionUnsuccessfulException() {
        // Arrange
        final var input = getSampleInput();

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenThrow(new ApiInteractionUnsuccessfulException("error", new NotFoundException()));
        when(objectMapper.writeValueAsString(input))
            .thenReturn("{\"dummy\":\"JSON\"}");

        // Act & Assert
        assertThatThrownBy(() -> operator.create(input))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());
    }

    @Test
    @SneakyThrows
    void update_successfulRequest_throwNoException() {
        // Arrange
        final var input = getSampleInput();
        final var uuid = UUID.randomUUID();

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenReturn("Dummy String");
        when(objectMapper.writeValueAsString(input))
            .thenReturn("{\"dummy\":\"JSON\"}");

        // Act & Assert
        assertDoesNotThrow(() -> operator.update(uuid, input));
    }

    @Test
    @SneakyThrows
    void update_unsuccessfulRequest_throwApiInteractionUnsuccessfulException() {
        final var input = getSampleInput();
        final var uuid = UUID.randomUUID();

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenThrow(new ApiInteractionUnsuccessfulException("error", new NotFoundException()));
        when(objectMapper.writeValueAsString(input)).thenReturn("{\"dummy\":\"JSON\"}");

        // Act & Assert
        assertThatThrownBy(() -> operator.update(uuid, input))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());
    }

    @Test
    @SneakyThrows
    void updateActive_successfulRequest_throwNoException() {
        // Arrange
        final var uuid = UUID.randomUUID();

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenReturn("Dummy String");

        // Act & Assert
        assertDoesNotThrow(() -> operator.active(uuid));
    }

    @Test
    @SneakyThrows
    void updateActive_unsuccessfulRequest_throwApiInteractionUnsuccessfulException() {
        final var uuid = UUID.randomUUID();

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenThrow(new ApiInteractionUnsuccessfulException("error", new NotFoundException()));

        // Act & Assert
        assertThatThrownBy(() -> operator.active(uuid))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());
    }

    @Test
    @SneakyThrows
    void delete_successfulRequest_throwNoException() {
        // Arrange
        final var uuid = UUID.randomUUID();

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenReturn("Dummy String");

        // Act & Assert
        assertDoesNotThrow(() -> operator.delete(uuid));
    }

    @Test
    @SneakyThrows
    void delete_unsuccessfulRequest_throwApiInteractionUnsuccessfulException() {
        // Arrange
        final var uuid = UUID.randomUUID();

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenThrow(new ApiInteractionUnsuccessfulException("error", new NotFoundException()));

        // Act & Assert
        assertThatThrownBy(() -> operator.delete(uuid))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());
    }

    /**********************************************************************************************
        Utilities
     **********************************************************************************************/

    private EntityInput<Configuration> getSampleInput() {
        final var input = new ConfigurationInput();
        input.setTitle("ConfigurationInputTestTitle");
        input.setAdditional(Map.of("key", "value"));
        return input;
    }

    private String getConfigurationSingleOutputExample() {
        return "{\n" +
            "        \"creationDate\": \"2021-12-08T11:14:24.672+0100\",\n" +
            "        \"modificationDate\": \"2021-12-08T11:14:24.672+0100\",\n" +
            "        \"connectorId\": \"https://w3id" +
            ".org/idsa/autogen/baseConnector/7b934432-a85e-41c5-9f65-669219dde4ea\",\n" +
            "        \"title\": \"Dataspace Connector\",\n" +
            "        \"description\": \"IDS Connector with static example resources hosted by the" +
            " Fraunhofer ISST\",\n" +
            "        \"defaultEndpoint\": \"https://localhost:8080/api/ids/data\",\n" +
            "        \"version\": \"6.5.1\",\n" +
            "        \"curator\": \"https://www.isst.fraunhofer.de/\",\n" +
            "        \"maintainer\": \"https://www.isst.fraunhofer.de/\",\n" +
            "        \"inboundModelVersion\": [\n" +
            "          \"4.2.6\",\n" +
            "          \"4.2.0\",\n" +
            "          \"4.2.1\",\n" +
            "          \"4.1.2\",\n" +
            "          \"4.0.0\",\n" +
            "          \"4.1.0\",\n" +
            "          \"4.2.4\",\n" +
            "          \"4.2.5\",\n" +
            "          \"4.2.2\",\n" +
            "          \"4.2.3\"\n" +
            "        ],\n" +
            "        \"outboundModelVersion\": \"4.2.6\",\n" +
            "        \"securityProfile\": \"Base Security\",\n" +
            "        \"status\": \"Online\",\n" +
            "        \"logLevel\": \"Warn\",\n" +
            "        \"deployMode\": \"Test\",\n" +
            "        \"proxy\": null,\n" +
            "        \"trustStore\": {\n" +
            "          \"location\": \"file:///conf/truststore.p12\",\n" +
            "          \"alias\": \"1\"\n" +
            "        },\n" +
            "        \"keyStore\": {\n" +
            "          \"location\": \"file:///conf/keystore-localhost.p12\",\n" +
            "          \"alias\": \"1\"\n" +
            "        },\n" +
            "        \"_links\": {\n" +
            "          \"self\": {\n" +
            "            \"href\": \"http://localhost:8080/api/configurations/ff4b56f1-2379-4819" +
            "-bf85-bd52695d2517\"\n" +
            "          }\n" +
            "        }\n" +
            "      }";
    }

    private TestMultiOutput getTestMultiOutput() {
        final var output = new TestMultiOutput();
        output.testField = "testContent";
        return output;
    }

    private TestOutput getSingleOutput() {
        final var output = new TestOutput();
        output.testField = "testContent";
        return output;
    }

    static class TestMultiOutput extends ConfigurationMultiOutput {
        public String testField;
    }

    static class TestOutput extends ConfigurationOutput {
        public String testField;
    }
}
