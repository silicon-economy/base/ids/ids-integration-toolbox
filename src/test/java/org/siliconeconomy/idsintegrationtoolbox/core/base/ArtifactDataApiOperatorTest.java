/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.base;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.siliconeconomy.idsintegrationtoolbox.model.RestRequest;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.QueryInput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.ResourceData;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * ArtifactDataApiOperatorTest
 *
 * @author Ronja Quensel
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {ArtifactDataApiOperator.class})
class ArtifactDataApiOperatorTest {

    /** Class under test */
    @Autowired
    private ArtifactDataApiOperator operator;

    @MockBean
    private RestRequestHandler restRequestHandler;

    @MockBean
    private ObjectMapper objectMapper;

    @Captor
    private ArgumentCaptor<RestRequest> requestCaptor;

    private final ResourceData data = new ResourceData("mimeType", new byte[1]);

    @Test
    @SneakyThrows
    void uploadData_successfulRequest_sendMessage() {
        // Arrange
        final var uuid = UUID.randomUUID();
        final var data = "I AM DATA";

        when(restRequestHandler.sendRequest(any(RestRequest.class))).thenReturn("response");

        // Act
        operator.upload(uuid, data);

        // Assert
        verify(restRequestHandler, times(1)).sendRequest(requestCaptor.capture());
        var request = requestCaptor.getValue();
        assertThat(request.getBody()).isEqualTo(data);
    }

    @Test
    @SneakyThrows
    void getDataPOST_withRouteIds_returnResponse() {
        // Arrange
        final var uuid = UUID.randomUUID();
        final var routeIds = List.of(
            URI.create("http://localhost:8080/api/routes/1549ecfb-e086-4513-97b1-5b73dd5bc9bc"));

        when(restRequestHandler.sendDataRequest(any(RestRequest.class))).thenReturn(data);

        // Act
        final var result = operator.get(uuid, null, routeIds);

        // Assert
        assertEquals(data, result);

        verify(restRequestHandler, times(1)).sendDataRequest(requestCaptor.capture());
        var request = requestCaptor.getValue();
        assertThat(request.getQueryParameters())
            .hasSize(1)
            .containsEntry("routeIds", List.of(routeIds.get(0).toString()));
        assertThat(request.getBody()).isNull();
    }

    @Test
    @SneakyThrows
    void getDataPOST_withQueryInput_returnResponse() {
        // Arrange
        final var uuid = UUID.randomUUID();

        final var queryInput = new QueryInput();
        queryInput.setOptional("optional");
        final var queryInputString = "{\"optional\": \"optional\"}";

        when(restRequestHandler.sendDataRequest(any(RestRequest.class))).thenReturn(data);
        when(objectMapper.writeValueAsString(queryInput)).thenReturn(queryInputString);

        // Act
        final var result = operator.get(uuid, queryInput, null);

        // Assert
        assertEquals(data, result);

        verify(restRequestHandler, times(1)).sendDataRequest(requestCaptor.capture());
        var request = requestCaptor.getValue();
        assertThat(request.getBody()).isNotNull().isEqualTo(queryInputString);
        assertThat(request.getQueryParameters()).isEmpty();
    }

    @Test
    @SneakyThrows
    void getDataGET_withRouteIds_returnResponse() {
        // Arrange
        final var uuid = UUID.randomUUID();
        final var download = true;
        final var routeIds = List.of(
            URI.create("http://localhost:8080/api/routes/1549ecfb-e086-4513-97b1-5b73dd5bc9bc"));

        when(restRequestHandler.sendDataRequest(any(RestRequest.class))).thenReturn(data);

        // Act
        final var result = operator.get(uuid, download, null, routeIds, null, null, null);

        // Assert
        assertEquals(data, result);

        verify(restRequestHandler, times(1)).sendDataRequest(requestCaptor.capture());
        var request = requestCaptor.getValue();
        assertThat(request.getQueryParameters())
            .hasSize(2)
            .containsEntry("routeIds", List.of(routeIds.get(0).toString()))
            .containsEntry("download", List.of(String.valueOf(download)));
        assertThat(request.getBody()).isNull();
    }

    @Test
    @SneakyThrows
    void getDataGET_successfulRequestWithPath_returnResponse() {
        // Arrange
        final var uuid = UUID.randomUUID();
        final var download = true;
        final var path = "/";

        when(restRequestHandler.sendDataRequest(any(RestRequest.class))).thenReturn(data);

        // Act
        final var result = operator.get(uuid, download, null, null, null, null, path);

        // Assert
        assertEquals(data, result);

        verify(restRequestHandler, times(1)).sendDataRequest(requestCaptor.capture());
        var request = requestCaptor.getValue();
        assertThat(request.getEndpointPathSegments()).contains(path);
        assertThat(request.getBody()).isNull();
        assertThat(request.getQueryParameters())
            .hasSize(1)
            .containsEntry("download", List.of(String.valueOf(download)));
    }

    @Test
    @SneakyThrows
    void getDataGET_successfulRequestWithAgreementParamsAndHeaders_returnResponse() {
        // Arrange
        final var uuid = UUID.randomUUID();
        final var download = true;
        final var agreement = URI.create("https://agreement.com");

        final var headerKey = "header-key";
        final var headerValue = "header-value";
        final var headers = new HashMap<String, String>() {{
            put(headerKey, headerValue);
        }};

        final var paramKey = "param-key";
        final var paramValue = "param-value";
        final var params = new HashMap<String, String>() {{
            put(paramKey, paramValue);
        }};

        when(restRequestHandler.sendDataRequest(any(RestRequest.class))).thenReturn(data);

        // Act
        final var result = operator.get(uuid, download, agreement, null, params, headers,
            null);

        // Assert
        assertEquals(data, result);

        verify(restRequestHandler, times(1)).sendDataRequest(requestCaptor.capture());
        var request = requestCaptor.getValue();
        assertThat(request.getBody()).isNull();
        assertThat(request.getQueryParameters())
            .hasSize(3)
            .containsEntry("agreementUri", List.of(agreement.toString()))
            .containsEntry(paramKey, List.of(paramValue))
            .containsEntry("download", List.of(String.valueOf(download)));
        assertThat(request.getHeaders())
            .hasSize(1)
            .containsEntry(headerKey, headerValue);
    }

}
