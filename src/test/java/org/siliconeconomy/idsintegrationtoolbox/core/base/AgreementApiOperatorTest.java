/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.base;

import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.AgreementArtifactsApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Agreement;
import org.siliconeconomy.idsintegrationtoolbox.model.input.EntityInput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.AgreementMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.AgreementOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.OperationNotAllowedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

/**
 * AgreementApiOperatorTest
 *
 * @author Florian Zimmer
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {AgreementApiOperator.class})
class AgreementApiOperatorTest {

    /** Class under test */
    @Autowired
    private AgreementApiOperator operator;

    @MockBean
    private AgreementArtifactsApi artifacts;

    @MockBean
    private ObjectMapper objectMapper;

    @MockBean
    private RestRequestHandler requestHandler;

    @Test
    void create_notAllowed_throwOperationNotAllowedException() {
        // Arrange
        final var input = new AgreementInput();

        // Act & Assert
        assertThatThrownBy(() -> operator.create(input))
            .isInstanceOf(OperationNotAllowedException.class);
    }

    @Test
    void update_notAllowed_throwOperationNotAllowedException() {
        // Arrange
        final var uuid = UUID.randomUUID();
        final var input = new AgreementInput();

        // Act & Assert
        assertThatThrownBy(() -> operator.update(uuid, input))
            .isInstanceOf(OperationNotAllowedException.class);
    }

    @Test
    void delete_notAllowed_throwOperationNotAllowedException() {
        // Arrange
        final var uuid = UUID.randomUUID();

        // Act & Assert
        assertThatThrownBy(() -> operator.delete(uuid))
            .isInstanceOf(OperationNotAllowedException.class);
    }

    @Test
    void getEntitySingleOutputClass_returnClass() {
        // Act
        final var result = operator.getEntitySingleOutputClass();

        // Assert
        assertThat(result).isEqualTo(AgreementOutput.class);
    }

    @Test
    void getEntityMultiOutputClass_returnClass() {
        // Act
        final var result = operator.getEntityMultiOutputClass();

        // Assert
        assertThat(result).isEqualTo(AgreementMultiOutput.class);
    }

    private static class AgreementInput extends EntityInput<Agreement> {
    }
}
