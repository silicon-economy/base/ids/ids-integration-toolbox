/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.base;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.siliconeconomy.idsintegrationtoolbox.model.RestRequest;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.SubscriptionMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.SubscriptionOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.client.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

/**
 * SubscriptionApiOperatorTest
 *
 * @author Florian Zimmer
 */
@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest(classes = SubscriptionApiOperator.class)
class SubscriptionApiOperatorTest {

    /** Class under test */
    @Autowired
    private SubscriptionApiOperator operator;

    @MockBean
    private RestRequestHandler restRequestHandler;

    @MockBean
    private ObjectMapper objectMapper;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    @SneakyThrows
    void getOwned_successfulRequest_returnResponse() {
        // Arrange
        final var dummyString = "String";

        when(restRequestHandler.sendRequest(any(RestRequest.class))).thenReturn(dummyString);
        when(objectMapper.readValue(anyString(), eq(SubscriptionMultiOutput.class)))
            .thenReturn(getTestMultiOutput());

        // Act
        final var result = (TestMultiOutput) operator.owned();

        // Assert
        assertThat(result.testField).isEqualTo("testContent");
    }

    @Test
    @SneakyThrows
    void getOwned_unsuccessfulRequest_throwApiInteractionUnsuccessfulException() {
        // Arrange
        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenThrow(new ApiInteractionUnsuccessfulException("error", new NotFoundException()));

        // Act & Assert
        assertThatThrownBy(() -> operator.owned())
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());
    }

    @Test
    void getEntitySingleOutputClass_returnClass() {
        // Act
        final var result = operator.getEntitySingleOutputClass();

        // Assert
        assertThat(result).isEqualTo(SubscriptionOutput.class);
    }

    @Test
    void getEntityMultiOutputClass_returnClass() {
        // Act
        final var result = operator.getEntityMultiOutputClass();

        // Assert
        assertThat(result).isEqualTo(SubscriptionMultiOutput.class);
    }

    /**********************************************************************************************
        Utilities
     **********************************************************************************************/

    private TestMultiOutput getTestMultiOutput() {
        final var output = new TestMultiOutput();
        output.testField = "testContent";
        return output;
    }

    static class TestMultiOutput extends SubscriptionMultiOutput {
        public String testField;
    }
}
