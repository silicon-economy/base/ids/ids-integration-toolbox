/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.base;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.siliconeconomy.idsintegrationtoolbox.model.RestRequest;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.AbstractEntity;
import org.siliconeconomy.idsintegrationtoolbox.model.input.EntityInput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.Embedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.Link;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.Links;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.EntityMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.EntityOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.client.NotFoundException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

/**
 * BaseApiOperatorTest
 *
 * @author Haydar Qarawlus
 */
@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class BaseApiOperatorTest {

    /** Class under test */
    private BaseApiOperator operator;

    @Mock
    private ObjectMapper objectMapper;

    @Mock
    private RestRequestHandler requestHandler;

    @Captor
    private ArgumentCaptor<RestRequest> requestArgumentCaptor;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
        operator = Mockito.mock(BaseApiOperator.class, Mockito.withSettings()
            .useConstructor(requestHandler, objectMapper)
            .defaultAnswer(Mockito.CALLS_REAL_METHODS));
    }

    @Test
    @SneakyThrows
    void getAll_successfulRequest_returnResponse() {
        // Arrange
        final var testClass = TestMultiOutput.class;
        final var dummyString = "Dummy String";
        final var output = getSampleOutputMultiple();

        when(requestHandler.sendRequest(any(RestRequest.class))).thenReturn(dummyString);
        when(objectMapper.readValue(anyString(), eq(testClass))).thenReturn(output);
        when(operator.getEntityMultiOutputClass()).thenReturn(testClass);

        // Act
        final var result = (TestMultiOutput) operator.getAll();

        // Assert
        assertThat(result.testField).isEqualTo("testContent");
    }

    @Test
    @SneakyThrows
    void getAll_successfulRequestWithPaging_returnResponse() {
        // Arrange
        final var testClass = TestMultiOutput.class;
        final var dummyString = "Dummy String";
        final var output = getSampleOutputMultiple();
        final var page = Integer.valueOf(1);
        final var size = Integer.valueOf(2);

        when(requestHandler.sendRequest(any(RestRequest.class))).thenReturn(dummyString);
        when(objectMapper.readValue(anyString(), eq(testClass))).thenReturn(output);
        when(operator.getEntityMultiOutputClass()).thenReturn(testClass);

        // Act
        final var result = (TestMultiOutput) operator.getAll(page, size);

        // Assert
        assertThat(result.testField).isEqualTo("testContent");
    }

    @Test
    @SneakyThrows
    void getAll_unsuccessfulRequest_throwApiInteractionUnsuccessfulException() {
        // Arrange
        when(requestHandler.sendRequest(any(RestRequest.class)))
            .thenThrow(new ApiInteractionUnsuccessfulException("error", new NotFoundException()));

        // Act & Assert
        assertThatThrownBy(() -> operator.getAll())
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());
    }

    @Test
    @SneakyThrows
    void getOne_successfulRequest_returnResponse() {
        // Arrange
        final var uuid = UUID.randomUUID();

        when(requestHandler.sendRequest(any(RestRequest.class))).thenReturn("Dummy String");
        when(objectMapper.readValue(anyString(), any(Class.class)))
            .thenReturn(getSampleOutputSingle());
        when(operator.getEntitySingleOutputClass()).thenReturn(TestOutput.class);

        // Act
        final var result = (TestOutput) operator.getOne(uuid);

        // Assert
        assertThat(result.testField).isEqualTo("testContent");
    }

    @Test
    @SneakyThrows
    void getOne_unsuccessfulRequest_throwApiInteractionUnsuccessfulException() {
        // Arrange
        final var uuid = UUID.randomUUID();

        when(requestHandler.sendRequest(any(RestRequest.class)))
            .thenThrow(new ApiInteractionUnsuccessfulException("error", new NotFoundException()));

        // Act & Assert
        assertThatThrownBy(() -> operator.getOne(uuid))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());
    }

    @Test
    @SneakyThrows
    void create_successfulRequest_returnResponse() {
        // Arrange
        final var input = getSampleInput();

        when(requestHandler.sendRequest(requestArgumentCaptor.capture())).thenReturn("Dummy String");
        when(objectMapper.writeValueAsString(input)).thenReturn("{\"dummy\":\"JSON\"}");
        when(objectMapper.readValue(anyString(), any(Class.class)))
            .thenReturn(getSampleOutputSingle());
        when(operator.getEntitySingleOutputClass()).thenReturn(TestOutput.class);

        // Act
        final var result = (TestOutput) operator.create(input);

        // Assert
        assertThat(result.testField).isEqualTo("testContent");
        assertThat(requestArgumentCaptor.getValue().getBody()).isEqualTo("{\"dummy\":\"JSON\"," +
            "\"key\":\"value\"}");
    }

    @Test
    @SneakyThrows
    void create_successfulRequestWithoutAdditional_returnResponse() {
        // Arrange
        final var input = getSampleInputWithoutAdditional();

        when(requestHandler.sendRequest(requestArgumentCaptor.capture())).thenReturn("Dummy String");
        when(objectMapper.writeValueAsString(input)).thenReturn("{\"dummy\":\"JSON\"}");
        when(objectMapper.readValue(anyString(), any(Class.class)))
            .thenReturn(getSampleOutputSingle());
        when(operator.getEntitySingleOutputClass()).thenReturn(TestOutput.class);

        // Act
        final var result = (TestOutput) operator.create(input);

        // Assert
        assertThat(result.testField).isEqualTo("testContent");
        assertThat(requestArgumentCaptor.getValue().getBody()).isEqualTo("{\"dummy\":\"JSON\"}");
    }

    @Test
    @SneakyThrows
    void create_unsuccessfulRequest_throwApiInteractionUnsuccessfulException() {
        // Assert
        final var input = getSampleInput();

        when(requestHandler.sendRequest(any(RestRequest.class)))
            .thenThrow(new ApiInteractionUnsuccessfulException("error", new NotFoundException()));
        when(objectMapper.writeValueAsString(input)).thenReturn("{\"dummy\":\"JSON\"}");

        // Act & Assert
        assertThatThrownBy(() -> operator.create(input))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());
    }

    @Test
    @SneakyThrows
    void update_successfulRequest_doNotThrowException() {
        // Arrange
        final var input = getSampleInput();
        final var uuid = UUID.randomUUID();

        when(requestHandler.sendRequest(requestArgumentCaptor.capture())).thenReturn("Dummy String");
        when(objectMapper.writeValueAsString(input)).thenReturn("{\"dummy\":\"JSON\"}");

        // Act & Assert
        assertDoesNotThrow(() -> operator.update(uuid, input));
        assertThat(requestArgumentCaptor.getValue().getBody()).isEqualTo("{\"dummy\":\"JSON\"," +
            "\"key\":\"value\"}");
    }

    @Test
    @SneakyThrows
    void update_unsuccessfulRequest_throwApiInteractionUnsuccessfulException() {
        // Arrange
        final var input = getSampleInput();
        final var uuid = UUID.randomUUID();

        when(requestHandler.sendRequest(any(RestRequest.class)))
            .thenThrow(new ApiInteractionUnsuccessfulException("error", new NotFoundException()));
        when(objectMapper.writeValueAsString(input)).thenReturn("{\"dummy\":\"JSON\"}");

        // Act & Assert
        assertThatThrownBy(() -> operator.update(uuid, input))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());

    }

    @Test
    @SneakyThrows
    void delete_successfulRequest_doNotThrowException() {
        // Arrange
        final var uuid = UUID.randomUUID();

        when(requestHandler.sendRequest(any(RestRequest.class))).thenReturn("Dummy String");

        // Act & Assert
        assertDoesNotThrow(() -> operator.delete(uuid));
    }

    @Test
    @SneakyThrows
    void delete_unsuccessfulRequest_throwApiInteractionUnsuccessfulException() {
        // Arrange
        final var uuid = UUID.randomUUID();
        when(requestHandler.sendRequest(any(RestRequest.class)))
            .thenThrow(new ApiInteractionUnsuccessfulException("error", new NotFoundException()));

        // Act & Assert
        assertThatThrownBy(() -> operator.delete(uuid))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());
    }

    /**********************************************************************************************
        Utilities
     **********************************************************************************************/

    private EntityInput<AbstractEntity> getSampleInput() {
        final var input = new EntityInput<>() {
        };
        input.setAdditional(Map.of("key", "value"));
        return input;
    }

    private EntityInput<AbstractEntity> getSampleInputWithoutAdditional() {
        return new EntityInput<>() {
        };
    }

    public EntityOutput<AbstractEntity> getSampleOutputSingle() {
        final var output = new TestOutput();
        output.testField = "testContent";
        return output;
    }

    public TestMultiOutput getSampleOutputMultiple() {
        final var output = new TestMultiOutput();
        output.testField = "testContent";
        return output;
    }

    static class TestMultiOutput extends EntityMultiOutput<AbstractEntity, TestOutput, TestEmbedded,
        TestLinks> {
        public String testField;

    }

    static class TestOutput extends EntityOutput<AbstractEntity> {
        public String testField;

        @Override
        public Links<AbstractEntity> getLinks() {
            return null;
        }
    }

    static class TestEmbedded extends Embedded<AbstractEntity, TestOutput> {
        public String testField;

        @Override
        public List<TestOutput> getEntries() {
            return null;
        }
    }

    static class TestLinks implements Links<AbstractEntity> {

        @Override
        public Link getSelf() {
            return null;
        }
    }
}
