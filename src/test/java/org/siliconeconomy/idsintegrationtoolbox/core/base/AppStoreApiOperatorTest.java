/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.base;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.AppStoreAppsApi;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.AppStoreMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.AppStoreOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * AppStoreApiOperatorTest
 *
 * @author Steffen Biehs
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {AppStoreApiOperator.class})
class AppStoreApiOperatorTest {

    /**
     * Class under test
     */
    @Autowired
    private AppStoreApiOperator operator;

    @MockBean
    private AppStoreAppsApi apps;

    @MockBean
    private ObjectMapper objectMapper;

    @MockBean
    private RestRequestHandler requestHandler;

    @Test
    void getEntitySingleOutputClass_returnClass() {
        // Act
        final var result = operator.getEntitySingleOutputClass();

        // Assert
        assertThat(result).isEqualTo(AppStoreOutput.class);
    }

    @Test
    void getEntityMultiOutputClass_returnClass() {
        // Act
        final var result = operator.getEntityMultiOutputClass();

        // Assert
        assertThat(result).isEqualTo(AppStoreMultiOutput.class);
    }
}
