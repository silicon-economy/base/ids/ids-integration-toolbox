/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.base;

import java.net.URI;
import java.util.Map;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RouteEndpointsApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RouteSubRoutesApi;
import org.siliconeconomy.idsintegrationtoolbox.model.RestRequest;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Route;
import org.siliconeconomy.idsintegrationtoolbox.model.input.EntityInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.RouteInput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ArtifactLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.Link;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.RouteMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ArtifactOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.RouteOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.client.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

/**
 * RouteApiOperatorTest
 *
 * @author Florian Zimmer
 */
@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest(classes = {RouteApiOperator.class})
class RouteApiOperatorTest {

    /** Class under test */
    @Autowired
    private RouteApiOperator operator;

    @MockBean
    private RouteEndpointsApi endpoints;

    @MockBean
    private RouteSubRoutesApi subRoutes;

    @MockBean
    private RestRequestHandler restRequestHandler;

    @MockBean
    private ObjectMapper objectMapper;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    @SneakyThrows
    void getAll_successfulRequest_returnResponse() {
        // Arrange
        final var dummyString = "String";

        when(restRequestHandler.sendRequest(any(RestRequest.class))).thenReturn(dummyString);
        when(objectMapper.readValue(anyString(), eq(RouteMultiOutput.class)))
            .thenReturn(getTestMultiOutput());

        // Act
        final var result = (TestMultiOutput) operator.getAll();

        // Assert
        assertThat(result.testField).isEqualTo("testContent");
    }

    @Test
    @SneakyThrows
    void getAll_unsuccessfulRequest_throwApiInteractionUnsuccessfulException() {
        // Arrange
        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenThrow(new ApiInteractionUnsuccessfulException("error", new NotFoundException()));

        // Act & Assert
        assertThatThrownBy(() -> operator.getAll())
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());
    }

    @Test
    @SneakyThrows
    void getOne_successfulRequest_returnResponse() {
        // Arrange
        final var uuid = UUID.randomUUID();

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenReturn(getRouteSingleOutputExample());
        when(objectMapper.readValue(anyString(), any(Class.class)))
            .thenReturn(getSingleOutput());

        // Act
        final var result = (TestOutput) operator.getOne(uuid);

        // Assert
        assertThat(result.testField).isEqualTo("testContent");
    }

    @Test
    @SneakyThrows
    void getOne_unsuccessfulRequest_throwApiInteractionUnsuccessfulException() {
        // Arrange
        final var uuid = UUID.randomUUID();

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenThrow(new ApiInteractionUnsuccessfulException("error", new NotFoundException()));

        // Act & Assert
        assertThatThrownBy(() -> operator.getOne(uuid))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());
    }

    @Test
    @SneakyThrows
    void create_successfulRequest_returnResponse() {
        // Arrange
        final var input = getSampleInput();

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenReturn(getRouteSingleOutputExample());
        when(objectMapper.writeValueAsString(input))
            .thenReturn("{\"dummy\":\"JSON\"}");
        when(objectMapper.readValue(anyString(), any(Class.class))).thenReturn(getSingleOutput());

        // Act
        final var result = (TestOutput) operator.create(input);

        // Assert
        assertThat(result.testField).isEqualTo("testContent");
    }

    @Test
    @SneakyThrows
    void create_localRouteSuccessfulRequest_returnResponse() {
        // Arrange
        final var input = new RouteInput();
        input.setAdditional(Map.of("key", "value"));

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenReturn(getRouteSingleOutputExample());
        when(objectMapper.writeValueAsString(input))
            .thenReturn("{\"dummy\":\"JSON\"}");
        when(objectMapper.readValue(anyString(), any(Class.class))).thenReturn(getSingleOutput());

        // Act
        final var result = (TestOutput) operator.create(input);

        // Assert
        assertThat(result.testField).isEqualTo("testContent");
    }

    @Test
    @SneakyThrows
    void create_remoteRouteSuccessfulRequest_returnResponse() {
        // Arrange
        final var input = new RouteInput();
        input.setAdditional(Map.of("key", "value"));

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenReturn(getRouteSingleOutputExample());
        when(objectMapper.writeValueAsString(input))
            .thenReturn("{\"dummy\":\"JSON\"}");
        when(objectMapper.readValue(anyString(), any(Class.class))).thenReturn(getSingleOutput());

        // Act
        final var result = (TestOutput) operator.create(input);

        // Assert
        assertThat(result.testField).isEqualTo("testContent");
    }

    @Test
    @SneakyThrows
    void create_unsuccessfulRequest_throwApiInteractionUnsuccessfulException() {
        // Arrange
        final var input = getSampleInput();

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenThrow(new ApiInteractionUnsuccessfulException("error", new NotFoundException()));
        when(objectMapper.writeValueAsString(input))
            .thenReturn("{\"dummy\":\"JSON\"}");

        // Act & Assert
        assertThatThrownBy(() -> operator.create(input))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());
    }

    @Test
    @SneakyThrows
    void update_successfulRequest_throwNoException() {
        // Arrange
        final var input = getSampleInput();
        final var uuid = UUID.randomUUID();

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenReturn("Dummy String");
        when(objectMapper.writeValueAsString(input))
            .thenReturn("{\"dummy\":\"JSON\"}");

        // Act & Assert
        assertDoesNotThrow(() -> operator.update(uuid, input));
    }

    @Test
    @SneakyThrows
    void update_unsuccessfulRequest_throwApiInteractionUnsuccessfulException() {
        final var input = getSampleInput();
        final var uuid = UUID.randomUUID();

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenThrow(new ApiInteractionUnsuccessfulException("error", new NotFoundException()));
        when(objectMapper.writeValueAsString(input)).thenReturn("{\"dummy\":\"JSON\"}");

        // Act & Assert
        assertThatThrownBy(() -> operator.update(uuid, input))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());
    }

    @Test
    @SneakyThrows
    void delete_successfulRequest_throwNoException() {
        // Arrange
        final var uuid = UUID.randomUUID();

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenReturn("Dummy String");

        // Act & Assert
        assertDoesNotThrow(() -> operator.delete(uuid));
    }

    @Test
    @SneakyThrows
    void delete_unsuccessfulRequest_throwApiInteractionUnsuccessfulException() {
        // Arrange
        final var uuid = UUID.randomUUID();

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenThrow(new ApiInteractionUnsuccessfulException("error", new NotFoundException()));

        // Act & Assert
        assertThatThrownBy(() -> operator.delete(uuid))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());
    }

    @Test
    @SneakyThrows
    void getOutput_successfulRequestWithPaging_returnResponse() {
        // Arrange
        final var uuid = UUID.randomUUID();
        final var artifactSingleOutput = getArtifact();

        when(restRequestHandler.sendRequest(any(RestRequest.class))).thenReturn("Dummy String");
        when(objectMapper.readValue(anyString(), any(Class.class))).thenReturn(artifactSingleOutput);

        // Act
        final var result = operator.getOutput(uuid);

        // Assert
        assertThat(result.getLinks().getSelf().getHref()).isEqualTo("https://self-link");
    }

    @Test
    @SneakyThrows
    void getOutput_unsuccessfulRequest_throwApiInteractionUnsuccessfulException() {
        // Arrange
        final var uuid = UUID.randomUUID();

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenThrow(new ApiInteractionUnsuccessfulException("error", new NotFoundException()));

        // Act & Assert
        assertThatThrownBy(() -> operator.getOutput(uuid))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());
    }

    /**********************************************************************************************
     Utilities
     **********************************************************************************************/

    private EntityInput<Route> getSampleInput() {
        final var input = new RouteInput();
        input.setTitle("RouteInputTestTitle");
        input.setAdditional(Map.of("key", "value"));
        return input;
    }

    private ArtifactOutput getArtifact() {
        var link = new Link();
        ReflectionTestUtils.setField(link, "href", "https://self-link");
        var links = new ArtifactLinks();
        ReflectionTestUtils.setField(links, "self", link);

        var artifact = new ArtifactOutput();
        ReflectionTestUtils.setField(artifact, "title", "title");
        ReflectionTestUtils.setField(artifact, "description", "description");
        ReflectionTestUtils.setField(artifact, "remoteId", URI.create("https://remote-id"));
        ReflectionTestUtils.setField(artifact, "links", links);
        return artifact;
    }

    private String getRouteSingleOutputExample() {
        return "{\n" +
            "        \"creationDate\": \"2022-01-19T10:20:04.072+0100\",\n" +
            "        \"modificationDate\": \"2022-01-19T10:20:04.072+0100\",\n" +
            "        \"title\": \"string\",\n" +
            "        \"description\": \"string\",\n" +
            "        \"configuration\": \"string\",\n" +
            "        \"deploy\": \"None\",\n" +
            "        \"start\": null,\n" +
            "        \"end\": null,\n" +
            "        \"additional\": {},\n" +
            "        \"_links\": {\n" +
            "          \"self\": {\n" +
            "            \"href\": \"http://localhost:8080/api/routes/2fd8ee17-615b-429d-9257" +
            "-ec5330634162\"\n" +
            "          },\n" +
            "          \"routes\": {\n" +
            "            \"href\": \"http://localhost:8080/api/routes/2fd8ee17-615b-429d-9257" +
            "-ec5330634162/steps{?page,size}\",\n" +
            "            \"templated\": true\n" +
            "          },\n" +
            "          \"artifacts\": {\n" +
            "            \"href\": \"http://localhost:8080/api/routes/2fd8ee17-615b-429d-9257" +
            "-ec5330634162/outputs{?page,size}\",\n" +
            "            \"templated\": true\n" +
            "          }\n" +
            "        }\n" +
            "      }";
    }

    private TestMultiOutput getTestMultiOutput() {
        final var output = new TestMultiOutput();
        output.testField = "testContent";
        return output;
    }

    private TestOutput getSingleOutput() {
        final var output = new TestOutput();
        output.testField = "testContent";
        return output;
    }

    static class TestMultiOutput extends RouteMultiOutput {
        public String testField;
    }

    static class TestOutput extends RouteOutput {
        public String testField;
    }
}
