/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.base;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.CatalogOfferedResourcesApi;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.CatalogMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.CatalogOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * CatalogApiOperatorTest
 *
 * @author Florian Zimmer
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {CatalogApiOperator.class})
class CatalogApiOperatorTest {

    /** Class under test */
    @Autowired
    private CatalogApiOperator operator;

    @MockBean
    private CatalogOfferedResourcesApi offers;

    @MockBean
    private ObjectMapper objectMapper;

    @MockBean
    private RestRequestHandler requestHandler;

    @Test
    void getEntitySingleOutputClass_returnClass() {
        // Act
        final var result = operator.getEntitySingleOutputClass();

        // Assert
        assertThat(result).isEqualTo(CatalogOutput.class);
    }

    @Test
    void getEntityMultiOutputClass_returnClass() {
        // Act
        final var result = operator.getEntityMultiOutputClass();

        // Assert
        assertThat(result).isEqualTo(CatalogMultiOutput.class);
    }
}
