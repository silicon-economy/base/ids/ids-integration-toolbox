/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.base;

import java.util.Map;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.EndpointDataSourcesApi;
import org.siliconeconomy.idsintegrationtoolbox.model.RestRequest;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Endpoint;
import org.siliconeconomy.idsintegrationtoolbox.model.input.EntityInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.GenericEndpointInput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.EndpointMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.EndpointOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.client.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

/**
 * EndpointApiOperatorTest
 *
 * @author Florian Zimmer
 */
@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest(classes = EndpointApiOperator.class)
class EndpointApiOperatorTest {

    /** Class under test */
    @Autowired
    private EndpointApiOperator operator;

    @MockBean
    private EndpointDataSourcesApi dataSources;

    @MockBean
    private RestRequestHandler restRequestHandler;

    @MockBean
    private ObjectMapper objectMapper;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    @SneakyThrows
    void getAll_successfulRequest_returnResponse() {
        // Arrange
        final var dummyString = "String";

        when(restRequestHandler.sendRequest(any(RestRequest.class))).thenReturn(dummyString);
        when(objectMapper.readValue(anyString(), eq(EndpointMultiOutput.class)))
            .thenReturn(getTestMultiOutput());

        // Act
        final var result = (TestMultiOutput) operator.getAll();

        // Assert
        assertThat(result.testField).isEqualTo("testContent");
    }

    @Test
    @SneakyThrows
    void getAll_unsuccessfulRequest_throwApiInteractionUnsuccessfulException() {
        // Arrange
        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenThrow(new ApiInteractionUnsuccessfulException("error", new NotFoundException()));

        // Act & Assert
        assertThatThrownBy(() -> operator.getAll())
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());
    }

    @Test
    @SneakyThrows
    void getOne_successfulRequest_returnResponse() {
        // Arrange
        final var uuid = UUID.randomUUID();

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenReturn(getEndpointSingleOutputExample());
        when(objectMapper.readValue(anyString(), any(Class.class)))
            .thenReturn(getSingleOutput());

        // Act
        final var result = (TestOutput) operator.getOne(uuid);

        // Assert
        assertThat(result.testField).isEqualTo("testContent");
    }

    @Test
    @SneakyThrows
    void getOne_unsuccessfulRequest_throwApiInteractionUnsuccessfulException() {
        // Arrange
        final var uuid = UUID.randomUUID();

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenThrow(new ApiInteractionUnsuccessfulException("error", new NotFoundException()));

        // Act & Assert
        assertThatThrownBy(() -> operator.getOne(uuid))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());
    }

    @Test
    @SneakyThrows
    void create_successfulRequest_returnResponse() {
        // Arrange
        final var input = getSampleInput();

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenReturn(getEndpointSingleOutputExample());
        when(objectMapper.writeValueAsString(input))
            .thenReturn("{\"dummy\":\"JSON\"}");
        when(objectMapper.readValue(anyString(), any(Class.class))).thenReturn(getSingleOutput());

        // Act
        final var result = (TestOutput) operator.create(input);

        // Assert
        assertThat(result.testField).isEqualTo("testContent");
    }

    @Test
    @SneakyThrows
    void create_localEndpointSuccessfulRequest_returnResponse() {
        // Arrange
        final var input = getSampleInput();

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenReturn(getEndpointSingleOutputExample());
        when(objectMapper.writeValueAsString(input))
            .thenReturn("{\"dummy\":\"JSON\"}");
        when(objectMapper.readValue(anyString(), any(Class.class))).thenReturn(getSingleOutput());

        // Act
        final var result = (TestOutput) operator.create(input);

        // Assert
        assertThat(result.testField).isEqualTo("testContent");
    }

    @Test
    @SneakyThrows
    void create_remoteEndpointSuccessfulRequest_returnResponse() {
        // Arrange
        final var input = new GenericEndpointInput();
        input.setAdditional(Map.of("key", "value"));

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenReturn(getEndpointSingleOutputExample());
        when(objectMapper.writeValueAsString(input))
            .thenReturn("{\"dummy\":\"JSON\"}");
        when(objectMapper.readValue(anyString(), any(Class.class))).thenReturn(getSingleOutput());

        // Act
        final var result = (TestOutput) operator.create(input);

        // Assert
        assertThat(result.testField).isEqualTo("testContent");
    }

    @Test
    @SneakyThrows
    void create_unsuccessfulRequest_throwApiInteractionUnsuccessfulException() {
        // Arrange
        final var input = getSampleInput();

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenThrow(new ApiInteractionUnsuccessfulException("error", new NotFoundException()));
        when(objectMapper.writeValueAsString(input))
            .thenReturn("{\"dummy\":\"JSON\"}");

        // Act & Assert
        assertThatThrownBy(() -> operator.create(input))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());
    }

    @Test
    @SneakyThrows
    void update_successfulRequest_throwNoException() {
        // Arrange
        final var input = getSampleInput();
        final var uuid = UUID.randomUUID();

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenReturn("Dummy String");
        when(objectMapper.writeValueAsString(input))
            .thenReturn("{\"dummy\":\"JSON\"}");

        // Act & Assert
        assertDoesNotThrow(() -> operator.update(uuid, input));
    }

    @Test
    @SneakyThrows
    void update_unsuccessfulRequest_throwApiInteractionUnsuccessfulException() {
        final var input = getSampleInput();
        final var uuid = UUID.randomUUID();

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenThrow(new ApiInteractionUnsuccessfulException("error", new NotFoundException()));
        when(objectMapper.writeValueAsString(input)).thenReturn("{\"dummy\":\"JSON\"}");

        // Act & Assert
        assertThatThrownBy(() -> operator.update(uuid, input))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());
    }

    @Test
    @SneakyThrows
    void delete_successfulRequest_throwNoException() {
        // Arrange
        final var uuid = UUID.randomUUID();

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenReturn("Dummy String");

        // Act & Assert
        assertDoesNotThrow(() -> operator.delete(uuid));
    }

    @Test
    @SneakyThrows
    void delete_unsuccessfulRequest_throwApiInteractionUnsuccessfulException() {
        // Arrange
        final var uuid = UUID.randomUUID();

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenThrow(new ApiInteractionUnsuccessfulException("error", new NotFoundException()));

        // Act & Assert
        assertThatThrownBy(() -> operator.delete(uuid))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());
    }

    /**********************************************************************************************
        Utilities
     **********************************************************************************************/

    private EntityInput<Endpoint> getSampleInput() {
        final var input = new GenericEndpointInput();
        input.setAdditional(Map.of("key", "value"));
        return input;
    }

    private String getEndpointSingleOutputExample() {
        return "{\n" +
                "        \"creationDate\": \"2021-06-14T11:30:33.280+0200\",\n" +
                "        \"modificationDate\": \"2021-06-14T11:30:33.280+0200\",\n" +
                "        \"remoteId\": \"genesis\",\n" +
                "        \"title\": \"ArtifactTestTitle\",\n" +
                "        \"numAccessed\": 0,\n" +
                "        \"byteSize\": 32,\n" +
                "        \"checkSum\": 3110206735,\n" +
                "        \"additional\": {},\n" +
                "        \"_links\": {\n" +
                "          \"self\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/4e9c5c7b-2808-470d" +
                "-b2d5-9a8f3ea04d6a\"\n" +
                "          },\n" +
                "          \"data\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/4e9c5c7b-2808-470d" +
                "-b2d5-9a8f3ea04d6a/data\"\n" +
                "          },\n" +
                "          \"representations\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/4e9c5c7b-2808-470d" +
                "-b2d5-9a8f3ea04d6a/representations{?page,size}\",\n" +
                "            \"templated\": true\n" +
                "          },\n" +
                "          \"agreements\": {\n" +
                "            \"href\": \"http://localhost:8080/api/artifacts/4e9c5c7b-2808-470d" +
                "-b2d5-9a8f3ea04d6a/agreements{?page,size}\",\n" +
                "            \"templated\": true\n" +
                "          }\n" +
                "        }\n" +
                "      }";
    }

    private TestMultiOutput getTestMultiOutput() {
        final var output = new TestMultiOutput();
        output.testField = "testContent";
        return output;
    }

    private TestOutput getSingleOutput() {
        final var output = new TestOutput();
        output.testField = "testContent";
        return output;
    }

    static class TestMultiOutput extends EndpointMultiOutput {
        public String testField;
    }

    static class TestOutput extends EndpointOutput {
        public String testField;

        public String getType() {
            return "TEST";
        }
    }
}
