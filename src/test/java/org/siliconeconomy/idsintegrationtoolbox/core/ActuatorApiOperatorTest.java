/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core;

import lombok.SneakyThrows;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.siliconeconomy.idsintegrationtoolbox.config.IntegrationConfig;
import org.siliconeconomy.idsintegrationtoolbox.model.RestRequest;
import org.siliconeconomy.idsintegrationtoolbox.model.output.ActuatorInfoOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * ActuatorApiOperatorTest
 *
 * @author Florian Zimmer
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {ActuatorApiOperator.class, IntegrationConfig.class})
class ActuatorApiOperatorTest {

    /** Class under test */
    @Autowired
    private ActuatorApiOperator operator;

    @MockBean
    private RestRequestHandler requestHandler;

    @Test
    @SneakyThrows
    void getInfo_successfulRequestWithDatObject_TryToGetDatObject() {
        // Arrange
        when(requestHandler.sendRequest(any(RestRequest.class))).thenReturn(getInfoResponseWithDatObject());

        // Act
        final var result = operator.getInfo();

        // Assert
        Assertions.assertThat(result).isInstanceOf(ActuatorInfoOutput.class);
        Assertions.assertThat(result.getIds().getDatObject()).isInstanceOf(ActuatorInfoOutput.Dat.class);
    }

    @Test
    @SneakyThrows
    void getInfo_successfulRequestWithDatObject_TryToGetDatBool() {
        // Arrange
        when(requestHandler.sendRequest(any(RestRequest.class))).thenReturn(getInfoResponseWithDatObject());

        // Act
        final var result = operator.getInfo().getIds();

        // Assert
        Assertions.assertThatThrownBy(result::getDatBool).isInstanceOf(NullPointerException.class);
    }

    @Test
    @SneakyThrows
    void getInfo_successfulRequestWithDatBool_TryToGetDatBool() {
        // Arrange
        when(requestHandler.sendRequest(any(RestRequest.class))).thenReturn(getInfoResponseWithDatBool());

        // Act
        final var result = operator.getInfo();

        // Assert
        Assertions.assertThat(result).isInstanceOf(ActuatorInfoOutput.class);
        Assertions.assertThat(result.getIds().getDatBool()).isInstanceOf(Boolean.class);
    }

    @Test
    @SneakyThrows
    void getInfo_successfulRequestWithDatBool_TryToGetDatObject() {
        // Arrange
        when(requestHandler.sendRequest(any(RestRequest.class))).thenReturn(getInfoResponseWithDatBool());

        // Act
        final var result = operator.getInfo();

        // Assert
        Assertions.assertThat(result).isInstanceOf(ActuatorInfoOutput.class);
        Assertions.assertThat(result.getIds().getDatObject()).isNull();
    }

    private String getInfoResponseWithDatObject() {
        return "{\"title\":\"Dataspace Connector\",\"description\":\"IDS Connector originally " +
            "developed by the Fraunhofer ISST\",\"version\":\"6.5.0\"," +
            "\"contact\":{\"organization\":\"Fraunhofer Institute for Software and Systems " +
            "Engineering\",\"website\":\"https://www.dataspace-connector.io/\"," +
            "\"email\":\"info@dataspace-connector.de\"},\"license\":{\"name\":\"Apache License, " +
            "Version 2.0\",\"location\":\"https://www.apache.org/licenses/LICENSE-2.0.txt\"}," +
            "\"camel\":{\"name\":\"camel-1\",\"version\":\"3.12.0\"," +
            "\"startDate\":\"2021-11-24T11:09:46.147+00:00\",\"uptime\":\"41s323ms\"," +
            "\"status\":\"Started\"},\"configuration\":{\"deployMode\":\"Test\"," +
            "\"connectorStatus\":\"Online\"},\"ids\":{\"infoModel\":{\"inboundVersion\":[\"4.2" +
            ".6\",\"4.2.0\",\"4.2.1\",\"4.1.2\",\"4.0.0\",\"4.1.0\",\"4.2.4\",\"4.2.5\",\"4.2" +
            ".2\",\"4.2.3\"],\"outboundVersion\":\"4.2.6\"}," +
            "\"dat\":{\"audience\":\"idsc:IDS_CONNECTORS_ALL\"," +
            "\"securityProfile\":\"idsc:BASE_SECURITY_PROFILE\"," +
            "\"issuedAt\":\"2021-11-24T11:10:27.000+00:00\"," +
            "\"referringConnector\":\"http://plugfest2021.08.localhost.demo\"," +
            "\"issuer\":\"https://daps.aisec.fraunhofer.de\"," +
            "\"expirationDate\":\"2021-11-24T12:10:27.000+00:00\"}," +
            "\"certificate\":{\"issuedAt\":\"2021-08-16T11:51:57.000+00:00\",\"type\":\"X.509\"," +
            "\"version\":3,\"issuer\":\"CN=IDS Test SubCA 2018, O=Fraunhofer, C=DE\"," +
            "\"expirationDate\":\"2023-08-16T11:51:57.000+00:00\"," +
            "\"sigAlgName\":\"SHA256withRSA\"}},\"update\":{\"available\":true," +
            "\"location\":\"https://github" +
            ".com/International-Data-Spaces-Association/DataspaceConnector/releases/tag/v6.5.1\"," +
            "\"type\":\"Patch\",\"version\":\"6.5.1\"}}";
    }
    private String getInfoResponseWithDatBool(){
        return "{\n" +
            "\"title\": \"Dataspace Connector\",\n" +
            "\"description\": \"IDS Connector originally developed by the Fraunhofer ISST\",\n" +
            "\"version\": \"6.5.1\",\n" +
            "\"contact\": {\n" +
            "\"organization\": \"Fraunhofer Institute for Software and Systems Engineering\",\n" +
            "\"website\": \"https://www.dataspace-connector.io/\",\n" +
            "\"email\": \"info@dataspace-connector.de\"\n" +
            "},\n" +
            "\"license\": {\n" +
            "\"name\": \"Apache License, Version 2.0\",\n" +
            "\"location\": \"https://www.apache.org/licenses/LICENSE-2.0.txt\"\n" +
            "},\n" +
            "\"camel\": {\n" +
            "\"name\": \"camel-1\",\n" +
            "\"version\": \"3.12.0\",\n" +
            "\"startDate\": \"2021-11-24T09:01:16.422+00:00\",\n" +
            "\"uptime\": \"8m37s\",\n" +
            "\"status\": \"Started\"\n" +
            "},\n" +
            "\"configuration\": {\n" +
            "\"deployMode\": \"Test\",\n" +
            "\"connectorStatus\": \"Online\"\n" +
            "},\n" +
            "\"ids\": {\n" +
            "\"infoModel\": {\n" +
            "\"inboundVersion\": [\n" +
            "\"4.2.6\",\n" +
            "\"4.2.0\",\n" +
            "\"4.2.1\",\n" +
            "\"4.1.2\",\n" +
            "\"4.0.0\",\n" +
            "\"4.1.0\",\n" +
            "\"4.2.4\",\n" +
            "\"4.2.5\",\n" +
            "\"4.2.2\",\n" +
            "\"4.2.3\"\n" +
            "],\n" +
            "\"outboundVersion\": \"4.2.6\"\n" +
            "},\n" +
            "\"dat\": false,\n" +
            "\"certificate\": {\n" +
            "\"issuedAt\": \"2019-11-20T10:44:12.000+00:00\",\n" +
            "\"type\": \"X.509\",\n" +
            "\"version\": 3,\n" +
            "\"issuer\": \"CN=localhost, OU=Unknown, O=Unknown, L=Unknown, ST=Unknown, " +
            "C=Unknown\",\n" +
            "\"expirationDate\": \"2029-11-17T10:44:12.000+00:00\",\n" +
            "\"sigAlgName\": \"SHA256withRSA\"\n" +
            "}\n" +
            "},\n" +
            "\"update\": {\n" +
            "\"available\": false\n" +
            "}\n" +
            "}";
    }
}
