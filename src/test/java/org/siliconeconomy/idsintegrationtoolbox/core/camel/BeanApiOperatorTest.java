/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.camel;

import java.util.UUID;

import lombok.SneakyThrows;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.siliconeconomy.idsintegrationtoolbox.utils.FileUtils;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.InvalidInputException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {BeanApiOperator.class})
class BeanApiOperatorTest {

    @Mock
    private FileSystemResource fileSystemResource;

    @MockBean
    private RestTemplate restTemplate;

    @Autowired
    private BeanApiOperator beanApiOperator;

    private final MockedStatic<FileUtils> fileUtilsMock = Mockito.mockStatic(FileUtils.class);

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @AfterEach
    public void closeMock() {
        fileUtilsMock.close();
    }

    @Test
    @SneakyThrows
    void upload_fileOkResponseOK_shouldReturnOK() {
        // Arrange
        final var response = new ResponseEntity<>("Successful", HttpStatus.OK);

        when(restTemplate.exchange(any(), any(), any(), eq(String.class)))
            .thenReturn(response);
        fileUtilsMock.when(() -> FileUtils.readFile(Mockito.any())).thenReturn(fileSystemResource);

        // Act & Assert
        assertDoesNotThrow(() -> beanApiOperator.upload("test-path.xml"));
    }

    @Test
    @SneakyThrows
    void upload_fileOkResponseBadRequest_shouldThrowException() {
        // Arrange
        final var response = new ResponseEntity<>("Not Successful", HttpStatus.BAD_REQUEST);

        when(restTemplate.exchange(any(), any(), any(), eq(String.class)))
            .thenReturn(response);
        fileUtilsMock.when(() -> FileUtils.readFile(Mockito.any())).thenReturn(fileSystemResource);

        // Act & Assert
        assertThrows(ApiInteractionUnsuccessfulException.class,
            () -> beanApiOperator.upload("test-path.xml"));
    }

    @Test
    @SneakyThrows
    void upload_fileInvalid_shouldThrowException() {
        // Arrange
        fileUtilsMock.when(() -> FileUtils.readFile(Mockito.any())).thenReturn(fileSystemResource);

        // Act & Assert
        assertThrows(InvalidInputException.class,
            () -> beanApiOperator.upload("test-path.txt"));
    }


    @Test
    @SneakyThrows
    void delete_idOkResponseOK_shouldReturn() {
        // Arrange
        final var uuid = UUID.randomUUID();
        final var response = new ResponseEntity<>("Successful", HttpStatus.OK);

        when(restTemplate.exchange(anyString(), any(), any(), eq(String.class)))
            .thenReturn(response);

        // Act & Assert
        assertDoesNotThrow(() -> beanApiOperator.delete(uuid.toString()));
    }

    @Test
    @SneakyThrows
    void delete_idOkResponseInternalServerError_shouldThrowException() {
        // Arrange
        final var uuid = UUID.randomUUID();
        final var responseEntity = new ResponseEntity<>("Not Successful",
            HttpStatus.INTERNAL_SERVER_ERROR);

        when(restTemplate.exchange(anyString(), any(), any(), eq(String.class)))
            .thenReturn(responseEntity);

        // Act & Assert
        assertThrows(ApiInteractionUnsuccessfulException.class,
            () -> beanApiOperator.delete(uuid.toString()));
    }

}
