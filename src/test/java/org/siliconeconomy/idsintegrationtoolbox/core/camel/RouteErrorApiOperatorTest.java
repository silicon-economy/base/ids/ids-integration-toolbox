/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.camel;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.idsintegrationtoolbox.config.IntegrationConfig;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.RouteError;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.client.UnauthorizedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {RouteErrorApiOperator.class, IntegrationConfig.class})
class RouteErrorApiOperatorTest {

    @MockBean
    private RestTemplate restTemplate;

    @Autowired
    private RouteErrorApiOperator operator;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    @SneakyThrows
    void getRouteErrors_statusOK_returnRouteErrors() {
        // Arrange
        final var routeError = new RouteError("routeId", "https://backend:8080", "Unknown host.",
            "2021-11-08T11:33:44.995+02:00");
        final var routeErrorString = objectMapper.writeValueAsString(routeError);
        final var response = new ResponseEntity<>("[" + routeErrorString + "]", HttpStatus.OK);

        when(restTemplate.exchange(any(), any(), any(), eq(String.class))).thenReturn(response);

        // Act
        final var result = operator.getRouteErrors();

        // Assert
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(1, result.size());

        final var resultRouteError = result.get(0);
        assertEquals(routeError.getRouteId(), resultRouteError.getRouteId());
        assertEquals(routeError.getEndpoint(), resultRouteError.getEndpoint());
        assertEquals(routeError.getMessage(), resultRouteError.getMessage());
        assertEquals(routeError.getTimestamp(), resultRouteError.getTimestamp());
    }

    @Test
    void getRouteErrors_statusUnauthorized_throwApiInteractionUnsuccessfulException() {
        // Arrange
        final var response = new ResponseEntity<>("Error", HttpStatus.UNAUTHORIZED);

        when(restTemplate.exchange(any(), any(), any(), eq(String.class))).thenReturn(response);

        // Act & Assert
        assertThatThrownBy(() -> operator.getRouteErrors())
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCauseInstanceOf(UnauthorizedException.class);
    }

}
