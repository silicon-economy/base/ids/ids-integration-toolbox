/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import de.fraunhofer.iais.eis.Action;
import de.fraunhofer.iais.eis.PermissionBuilder;
import de.fraunhofer.iais.eis.Rule;
import de.fraunhofer.iais.eis.ids.jsonld.Serializer;
import de.fraunhofer.iais.eis.util.Util;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.PolicyPattern;
import org.siliconeconomy.idsintegrationtoolbox.model.input.policy.ProvideAccessPolicyInput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 * UsageControlApiOperatorTest
 *
 * @author Florian Zimmer
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {ExamplesApiOperator.class})
class ExamplesApiOperatorTest {

    /** Class under test */
    @Autowired
    private ExamplesApiOperator examplesApiOperator;
    @MockBean
    private RestRequestHandler restRequestHandler;
    @MockBean
    private Serializer serializer;

    @Test
    @SneakyThrows
    void getExamplePolicy_validInput_returnExamplePolicy() {
        //Arrange
        final var policyInput = new ProvideAccessPolicyInput.Builder().build();
        final var response = "response";
        final var rule = new PermissionBuilder()
            ._action_(Util.asList(Action.USE))
            .build();

        when(restRequestHandler.sendRequest(any())).thenReturn(response);
        when(serializer.deserialize(response, Rule.class)).thenReturn(rule);

        //Act
        final var result = examplesApiOperator.policy(policyInput);

        //Assert
        assertThat(result).isEqualTo(rule);
    }

    @Test
    @SneakyThrows
    void getExampleValidation_inputValid_returnPolicyPattern() {
        //Arrange
        final var validation = "validation";
        final var response = "{ \"value\" : \"PROVIDE_ACCESS\" }";

        when(restRequestHandler.sendRequest(any())).thenReturn(response);

        //Act
        final var result = examplesApiOperator.policyValidation(validation);

        //Assert
        assertThat(result).isEqualTo(PolicyPattern.PROVIDE_ACCESS);
    }
}
