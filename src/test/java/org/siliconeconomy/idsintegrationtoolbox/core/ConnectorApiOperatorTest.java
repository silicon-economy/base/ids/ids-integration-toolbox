/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.net.URI;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.fraunhofer.iais.eis.BaseConnectorBuilder;
import de.fraunhofer.iais.eis.Connector;
import de.fraunhofer.iais.eis.ConnectorEndpointBuilder;
import de.fraunhofer.iais.eis.SecurityProfile;
import de.fraunhofer.iais.eis.ids.jsonld.Serializer;
import de.fraunhofer.iais.eis.util.Util;
import lombok.SneakyThrows;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.siliconeconomy.idsintegrationtoolbox.model.RestRequest;
import org.siliconeconomy.idsintegrationtoolbox.model.output.StatusResponse;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;


/**
 * ConnectorApiOperatorTest
 *
 * @author Haydar Qarawlus
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {ConnectorApiOperator.class})
class ConnectorApiOperatorTest {

    private final StatusResponse statusResponse = new StatusResponse();
    /** Class under test */
    @Autowired
    private ConnectorApiOperator operator;

    @MockBean
    private RestRequestHandler requestHandler;

    @MockBean
    private Serializer serializer;

    @MockBean
    private ObjectMapper objectMapper;

    @Test
    @SneakyThrows
    void getPublicSelfDescription_successfulRequest_returnResponse() {
        // Arrange
        final var response = "response";
        final var connector = getConnector();

        when(requestHandler.sendRequest(any(RestRequest.class))).thenReturn(response);
        when(serializer.deserialize(response, Connector.class)).thenReturn(connector);

        // Act
        final var result = operator.getPublic();

        // Assert
        Assertions.assertThat(result).isEqualTo(connector);
    }


    @Test
    @SneakyThrows
    void getSelfDescription_successfulRequest_returnResponse() {
        // Arrange
        final var response = "response";
        final var connector = getConnector();

        when(requestHandler.sendRequest(any(RestRequest.class))).thenReturn(response);
        when(serializer.deserialize(response, Connector.class)).thenReturn(connector);

        // Act
        final var result = operator.getPrivate();

        // Assert
        Assertions.assertThat(result).isEqualTo(connector);
    }

    @Test
    @SneakyThrows
    void setNegotiationStatus_statusTrue_returnTrue() {
        //Arrange
        final var status = true;
        final var response = "response";

        statusResponse.setStatus(true);

        when(requestHandler.sendRequest(any())).thenReturn(response);
        when(objectMapper.readValue(response, StatusResponse.class)).thenReturn(statusResponse);

        //Act
        final var result = operator.negotiation(status);

        //Assert
        assertThat(result).isEqualTo(status);
    }

    @Test
    @SneakyThrows
    void setNegotiationStatus_statusFalse_returnFalse() {
        //Arrange
        final var status = false;
        final var response = "response";

        statusResponse.setStatus(false);

        when(requestHandler.sendRequest(any())).thenReturn(response);
        when(objectMapper.readValue(response, StatusResponse.class)).thenReturn(statusResponse);

        //Act
        final var result = operator.negotiation(status);

        //Assert
        assertThat(result).isEqualTo(status);
    }

    @Test
    @SneakyThrows
    void getNegotiationStatus_statusTrue_returnTrue() {
        //Arrange
        final var status = true;
        final var response = "response";

        statusResponse.setStatus(status);

        when(requestHandler.sendRequest(any())).thenReturn(response);
        when(objectMapper.readValue(response, StatusResponse.class)).thenReturn(statusResponse);

        //Act
        final var result = operator.negotiation();

        //Assert
        assertThat(result).isEqualTo(status);
    }

    @Test
    @SneakyThrows
    void setUnsupportedPatterns_statusTrue_returnTrue() {
        //Arrange
        final var status = true;
        final var response = "returnString";

        statusResponse.setStatus(status);

        when(requestHandler.sendRequest(any())).thenReturn(response);
        when(objectMapper.readValue(response, StatusResponse.class)).thenReturn(statusResponse);

        //Act
        final var result = operator.unsupportedPatterns(status);

        //Assert
        assertThat(result).isEqualTo(status);
    }

    @Test
    @SneakyThrows
    void setUnsupportedPatterns_statusFalse_returnFalse() {
        //Arrange
        final var status = false;
        final var response = "response";

        statusResponse.setStatus(status);

        when(requestHandler.sendRequest(any())).thenReturn(response);
        when(objectMapper.readValue(response, StatusResponse.class)).thenReturn(statusResponse);

        //Act
        final var result = operator.unsupportedPatterns(status);

        //Assert
        assertThat(result).isEqualTo(status);
    }

    @Test
    @SneakyThrows
    void getUnsupportedPatterns_statusTrue_returnTrue() {
        //Arrange
        final var status = true;
        final var response = "response";

        statusResponse.setStatus(status);

        when(requestHandler.sendRequest(any())).thenReturn(response);
        when(objectMapper.readValue(response, StatusResponse.class)).thenReturn(statusResponse);

        //Act
        final var result = operator.unsupportedPatterns();

        //Assert
        assertThat(result).isEqualTo(status);
    }

    /**********************************************************************************************
        Utilities
     **********************************************************************************************/

    private Connector getConnector() {
        return new BaseConnectorBuilder()
            ._securityProfile_(SecurityProfile.BASE_SECURITY_PROFILE)
            ._maintainer_(URI.create("https://some-url.com"))
            ._curator_(URI.create("https://some-url.com"))
            ._hasDefaultEndpoint_(new ConnectorEndpointBuilder()
                ._accessURL_(URI.create("https://some-url.com"))
                .build())
            ._inboundModelVersion_(Util.asList("4.0.0"))
            ._outboundModelVersion_("4.0.0")
            .build();
    }

}
