/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core;

import java.util.Base64;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.test.util.ReflectionTestUtils;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * DscApiOperatorTest
 *
 * @author Ronja Quensel
 */
@SpringBootTest(classes = DscApiOperatorTest.TestDscApiOperator.class)
class DscApiOperatorTest {

    private final String username = "username";
    private final String password = "password";

    @Autowired
    private TestDscApiOperator operator;

    @Test
    void addAuthHeader_authPresent_addHeader() {
        // Arrange
        ReflectionTestUtils.setField(operator, "connectorUsername", username);
        ReflectionTestUtils.setField(operator, "connectorPassword", password);

        final var headers = new HttpHeaders();

        // Act
        operator.addAuth(headers);

        // Assert
        final var authHeader = headers.getFirst(HttpHeaders.AUTHORIZATION);
        assertThat(authHeader)
            .isNotNull()
            .contains("Basic");
        final var authInfo = authHeader.substring(6);
        assertThat(authInfo).isBase64();
        assertThat(new String(Base64.getDecoder().decode(authInfo)))
            .contains(username)
            .contains(password);
    }

    @Test
    void addAuthHeader_usernameNull_addHeader() {
        // Arrange
        ReflectionTestUtils.setField(operator, "connectorUsername", null);
        ReflectionTestUtils.setField(operator, "connectorPassword", password);

        final var headers = new HttpHeaders();

        // Act
        operator.addAuth(headers);

        // Assert
        final var authHeader = headers.getFirst(HttpHeaders.AUTHORIZATION);
        assertThat(authHeader).isNull();
    }

    @Test
    void addAuthHeader_passwordNull_addHeader() {
        // Arrange
        ReflectionTestUtils.setField(operator, "connectorUsername", username);
        ReflectionTestUtils.setField(operator, "connectorPassword", null);

        final var headers = new HttpHeaders();

        // Act
        operator.addAuth(headers);

        // Assert
        final var authHeader = headers.getFirst(HttpHeaders.AUTHORIZATION);
        assertThat(authHeader).isNull();
    }

    @Test
    void addAuthHeader_noAuthPresent_addHeader() {
        // Arrange
        ReflectionTestUtils.setField(operator, "connectorUsername", null);
        ReflectionTestUtils.setField(operator, "connectorPassword", null);

        final var headers = new HttpHeaders();

        // Act
        operator.addAuth(headers);

        // Assert
        final var authHeader = headers.getFirst(HttpHeaders.AUTHORIZATION);
        assertThat(authHeader).isNull();
    }

    @Component
    static class TestDscApiOperator extends DscApiOperator {

        public void addAuth(final HttpHeaders headers) {
            addAuthHeader(headers);
        }

    }

}
