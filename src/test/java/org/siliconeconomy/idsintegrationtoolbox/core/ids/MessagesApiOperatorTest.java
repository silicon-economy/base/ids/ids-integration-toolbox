/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.ids;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.fraunhofer.iais.eis.AbstractConstraint;
import de.fraunhofer.iais.eis.Action;
import de.fraunhofer.iais.eis.AppResource;
import de.fraunhofer.iais.eis.AppResourceBuilder;
import de.fraunhofer.iais.eis.Artifact;
import de.fraunhofer.iais.eis.ArtifactBuilder;
import de.fraunhofer.iais.eis.ComponentCertification;
import de.fraunhofer.iais.eis.ContractOffer;
import de.fraunhofer.iais.eis.ContractOfferBuilder;
import de.fraunhofer.iais.eis.Duty;
import de.fraunhofer.iais.eis.DutyBuilder;
import de.fraunhofer.iais.eis.InfrastructureComponent;
import de.fraunhofer.iais.eis.Location;
import de.fraunhofer.iais.eis.Participant;
import de.fraunhofer.iais.eis.Permission;
import de.fraunhofer.iais.eis.PermissionBuilder;
import de.fraunhofer.iais.eis.Prohibition;
import de.fraunhofer.iais.eis.ProhibitionBuilder;
import de.fraunhofer.iais.eis.PublicKey;
import de.fraunhofer.iais.eis.Representation;
import de.fraunhofer.iais.eis.RepresentationBuilder;
import de.fraunhofer.iais.eis.Resource;
import de.fraunhofer.iais.eis.ResourceBuilder;
import de.fraunhofer.iais.eis.ResourceCatalog;
import de.fraunhofer.iais.eis.ResourceCatalogBuilder;
import de.fraunhofer.iais.eis.Rule;
import de.fraunhofer.iais.eis.ids.jsonld.Serializer;
import de.fraunhofer.iais.eis.util.TypedLiteral;
import de.fraunhofer.iais.eis.util.UriOrModelClass;
import de.fraunhofer.iais.eis.util.Util;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.siliconeconomy.idsintegrationtoolbox.model.RestRequest;
import org.siliconeconomy.idsintegrationtoolbox.model.input.SubscriptionInput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.AgreementOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.InvalidInputException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.NoParsableResponseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

/**
 * MessagesApiOperatorTest
 *
 * @author Florian Zimmer
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {MessagesApiOperator.class})
class MessagesApiOperatorTest {

    /** Class under test */
    @Autowired
    private MessagesApiOperator operator;

    @MockBean
    private Serializer serializer;

    @MockBean
    private ObjectMapper objectMapper;

    @MockBean
    private RestRequestHandler requestHandler;

    @Captor
    private ArgumentCaptor<RestRequest> requestCaptor;

    private final URI recipient = URI.create("https://recipient");

    @Test
    @SneakyThrows
    void sendConnectorUnavailableMessage_successfulRequest_returnResponse() {
        // Arrange
        final var recipient = URI.create("recipientUri");
        final var response = "response";

        when(requestHandler.sendRequest(any())).thenReturn(response);

        // Act & Assert
        assertDoesNotThrow(() -> operator.connectorUnavailable(recipient));
    }

    @Test
    @SneakyThrows
    void sendConnectorUpdateMessage_successfulRequest_returnResponse() {
        // Arrange
        final var recipient = URI.create("recipientUri");
        final var response = "response";

        when(requestHandler.sendRequest(any())).thenReturn(response);

        // Act & Assert
        assertDoesNotThrow(() -> operator.connectorUpdate(recipient));
    }

    @Test
    @SneakyThrows
    void negotiateContract_permissionRule_returnAgreementOutput() {
        // Arrange
        final var recipient = URI.create("recipientUri");
        final var resourceIds = List.of(URI.create("resourceId"));
        final var artifactIds = List.of(URI.create("artifactId"));
        final var download = true;
        final List<Rule> rules = List.of(new TestPermissionRule(), new TestPermissionRule());
        final var jsonString = "{\"@type\":\"value\"}";

        when(requestHandler.sendRequest(requestCaptor.capture())).thenReturn("response");
        when(objectMapper.writeValueAsString(any())).thenReturn(jsonString);
        when(objectMapper.readValue(anyString(), any(Class.class)))
            .thenReturn(new AgreementOutput());

        // Act
        operator.contractNegotiation(recipient, resourceIds, artifactIds, download, rules);

        // Assert
        final var captorResults = requestCaptor.getValue();

        assertThat(captorResults.getBody())
            .isEqualTo("[{\"@type\":\"ids:Permission\"},{\"@type\":\"ids:Permission\"}]");
        assertThat(captorResults.getQueryParameters())
            .containsEntry("artifactIds", List.of("artifactId"));
        assertThat(captorResults.getQueryParameters())
            .containsEntry("resourceIds", List.of("resourceId"));
    }

    @Test
    @SneakyThrows
    void negotiateContract_dutyRule_returnAgreementOutput() {
        // Arrange
        final var recipient = URI.create("recipientUri");
        final var resourceIds = List.of(URI.create("resourceId"));
        final var artifactIds = List.of(URI.create("artifactId"));
        final var download = true;
        final List<Rule> rules = List.of(new TestDutyRule(), new TestDutyRule());
        final var jsonString = "{\"@type\":\"value\"}";

        when(requestHandler.sendRequest(requestCaptor.capture())).thenReturn("response");
        when(objectMapper.writeValueAsString(any())).thenReturn(jsonString);
        when(objectMapper.readValue(anyString(), any(Class.class)))
            .thenReturn(new AgreementOutput());

        // Act
        operator.contractNegotiation(recipient, resourceIds, artifactIds, download, rules);

        // Assert
        final var captorResults = requestCaptor.getValue();

        assertThat(captorResults.getBody())
            .isEqualTo("[{\"@type\":\"ids:Duty\"},{\"@type\":\"ids:Duty\"}]");
        assertThat(captorResults.getQueryParameters())
            .containsEntry("artifactIds", List.of("artifactId"));
        assertThat(captorResults.getQueryParameters())
            .containsEntry("resourceIds", List.of("resourceId"));
    }

    @Test
    @SneakyThrows
    void negotiateContract_prohibitionRule_returnAgreementOutput() {
        // Arrange
        final var recipientUri = URI.create("recipientUri");
        final var resourceIds = List.of(URI.create("resourceId"));
        final var artifactIds = List.of(URI.create("artifactId"));
        final var download = true;
        final List<Rule> rules = List.of(new TestProhibitionRule(), new TestProhibitionRule());
        final var jsonString = "{\"@type\":\"value\"}";

        when(requestHandler.sendRequest(requestCaptor.capture())).thenReturn("response");
        when(objectMapper.writeValueAsString(any())).thenReturn(jsonString);
        when(objectMapper.readValue(anyString(), any(Class.class)))
            .thenReturn(new AgreementOutput());

        // Act
        operator.contractNegotiation(recipientUri, resourceIds, artifactIds, download, rules);

        // Assert
        final var captorResults = requestCaptor.getValue();

        assertThat(captorResults.getBody())
            .isEqualTo("[{\"@type\":\"ids:Prohibition\"},{\"@type\":\"ids:Prohibition\"}]");
        assertThat(captorResults.getQueryParameters())
            .containsEntry("artifactIds", List.of("artifactId"));
        assertThat(captorResults.getQueryParameters())
            .containsEntry("resourceIds", List.of("resourceId"));
    }

    @Test
    void negotiateContract_resourceIdsNull_throwInvalidInputException() {
        // Arrange
        final var artifactIds = Collections.singletonList(URI.create("https://id"));
        final List<Rule> rules = Collections.singletonList(new PermissionBuilder()
            ._action_(Util.asList(Action.USE))
            .build());

        // Act & Assert
        assertThatThrownBy(() -> operator
            .contractNegotiation(recipient, null, artifactIds, true, rules))
            .isInstanceOf(InvalidInputException.class);
    }

    @Test
    void negotiateContract_resourceIdsEmpty_throwInvalidInputException() {
        // Arrange
        final var resourceIds = new ArrayList<URI>();
        final var artifactIds = Collections.singletonList(URI.create("https://id"));
        final List<Rule> rules = Collections.singletonList(new PermissionBuilder()
            ._action_(Util.asList(Action.USE))
            .build());

        // Act & Assert
        assertThatThrownBy(() -> operator
            .contractNegotiation(recipient, resourceIds, artifactIds, true, rules))
            .isInstanceOf(InvalidInputException.class);
    }

    @Test
    void negotiateContract_artifactIdsNull_throwInvalidInputException() {
        // Arrange
        final var resourceIds = Collections.singletonList(URI.create("https://id"));
        final List<Rule> rules = Collections.singletonList(new PermissionBuilder()
            ._action_(Util.asList(Action.USE))
            .build());

        // Act & Assert
        assertThatThrownBy(() -> operator
            .contractNegotiation(recipient, resourceIds, null, true, rules))
            .isInstanceOf(InvalidInputException.class);
    }

    @Test
    void negotiateContract_artifactIdsEmpty_throwInvalidInputException() {
        // Arrange
        final var resourceIds = Collections.singletonList(URI.create("https://id"));
        final var artifactIds = new ArrayList<URI>();
        final List<Rule> rules = Collections.singletonList(new PermissionBuilder()
            ._action_(Util.asList(Action.USE))
            .build());

        // Act & Assert
        assertThatThrownBy(() -> operator
            .contractNegotiation(recipient, resourceIds, artifactIds, true, rules))
            .isInstanceOf(InvalidInputException.class);
    }

    @Test
    void negotiateContract_rulesNull_throwInvalidInputException() {
        // Arrange
        final var resourceIds = Collections.singletonList(URI.create("https://id"));
        final var artifactIds = Collections.singletonList(URI.create("https://id"));

        // Act & Assert
        assertThatThrownBy(() -> operator
            .contractNegotiation(recipient, resourceIds, artifactIds, true, null))
            .isInstanceOf(InvalidInputException.class);
    }

    @Test
    void negotiateContract_rulesEmpty_throwInvalidInputException() {
        // Arrange
        final var resourceIds = Collections.singletonList(URI.create("https://id"));
        final var artifactIds = Collections.singletonList(URI.create("https://id"));
        final var rules = new ArrayList<Rule>();

        // Act & Assert
        assertThatThrownBy(() -> operator
            .contractNegotiation(recipient, resourceIds, artifactIds, true, rules))
            .isInstanceOf(InvalidInputException.class);
    }

    @Test
    @SneakyThrows
    void sendDescriptionRequestMessage_noElementId_returnConnector() {
        // Arrange
        final var recipient = URI.create("recipientUri");

        when(requestHandler.sendRequest(requestCaptor.capture())).thenReturn("response");
        when(serializer.deserialize(anyString(), any()))
            .thenReturn(new TestInfrastructureComponent());

        // Act
        operator.descriptionRequest(recipient);

        // Assert
        assertThat(requestCaptor.getValue().getQueryParameters())
            .containsEntry("recipient", List.of("recipientUri"));
    }

    @ParameterizedTest
    @MethodSource("provideArgumentsForDescriptionRequest")
    @SneakyThrows
    void sendDescriptionRequestMessage_withElementId_returnInfomodelObject(String json,
                                                                           Object deserialized,
                                                                           Class<Object> clazz) {
        // Arrange
        final var recipient = URI.create("recipientUri");
        final var elementId = URI.create("elementIdUri");

        when(requestHandler.sendRequest(requestCaptor.capture())).thenReturn(json);
        when(serializer.deserialize(json, clazz)).thenReturn(deserialized);

        // Act
        final var result = operator.descriptionRequest(recipient, elementId);

        // Assert
        assertThat(result).isNotNull();
        assertThat(clazz).isAssignableFrom(result.getClass());
        assertThat(result).isEqualTo(deserialized);

        final var captorResults = requestCaptor.getValue().getQueryParameters();
        assertThat(captorResults)
            .containsEntry("recipient", List.of("recipientUri"))
            .containsEntry("elementId", List.of("elementIdUri"));
    }

    @Test
    @SneakyThrows
    void sendDescriptionRequestMessage_withInvalidElementId_returnResponse() {
        // Arrange
        final var recipient = URI.create("recipientUri");
        final var elementId = URI.create("elementIdUri");
        final var jsonString = "{\"@type\":\"ids:InvalidElement\"}";

        when(requestHandler.sendRequest(requestCaptor.capture())).thenReturn(jsonString);
        when(serializer.deserialize(anyString(), any())).thenReturn("Object");

        // Act
        final var result = operator.descriptionRequest(recipient, elementId);

        // Assert
        assertThat(result).isNull();

        final var captorResults = requestCaptor.getValue().getQueryParameters();
        assertThat(captorResults)
            .containsEntry("recipient", List.of("recipientUri"))
            .containsEntry("elementId", List.of("elementIdUri"));
    }

    @Test
    @SneakyThrows
    void sendDescriptionRequestMessage_noParsableResponse_throwNoParsableResponseException() {
        // Arrange
        final var recipient = URI.create("recipientUri");
        final var elementId = URI.create("elementIdUri");
        final var jsonString = "{\"@type\":\"ids:Artifact\"}";

        when(requestHandler.sendRequest(requestCaptor.capture())).thenReturn(jsonString);
        when(serializer.deserialize(anyString(), any()))
            .thenThrow(new IOException("ioExceptionMessage"));

        // Act & Assert
        assertThatThrownBy(() -> operator.descriptionRequest(recipient, elementId))
            .isInstanceOf(NoParsableResponseException.class);
    }

    @Test
    @SneakyThrows
    void sendQueryMessage_successfulRequest_returnResponse() {
        // Arrange
        final var recipient = URI.create("recipientUri");
        final var query = "queryString";

        when(requestHandler.sendRequest(requestCaptor.capture())).thenReturn("response");

        // Act
        operator.query(recipient, query);

        // Assert
        final var captorResults = requestCaptor.getValue();

        assertThat(captorResults.getQueryParameters())
            .containsEntry("recipient", List.of("recipientUri"));
        assertThat(captorResults.getBody()).isEqualTo(query);
    }

    @Test
    @SneakyThrows
    void sendResourceUnavailableMessage_successfulRequest_returnResponse() {
        // Arrange
        final var recipient = URI.create("recipientUri");
        final var resourceId = URI.create("resourceIdUri");

        when(requestHandler.sendRequest(requestCaptor.capture())).thenReturn("response");

        // Act
        operator.resourceUnavailable(recipient, resourceId);

        // Assert
        final var captorResults = requestCaptor.getValue().getQueryParameters();

        assertThat(captorResults)
            .containsEntry("recipient", List.of("recipientUri"))
            .containsEntry("resourceId", List.of("resourceIdUri"));
    }

    @Test
    @SneakyThrows
    void sendResourceUpdateMessage_successfulRequest_returnResponse() {
        // Arrange
        final var recipient = URI.create("recipientUri");
        final var resourceId = URI.create("resourceIdUri");

        when(requestHandler.sendRequest(requestCaptor.capture())).thenReturn("response");

        // Act
        operator.resourceUpdate(recipient, resourceId);

        // Assert
        final var captorResults = requestCaptor.getValue().getQueryParameters();

        assertThat(captorResults)
            .containsEntry("recipient", List.of("recipientUri"))
            .containsEntry("resourceId", List.of("resourceIdUri"));
    }

    @Test
    @SneakyThrows
    void sendSearchMessage_successfulRequest_returnResponse() {
        // Arrange
        final var recipient = URI.create("recipientUri");
        final var body = "Body";
        final var limit = 10;
        final var offset = 5;

        when(requestHandler.sendRequest(requestCaptor.capture())).thenReturn("response");

        // Act
        operator.search(recipient, body, limit, offset);

        // Assert
        final var captorResults = requestCaptor.getValue().getQueryParameters();

        assertThat(captorResults)
            .containsEntry("recipient", List.of("recipientUri"))
            .containsEntry("limit", List.of(String.valueOf(limit)))
            .containsEntry("offset", List.of(String.valueOf(offset)));
    }

    @Test
    @SneakyThrows
    void sendSubscribeMessage_successfulRequest_returnResponse() {
        // Arrange
        final var recipient = URI.create("recipientUri");
        final var body = new SubscriptionInput();

        when(requestHandler.sendRequest(requestCaptor.capture())).thenReturn("response");

        // Act
        operator.subscribe(recipient, body);

        // Assert
        final var captorResults = requestCaptor.getValue().getQueryParameters();

        assertThat(captorResults)
            .containsEntry("recipient", List.of("recipientUri"));
    }

    @Test
    @SneakyThrows
    void sendUnsubscribeMessage_successfulRequest_returnResponse() {
        // Arrange
        final var recipient = URI.create("recipientUri");
        final var elementId = URI.create("elementId");

        when(requestHandler.sendRequest(requestCaptor.capture())).thenReturn("response");

        // Act
        operator.unsubscribe(recipient, elementId);

        // Assert
        final var captorResults = requestCaptor.getValue().getQueryParameters();

        assertThat(captorResults)
            .containsEntry("recipient", List.of("recipientUri"))
            .containsEntry("elementId", List.of("elementId"));
    }

    @Test
    @SneakyThrows
    void sendNotifyMessage_successfulRequest_returnResponse() {
        // Arrange
        final var elementId = URI.create("elementId");

        when(requestHandler.sendRequest(requestCaptor.capture())).thenReturn("response");

        // Act
        operator.notify(elementId);

        // Assert
        final var captorResults = requestCaptor.getValue().getQueryParameters();

        assertThat(captorResults)
            .containsEntry("elementId", List.of("elementId"));
    }

    @Test
    @SneakyThrows
    void sendDownloadAppMessage_successfulRequest_returnResponse() {
        // Arrange
        final var recipient = URI.create("recipientUri");
        final var appId = URI.create("appId");

        when(requestHandler.sendRequest(requestCaptor.capture())).thenReturn("response");

        // Act
        operator.appDownload(recipient, appId);

        // Assert
        final var captorResults = requestCaptor.getValue().getQueryParameters();

        assertThat(captorResults)
            .containsEntry("recipient", List.of("recipientUri"))
            .containsEntry("appId", List.of("appId"));
    }

    /**********************************************************************************************
     Utilities
     **********************************************************************************************/

    private static Stream<Arguments> provideArgumentsForDescriptionRequest() {
        return Stream.of(
            Arguments.of("{\"@type\":\"ids:ResourceCatalog\"}",
                getCatalog(), ResourceCatalog.class),
            Arguments.of("{\"@type\":\"ids:Resource\"}",
                getResource(), Resource.class),
            Arguments.of("{\"@type\":\"ids:Representation\"}",
                getRepresentation(), Representation.class),
            Arguments.of("{\"@type\":\"ids:Artifact\"}",
                getArtifact(), Artifact.class),
            Arguments.of("{\"@type\":\"ids:ContractOffer\"}",
                getContractOffer(), ContractOffer.class),
            Arguments.of("{\"@type\":\"ids:Permission\"}",
                getPermission(), Permission.class),
            Arguments.of("{\"@type\":\"ids:Prohibition\"}",
                getProhibition(), Prohibition.class),
            Arguments.of("{\"@type\":\"ids:Duty\"}",
                getDuty(), Duty.class),
            Arguments.of("{\"@type\":\"ids:AppResource\"}",
                getApp(), AppResource.class));
    }

    private static ResourceCatalog getCatalog() {
        return new ResourceCatalogBuilder().build();
    }

    private static Resource getResource() {
        return new ResourceBuilder().build();
    }

    private static Representation getRepresentation() {
        return new RepresentationBuilder().build();
    }

    private static Artifact getArtifact() {
        return new ArtifactBuilder().build();
    }

    private static ContractOffer getContractOffer() {
        return new ContractOfferBuilder().build();
    }

    private static Permission getPermission() {
        return new PermissionBuilder()._action_(List.of(Action.USE)).build();
    }

    private static Prohibition getProhibition() {
        return new ProhibitionBuilder()._action_(List.of(Action.USE)).build();
    }

    private static Duty getDuty() {
        return new DutyBuilder()._action_(List.of(Action.USE)).build();
    }

    private static AppResource getApp() {
        return new AppResourceBuilder().build();
    }

    public static class TestPermissionRule implements Permission {

        @Override
        public @NotNull URI getId() {
            return null;
        }

        @Override
        public List<TypedLiteral> getLabel() {
            return null;
        }

        @Override
        public List<TypedLiteral> getComment() {
            return null;
        }

        @Override
        public String toRdf() {
            return null;
        }

        @Override
        public Map<String, Object> getProperties() {
            return null;
        }

        @Override
        public void setProperty(String s, Object o) {

        }

        @Override
        public List<TypedLiteral> getTitle() {
            return null;
        }

        @Override
        public void setTitle(List<TypedLiteral> list) {

        }


        @Override
        public List<TypedLiteral> getDescription() {
            return null;
        }

        @Override
        public void setDescription(List<TypedLiteral> list) {

        }

        @Override
        public @NotEmpty List<Action> getAction() {
            return null;
        }

        @Override
        public void setAction(List<Action> list) {

        }

        @Override
        public AbstractConstraint getAssetRefinement() {
            return null;
        }

        @Override
        public void setAssetRefinement(AbstractConstraint abstractConstraint) {

        }

        @Override
        public List<AbstractConstraint> getConstraint() {
            return null;
        }

        @Override
        public void setConstraint(List<AbstractConstraint> list) {

        }

        @Override
        public List<URI> getAssigner() {
            return null;
        }

        @Override
        public void setAssigner(List<URI> list) {

        }

        @Override
        public List<URI> getAssignee() {
            return null;
        }

        @Override
        public void setAssignee(List<URI> list) {

        }

        @Override
        public URI getTarget() {
            return null;
        }

        @Override
        public void setTarget(URI uri) {

        }

        @Override
        public Permission deepCopy() {
            return null;
        }

        @Override
        public List<Duty> getPreDuty() {
            return null;
        }

        @Override
        public void setPreDuty(List<Duty> list) {

        }

        @Override
        public List<Duty> getPostDuty() {
            return null;
        }

        @Override
        public void setPostDuty(List<Duty> list) {

        }
    }

    public static class TestDutyRule implements Duty {

        @Override
        public @NotNull URI getId() {
            return null;
        }

        @Override
        public List<TypedLiteral> getLabel() {
            return null;
        }

        @Override
        public List<TypedLiteral> getComment() {
            return null;
        }

        @Override
        public String toRdf() {
            return null;
        }

        @Override
        public Map<String, Object> getProperties() {
            return null;
        }

        @Override
        public void setProperty(String s, Object o) {

        }

        @Override
        public List<TypedLiteral> getTitle() {
            return null;
        }

        @Override
        public void setTitle(List<TypedLiteral> list) {

        }

        @Override
        public List<TypedLiteral> getDescription() {
            return null;
        }

        @Override
        public void setDescription(List<TypedLiteral> list) {

        }

        @Override
        public @NotEmpty List<Action> getAction() {
            return null;
        }

        @Override
        public void setAction(List<Action> list) {

        }

        @Override
        public AbstractConstraint getAssetRefinement() {
            return null;
        }

        @Override
        public void setAssetRefinement(AbstractConstraint abstractConstraint) {

        }

        @Override
        public List<AbstractConstraint> getConstraint() {
            return null;
        }

        @Override
        public void setConstraint(List<AbstractConstraint> list) {

        }

        @Override
        public List<URI> getAssigner() {
            return null;
        }

        @Override
        public void setAssigner(List<URI> list) {

        }

        @Override
        public List<URI> getAssignee() {
            return null;
        }

        @Override
        public void setAssignee(List<URI> list) {

        }

        @Override
        public URI getTarget() {
            return null;
        }

        @Override
        public void setTarget(URI uri) {

        }

        @Override
        public Duty deepCopy() {
            return null;
        }
    }

    public static class TestProhibitionRule implements Prohibition {

        @Override
        public @NotNull URI getId() {
            return null;
        }

        @Override
        public List<TypedLiteral> getLabel() {
            return null;
        }

        @Override
        public List<TypedLiteral> getComment() {
            return null;
        }

        @Override
        public String toRdf() {
            return null;
        }

        @Override
        public Map<String, Object> getProperties() {
            return null;
        }

        @Override
        public void setProperty(String s, Object o) {

        }

        @Override
        public List<TypedLiteral> getTitle() {
            return null;
        }

        @Override
        public void setTitle(List<TypedLiteral> list) {

        }

        @Override
        public List<TypedLiteral> getDescription() {
            return null;
        }

        @Override
        public void setDescription(List<TypedLiteral> list) {

        }

        @Override
        public @NotEmpty List<Action> getAction() {
            return null;
        }

        @Override
        public void setAction(List<Action> list) {

        }

        @Override
        public AbstractConstraint getAssetRefinement() {
            return null;
        }

        @Override
        public void setAssetRefinement(AbstractConstraint abstractConstraint) {

        }

        @Override
        public List<AbstractConstraint> getConstraint() {
            return null;
        }

        @Override
        public void setConstraint(List<AbstractConstraint> list) {

        }

        @Override
        public List<URI> getAssigner() {
            return null;
        }

        @Override
        public void setAssigner(List<URI> list) {

        }

        @Override
        public List<URI> getAssignee() {
            return null;
        }

        @Override
        public void setAssignee(List<URI> list) {

        }

        @Override
        public URI getTarget() {
            return null;
        }

        @Override
        public void setTarget(URI uri) {

        }

        @Override
        public Prohibition deepCopy() {
            return null;
        }
    }

    public static class TestInfrastructureComponent implements InfrastructureComponent {

        @Override
        public @NotNull URI getId() {
            return null;
        }

        @Override
        public List<TypedLiteral> getLabel() {
            return null;
        }

        @Override
        public List<TypedLiteral> getComment() {
            return null;
        }

        @Override
        public String toRdf() {
            return null;
        }

        @Override
        public Map<String, Object> getProperties() {
            return null;
        }

        @Override
        public void setProperty(String s, Object o) {

        }

        @Override
        public List<TypedLiteral> getTitle() {
            return null;
        }

        @Override
        public void setTitle(List<TypedLiteral> list) {

        }

        @Override
        public List<TypedLiteral> getDescription() {
            return null;
        }

        @Override
        public void setDescription(List<TypedLiteral> list) {

        }

        @Override
        public String getVersion() {
            return null;
        }

        @Override
        public void setVersion(String s) {

        }

        @Override
        public InfrastructureComponent deepCopy() {
            return null;
        }

        @Override
        public @NotNull URI getMaintainer() {
            return null;
        }

        @Override
        public void setMaintainer(URI uri) {

        }

        @Override
        public Participant getMaintainerAsParticipant() {
            return null;
        }

        @Override
        public void setMaintainerAsParticipant(Participant participant) {

        }

        @Override
        public @NotNull UriOrModelClass getMaintainerAsObject() {
            return null;
        }

        @Override
        public @NotNull URI getCurator() {
            return null;
        }

        @Override
        public void setCurator(URI uri) {

        }

        @Override
        public Participant getCuratorAsParticipant() {
            return null;
        }

        @Override
        public void setCuratorAsParticipant(Participant participant) {

        }

        @Override
        public UriOrModelClass getCuratorAsObject() {
            return null;
        }

        @Override
        public @NotEmpty List<String> getInboundModelVersion() {
            return null;
        }

        @Override
        public void setInboundModelVersion(List<String> list) {

        }

        @Override
        public @NotNull String getOutboundModelVersion() {
            return null;
        }

        @Override
        public void setOutboundModelVersion(String s) {

        }

        @Override
        public Location getPhysicalLocation() {
            return null;
        }

        @Override
        public void setPhysicalLocation(Location location) {

        }

        @Override
        public ComponentCertification getComponentCertification() {
            return null;
        }

        @Override
        public void setComponentCertification(ComponentCertification componentCertification) {

        }

        @Override
        public PublicKey getPublicKey() {
            return null;
        }

        @Override
        public void setPublicKey(PublicKey publicKey) {

        }
    }
}
