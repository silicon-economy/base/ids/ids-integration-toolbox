/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.relation;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.net.URI;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.AppEndpointRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.OperationNotAllowedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 * AppEndpointsApiOperatorTest
 *
 * @author Florian Zimmer
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {AppEndpointsApiOperator.class})
class AppEndpointsApiOperatorTest {

    /** Class under test */
    @Autowired
    private AppEndpointsApiOperator operator;

    @MockBean
    private ObjectMapper objectMapper;

    @MockBean
    private RestRequestHandler requestHandler;

    @Test
    void add_notAllowed_throwOperationNotAllowedException() {
        // Arrange
        final var uuid = UUID.randomUUID();
        final var links = List.of(URI.create("link"));

        // Act & Assert
        assertThatThrownBy(() -> operator.add(uuid, links))
            .isInstanceOf(OperationNotAllowedException.class);
    }

    @Test
    void replace_notAllowed_throwOperationNotAllowedException() {
        // Arrange
        final var uuid = UUID.randomUUID();
        final var links = List.of(URI.create("link"));

        // Act & Assert
        assertThatThrownBy(() -> operator.replace(uuid, links))
            .isInstanceOf(OperationNotAllowedException.class);
    }

    @Test
    void remove_notAllowed_throwOperationNotAllowedException() {
        // Arrange
        final var uuid = UUID.randomUUID();
        final var links = List.of(URI.create("link"));

        // Act & Assert
        assertThatThrownBy(() -> operator.remove(uuid, links))
            .isInstanceOf(OperationNotAllowedException.class);
    }

    @Test
    void getEntityRelationOutputClass_returnClass() {
        // Act
        final var result = operator.getEntityRelationOutputClass();

        // Assert
        assertThat(result).isEqualTo(AppEndpointRelationOutput.class);
    }
}
