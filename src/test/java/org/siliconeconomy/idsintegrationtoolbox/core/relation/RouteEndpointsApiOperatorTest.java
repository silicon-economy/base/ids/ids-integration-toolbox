/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.relation;

import java.net.URI;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.siliconeconomy.idsintegrationtoolbox.model.RestRequest;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.RouteEndpointRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * RouteEndpointsApiOperatorTest
 *
 * @author Florian Zimmer
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {RouteEndpointsApiOperator.class})
class RouteEndpointsApiOperatorTest {

    /** Class under test */
    @Autowired
    private RouteEndpointsApiOperator operator;

    @MockBean
    private RestRequestHandler restRequestHandler;

    @MockBean
    private ObjectMapper objectMapper;

    @Test
    @SneakyThrows
    void setEndpoint_routeStart_shouldReturnResponse() {
        // Arrange
        final var routeUuid = UUID.randomUUID();
        final var endpointId = URI.create("https://example.com/endpointUuid");

        // Act
        operator.start(routeUuid, endpointId);

        // Assert
        verify(restRequestHandler, times(1))
            .sendRequest(any(RestRequest.class));
    }

    @Test
    @SneakyThrows
    void setEndpoint_routeEnd_shouldReturnResponse() {
        // Arrange
        final var routeUuid = UUID.randomUUID();
        final var endpointId = URI.create("https://example.com/endpointId");

        // Act
        operator.end(routeUuid, endpointId);

        // Assert
        verify(restRequestHandler, times(1))
            .sendRequest(any(RestRequest.class));
    }

    @Test
    @SneakyThrows
    void removeEndpoint_routeStart_shouldReturnResponse() {
        // Arrange
        final var routeUuid = UUID.randomUUID();

        // Act
        operator.start(routeUuid, null);

        // Assert
        verify(restRequestHandler, times(1))
            .sendRequest(any(RestRequest.class));
    }

    @Test
    @SneakyThrows
    void removeEndpoint_routeEnd_shouldReturnResponse() {
        // Arrange
        final var routeUuid = UUID.randomUUID();

        // Act
        operator.end(routeUuid, null);

        // Assert
        verify(restRequestHandler, times(1))
            .sendRequest(any(RestRequest.class));
    }

    @Test
    void getBaseApiPath_returnString() {
        // Act
        final var result = operator.getBaseApiPath();

        // Assert
        assertThat(result).isInstanceOf(String.class);
    }

    @Test
    void getRelationPath_returnString() {
        // Act
        final var result = operator.getRelationPath();

        // Assert
        assertThat(result).isInstanceOf(String.class);
    }

    private RouteEndpointRelationOutput getRelationOutput() {
        final var output = new TestRelationOutput();
        output.testField = "testContent";
        return output;
    }

    static class TestRelationOutput extends RouteEndpointRelationOutput {
        public String testField;
    }
}
