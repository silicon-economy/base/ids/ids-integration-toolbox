/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.relation;

import java.util.UUID;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.siliconeconomy.idsintegrationtoolbox.model.RestRequest;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.EndpointDatasourceRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * EndpointDatasourcesApiOperatorTest
 *
 * @author Florian Zimmer
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {EndpointDatasourcesApiOperator.class})
class EndpointDatasourcesApiOperatorTest {

    /** Class under test */
    @Autowired
    private EndpointDatasourcesApiOperator operator;

    @MockBean
    private RestRequestHandler restRequestHandler;

    @Test
    @SneakyThrows
    void add_shouldReturnResponse() {
        // Arrange
        final var endpointUuid = UUID.randomUUID();
        final var datasourceUuid = UUID.randomUUID();

        // Act
        operator.add(endpointUuid, datasourceUuid);

        // Assert
        verify(restRequestHandler, times(1))
            .sendRequest(any(RestRequest.class));
    }

    @Test
    void getBaseApiPath_returnString() {
        // Act
        final var result = operator.getBaseApiPath();

        // Assert
        assertThat(result).isInstanceOf(String.class);
    }

    @Test
    void getRelationPath_returnString() {
        // Act
        final var result = operator.getRelationPath();

        // Assert
        assertThat(result).isInstanceOf(String.class);
    }

    private EndpointDatasourceRelationOutput getRelationOutput() {
        final var output = new TestRelationOutput();
        output.testField = "testContent";
        return output;
    }

    static class TestRelationOutput extends EndpointDatasourceRelationOutput {
        public String testField;
    }
}
