/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.relation;

import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.client.NotFoundException;
import org.siliconeconomy.idsintegrationtoolbox.model.RestRequest;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.ArtifactRepresentationRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * ArtifactRepresentationsApiOperatorTest
 *
 * @author Florian Zimmer
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {ArtifactRepresentationsApiOperator.class})
class ArtifactRepresentationsApiOperatorTest {

    /** Class under test */
    @Autowired
    private ArtifactRepresentationsApiOperator operator;

    @MockBean
    private ObjectMapper objectMapper;

    @MockBean
    private RestTemplate restTemplate;

    @MockBean
    private RestRequestHandler restRequestHandler;

    @Test
    @SneakyThrows
    void getAll_successfulRequest_returnResponse() {
        // Arrange
        final var uuid = UUID.randomUUID();
        final var argumentCaptor = ArgumentCaptor.forClass(RestRequest.class);

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenReturn(getArtifactRepresentationRelationExample());
        when(objectMapper.writeValueAsString(any())).thenReturn("Sample String");
        when(objectMapper.readValue(anyString(), eq(ArtifactRepresentationRelationOutput.class)))
                .thenReturn(getArtifactRepresentationRelationOutputExample());

        // Act
        final var result = (ArtifactRepresentationRelationOutput) operator.getAll(uuid);

        // Assert
        verify(restRequestHandler, Mockito.times(1))
            .sendRequest(argumentCaptor.capture());
    }

    @Test
    @SneakyThrows
    void getAll_unsuccessfulRequest_throwApiInteractionUnsuccessfulException() {
        // Arrange
        final var uuid = UUID.randomUUID();
        final var argumentCaptor = ArgumentCaptor.forClass(RestRequest.class);

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenThrow(new ApiInteractionUnsuccessfulException("error", new NotFoundException()));
        when(objectMapper.writeValueAsString(any())).thenReturn("Sample String");

        // Act & Assert
        assertThatThrownBy(() -> operator.getAll(uuid))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());
        verify(restRequestHandler, Mockito.times(1)).sendRequest(argumentCaptor.capture());
    }

    @Test
    @SneakyThrows
    void add_successfulRequest_returnResponse() {
        // Arrange
        final var input = getSampleInput();
        final var uuid = UUID.randomUUID();
        final var argumentCaptor = ArgumentCaptor.forClass(RestRequest.class);

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenReturn(getArtifactRepresentationRelationExample());
        when(objectMapper.writeValueAsString(any())).thenReturn("Sample String");
        when(objectMapper.readValue(anyString(), eq(ArtifactRepresentationRelationOutput.class)))
                .thenReturn(getArtifactRepresentationRelationOutputExample());

        // Act
        final var result = (ArtifactRepresentationRelationOutput) operator.add(uuid, input);

        // Assert
        verify(restRequestHandler, Mockito.times(1)).sendRequest(argumentCaptor.capture());

    }

    @Test
    @SneakyThrows
    void add_unsuccessfulRequest_throwApiInteractionUnsuccessfulException() {
        final var input = getSampleInput();
        final var uuid = UUID.randomUUID();
        final var argumentCaptor = ArgumentCaptor.forClass(RestRequest.class);

        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenThrow(new ApiInteractionUnsuccessfulException("error", new NotFoundException()));
        when(objectMapper.writeValueAsString(any())).thenReturn("Sample String");

        // Act & Assert
        assertThatThrownBy(() -> operator.add(uuid, input))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());
        verify(restRequestHandler, Mockito.times(1)).sendRequest(argumentCaptor.capture());
    }

    @Test
    @SneakyThrows
    void update_successfulRequest_throwNoException() {
        // Arrange
        final var input = getSampleInput();
        final var uuid = UUID.randomUUID();
        final var argumentCaptor = ArgumentCaptor.forClass(RestRequest.class);

        when(objectMapper.writeValueAsString(any())).thenReturn("Sample String");
        when(restRequestHandler.sendRequest(any(RestRequest.class))).thenReturn("Dummy String");

        // Act & Assert
        assertDoesNotThrow(() -> operator.replace(uuid, input));
        verify(restRequestHandler, Mockito.times(2)).sendRequest(argumentCaptor.capture());

        var deleteRequest = argumentCaptor.getAllValues().get(0);
        assertThat(deleteRequest.getPathVariables().get("id"))
            .isEqualTo(uuid.toString());
        assertThat(deleteRequest.getHttpMethod()).isEqualTo(HttpMethod.DELETE);

        var addRequest =  argumentCaptor.getAllValues().get(1);
        assertThat(addRequest.getPathVariables().get("id"))
            .isEqualTo(uuid.toString());
        assertThat(addRequest.getHttpMethod()).isEqualTo(HttpMethod.POST);

    }

    @Test
    @SneakyThrows
    void update_unsuccessfulRequest_throwApiInteractionUnsuccessfulException() {
        // Arrange
        final var input = getSampleInput();
        final var uuid = UUID.randomUUID();
        final var argumentCaptor = ArgumentCaptor.forClass(RestRequest.class);

        when(objectMapper.writeValueAsString(any())).thenReturn("Sample String");
        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenThrow(new ApiInteractionUnsuccessfulException("error", new NotFoundException()));

        // Act & Assert
        assertThatThrownBy(() -> operator.replace(uuid, input))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());
        verify(restRequestHandler, Mockito.times(1)).sendRequest(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue().getPathVariables().get("id"))
            .isEqualTo(uuid.toString());
    }

    @Test
    @SneakyThrows
    void remove_successfulRequest_throwNoException() {
        // Arrange
        final var input = getSampleInput();
        final var uuid = UUID.randomUUID();
        final var argumentCaptor = ArgumentCaptor.forClass(RestRequest.class);

        when(objectMapper.writeValueAsString(any())).thenReturn("Sample String");
        when(restRequestHandler.sendRequest(any(RestRequest.class))).thenReturn("Dummy String");

        // Act & Assert
        assertDoesNotThrow(() -> operator.remove(uuid, input));
        verify(restRequestHandler, Mockito.times(1)).sendRequest(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue().getPathVariables().get("id"))
            .isEqualTo(uuid.toString());
    }

    @Test
    @SneakyThrows
    void remove_unsuccessfulRequest_throwApiInteractionUnsuccessfulException() {
        // Arrange
        final var input = getSampleInput();
        final var uuid = UUID.randomUUID();
        final var argumentCaptor = ArgumentCaptor.forClass(RestRequest.class);

        when(objectMapper.writeValueAsString(any())).thenReturn("Sample String");
        when(restRequestHandler.sendRequest(any(RestRequest.class)))
            .thenThrow(new ApiInteractionUnsuccessfulException("error", new NotFoundException()));

        // Act & Assert
        assertThatThrownBy(() -> operator.remove(uuid, input))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());
        verify(restRequestHandler, Mockito.times(1)).sendRequest(argumentCaptor.capture());

        assertThat(argumentCaptor.getValue().getPathVariables().get("id"))
            .isEqualTo(uuid.toString());
    }

    private List<URI> getSampleInput() {
        return Collections.singletonList(URI.create("http://localhost"));
    }

    @SneakyThrows
    private ArtifactRepresentationRelationOutput getArtifactRepresentationRelationOutputExample() {
        final var objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());

        return objectMapper.readValue(getArtifactRepresentationRelationExample(),
                ArtifactRepresentationRelationOutput.class);
    }

    /**********************************************************************************************
        Utilities
     **********************************************************************************************/

    private String getArtifactRepresentationRelationExample() {
        return "{\n" +
                "  \"_embedded\": {\n" +
                "    \"representations\": [\n" +
                "      {\n" +
                "        \"creationDate\": \"2021-06-14T11:30:33.181+0200\",\n" +
                "        \"modificationDate\": \"2021-06-14T11:30:33.181+0200\",\n" +
                "        \"remoteId\": \"agreementTestId\",\n" +
                "        \"title\": \"\",\n" +
                "        \"mediaType\": \"\",\n" +
                "        \"language\": \"EN\",\n" +
                "        \"additional\": {},\n" +
                "        \"_links\": {\n" +
                "          \"self\": \n" +
                "            {\n" +
                "              \"href\": \"http://localhost:8080/api/representations/493edac7" +
                "-774e-45d3-9a80-84f3a12fa3c2\"\n" +
                "            }" +
                "          ,\n" +
                "          \"artifacts\": {\n" +
                "            \"href\": \"http://localhost:8080/api/representations/493edac7-774e" +
                "-45d3-9a80-84f3a12fa3c2/artifacts{?page,size}\",\n" +
                "            \"templated\": true\n" +
                "          },\n" +
                "          \"offers\": {\n" +
                "            \"href\": \"http://localhost:8080/api/representations/493edac7-774e" +
                "-45d3-9a80-84f3a12fa3c2/offers{?page,size}\",\n" +
                "            \"templated\": true\n" +
                "          }\n" +
                "        }\n" +
                "      }\n" +
                "    ]\n" +
                "  },\n" +
                "  \"_links\": {\n" +
                "    \"self\": {\n" +
                "      \"href\": \"http://localhost:8080/api/artifacts/4e9c5c7b-2808-470d-b2d5" +
                "-9a8f3ea04d6a/representations?page=0&size=30\"\n" +
                "    }\n" +
                "  },\n" +
                "  \"page\": {\n" +
                "    \"size\": 30,\n" +
                "    \"totalElements\": 1,\n" +
                "    \"totalPages\": 1,\n" +
                "    \"number\": 0\n" +
                "  }\n" +
                "}";
    }
}
