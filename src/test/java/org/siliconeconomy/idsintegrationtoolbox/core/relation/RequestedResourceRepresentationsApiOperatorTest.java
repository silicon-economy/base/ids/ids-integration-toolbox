/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.relation;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.RequestedResourceRepresentationRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * RequestedResourceRepresentationsApiOperatorTest
 *
 * @author Florian Zimmer
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {RequestedResourceRepresentationsApiOperator.class})
class RequestedResourceRepresentationsApiOperatorTest {

    /** Class under test */
    @Autowired
    private RequestedResourceRepresentationsApiOperator operator;

    @MockBean
    private ObjectMapper objectMapper;

    @MockBean
    private RestRequestHandler requestHandler;

    @Test
    void getBaseApiPath_returnString() {
        // Act
        final var result = operator.getBaseApiPath();

        // Assert
        assertThat(result).isInstanceOf(String.class);
    }

    @Test
    void getRelationPath_returnString() {
        // Act
        final var result = operator.getRelationPath();

        // Assert
        assertThat(result).isInstanceOf(String.class);
    }

    @Test
    void getEntityRelationOutputClass_returnClass() {
        // Act
        final var result = operator.getEntityRelationOutputClass();

        // Assert
        assertThat(result).isEqualTo(RequestedResourceRepresentationRelationOutput.class);
    }
}
