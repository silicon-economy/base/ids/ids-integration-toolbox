/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.relation;

import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.Embedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.Link;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.Links;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.EntityOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.client.NotFoundException;
import org.siliconeconomy.idsintegrationtoolbox.model.RestRequest;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.AbstractEntity;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.EntityRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

/**
 * RelationApiOperatorTest
 *
 * @author Haydar Qarawlus
 */
@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class RelationApiOperatorTest {

    /** Class under test */
    private RelationApiOperator operator;

    @MockBean
    private ObjectMapper objectMapper;

    @MockBean
    private RestRequestHandler requestHandler;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
        operator = Mockito.mock(RelationApiOperator.class, Mockito.withSettings()
            .useConstructor(requestHandler, objectMapper)
            .defaultAnswer(Mockito.CALLS_REAL_METHODS));
    }

    @Test
    @SneakyThrows
    void getAll_successfulRequest_returnResponse() {
        // Arrange
        final var testClass = TestRelationOutput.class;
        final var uuid = UUID.randomUUID();
        final var output = getSampleOutput();

        when(requestHandler.sendRequest(any(RestRequest.class))).thenReturn("dummy");
        when(objectMapper.readValue(anyString(), any(Class.class))).thenReturn(output);
        when(operator.getEntityRelationOutputClass()).thenReturn(testClass);

        // Act
        final var result = (TestRelationOutput) operator.getAll(uuid);

        // Assert
        assertThat(result.testField).isEqualTo("testContent");
    }

    @Test
    @SneakyThrows
    void getAll_successfulRequestWithPaging_returnResponse() {
        // Arrange
        final var testClass = TestRelationOutput.class;
        final var uuid = UUID.randomUUID();
        final var output = getSampleOutput();
        final var page = Integer.valueOf(1);
        final var size = Integer.valueOf(2);

        when(requestHandler.sendRequest(any(RestRequest.class))).thenReturn("dummy");
        when(objectMapper.readValue(anyString(), any(Class.class))).thenReturn(output);
        when(operator.getEntityRelationOutputClass()).thenReturn(testClass);

        // Act
        final var result = (TestRelationOutput) operator.getAll(uuid, page, size);

        // Assert
        assertThat(result.testField).isEqualTo("testContent");
    }

    @Test
    @SneakyThrows
    void getAll_unsuccessfulRequest_throwApiInteractionUnsuccessfulException() {
        // Arrange
        final var uuid = UUID.randomUUID();

        when(requestHandler.sendRequest(any(RestRequest.class)))
            .thenThrow(new ApiInteractionUnsuccessfulException("error", new NotFoundException()));

        // Act & Assert
        assertThatThrownBy(() -> operator.getAll(uuid))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());
    }

    @Test
    @SneakyThrows
    void add_successfulRequest_returnResponse() {
        // Arrange
        final var testClass = TestRelationOutput.class;
        final var uuid = UUID.randomUUID();
        final var input = getSampleInput();

        when(requestHandler.sendRequest(any(RestRequest.class))).thenReturn("Dummy String");
        when(objectMapper.writeValueAsString(input)).thenReturn("Sample String");
        when(objectMapper.readValue(anyString(), eq(testClass))).thenReturn(getSampleOutput());
        when(operator.getEntityRelationOutputClass()).thenReturn(testClass);

        // Act
        final var result = (TestRelationOutput) operator.add(uuid, input);

        // Assert
        assertThat(result.testField).isEqualTo("testContent");
    }

    @Test
    @SneakyThrows
    void add_unsuccessfulRequest_throwApiInteractionUnsuccessfulException() {
        // Arrange
        final var uuid = UUID.randomUUID();
        final var input = getSampleInput();

        when(requestHandler.sendRequest(any(RestRequest.class)))
            .thenThrow(new ApiInteractionUnsuccessfulException("error", new NotFoundException()));

        // Act & Assert
        assertThatThrownBy(() -> operator.add(uuid, input))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());
    }

    @Test
    @SneakyThrows
    void update_successfulRequest_doNotThrowException() {
        // Arrange
        final var uuid = UUID.randomUUID();
        final var input = getSampleInput();

        when(requestHandler.sendRequest(any(RestRequest.class))).thenReturn("Dummy String");
        when(objectMapper.writeValueAsString(input)).thenReturn("Sample String");

        // Act & Assert
        assertDoesNotThrow(() -> operator.replace(uuid, input));
    }

    @Test
    @SneakyThrows
    void update_unsuccessfulRequest_throwApiInteractionUnsuccessfulException() {
        // Arrange
        final var uuid = UUID.randomUUID();
        final var input = getSampleInput();

        when(requestHandler.sendRequest(any(RestRequest.class)))
            .thenThrow(new ApiInteractionUnsuccessfulException("error", new NotFoundException()));
        when(objectMapper.writeValueAsString(input)).thenReturn("Sample String");

        // Act & Assert
        assertThatThrownBy(() -> operator.replace(uuid, input))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());
    }

    @Test
    @SneakyThrows
    void remove_successfulRequest_doNotThrowException() {
        // Arrange
        final var uuid = UUID.randomUUID();
        final var input = getSampleInput();

        when(requestHandler.sendRequest(any(RestRequest.class))).thenReturn("Dummy String");
        when(objectMapper.writeValueAsString(input)).thenReturn("Sample String");

        // Act & Assert
        assertDoesNotThrow(() -> operator.remove(uuid, input));
    }

    @Test
    @SneakyThrows
    void remove_unsuccessfulRequest_throwApiInteractionUnsuccessfulException() {
        // Arrange
        final var uuid = UUID.randomUUID();
        final var input = getSampleInput();

        when(requestHandler.sendRequest(any(RestRequest.class)))
            .thenThrow(new ApiInteractionUnsuccessfulException("error", new NotFoundException()));
        when(objectMapper.writeValueAsString(input)).thenReturn("Sample String");

        // Act & Assert
        assertThatThrownBy(() -> operator.remove(uuid, input))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());
    }

    /**********************************************************************************************
        Utilities
     **********************************************************************************************/

    private List<URI> getSampleInput() {
        return Collections.singletonList(URI.create("http://localhost"));
    }

    private TestRelationOutput getSampleOutput() {
        final var output = new TestRelationOutput();
        output.testField = "testContent";
        return output;
    }

    static class TestRelationOutput extends EntityRelationOutput<AbstractEntity,
        TestLinks, AbstractEntity, TestOutput, TestEmbedded> {
        public String testField;

    }

    static class TestOutput extends EntityOutput<AbstractEntity> {
        public String testField;

        @Override
        public Links<AbstractEntity> getLinks() {
            return null;
        }
    }

    static class TestEmbedded extends Embedded<AbstractEntity, TestOutput> {
        public String testField;

        @Override
        public List<TestOutput> getEntries() {
            return null;
        }
    }

    static class TestLinks implements Links<AbstractEntity> {

        @Override
        public Link getSelf() {
            return null;
        }
    }
}
