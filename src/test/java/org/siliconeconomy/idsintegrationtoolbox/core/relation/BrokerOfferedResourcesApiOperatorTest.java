/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.relation;

import java.net.URI;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.BrokerOfferedResourceRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.OperationNotAllowedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

/**
 * BrokerOfferedResourcesApiOperatorTest
 *
 * @author Florian Zimmer
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {BrokerOfferedResourcesApiOperator.class})
class BrokerOfferedResourcesApiOperatorTest {

    private final UUID uuid = UUID.randomUUID();
    private final URI uri = URI.create("https://my-uri");

    /** Class under test */
    @Autowired
    private BrokerOfferedResourcesApiOperator operator;

    @MockBean
    private ObjectMapper objectMapper;

    @MockBean
    private RestRequestHandler requestHandler;

    @Test
    void getEntityRelationOutputClass_returnClass() {
        // Act
        final var result = operator.getEntityRelationOutputClass();

        // Assert
        assertThat(result).isEqualTo(BrokerOfferedResourceRelationOutput.class);
    }

    @Test
    void add_throwMethodNotAllowedException() {
        // Arrange
        final var list = List.of(uri);

        // Act && Assert
        assertThatThrownBy(() -> operator.add(uuid, list))
            .isInstanceOf(OperationNotAllowedException.class);
    }

    @Test
    void replace_throwMethodNotAllowedException() {
        // Arrange
        final var list = List.of(uri);

        // Act && Assert
        assertThatThrownBy(() -> operator.replace(uuid, list))
            .isInstanceOf(OperationNotAllowedException.class);
    }

    @Test
    void remove_throwMethodNotAllowedException() {
        // Arrange
        final var list = List.of(uri);

        // Act && Assert
        assertThatThrownBy(() -> operator.remove(uuid, list))
            .isInstanceOf(OperationNotAllowedException.class);
    }
}
