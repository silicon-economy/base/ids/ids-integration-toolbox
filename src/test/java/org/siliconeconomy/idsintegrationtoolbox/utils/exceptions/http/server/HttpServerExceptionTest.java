/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.server;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * HttpServerExceptionTest
 *
 * @author Malte Hellmeier
 */
class HttpServerExceptionTest {

    @Test
    @SneakyThrows
    void constructor_messageArgument_throwHttpServerException() {
        // Arrange
        final var message = "errorMessage";
        final var httpServerException = new HttpServerException(message);

        // Act & Assert
        assertThat(httpServerException.getMessage()).contains(message);
    }

    @Test
    @SneakyThrows
    void constructor_messageThrowableArgument_throwHttpServerException() {
        // Arrange
        final var errorMessage = "errorMessage";
        final var causeMessage = "causeMessage";
        final var cause = new Exception(causeMessage);
        final var httpServerException = new HttpServerException(errorMessage, cause);

        // Act & Assert
        assertThat(httpServerException.getMessage()).contains(errorMessage);
        assertThat(httpServerException.getCause().getMessage()).isEqualTo(causeMessage);
    }
}
