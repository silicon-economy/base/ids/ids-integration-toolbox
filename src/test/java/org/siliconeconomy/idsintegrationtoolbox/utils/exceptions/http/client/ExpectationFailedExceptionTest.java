/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.client;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * ExpectationFailedExceptionTest
 *
 * @author Malte Hellmeier
 */
class ExpectationFailedExceptionTest {

    final String ERROR_CODE = "417";

    @Test
    @SneakyThrows
    void constructor_noArgument_throwExpectationFailedException() {
        // Arrange
        final var expectationFailedException = new ExpectationFailedException();

        // Act & Assert
        assertThat(expectationFailedException.getMessage()).contains(ERROR_CODE);
    }

    @Test
    @SneakyThrows
    void constructor_messageThrowableArgument_throwExpectationFailedException() {
        // Arrange
        final var causeMessage = "causeMessage";
        final var cause = new Exception(causeMessage);
        final var expectationFailedException = new ExpectationFailedException(cause);

        // Act & Assert
        assertThat(expectationFailedException.getMessage()).contains(ERROR_CODE);
        assertThat(expectationFailedException.getCause().getMessage()).isEqualTo(causeMessage);
    }

}
