/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.client;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * WrongMediaTypeExceptionTest
 *
 * @author Malte Hellmeier
 */
class WrongMediaTypeExceptionTest {

    final String ERROR_CODE = "415";

    @Test
    @SneakyThrows
    void constructor_noArgument_throwWrongMediaTypeException() {
        // Arrange
        final var wrongMediaTypeException = new WrongMediaTypeException();

        // Act & Assert
        assertThat(wrongMediaTypeException.getMessage()).contains(ERROR_CODE);
    }

    @Test
    @SneakyThrows
    void constructor_messageThrowableArgument_throwWrongMediaTypeException() {
        // Arrange
        final var causeMessage = "causeMessage";
        final var cause = new Exception(causeMessage);
        final var wrongMediaTypeException = new WrongMediaTypeException(cause);

        // Act & Assert
        assertThat(wrongMediaTypeException.getMessage()).contains(ERROR_CODE);
        assertThat(wrongMediaTypeException.getCause().getMessage()).isEqualTo(causeMessage);
    }

}
