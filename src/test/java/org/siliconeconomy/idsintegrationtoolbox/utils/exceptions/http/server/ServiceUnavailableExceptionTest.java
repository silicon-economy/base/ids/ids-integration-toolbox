/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.server;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * ServiceUnavailableExceptionTest
 *
 * @author Malte Hellmeier
 */
class ServiceUnavailableExceptionTest {

    final String ERROR_CODE = "503";

    @Test
    @SneakyThrows
    void constructor_noArgument_throwServiceUnavailableException() {
        // Arrange
        final var serviceUnavailableException = new ServiceUnavailableException();

        // Act & Assert
        assertThat(serviceUnavailableException.getMessage()).contains(ERROR_CODE);
    }

    @Test
    @SneakyThrows
    void constructor_messageThrowableArgument_throwServiceUnavailableException() {
        // Arrange
        final var causeMessage = "causeMessage";
        final var cause = new Exception(causeMessage);
        final var serviceUnavailableException = new ServiceUnavailableException(cause);

        // Act & Assert
        assertThat(serviceUnavailableException.getMessage()).contains(ERROR_CODE);
        assertThat(serviceUnavailableException.getCause().getMessage()).isEqualTo(causeMessage);
    }

}
