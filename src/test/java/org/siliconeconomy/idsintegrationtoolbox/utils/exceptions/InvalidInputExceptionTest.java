/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils.exceptions;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * InvalidInputExceptionTest
 *
 * @author Ronja Quensel
 */
class InvalidInputExceptionTest {

    @Test
    @SneakyThrows
    void constructor_messageArgument_returnInvalidInputException() {
        // Arrange
        final var message = "errorMessage";

        // Act
        final var exception = new InvalidInputException(message);

        // Assert
        assertThat(exception.getMessage()).isEqualTo(message);
    }

    @Test
    @SneakyThrows
    void constructor_messageThrowableArgument_returnInvalidInputException() {
        // Arrange
        final var errorMessage = "errorMessage";
        final var causeMessage = "causeMessage";
        final var cause = new Exception(causeMessage);

        // Act
        final var exception = new InvalidInputException(errorMessage, cause);

        // Assert
        assertThat(exception.getMessage()).isEqualTo(errorMessage);
        assertThat(exception.getCause().getMessage()).isEqualTo(causeMessage);
    }

}
