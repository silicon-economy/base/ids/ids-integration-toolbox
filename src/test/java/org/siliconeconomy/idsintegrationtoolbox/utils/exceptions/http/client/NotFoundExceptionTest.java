/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.client;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * NotFoundExceptionTest
 *
 * @author Malte Hellmeier
 */
class NotFoundExceptionTest {

    final String ERROR_CODE = "404";

    @Test
    @SneakyThrows
    void constructor_noArgument_throwNotFoundException() {
        // Arrange
        final var notFoundException = new NotFoundException();

        // Act & Assert
        assertThat(notFoundException.getMessage()).contains(ERROR_CODE);
    }

    @Test
    @SneakyThrows
    void constructor_messageThrowableArgument_throwNotFoundException() {
        // Arrange
        final var causeMessage = "causeMessage";
        final var cause = new Exception(causeMessage);
        final var notFoundException = new NotFoundException(cause);

        // Act & Assert
        assertThat(notFoundException.getMessage()).contains(ERROR_CODE);
        assertThat(notFoundException.getCause().getMessage()).isEqualTo(causeMessage);
    }

}
