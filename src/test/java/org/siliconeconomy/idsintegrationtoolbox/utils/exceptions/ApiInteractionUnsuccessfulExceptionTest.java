/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils.exceptions;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * ApiInteractionUnsuccessfulExceptionTest
 *
 * @author Malte Hellmeier
 */
class ApiInteractionUnsuccessfulExceptionTest {

    @Test
    @SneakyThrows
    void constructor_messageArgument_throwApiInteractionUnsuccessfulException() {
        // Arrange
        final var message = "errorMessage";
        final var apiInteractionUnsuccessfulException =
            new ApiInteractionUnsuccessfulException(message);

        // Act & Assert
        assertThat(apiInteractionUnsuccessfulException.getMessage()).isEqualTo(message);
    }

    @Test
    @SneakyThrows
    void constructor_messageThrowableArgument_throwApiInteractionUnsuccessfulException() {
        // Arrange
        final var errorMessage = "errorMessage";
        final var causeMessage = "causeMessage";
        final var cause = new Exception(causeMessage);
        final var apiInteractionUnsuccessfulException =
            new ApiInteractionUnsuccessfulException(errorMessage, cause);

        // Act & Assert
        assertThat(apiInteractionUnsuccessfulException.getMessage()).isEqualTo(errorMessage);
        assertThat(apiInteractionUnsuccessfulException.getCause().getMessage())
            .isEqualTo(causeMessage);
    }

}
