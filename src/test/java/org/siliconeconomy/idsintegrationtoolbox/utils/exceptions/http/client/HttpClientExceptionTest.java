/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.client;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * HttpClientExceptionTest
 *
 * @author Malte Hellmeier
 */
class HttpClientExceptionTest {

    @Test
    @SneakyThrows
    void constructor_messageArgument_throwHttpClientException() {
        // Arrange
        final var message = "errorMessage";
        final var httpClientException = new HttpClientException(message);

        // Act & Assert
        assertThat(httpClientException.getMessage()).contains(message);
    }

    @Test
    @SneakyThrows
    void constructor_messageThrowableArgument_throwHttpClientException() {
        // Arrange
        final var errorMessage = "errorMessage";
        final var causeMessage = "causeMessage";
        final var cause = new Exception(causeMessage);
        final var httpClientException = new HttpClientException(errorMessage, cause);

        // Act & Assert
        assertThat(httpClientException.getMessage()).contains(errorMessage);
        assertThat(httpClientException.getCause().getMessage()).isEqualTo(causeMessage);
    }

}
