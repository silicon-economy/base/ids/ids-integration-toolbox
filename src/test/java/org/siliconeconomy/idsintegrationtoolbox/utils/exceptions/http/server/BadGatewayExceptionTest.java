/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.server;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * BadGatewayExceptionTest
 *
 * @author Malte Hellmeier
 */
class BadGatewayExceptionTest {

    final String ERROR_CODE = "502";

    @Test
    @SneakyThrows
    void constructor_noArgument_throwBadGatewayException() {
        // Arrange
        final var badGatewayException = new BadGatewayException();

        // Act & Assert
        assertThat(badGatewayException.getMessage()).contains(ERROR_CODE);
    }

    @Test
    @SneakyThrows
    void constructor_messageThrowableArgument_throwBadGatewayException() {
        // Arrange
        final var causeMessage = "causeMessage";
        final var cause = new Exception(causeMessage);
        final var badGatewayException = new BadGatewayException(cause);

        // Act & Assert
        assertThat(badGatewayException.getMessage()).contains(ERROR_CODE);
        assertThat(badGatewayException.getCause().getMessage()).isEqualTo(causeMessage);
    }

}
