/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * HttpCommunicationExceptionTest
 *
 * @author Malte Hellmeier
 */
class HttpCommunicationExceptionTest {

    @Test
    @SneakyThrows
    void constructor_messageArgument_throwHttpCommunicationException() {
        // Arrange
        final var message = "errorMessage";
        final var httpCommunicationException = new HttpCommunicationException(message);

        // Act & Assert
        assertThat(httpCommunicationException.getMessage()).contains(message);
    }

    @Test
    @SneakyThrows
    void constructor_messageThrowableArgument_throwHttpCommunicationException() {
        // Arrange
        final var errorMessage = "errorMessage";
        final var causeMessage = "causeMessage";
        final var cause = new Exception(causeMessage);
        final var httpCommunicationException = new HttpCommunicationException(errorMessage,
            cause);

        // Act & Assert
        assertThat(httpCommunicationException.getMessage()).isEqualTo(errorMessage);
        assertThat(httpCommunicationException.getCause().getMessage()).isEqualTo(causeMessage);
    }

}
