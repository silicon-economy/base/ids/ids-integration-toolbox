/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils.exceptions;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * NoParsableResponseExceptionTest
 *
 * @author Malte Hellmeier
 */
class NoParsableResponseExceptionTest {

    @Test
    @SneakyThrows
    void constructor_messageArgument_throwNoParsableResponseException() {
        // Arrange
        final var message = "errorMessage";
        final var noParsableResponseException = new NoParsableResponseException(message);

        // Act & Assert
        assertThat(noParsableResponseException.getMessage()).isEqualTo(message);
    }

    @Test
    @SneakyThrows
    void constructor_messageThrowableArgument_throwNoParsableResponseException() {
        // Arrange
        final var errorMessage = "errorMessage";
        final var causeMessage = "causeMessage";
        final var cause = new Exception(causeMessage);
        final var noParsableResponseException = new NoParsableResponseException(errorMessage,
            cause);

        // Act & Assert
        assertThat(noParsableResponseException.getMessage()).isEqualTo(errorMessage);
        assertThat(noParsableResponseException.getCause().getMessage()).isEqualTo(causeMessage);
    }

}
