/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.client;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * RequestTimeoutExceptionTest
 *
 * @author Malte Hellmeier
 */
class RequestTimeoutExceptionTest {

    final String ERROR_CODE = "408";

    @Test
    @SneakyThrows
    void constructor_noArgument_throwRequestTimeoutException() {
        // Arrange
        final var requestTimeoutException = new RequestTimeoutException();

        // Act & Assert
        assertThat(requestTimeoutException.getMessage()).contains(ERROR_CODE);
    }

    @Test
    @SneakyThrows
    void constructor_messageThrowableArgument_throwRequestTimeoutException() {
        // Arrange
        final var causeMessage = "causeMessage";
        final var cause = new Exception(causeMessage);
        final var requestTimeoutException = new RequestTimeoutException(cause);

        // Act & Assert
        assertThat(requestTimeoutException.getMessage()).contains(ERROR_CODE);
        assertThat(requestTimeoutException.getCause().getMessage()).isEqualTo(causeMessage);
    }

}
