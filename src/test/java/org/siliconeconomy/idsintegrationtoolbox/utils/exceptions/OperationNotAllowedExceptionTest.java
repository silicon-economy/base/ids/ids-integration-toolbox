/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils.exceptions;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * OperationNotAllowedExceptionTest
 *
 * @author Malte Hellmeier
 */
class OperationNotAllowedExceptionTest {

    @Test
    @SneakyThrows
    void constructor_messageArgument_throwOperationNotAllowedException() {
        // Arrange
        final var message = "errorMessage";
        final var operationNotAllowedException = new OperationNotAllowedException(message);

        // Act & Assert
        assertThat(operationNotAllowedException.getMessage()).isEqualTo(message);
    }

    @Test
    @SneakyThrows
    void constructor_messageThrowableArgument_throwOperationNotAllowedException() {
        // Arrange
        final var errorMessage = "errorMessage";
        final var causeMessage = "causeMessage";
        final var cause = new Exception(causeMessage);
        final var operationNotAllowedException = new OperationNotAllowedException(errorMessage,
            cause);

        // Act & Assert
        assertThat(operationNotAllowedException.getMessage()).isEqualTo(errorMessage);
        assertThat(operationNotAllowedException.getCause().getMessage()).isEqualTo(causeMessage);
    }

}
