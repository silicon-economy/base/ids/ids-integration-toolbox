/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils.exceptions;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * EmptyFieldExceptionTest
 *
 * @author Florian Zimmer
 */
class EmptyFieldExceptionTest {

    @Test
    @SneakyThrows
    void constructor_messageArgument_returnWithMessage() {
        // Arrange
        final var message = "errorMessage";

        // Act
        final var emptyFieldException = new EmptyFieldException(message);

        // Act & Assert
        assertThat(emptyFieldException.getMessage()).isEqualTo(message);
    }

    @Test
    @SneakyThrows
    void constructor_messageAndCauseArguments_returnWithMessageAndCause() {
        // Arrange
        final var message = "errorMessage";
        final var causeMessage = "causeMessage";
        final var cause = new Exception(causeMessage);

        // Act
        final var emptyFieldException = new EmptyFieldException(message, cause);

        // Assert
        assertThat(emptyFieldException.getMessage()).isEqualTo(message);
        assertThat(emptyFieldException.getCause().getMessage()).isEqualTo(causeMessage);
    }

}
