/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.client;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * UnauthorizedExceptionTest
 *
 * @author Malte Hellmeier
 */
class UnauthorizedExceptionTest {

    final String ERROR_CODE = "401";

    @Test
    @SneakyThrows
    void constructor_noArgument_throwUnauthorizedException() {
        // Arrange
        final var unauthorizedException = new UnauthorizedException();

        // Act & Assert
        assertThat(unauthorizedException.getMessage()).contains(ERROR_CODE);
    }

    @Test
    @SneakyThrows
    void constructor_messageThrowableArgument_throwUnauthorizedException() {
        // Arrange
        final var causeMessage = "causeMessage";
        final var cause = new Exception(causeMessage);
        final var unauthorizedException = new UnauthorizedException(cause);

        // Act & Assert
        assertThat(unauthorizedException.getMessage()).contains(ERROR_CODE);
        assertThat(unauthorizedException.getCause().getMessage()).isEqualTo(causeMessage);
    }

}
