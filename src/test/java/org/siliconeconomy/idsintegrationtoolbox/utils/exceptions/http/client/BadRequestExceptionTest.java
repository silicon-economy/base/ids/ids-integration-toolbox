/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.client;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * BadRequestExceptionTest
 *
 * @author Malte Hellmeier
 */
class BadRequestExceptionTest {

    final String ERROR_CODE = "400";

    @Test
    @SneakyThrows
    void constructor_noArgument_throwBadRequestException() {
        // Arrange
        final var badRequestException = new BadRequestException();

        // Act & Assert
        assertThat(badRequestException.getMessage()).contains(ERROR_CODE);
    }

    @Test
    @SneakyThrows
    void constructor_messageThrowableArgument_throwBadRequestException() {
        // Arrange
        final var causeMessage = "causeMessage";
        final var cause = new Exception(causeMessage);
        final var badRequestException = new BadRequestException(cause);

        // Act & Assert
        assertThat(badRequestException.getMessage()).contains(ERROR_CODE);
        assertThat(badRequestException.getCause().getMessage()).isEqualTo(causeMessage);
    }

}
