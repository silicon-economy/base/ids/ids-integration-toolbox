/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils.exceptions;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * FailedToReadResponseExceptionTest
 *
 * @author Malte Hellmeier
 */
class FailedToReadResponseExceptionTest {

    @Test
    @SneakyThrows
    void constructor_messageArgument_throwFailedToReadResponseException() {
        // Arrange
        final var message = "errorMessage";
        final var failedToReadResponseException = new FailedToReadResponseException(message);

        // Act & Assert
        assertThat(failedToReadResponseException.getMessage()).isEqualTo(message);
    }

    @Test
    @SneakyThrows
    void constructor_messageThrowableArgument_throwFailedToReadResponseException() {
        // Arrange
        final var errorMessage = "errorMessage";
        final var causeMessage = "causeMessage";
        final var cause = new Exception(causeMessage);
        final var failedToReadResponseException = new FailedToReadResponseException(errorMessage,
            cause);

        // Act & Assert
        assertThat(failedToReadResponseException.getMessage()).isEqualTo(errorMessage);
        assertThat(failedToReadResponseException.getCause().getMessage()).isEqualTo(causeMessage);
    }

}
