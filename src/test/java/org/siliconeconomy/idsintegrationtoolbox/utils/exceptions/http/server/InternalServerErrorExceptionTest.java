/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.server;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * InternalServerErrorExceptionTest
 *
 * @author Malte Hellmeier
 */
class InternalServerErrorExceptionTest {

    final String ERROR_CODE = "500";

    @Test
    @SneakyThrows
    void constructor_noArgument_throwInternalServerErrorException() {
        // Arrange
        final var internalServerErrorException = new InternalServerErrorException();

        // Act & Assert
        assertThat(internalServerErrorException.getMessage()).contains(ERROR_CODE);
    }

    @Test
    @SneakyThrows
    void constructor_messageThrowableArgument_throwInternalServerErrorException() {
        // Arrange
        final var causeMessage = "causeMessage";
        final var cause = new Exception(causeMessage);
        final var internalServerErrorException = new InternalServerErrorException(cause);

        // Act & Assert
        assertThat(internalServerErrorException.getMessage()).contains(ERROR_CODE);
        assertThat(internalServerErrorException.getCause().getMessage()).isEqualTo(causeMessage);
    }

}
