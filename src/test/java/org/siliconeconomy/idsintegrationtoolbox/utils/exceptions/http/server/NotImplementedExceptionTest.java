/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.server;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * NotImplementedExceptionTest
 *
 * @author Malte Hellmeier
 */
class NotImplementedExceptionTest {

    final String ERROR_CODE = "501";

    @Test
    @SneakyThrows
    void constructor_noArgument_throwNotImplementedException() {
        // Arrange
        final var notImplementedException = new NotImplementedException();

        // Act & Assert
        assertThat(notImplementedException.getMessage()).contains(ERROR_CODE);
    }

    @Test
    @SneakyThrows
    void constructor_messageThrowableArgument_throwNotImplementedException() {
        // Arrange
        final var causeMessage = "causeMessage";
        final var cause = new Exception(causeMessage);
        final var notImplementedException = new NotImplementedException(cause);

        // Act & Assert
        assertThat(notImplementedException.getMessage()).contains(ERROR_CODE);
        assertThat(notImplementedException.getCause().getMessage()).isEqualTo(causeMessage);
    }

}
