/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils;

import java.io.FileNotFoundException;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.FileSystemResource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

/**
 * FileUtilsTest
 *
 * @author Haydar Qarawlus
 */
class FileUtilsTest {

    final String testFilePath = "src/test/resources/apache-camel/test-route.xml";

    @Test
    @SneakyThrows
    void readFile_correctInput_returnsFileObject() {
        // Act
        final var fileObject = FileUtils.readFile(testFilePath);
        // Assert
        assertThat(fileObject).isInstanceOf(FileSystemResource.class);
        assertThat(fileObject.isFile()).isTrue();
    }

    @Test
    @SneakyThrows
    void readFile_incorrectInput_throwsException() {
        // Arrange
        final var filePath = "Incorrect/path";
        // Act & Assert
        assertThrows(FileNotFoundException.class, () ->  FileUtils.readFile(filePath));
    }
}
