/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils;

import java.net.URI;
import java.time.ZonedDateTime;

import de.fraunhofer.iais.eis.PermissionImpl;
import de.fraunhofer.iais.eis.ProhibitionImpl;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.PolicyPattern;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * PolicyUtilsTest
 *
 * @author Steffen Biehs
 */
class PolicyUtilsTest {

    @Test
    @SneakyThrows
    void buildProvideAccessRule_returnRule() {
        // Arrange
        final var description = "provide-access";

        // Act
        final var result = PolicyUtils.buildProvideAccessRule("Provide Access rule");

        // Assert
        assertThat(result.getClass()).isEqualTo(PermissionImpl.class);
        assertThat(result.getDescription().get(0).getValue()).isEqualTo(description);
    }

    @Test
    @SneakyThrows
    void buildProhibitAccessRule_returnRule() {
        // Arrange
        final var description = "prohibit-access";

        // Act
        final var result = PolicyUtils.buildProhibitAccessRule("Prohibit Access Rule");

        // Assert
        assertThat(result.getClass()).isEqualTo(ProhibitionImpl.class);
        assertThat(result.getDescription().get(0).getValue()).isEqualTo(description);
    }

    @Test
    @SneakyThrows
    void buildNumberTimesUsageRule_returnRule() {
        // Arrange
        final var description = "n-times-usage";

        // Act
        final var result = PolicyUtils.buildNumberTimesUsageRule("usage rule", 5);

        // Assert
        assertThat(result.getClass()).isEqualTo(PermissionImpl.class);
        assertThat(result.getDescription().get(0).getValue()).isEqualTo(description);
    }

    @Test
    @SneakyThrows
    void buildDurationUsageRule_returnRule() {
        // Arrange
        final var description = "duration-usage";

        // Act
        final var result = PolicyUtils.buildDurationUsageRule("Duration Access Rule", "PT1M30.5S");

        // Assert
        assertThat(result.getClass()).isEqualTo(PermissionImpl.class);
        assertThat(result.getDescription().get(0).getValue()).isEqualTo(description);
    }

    @Test
    @SneakyThrows
    void buildIntervalUsageRule_returnRule() {
        // Arrange
        final var description = "usage-during-interval";

        // Act
        final var result = PolicyUtils.buildIntervalUsageRule("Interval Rule",
            ZonedDateTime.now(),
            ZonedDateTime.now());

        // Assert
        assertThat(result.getClass()).isEqualTo(PermissionImpl.class);
        assertThat(result.getDescription().get(0).getValue()).isEqualTo(description);
    }

    @Test
    @SneakyThrows
    void buildUsageUntilDeletionRule_returnRule() {
        // Arrange
        final var description = "usage-until-deletion";

        // Act
        final var result = PolicyUtils.buildUsageUntilDeletionRule("Usage until deletion rule",
            ZonedDateTime.now(), ZonedDateTime.now(), ZonedDateTime.now());

        // Assert
        assertThat(result.getClass()).isEqualTo(PermissionImpl.class);
        assertThat(result.getDescription().get(0).getValue()).isEqualTo(description);
    }

    @Test
    @SneakyThrows
    void buildUsageLoggingRule_returnRule() {
        // Arrange
        final var description = "usage-logging";

        // Act
        final var result = PolicyUtils.buildUsageLoggingRule("Usage Logging Rule");

        // Assert
        assertThat(result.getClass()).isEqualTo(PermissionImpl.class);
        assertThat(result.getDescription().get(0).getValue()).isEqualTo(description);
    }

    @Test
    @SneakyThrows
    void buildUsageNotificationRule_returnRule() {
        // Arrange
        final var description = "usage-notification";

        // Act
        final var result = PolicyUtils.buildUsageNotificationRule("Usage Notification Rule",
            URI.create("https://example.com"));

        // Assert
        assertThat(result.getClass()).isEqualTo(PermissionImpl.class);
        assertThat(result.getDescription().get(0).getValue()).isEqualTo(description);
    }

    @Test
    @SneakyThrows
    void buildConnectorRestrictedUsageRule_returnRule() {
        // Arrange
        final var description = "connector-restriction";

        // Act
        final var result = PolicyUtils.buildConnectorRestrictedUsageRule("Connector Restricted " +
            "Rule", URI.create("https://example.com"));

        // Assert
        assertThat(result.getClass()).isEqualTo(PermissionImpl.class);
        assertThat(result.getDescription().get(0).getValue()).isEqualTo(description);
    }

    @Test
    @SneakyThrows
    void buildSecurityProfileRestrictedUsageRule_returnRule() {
        // Arrange
        final var description = "security-level-restriction";

        // Act
        final var result = PolicyUtils.buildSecurityProfileRestrictedUsageRule("Example Usage " +
            "Policy", "BASE_SECURITY_PROFILE");

        // Assert
        assertThat(result.getClass()).isEqualTo(PermissionImpl.class);
        assertThat(result.getDescription().get(0).getValue()).isEqualTo(description);
    }

    @Test
    @SneakyThrows
    void removePolicyPatternSurroundings_returnString() {
        // Arrange
        final var policyPatternResponse = "PROVIDE_ACCESS";

        // Act
        final var result = PolicyUtils.removePolicyPatternSurroundings(policyPatternResponse);

        // Assert
        assertThat(PolicyPattern.valueOf(result)).isEqualTo(PolicyPattern.PROVIDE_ACCESS);
    }

}
