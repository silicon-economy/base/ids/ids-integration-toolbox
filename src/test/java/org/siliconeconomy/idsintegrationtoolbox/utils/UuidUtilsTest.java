/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils;

import java.net.URI;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.EmptyFieldException;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.AgreementLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.Link;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.AgreementOutput;
import org.springframework.test.util.ReflectionTestUtils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

/**
 * UuidUtilsTest
 *
 * @author Florian Zimmer
 */
class UuidUtilsTest {

    @Test
    void getUuidFromOutput_returnUuid() throws EmptyFieldException {
        // Arrange
        final var uuid = "6f457874-6c05-4525-928e-717309a45597";
        final var output = new AgreementOutput();
        final var links = new AgreementLinks();
        final var link = new Link();

        ReflectionTestUtils.setField(link, "href", "http://localhost:8080/api/agreements/" + uuid);
        ReflectionTestUtils.setField(links, "self", link);
        ReflectionTestUtils.setField(output, "links", links);

        // Act
        final var result = UuidUtils.getUuidFromOutput(output);

        // Assert
        assertThat(result).isEqualTo(UUID.fromString(uuid));
    }

    @Test
    void getUuidFromOutput_wrongInput_throwException() {
        // Arrange
        final var output = new AgreementOutput();

        // Act & Assert
        assertThatThrownBy(() -> UuidUtils.getUuidFromOutput(output))
            .isInstanceOf(EmptyFieldException.class);
    }

    @Test
    void getUuidFromUri_returnUuid() throws EmptyFieldException {
        // Arrange
        final var uuid = "06dc1644-f58d-4562-836f-197c0a65c0d6";
        final var example_uri = URI.create("http://localhost:8080/api/example/" + uuid);


        // Act
        final var result = UuidUtils.getUuidFromUri(example_uri);

        // Assert
        assertThat(result).isEqualTo(UUID.fromString(uuid));
    }

    @Test
    void getUuidFromUri_wrongInput_throwException() {
        // Arrange
        final var output = URI.create("http://example.com");

        // Act & Assert
        assertThatThrownBy(() -> UuidUtils.getUuidFromUri(output))
            .isInstanceOf(EmptyFieldException.class);
    }
}
