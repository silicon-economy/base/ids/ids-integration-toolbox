/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;

import lombok.SneakyThrows;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.FailedToReadResponseException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.client.BadRequestException;
import org.siliconeconomy.idsintegrationtoolbox.model.RestRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.Base64Utils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.StreamUtils;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * RestRequestHandlerTest
 *
 * @author Haydar Qarawlus
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {RestRequestHandler.class})
class RestRequestHandlerTest {

    /** Class under test */
    @Autowired
    private RestRequestHandler handler;

    @MockBean
    private RestTemplate restTemplate;

    private final HttpMethod METHOD = HttpMethod.GET;

    private final String URL = "https://base-url.com";

    private final String PATH = "some-path";

    @Captor
    private ArgumentCaptor<HttpEntity<String>> requestCaptor;

    @Test
    @SneakyThrows
    void send_successfulRequest_returnResponse() {
        // Arrange
        final var responseBody = "I AM THE RESPONSE";
        final var responseEntity = new ResponseEntity<>(responseBody, HttpStatus.OK);
        final var headerKey = "header-key";
        final var headerValue = "header-value";
        final var body = "body";

        final var request = new RestRequestBuilder(METHOD, URL, PATH)
            .header(headerKey, headerValue)
            .body(body).build();

        when(restTemplate.exchange(any(), any(), any(), eq(String.class)))
            .thenReturn(responseEntity);

        // Act
        final var result = handler.sendRequest(request);

        // Assert
        assertEquals(responseBody, result);

        final var expectedUri = URI.create(URL + "/" + PATH);
        verify(restTemplate, times(1))
            .exchange(eq(expectedUri), eq(METHOD), requestCaptor.capture(), eq(String.class));

        final var capturedRequest = requestCaptor.getValue();
        assertNull(capturedRequest.getHeaders().get("Authorization"));
    }

    @Test
    @SneakyThrows
    void send_successfulRequestWithBodyHeadersAuthAndParams_returnResponse() {
        // Arrange
        final var headerKey = "header-key";
        final var headerValue = "header-value";
        final var paramKey = "param-key";
        final var paramValue = "param-value";
        final var username = "username";
        final var password = "password";
        final var body = "body";

        final var responseBody = "I AM THE RESPONSE";
        final var responseEntity = new ResponseEntity<>(responseBody, HttpStatus.OK);

        final var request = new RestRequestBuilder(METHOD, URL, PATH)
            .header(headerKey, headerValue)
            .queryParameter(paramKey, paramValue)
            .basicAuth(username, password)
            .body(body).build();

        when(restTemplate.exchange(any(), any(), any(), eq(String.class)))
            .thenReturn(responseEntity);

        // Act
        final var result = handler.sendRequest(request);

        // Assert
        assertEquals(responseBody, result);

        final var expectedUri = URI.create(URL + "/" + PATH + "?" + paramKey + "=" + paramValue);
        verify(restTemplate, times(1))
            .exchange(eq(expectedUri), eq(METHOD), requestCaptor.capture(), eq(String.class));

        final var capturedRequest = requestCaptor.getValue();
        assertEquals(body, capturedRequest.getBody());

        final var headers = capturedRequest.getHeaders();
        final var expectedAuthHeader = "Basic " + new String(Base64Utils.encode(
            (username + ":" + password).getBytes(StandardCharsets.UTF_8)));
        assertNotNull(headers.get("Authorization"));
        assertEquals(1, headers.get("Authorization").size());
        assertEquals(expectedAuthHeader, headers.get("Authorization").get(0));
        assertNotNull(headers.get(headerKey));
        assertEquals(1, headers.get(headerKey).size());
        assertEquals(headerValue, headers.get(headerKey).get(0));
    }

    @Test
    @SneakyThrows
    void send_successfulRequestWithHeadersAndParams_returnResponse() {
        // Arrange
        final var headerKey = "header-key";
        final var headerValue = "header-value";
        final var paramKey = "param-key";
        final var paramValue = "param-value";
        final String[] multiplePathSegments = {"path1", "path2"};

        final var responseBody = "I AM THE RESPONSE";
        final var responseEntity = new ResponseEntity<>(responseBody, HttpStatus.OK);

        final var request = new RestRequestBuilder(METHOD, URL, multiplePathSegments)
            .header(headerKey, headerValue)
            .queryParameter(paramKey, paramValue)
            .basicAuth(null, null)
            .body(null).build();

        when(restTemplate.exchange(any(), any(), any(), eq(String.class)))
            .thenReturn(responseEntity);

        // Act
        final var result = handler.sendRequest(request);

        // Assert
        assertEquals(responseBody, result);

        final var expectedUri = URI.create(URL + "/" + multiplePathSegments[0] + "/"
            + multiplePathSegments[1] + "?" + paramKey + "=" + paramValue);
        verify(restTemplate, times(1))
            .exchange(eq(expectedUri), eq(METHOD), requestCaptor.capture(), eq(String.class));

        final var capturedRequest = requestCaptor.getValue();
        final var headers = capturedRequest.getHeaders();
        assertNull(headers.get("Authorization"));
        assertNotNull(headers.get(headerKey));
        assertEquals(1, headers.get(headerKey).size());
        assertEquals(headerValue, headers.get(headerKey).get(0));
    }

    @Test
    @SneakyThrows
    void send_withUsernameButPasswordNull_sendRequestWithoutAuth() {
        // Arrange
        final var responseBody = "I AM THE RESPONSE";
        final var responseEntity = new ResponseEntity<>(responseBody, HttpStatus.OK);

        final var request = new RestRequestBuilder(METHOD, URL, "/")
            .basicAuth("username", null)
            .body(null).build();

        when(restTemplate.exchange(any(), any(), any(), eq(String.class)))
            .thenReturn(responseEntity);

        // Act
        final var result = handler.sendRequest(request);

        // Assert
        final var expectedUri = URI.create(URL + "/");
        verify(restTemplate, times(1))
            .exchange(eq(expectedUri), eq(METHOD), requestCaptor.capture(), eq(String.class));

        final var capturedRequest = requestCaptor.getValue();
        final var headers = capturedRequest.getHeaders();
        assertNull(headers.get("Authorization"));
    }

    @Test
    @SneakyThrows
    void send_withPasswordButUsernameNull_sendRequestWithoutAuth() {
        // Arrange
        final var responseBody = "I AM THE RESPONSE";
        final var responseEntity = new ResponseEntity<>(responseBody, HttpStatus.OK);

        final var request = new RestRequestBuilder(METHOD, URL, "/")
            .basicAuth(null, "password")
            .body(null).build();

        when(restTemplate.exchange(any(), any(), any(), eq(String.class)))
            .thenReturn(responseEntity);

        // Act
        final var result = handler.sendRequest(request);

        // Assert
        final var expectedUri = URI.create(URL + "/");
        verify(restTemplate, times(1))
            .exchange(eq(expectedUri), eq(METHOD), requestCaptor.capture(), eq(String.class));

        final var capturedRequest = requestCaptor.getValue();
        final var headers = capturedRequest.getHeaders();
        assertNull(headers.get("Authorization"));
    }

    @Test
    void send_errorSendingRequest_throwRestClientException() {
        // Arrange
        final var request = getRequest();

        when(restTemplate.exchange(any(), any(), any(), eq(String.class)))
            .thenThrow(new RestClientException("error"));

        // Act & Assert
        assertThrows(RestClientException.class, () -> handler.sendRequest(request));
    }

    @Test
    void send_errorStatusCodeReturned_throwApiInteractionUnsuccessfulException() {
        // Arrange
        final var responseEntity = new ResponseEntity<>("body", HttpStatus.BAD_REQUEST);

        when(restTemplate.exchange(any(), any(), any(), eq(String.class)))
            .thenReturn(responseEntity);

        // Act & Assert
        final var exception = assertThrows(ApiInteractionUnsuccessfulException.class,
            () -> handler.sendRequest(getRequest()));
        assertTrue(exception.getCause() instanceof BadRequestException);
    }

    @Test
    void sendDataRequest_errorSendingRequest_throwRestClientException() {
        // Arrange
        final var request = getRequest();
        when(restTemplate.exchange(any(), any(), any(), eq(byte[].class)))
            .thenThrow(new RestClientException("error"));

        // Act & Assert
        assertThrows(RestClientException.class,
            () -> handler.sendDataRequest(request));
    }

    @Test
    void sendDataRequest_responseBodyNull_throwFailedToReadResponseException() {
        // Arrange
        final var responseEntity = new ResponseEntity<byte[]>(HttpStatus.OK);

        when(restTemplate.exchange(any(), any(), any(), eq(byte[].class)))
            .thenReturn(responseEntity);

        // Act & Assert
        final var exception = assertThrows(FailedToReadResponseException.class,
            () -> handler.sendDataRequest(getRequest()));
        assertNull(exception.getCause());
    }

    @Test
    void sendDataRequest_responseBodyExists_returnObject() throws ApiInteractionUnsuccessfulException {

        // Arrange
        final var rawBytes = "Body".getBytes(StandardCharsets.UTF_8);
        final LinkedMultiValueMap<String, String> headerMap = new LinkedMultiValueMap<>();
        headerMap.add("Content-Type", MediaType.APPLICATION_OCTET_STREAM.toString());
        final var responseEntity = new ResponseEntity<byte[]>(rawBytes,
            headerMap,
            HttpStatus.OK);

        when(restTemplate.exchange(any(), any(), any(), eq(byte[].class)))
            .thenReturn(responseEntity);

        // Act
        final var resourceData = handler.sendDataRequest(getRequest());
        // Assert
        Assertions.assertThat(resourceData.getRawData()).isEqualTo(rawBytes);
        Assertions.assertThat(resourceData.getMimeType()).isEqualTo(MediaType.APPLICATION_OCTET_STREAM.toString());
    }

    @Test
    void sendDataRequest_ioException_throwFailedToReadResponseException() {
        // Arrange
        final var response = "responseBody".getBytes(StandardCharsets.UTF_16);
        final var responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
        final var utilities = Mockito.mockStatic(StreamUtils.class);

        when(restTemplate.exchange(any(), any(), any(), eq(byte[].class)))
            .thenReturn(responseEntity);
        utilities.when(() -> StreamUtils.copy(eq(response), any(OutputStream.class)))
            .thenThrow(new IOException());

        // Act & Assert
        assertThrows(FailedToReadResponseException.class,
            () -> handler.sendDataRequest(getRequest()));
    }

    /**********************************************************************************************
        Utilities
     **********************************************************************************************/

    private RestRequest getRequest() {
        final var headers = new HashMap<String, String>();
        headers.put("key", "value");

        final var queryParams = new LinkedMultiValueMap<String, String>();
        queryParams.add("key", "value");

        final var pathVars = new HashMap<String, String>();
        pathVars.put("key", "value");

        return new RestRequest(HttpMethod.GET,
            URI.create(URL + "/" + PATH).toString(),
            "endpoint",
            new String[]{"String"},
            "username",
            "password",
            "body",
            headers,
            queryParams,
            pathVars
        );
    }


}
