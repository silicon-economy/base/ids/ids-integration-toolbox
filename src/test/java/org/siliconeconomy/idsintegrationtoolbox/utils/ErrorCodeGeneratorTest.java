/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.HttpCommunicationException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.client.BadRequestException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.client.ExpectationFailedException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.client.NotFoundException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.client.RequestTimeoutException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.client.UnauthorizedException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.client.WrongMediaTypeException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.server.BadGatewayException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.server.InternalServerErrorException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.server.NotImplementedException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.server.ServiceUnavailableException;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

/**
 * ErrorCodeGeneratorTest
 *
 * @author Haydar Qarawlus
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {ErrorCodeGenerator.class})
class ErrorCodeGeneratorTest {

    /** Class under test */
    ErrorCodeGenerator generator = Mockito.spy(ErrorCodeGenerator.class);

    @Test
    void checkHttpCode_statusCode200_doNotThrowException() {
        // Arrange
        final var response = new ResponseEntity<Integer>(HttpStatus.OK);

        // Act & Assert
        assertDoesNotThrow(() -> generator.checkHttpCode(response.getStatusCode()));
    }

    @Test
    void checkHttpCode_statusCode401_throwUnauthorizedException() {
        // Arrange
        final var response = new ResponseEntity<Integer>(HttpStatus.UNAUTHORIZED);

        // Act & Assert
        assertThatThrownBy(() -> generator.checkHttpCode(response.getStatusCode()))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new UnauthorizedException());
    }

    @Test
    void checkHttpCodeT_statusCode404_throwNotFoundException() {
        // Arrange
        final var response = new ResponseEntity<Integer>(HttpStatus.NOT_FOUND);

        // Act & Assert
        assertThatThrownBy(() -> generator.checkHttpCode(response.getStatusCode()))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotFoundException());
    }

    @Test
    void checkHttpCode_statusCode417_throwExpectationFailedException() {
        // Arrange
        final var response = new ResponseEntity<Integer>(HttpStatus.EXPECTATION_FAILED);

        // Act & Assert
        assertThatThrownBy(() -> generator.checkHttpCode(response.getStatusCode()))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new ExpectationFailedException());
    }

    @Test
    void checkHttpCode_statusCode415_throwWrongMediaTypeException() {
        // Arrange
        final var response = new ResponseEntity<Integer>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);

        // Act & Assert
        assertThatThrownBy(() -> generator.checkHttpCode(response.getStatusCode()))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new WrongMediaTypeException());
    }

    @Test
    void checkHttpCode_statusCode400_throwBadRequestException() {
        // Arrange
        final var response = new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);

        // Act & Assert
        assertThatThrownBy(() -> generator.checkHttpCode(response.getStatusCode()))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new BadRequestException());
    }

    @Test
    void checkHttpCode_statusCode408_throwRequestTimeoutException() {
        // Arrange
        final var response = new ResponseEntity<Integer>(HttpStatus.REQUEST_TIMEOUT);

        // Act & Assert
        assertThatThrownBy(() -> generator.checkHttpCode(response.getStatusCode()))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new RequestTimeoutException());
    }

    @Test
    void checkHttpCode_statusCode500_throwInternalServerErrorException() {
        // Arrange
        final var response = new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);

        // Act & Assert
        assertThatThrownBy(() -> generator.checkHttpCode(response.getStatusCode()))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new InternalServerErrorException());
    }

    @Test
    void checkHttpCode_statusCode503_throwServiceUnavailableException() {
        // Arrange
        final var response = new ResponseEntity<Integer>(HttpStatus.SERVICE_UNAVAILABLE);

        // Act & Assert
        assertThatThrownBy(() -> generator.checkHttpCode(response.getStatusCode()))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new ServiceUnavailableException());
    }

    @Test
    void checkHttpCode_statusCode502_throwBadGatewayException() {
        // Arrange
        final var response = new ResponseEntity<Integer>(HttpStatus.BAD_GATEWAY);

        // Act & Assert
        assertThatThrownBy(() -> generator.checkHttpCode(response.getStatusCode()))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new BadGatewayException());
    }

    @Test
    void checkHttpCode_statusCode501_throwNotImplementedException() {
        // Arrange
        final var response = new ResponseEntity<Integer>(HttpStatus.NOT_IMPLEMENTED);

        // Act & Assert
        assertThatThrownBy(() -> generator.checkHttpCode(response.getStatusCode()))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new NotImplementedException());
    }

    @Test
    void checkHttpCode_unknownStatusCode_throwHttpCommunicationException() {
        // Arrange
        final var response = new ResponseEntity<Integer>(HttpStatus.PAYLOAD_TOO_LARGE);

        // Act & Assert
        assertThatThrownBy(() -> generator.checkHttpCode(response.getStatusCode()))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class)
            .hasCause(new HttpCommunicationException("Unknown error code: "
                + response.getStatusCode()));
    }


}
