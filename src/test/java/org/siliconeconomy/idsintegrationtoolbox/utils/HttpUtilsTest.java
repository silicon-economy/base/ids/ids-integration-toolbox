/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils;

import java.util.List;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * HttpUtilsTest
 *
 * @author Ronja Quensel
 */
class HttpUtilsTest {

    @Test
    void getDefaultHttpHeaders_returnDefaultHttpHeaders(){
        // Act
        final var result = HttpUtils.getDefaultHttpHeaders();

        // Assert
        assertNotNull(result);
        assertEquals(HttpUtils.JSON, result.getContentType());
        assertEquals(3, result.getAccept().size());
        assertTrue(result.getAccept().containsAll(List.of(HttpUtils.HAL,
            HttpUtils.JSON, HttpUtils.JSON_LD)));
    }
}
