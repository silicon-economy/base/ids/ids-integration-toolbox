/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils;

import org.junit.jupiter.api.Test;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.InvalidInputException;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

/**
 * ValidationUtilsTest
 *
 * @author Ronja Quensel
 */
class ValidationUtilsTest {

    private final String errorMessage = "error message";

    @Test
    void assertNotNull_objectNotNull_doNothing() {
        // Act & Assert
        assertDoesNotThrow(() -> ValidationUtils.assertNotNull("I am not null", errorMessage));
    }

    @Test
    void assertNotNull_objectNull_doNothing() {
        // Act & Assert
        assertThatThrownBy(() -> ValidationUtils.assertNotNull(null, errorMessage))
            .isInstanceOf(InvalidInputException.class)
            .hasMessage(errorMessage);
    }

}
