/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpMethod;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * RestRequestBuilderTest
 *
 * @author Haydar Qarawlus
 */
class RestRequestGenericBuilderTest {

    private final String ENDPOINT = "endpoint";
    private final String BASEURL = "baseURL";
    private final HttpMethod HTTPMETHOD = HttpMethod.POST;
    private final String[] ENDPOINT_SEGMENTS = {"endpoint1", "endpoint2"};

    @Test
    void constructor_pathArgument_returnRestRequest() {
        // Act
        final var request = new RestRequestBuilder(HTTPMETHOD, BASEURL, ENDPOINT)
            .build();

        // Assert
        assertThat(request.getEndpointPath()).isEqualTo(ENDPOINT);
        assertThat(request.getBaseUrl()).isEqualTo(BASEURL);
        assertThat(request.getHttpMethod()).isEqualTo(HTTPMETHOD);
    }

    @Test
    void constructor_pathSegmentsArgument_returnRestRequest() {
        // Act
        final var request = new RestRequestBuilder(HTTPMETHOD, BASEURL, ENDPOINT_SEGMENTS)
            .build();

        // Assert
        assertThat(request.getEndpointPathSegments()).isEqualTo(ENDPOINT_SEGMENTS);
        assertThat(request.getBaseUrl()).isEqualTo(BASEURL);
        assertThat(request.getHttpMethod()).isEqualTo(HTTPMETHOD);
    }

    @Test
    void body_successfulBuild_returnRestRequestWithBody() {
        // Arrange
        final String body = "body";

        // Act
        final var request = new RestRequestBuilder(HTTPMETHOD, BASEURL, ENDPOINT)
            .body(body)
            .build();

        // Assert
        assertThat(request.getBody()).isEqualTo(body);
    }

    @Test
    void basicAuth_successfulBuild_returnRestRequestWithBasicAuth() {
        // Arrange
        final String username = "username";
        final String password = "password";

        // Act
        final var request = new RestRequestBuilder(HTTPMETHOD, BASEURL, ENDPOINT)
            .basicAuth(username, password)
            .build();

        // Assert
        assertThat(request.getUsername()).isEqualTo(username);
        assertThat(request.getPassword()).isEqualTo(password);
    }

    @Test
    void header_successfulBuild_returnRestRequestWithHeader() {
        // Arrange
        final String header = "headerValue";

        // Act
        final var request = new RestRequestBuilder(HTTPMETHOD, BASEURL, ENDPOINT)
            .header("header", header)
            .build();

        // Assert
        assertThat(request.getHeaders().get("header")).isEqualTo(header);
    }

    @Test
    void queryParameter_successfulBuild_returnRestRequestWithQueryParameter() {
        // Arrange
        final String queryParameter = "queryValue";

        // Act
        final var request = new RestRequestBuilder(HTTPMETHOD, BASEURL, ENDPOINT)
            .queryParameter("queryParameter", queryParameter)
            .build();

        // Assert
        assertThat(request.getQueryParameters().get("queryParameter").get(0))
            .isEqualTo(queryParameter);
    }

    @Test
    void pathVariable_successfulBuild_returnRestRequestWithPathVariable() {
        // Arrange
        final String pathVariable = "pathValue";

        // Act
        final var request = new RestRequestBuilder(HTTPMETHOD, BASEURL, ENDPOINT)
            .pathVariable("pathVariable", pathVariable)
            .build();

        // Assert
        assertThat(request.getPathVariables().get("pathVariable"))
            .isEqualTo(pathVariable);
    }
}
