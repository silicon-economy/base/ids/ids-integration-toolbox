/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils;

import java.security.Permission;

import lombok.SneakyThrows;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * HealthCheckerTest
 *
 * @author Florian Zimmer
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(
    classes = {HealthChecker.class},
    properties = {"connector.health-check.delay=1", "connector.health-check.tries=10"}
)
class HealthCheckerTest {

    /** Class under test */
    @Autowired
    private HealthChecker healthChecker;

    @MockBean
    private RestRequestHandler requestHandler;

    @BeforeEach
    void init() {
        System.setSecurityManager(new InterceptExitSecurityManager());
    }

    @AfterEach
    void shutDown() {
        System.setSecurityManager(null);
    }

    @Test
    @SneakyThrows
    void isConnectorReachable_successfulRequest_returnTrue() {
        // Arrange
        when(requestHandler.sendRequest(any())).thenReturn("response");

        // Act
        final var result = healthChecker.isConnectorReachable();

        // Assert
        assertThat(result).isTrue();
    }

    @Test
    @SneakyThrows
    void isConnectorReachable_errorStatusCodeReturned_returnFalse() {
        // Arrange
        when(requestHandler.sendRequest(any()))
            .thenThrow(new ApiInteractionUnsuccessfulException("Failed!"));

        // Act
        final var result = healthChecker.isConnectorReachable();

        // Assert
        assertThat(result).isFalse();
    }

    @Test
    @SneakyThrows
    void verifyConnection_noExitOnError_successfulRequest_doNotThrowException() {
        // Arrange
        when(requestHandler.sendRequest(any())).thenReturn("response");

        // Act && Assert
        assertDoesNotThrow(() -> healthChecker.verifyConnection(false));
    }

    @Test
    @SneakyThrows
    void verifyConnection_noExitOnError_errorStatusCodeReturned_throwException() {
        // Arrange
        when(requestHandler.sendRequest(any()))
            .thenThrow(new ApiInteractionUnsuccessfulException("Failed!"));

        // Act && Assert
        assertThrows(ApiInteractionUnsuccessfulException.class,
            () -> healthChecker.verifyConnection(false));
    }

    @Test
    @SneakyThrows
    void verifyConnection_exitOnError_successfulRequest_doNothing() {
        // Arrange
        when(requestHandler.sendRequest(any())).thenReturn("response");

        // Act && Assert
        assertDoesNotThrow(() -> healthChecker.verifyConnection(true));
    }

    @Test
    @SneakyThrows
    void verifyConnection_exitOnError_errorStatusCodeReturned_systemExit() {
        // Arrange
        when(requestHandler.sendRequest(any()))
            .thenThrow(new ApiInteractionUnsuccessfulException("Failed!"));

        // Act && Assert
        assertThrows(ExpectedExitException.class, () -> healthChecker.verifyConnection(true));
    }

    /**********************************************************************************************
        Utilities

        Configure security manager to throw a custom exception when System.exit() is called, as the
        HealthChecker calls System.exit() if the connector is not reachable. Therefore, the tests
        for the unsuccessful case would always crash.
    ***********************************************************************************************/

    protected static class ExpectedExitException extends SecurityException {
        public final int status;

        public ExpectedExitException(int status) {
            super("System.exit() called.");
            this.status = status;
        }
    }

    private static class InterceptExitSecurityManager extends SecurityManager {
        @Override
        public void checkPermission(Permission permission) {}

        @Override
        public void checkPermission(Permission permission, Object context) {}

        @Override
        public void checkExit(int status) {
            super.checkExit(status);
            throw new ExpectedExitException(status);
        }
    }
}
