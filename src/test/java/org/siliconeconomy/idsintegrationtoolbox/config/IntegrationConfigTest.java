/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * IntegrationConfigTest
 *
 * @author Haydar Qarawlus
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {IntegrationConfig.class})
@TestPropertySource(locations = "classpath:example.properties")
class IntegrationConfigTest {

    /** Class under test */
    @Autowired
    private IntegrationConfig integrationConfig;

    @Test
    @SneakyThrows
    void restTemplate_createAndReturnBeanWithTrustStore() {
        // Act
        final RestTemplate result = integrationConfig.restTemplate();

        // Assert
        assertThat(result).isInstanceOf(RestTemplate.class);
        assertThat(result.getRequestFactory()).isInstanceOf(HttpComponentsClientHttpRequestFactory.class);
    }

    @Test
    @SneakyThrows
    void restTemplate_trustStoreLocationNull_createAndReturnNormalBean() {
        // Arrange
        final var integrationConfig2 = new IntegrationConfig();
        // Act
        final RestTemplate result = integrationConfig2.restTemplate();

        // Assert
        assertThat(result).isInstanceOf(RestTemplate.class);
        assertThat(result.getRequestFactory()).isInstanceOf(SimpleClientHttpRequestFactory.class);

    }

    @Test
    @SneakyThrows
    void restTemplate_trustStoreLocationEmpty_createAndReturnNormalBean() {
        // Arrange
        final var integrationConfig2 = new IntegrationConfig();
        ReflectionTestUtils.setField(integrationConfig2, "trustStoreLocation", "");
        // Act
        final RestTemplate result = integrationConfig2.restTemplate();

        // Assert
        assertThat(result).isInstanceOf(RestTemplate.class);
        assertThat(result.getRequestFactory()).isInstanceOf(SimpleClientHttpRequestFactory.class);

    }


    @Test
    void overrideObjectMapperBean_createAndReturnBean() {
        // Act
        final var result = integrationConfig.overrideObjectMapperBean();

        // Assert
        assertThat(result).isInstanceOf(ObjectMapper.class);
    }
}
