/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.config;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.client.ClientHttpResponse;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.verifyNoInteractions;

/**
 * RestTemplateErrorHandlerTest
 *
 * @author Ronja Quensel
 */
@ExtendWith(MockitoExtension.class)
class RestTemplateErrorHandlerTest {

    private RestTemplateErrorHandler handler = new RestTemplateErrorHandler();

    @Mock
    private ClientHttpResponse response;

    @Test
    @SneakyThrows
    void hasError_responseNotNull_returnFalse() {
        // Act
        final var result = handler.hasError(response);

        // Assert
        assertThat(result).isFalse();
    }

    @Test
    @SneakyThrows
    void hasError_responseNull_returnFalse() {
        // Act && Assert
        assertThatThrownBy(() -> handler.hasError(null))
            .isInstanceOf(NullPointerException.class);
    }

    @Test
    @SneakyThrows
    void handleError_responseNotNull_doNothing() {
        // Act
        handler.handleError(response);

        // Assert
        verifyNoInteractions(response);
    }

    @Test
    @SneakyThrows
    void handleError_responseNull_doNothing() {
        // Act && Assert
        assertThatThrownBy(() -> handler.handleError(null))
            .isInstanceOf(NullPointerException.class);
    }

}
