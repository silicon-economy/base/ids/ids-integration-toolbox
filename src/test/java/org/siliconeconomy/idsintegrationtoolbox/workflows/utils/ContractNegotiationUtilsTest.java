/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.workflows.utils;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import de.fraunhofer.iais.eis.AbstractConstraint;
import de.fraunhofer.iais.eis.Action;
import de.fraunhofer.iais.eis.Artifact;
import de.fraunhofer.iais.eis.ArtifactBuilder;
import de.fraunhofer.iais.eis.ContractOfferBuilder;
import de.fraunhofer.iais.eis.IANAMediaTypeBuilder;
import de.fraunhofer.iais.eis.PermissionBuilder;
import de.fraunhofer.iais.eis.Representation;
import de.fraunhofer.iais.eis.RepresentationBuilder;
import de.fraunhofer.iais.eis.RepresentationInstance;
import de.fraunhofer.iais.eis.Resource;
import de.fraunhofer.iais.eis.ResourceBuilder;
import de.fraunhofer.iais.eis.Rule;
import de.fraunhofer.iais.eis.Value;
import de.fraunhofer.iais.eis.ValueBuilder;
import de.fraunhofer.iais.eis.util.TypedLiteral;
import de.fraunhofer.iais.eis.util.Util;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.idsintegrationtoolbox.utils.ContractNegotiationUtils;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * ContractNegotiationUtilsTest
 *
 * @author Ronja Quensel
 */
class ContractNegotiationUtilsTest {

    @Test
    void getAllArtifactsForResource_validInput_returnArtifactsIds() {
        // Arrange
        final var artifact1Id = URI.create("https://artifact1");
        final var artifact2Id = URI.create("https://artifact2");

        final var artifact1 = getArtifact(artifact1Id);
        final var artifact2 = getArtifact(artifact2Id);
        final var representation = getRepresentation(Util.asList(artifact1, artifact2));
        final var resource = getResource(Util.asList(representation));

        // Act
        final var result = ContractNegotiationUtils.getAllArtifactsForResource(resource);

        // Assert
        assertEquals(2, result.size());
        assertTrue(result.containsAll(Util.asList(artifact1Id, artifact2Id)));
    }

    @Test
    void getAllArtifactsForResource_representationsNull_returnEmptyList() {
        // Arrange
        final var resource = getResource(null);

        // Act
        final var result = ContractNegotiationUtils.getAllArtifactsForResource(resource);

        // Assert
        assertTrue(result.isEmpty());
    }

    @Test
    void getAllArtifactsForResource_representationInstanceNull_returnEmptyList() {
        // Arrange
        final var representation = getRepresentation(null);
        final var resource = getResource(Util.asList(representation));

        // Act
        final var result = ContractNegotiationUtils.getAllArtifactsForResource(resource);

        // Assert
        assertTrue(result.isEmpty());
    }

    @Test
    void getAllArtifactsForResource_representationInstanceContainsOnlyValue_returnEmptyList() {
        // Arrange
        final var value = getValue(URI.create("https://value"));
        final var representation = getRepresentation(Util.asList(value));
        final var resource = getResource(Util.asList(representation));

        // Act
        final var result = ContractNegotiationUtils.getAllArtifactsForResource(resource);

        // Assert
        assertTrue(result.isEmpty());
    }

    @Test
    void getArtifactsForResourceById_validInput_returnArtifactsIds() {
        // Arrange
        final var artifact1Id = URI.create("https://artifact1");
        final var artifact2Id = URI.create("https://artifact2");

        final var artifact1 = getArtifact(artifact1Id);
        final var artifact2 = getArtifact(artifact2Id);
        final var representation = getRepresentation(Util.asList(artifact1, artifact2));
        final var resource = getResource(Util.asList(representation));

        // Act
        final var result = ContractNegotiationUtils
            .getArtifactsForResourceById(resource, Util.asList(artifact1Id, artifact2Id));

        // Assert
        assertEquals(2, result.size());
        assertTrue(result.containsAll(Util.asList(artifact1Id, artifact2Id)));
    }

    @Test
    void getArtifactsForResourceById_representationsNull_returnEmptyList() {
        // Arrange
        final var resource = getResource(null);

        // Act
        final var result = ContractNegotiationUtils
            .getArtifactsForResourceById(resource, Util.asList(URI.create("https://artifact")));

        // Assert
        assertTrue(result.isEmpty());
    }

    @Test
    void getArtifactsForResourceById_representationInstanceNull_returnEmptyList() {
        // Arrange
        final var representation = getRepresentation(null);
        final var resource = getResource(Util.asList(representation));

        // Act
        final var result = ContractNegotiationUtils
            .getArtifactsForResourceById(resource, Util.asList(URI.create("https://artifact")));

        // Assert
        assertTrue(result.isEmpty());
    }

    @Test
    void getArtifactsForResourceById_representationInstanceContainsOnlyValue_returnEmptyList() {
        // Arrange
        final var artifactId = URI.create("https://value");
        final var value = getValue(artifactId);
        final var representation = getRepresentation(Util.asList(value));
        final var resource = getResource(Util.asList(representation));

        // Act
        final var result = ContractNegotiationUtils
            .getArtifactsForResourceById(resource, Util.asList(artifactId));

        // Assert
        assertTrue(result.isEmpty());
    }

    @Test
    void addTargetToRule_unknownRuleType_throwIllegalArgumentException() {
        // Arrange
        final var rule = new CustomRule();
        final var target = URI.create("https://target");

        // Act & Assert
        assertThatThrownBy(() -> ContractNegotiationUtils.addTargetToRule(rule, target))
            .isInstanceOf(IllegalArgumentException.class);
    }

    /**********************************************************************************************
     Utilities
     **********************************************************************************************/

    private Artifact getArtifact(final URI id) {
        return new ArtifactBuilder(id)
            ._checkSum_("checksum")
            ._fileName_("artifact")
            .build();
    }

    private Value getValue(final URI id) {
        return new ValueBuilder(id).build();
    }

    private Representation getRepresentation(final ArrayList<RepresentationInstance> artifacts) {
        return new RepresentationBuilder()
            ._instance_(artifacts)
            ._representationStandard_(URI.create("https://standard.com"))
            ._mediaType_(new IANAMediaTypeBuilder()
                ._filenameExtension_("json")
                .build())
            .build();
    }

    private Resource getResource(final ArrayList<Representation> representations) {
        return new ResourceBuilder()
            ._representation_(representations)
            ._contractOffer_(Util.asList(new ContractOfferBuilder()
                ._permission_(Util.asList(new PermissionBuilder()
                    ._action_(Util.asList(Action.USE))
                    .build()))
                .build()))
            .build();
    }

    private static class CustomRule implements Rule {

        @Override
        public @NotNull URI getId() {
            return null;
        }

        @Override
        public List<TypedLiteral> getLabel() {
            return null;
        }

        @Override
        public List<TypedLiteral> getComment() {
            return null;
        }

        @Override
        public String toRdf() {
            return null;
        }

        @Override
        public Map<String, Object> getProperties() {
            return null;
        }

        @Override
        public void setProperty(String s, Object o) {

        }

        @Override
        public List<TypedLiteral> getTitle() {
            return null;
        }

        @Override
        public void setTitle(List<TypedLiteral> list) {

        }

        @Override
        public List<TypedLiteral> getDescription() {
            return null;
        }

        @Override
        public void setDescription(List<TypedLiteral> list) {

        }

        @Override
        public @NotEmpty List<Action> getAction() {
            return null;
        }

        @Override
        public void setAction(List<Action> list) {

        }

        @Override
        public AbstractConstraint getAssetRefinement() {
            return null;
        }

        @Override
        public void setAssetRefinement(AbstractConstraint abstractConstraint) {

        }

        @Override
        public List<AbstractConstraint> getConstraint() {
            return null;
        }

        @Override
        public void setConstraint(List<AbstractConstraint> list) {

        }

        @Override
        public List<URI> getAssigner() {
            return null;
        }

        @Override
        public void setAssigner(List<URI> list) {

        }

        @Override
        public Rule deepCopy() {
            return null;
        }

        @Override
        public List<URI> getAssignee() {
            return null;
        }

        @Override
        public void setAssignee(List<URI> list) {

        }

        @Override
        public URI getTarget() {
            return null;
        }

        @Override
        public void setTarget(URI uri) {

        }
    }

}
