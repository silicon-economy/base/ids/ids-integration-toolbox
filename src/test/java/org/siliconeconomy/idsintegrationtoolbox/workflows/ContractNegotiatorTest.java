/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.workflows;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import de.fraunhofer.iais.eis.Action;
import de.fraunhofer.iais.eis.Artifact;
import de.fraunhofer.iais.eis.ArtifactBuilder;
import de.fraunhofer.iais.eis.ContractOffer;
import de.fraunhofer.iais.eis.ContractOfferBuilder;
import de.fraunhofer.iais.eis.Duty;
import de.fraunhofer.iais.eis.DutyBuilder;
import de.fraunhofer.iais.eis.DutyImpl;
import de.fraunhofer.iais.eis.IANAMediaTypeBuilder;
import de.fraunhofer.iais.eis.Permission;
import de.fraunhofer.iais.eis.PermissionBuilder;
import de.fraunhofer.iais.eis.PermissionImpl;
import de.fraunhofer.iais.eis.Prohibition;
import de.fraunhofer.iais.eis.ProhibitionBuilder;
import de.fraunhofer.iais.eis.ProhibitionImpl;
import de.fraunhofer.iais.eis.Representation;
import de.fraunhofer.iais.eis.RepresentationBuilder;
import de.fraunhofer.iais.eis.RepresentationInstance;
import de.fraunhofer.iais.eis.Resource;
import de.fraunhofer.iais.eis.ResourceBuilder;
import de.fraunhofer.iais.eis.Rule;
import de.fraunhofer.iais.eis.ValueBuilder;
import de.fraunhofer.iais.eis.util.Util;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.InvalidInputException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.NoParsableResponseException;
import org.siliconeconomy.idsintegrationtoolbox.core.ids.MessagesApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.AgreementOutput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * ContractNegotiatorTest
 *
 * @author Ronja Quensel
 */
@SpringBootTest(classes = ContractNegotiator.class)
class ContractNegotiatorTest {

    /** Class under test */
    @Autowired
    private ContractNegotiator negotiator;

    @MockBean
    private MessagesApiOperator apiOperator;

    @Captor
    private ArgumentCaptor<List<Rule>> ruleCaptor ;

    private final URI recipient = URI.create("https://recipient");

    private final boolean download = false;

    /**********************************************************************************************
     * Multiple resources, specific artifacts
     **********************************************************************************************/

    @Test
    @SneakyThrows
    void negotiateContract0_validInput_returnAgreement() {
        // Arrange
        final var resource1Id = URI.create("https://resource1");
        final var resource2Id = URI.create("https://resource2");
        final var artifact1Id = URI.create("https://artifact1");
        final var artifact2Id = URI.create("https://artifact2");
        final var contract1Id = URI.create("https://contract1");
        final var contract2Id = URI.create("https://contract2");

        final var artifact1 = getArtifact(artifact1Id);
        final var artifact2 = getArtifact(artifact2Id);
        final var representation1 = getRepresentation(getArrayList(artifact1));
        final var representation2 = getRepresentation(getArrayList(artifact2));
        final var permission = getPermission();
        final var prohibition = getProhibition();
        final var duty = getDuty();
        final var contract1 = getContractOffer(contract1Id, getArrayList(permission),
            getArrayList(prohibition), null);
        final var contract2 = getContractOffer(contract2Id, getArrayList(permission),
            null, getArrayList(duty));
        final var resource1 = getResource(resource1Id, getArrayList(representation1),
            getArrayList(contract1));
        final var resource2 = getResource(resource2Id, getArrayList(representation2),
            getArrayList(contract2));

        when(apiOperator.descriptionRequest(recipient, resource1Id)).thenReturn(resource1);
        when(apiOperator.descriptionRequest(recipient, resource2Id)).thenReturn(resource2);
        when(apiOperator.contractNegotiation(eq(recipient), any(), any(), eq(download), any()))
            .thenReturn(new AgreementOutput());

        final var resourceIds = Util.asList(resource1Id, resource2Id);
        final var artifactIds = Util.asList(artifact1Id, artifact2Id);
        final var contractIds = Util.asList(contract1Id, contract2Id);

        // Act
        final var result = negotiator
            .negotiateContract(recipient, resourceIds, artifactIds, contractIds, download);

        // Assert
        verify(apiOperator, times(1)).contractNegotiation(eq(recipient), eq(resourceIds),
            eq(artifactIds), eq(download), ruleCaptor.capture());

        final var rules = ruleCaptor.getValue();
        assertEquals(4, rules.size());

        final var permissionForArtifact1 = new AtomicBoolean(false);
        final var prohibitionForArtifact1 = new AtomicBoolean(false);
        final var permissionForArtifact2 = new AtomicBoolean(false);
        final var dutyForArtifact2 = new AtomicBoolean(false);

        rules.forEach(r -> {
            assertEquals(1, r.getAction().size());
            assertEquals(Action.USE, r.getAction().get(0));
            assertThat(r.getConstraint()).isEmpty();
            if (r instanceof PermissionImpl) {
                if (artifact1Id.equals(r.getTarget())) {
                    permissionForArtifact1.set(true);
                } else if (artifact2Id.equals(r.getTarget())) {
                    permissionForArtifact2.set(true);
                }
            } else if (r instanceof ProhibitionImpl) {
                if (artifact1Id.equals(r.getTarget())) {
                    prohibitionForArtifact1.set(true);
                }
            } else if (r instanceof DutyImpl) {
                if (artifact2Id.equals(r.getTarget())) {
                    dutyForArtifact2.set(true);
                }
            }
        });

        assertTrue(permissionForArtifact1.get());
        assertTrue(prohibitionForArtifact1.get());
        assertTrue(permissionForArtifact2.get());
        assertTrue(dutyForArtifact2.get());
    }

    @Test
    @SneakyThrows
    void negotiateContract0_communicationFails_throwApiInteractionUnsuccessfulException() {
        // Arrange
        final var resourceId = URI.create("https://resource");
        final var contractId = URI.create("https://contract");
        final var artifactId = URI.create("https://artifact");

        when(apiOperator.descriptionRequest(recipient, resourceId))
            .thenThrow(ApiInteractionUnsuccessfulException.class);

        // Act & Assert
        assertThatThrownBy(() -> negotiator
            .negotiateContract(recipient, Util.asList(resourceId), Util.asList(artifactId),
                Util.asList(contractId), download))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class);
        verify(apiOperator, never()).contractNegotiation(any(URI.class), anyList(), anyList(),
            anyBoolean(), anyList());
    }

    @Test
    @SneakyThrows
    void negotiateContract0_resourceDescNotParsable_throwNoParsableResponseException() {
        // Arrange
        final var resourceId = URI.create("https://resource");
        final var contractId = URI.create("https://contract");
        final var artifactId = URI.create("https://artifact");

        when(apiOperator.descriptionRequest(recipient, resourceId))
            .thenThrow(NoParsableResponseException.class);

        // Act & Assert
        assertThatThrownBy(() -> negotiator
            .negotiateContract(recipient, Util.asList(resourceId), Util.asList(artifactId),
                Util.asList(contractId), download))
            .isInstanceOf(NoParsableResponseException.class);
        verify(apiOperator, never()).contractNegotiation(any(URI.class), anyList(), anyList(),
            anyBoolean(), anyList());
    }

    @Test
    @SneakyThrows
    void negotiateContract0_invalidContractIds_throwInvalidInputException() {
        // Arrange
        final var resourceId = URI.create("https://resource");
        final var contractId = URI.create("https://contract");
        final var artifactId = URI.create("https://artifact");

        final var permission = getPermission();
        final var contract = getContractOffer(URI.create("https://invalid"),
            getArrayList(permission), null, null);
        final var artifact = getArtifact(artifactId);
        final var representation = getRepresentation(getArrayList(artifact));
        final var resource = getResource(resourceId, getArrayList(representation),
            getArrayList(contract));

        when(apiOperator.descriptionRequest(recipient, resourceId)).thenReturn(resource);

        final var resourceIds = Util.asList(resourceId);
        final var artifactIds = Util.asList(artifactId);
        final var contractIds = Util.asList(contractId);

        // Act & Assert
        assertThatThrownBy(() -> negotiator
            .negotiateContract(recipient, resourceIds, artifactIds, contractIds, download))
            .isInstanceOf(InvalidInputException.class);
        verify(apiOperator, never()).contractNegotiation(any(URI.class), anyList(), anyList(),
            anyBoolean(), anyList());
    }

    @Test
    @SneakyThrows
    void negotiateContract0_invalidArtifactId_logWarning() {
        // Arrange
        final var resourceId = URI.create("https://resource");
        final var contractId = URI.create("https://contract");
        final var artifactId = URI.create("https://artifact");
        final var invalidArtifactId = URI.create("https://invalid");
        final var expectedLogMessage = "Could not find all specified artifacts in the specified"
            + " resources. Will not request the following artifacts: {}";

        final var permission = getPermission();
        final var contract = getContractOffer(contractId, getArrayList(permission), null, null);
        final var artifact1 = getArtifact(URI.create("https://some-id"));
        final var artifact2 = getArtifact(artifactId);
        final var representation = getRepresentation(getArrayList(artifact1, artifact2));
        final var resource = getResource(resourceId, getArrayList(representation),
            getArrayList(contract));

        when(apiOperator.descriptionRequest(recipient, resourceId)).thenReturn(resource);

        // Act
        final var result = negotiator
            .negotiateContract(recipient, Util.asList(resourceId),
                Util.asList(artifactId, invalidArtifactId),
                Util.asList(contractId), download);

        // Assert
        verify(apiOperator, times(1)).contractNegotiation(eq(recipient), eq(Util.asList(resourceId)),
            eq(Util.asList(artifactId)), eq(download), ruleCaptor.capture());
    }

    @Test
    @SneakyThrows
    void negotiateContract0_noValidArtifactId_throwInvalidInputException() {
        // Arrange
        final var resourceId = URI.create("https://resource");
        final var contractId = URI.create("https://contract");
        final var artifactId = URI.create("https://artifact");

        final var permission = getPermission();
        final var contract = getContractOffer(contractId, getArrayList(permission), null, null);
        final var artifact = getArtifact(artifactId);
        final var representation = getRepresentation(getArrayList(artifact));
        final var resource = getResource(resourceId, getArrayList(representation),
            getArrayList(contract));

        when(apiOperator.descriptionRequest(recipient, resourceId)).thenReturn(resource);
        when(apiOperator.contractNegotiation(eq(recipient), eq(Util.asList(resourceId)),
            eq(Collections.emptyList()), eq(download), anyList()))
            .thenThrow(InvalidInputException.class);

        final var resourceIds = Util.asList(resourceId);
        final var artifactIds = Util.asList(URI.create("https://invalid"));
        final var contractIds = Util.asList(contractId);

        // Act & Assert
        assertThatThrownBy(() -> negotiator
            .negotiateContract(recipient, resourceIds, artifactIds, contractIds, download))
            .isInstanceOf(InvalidInputException.class);
    }

    /**********************************************************************************************
     * Multiple resources, all artifacts
     **********************************************************************************************/

    @Test
    @SneakyThrows
    void negotiateContract1_validInput_returnAgreement() {
        // Arrange
        final var resource1Id = URI.create("https://resource1");
        final var resource2Id = URI.create("https://resource2");
        final var artifact1Id = URI.create("https://artifact1");
        final var artifact2Id = URI.create("https://artifact2");
        final var contract1Id = URI.create("https://contract1");
        final var contract2Id = URI.create("https://contract2");

        final var artifact1 = getArtifact(artifact1Id);
        final var artifact2 = getArtifact(artifact2Id);
        final var representation1 = getRepresentation(getArrayList(artifact1));
        final var representation2 = getRepresentation(getArrayList(artifact2));
        final var prohibition = getProhibition();
        final var duty = getDuty();
        final var contract1 = getContractOffer(contract1Id, null, getArrayList(prohibition), null);
        final var contract2 = getContractOffer(contract2Id, null, null, getArrayList(duty));
        final var resource1 = getResource(resource1Id, getArrayList(representation1),
            getArrayList(contract1));
        final var resource2 = getResource(resource2Id, getArrayList(representation2),
            getArrayList(contract2));

        when(apiOperator.descriptionRequest(recipient, resource1Id)).thenReturn(resource1);
        when(apiOperator.descriptionRequest(recipient, resource2Id)).thenReturn(resource2);
        when(apiOperator.contractNegotiation(eq(recipient), any(), any(), eq(download), any()))
            .thenReturn(new AgreementOutput());

        final var resourceIds = Util.asList(resource1Id, resource2Id);
        final var artifactIds = Util.asList(artifact1Id, artifact2Id);
        final var contractIds = Util.asList(contract1Id, contract2Id);

        // Act
        final var result = negotiator
            .negotiateContract(recipient, resourceIds, contractIds, download);

        // Assert
        verify(apiOperator, times(1)).contractNegotiation(eq(recipient), eq(resourceIds),
            eq(artifactIds), eq(download), ruleCaptor.capture());

        final var rules = ruleCaptor.getValue();
        assertEquals(2, rules.size());

        rules.forEach(r -> {
            assertEquals(1, r.getAction().size());
            assertEquals(Action.USE, r.getAction().get(0));
            assertThat(r.getConstraint()).isEmpty();
        });

        final var targets = rules.stream().map(Rule::getTarget).collect(Collectors.toSet());
        assertEquals(2, targets.size());
        assertTrue(targets.containsAll(Util.asList(artifact1Id, artifact2Id)));

        final var types = rules.stream().map(Rule::getClass).collect(Collectors.toSet());
        assertEquals(2, types.size());
        assertTrue(types.containsAll(Util.asList(ProhibitionImpl.class, DutyImpl.class)));
    }

    @Test
    @SneakyThrows
    void negotiateContract1_communicationFails_throwApiInteractionUnsuccessfulException() {
        // Arrange
        final var resourceId = URI.create("https://resource");
        final var contractId = URI.create("https://contract");
        final var artifactId = URI.create("https://artifact");

        when(apiOperator.descriptionRequest(recipient, resourceId))
            .thenThrow(ApiInteractionUnsuccessfulException.class);

        // Act & Assert
        assertThatThrownBy(() -> negotiator
            .negotiateContract(recipient, Util.asList(resourceId), Util.asList(contractId),
                download))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class);
        verify(apiOperator, never()).contractNegotiation(any(URI.class), anyList(), anyList(),
            anyBoolean(), anyList());
    }

    @Test
    @SneakyThrows
    void negotiateContract1_resourceDescNotParsable_throwNoParsableResponseException() {
        // Arrange
        final var resourceId = URI.create("https://resource");
        final var contractId = URI.create("https://contract");
        final var artifactId = URI.create("https://artifact");

        when(apiOperator.descriptionRequest(recipient, resourceId))
            .thenThrow(NoParsableResponseException.class);

        // Act & Assert
        assertThatThrownBy(() -> negotiator
            .negotiateContract(recipient, Util.asList(resourceId), Util.asList(contractId),
                download))
            .isInstanceOf(NoParsableResponseException.class);
        verify(apiOperator, never()).contractNegotiation(any(URI.class), anyList(), anyList(),
            anyBoolean(), anyList());
    }

    @Test
    @SneakyThrows
    void negotiateContract1_invalidContractIds_throwInvalidInputException() {
        // Arrange
        final var resourceId = URI.create("https://resource");
        final var contractId = URI.create("https://contract");
        final var artifactId = URI.create("https://artifact");

        final var permission = getPermission();
        final var contract = getContractOffer(URI.create("https://invalid"),
            getArrayList(permission), null, null);
        final var artifact = getArtifact(artifactId);
        final var representation = getRepresentation(getArrayList(artifact));
        final var resource = getResource(resourceId, getArrayList(representation),
            getArrayList(contract));

        when(apiOperator.descriptionRequest(recipient, resourceId)).thenReturn(resource);

        final var resourceIds = Util.asList(resourceId);
        final var contractIds = Util.asList(contractId);

        // Act & Assert
        assertThatThrownBy(() -> negotiator
            .negotiateContract(recipient, resourceIds, contractIds, download))
            .isInstanceOf(InvalidInputException.class);
        verify(apiOperator, never()).contractNegotiation(any(URI.class), anyList(), anyList(),
            anyBoolean(), anyList());
    }

    /**********************************************************************************************
     * Single resource, single artifact
     **********************************************************************************************/

    @Test
    @SneakyThrows
    void negotiateContract2_validInput_returnAgreement() {
        // Arrange
        final var resourceId = URI.create("https://resource");
        final var contractId = URI.create("https://contract");
        final var artifactId = URI.create("https://artifact");

        final var permission = getPermission();
        final var contract = getContractOffer(contractId, getArrayList(permission), null, null);
        final var artifact = getArtifact(artifactId);
        final var representation = getRepresentation(getArrayList(artifact));
        final var resource = getResource(resourceId, getArrayList(representation),
            getArrayList(contract));

        when(apiOperator.descriptionRequest(recipient, resourceId)).thenReturn(resource);
        when(apiOperator.contractNegotiation(eq(recipient), any(), any(), eq(download), any()))
            .thenReturn(new AgreementOutput());

        // Act
        final var result = negotiator
            .negotiateContract(recipient, resourceId, artifactId, contractId, download);


        // Assert
        verify(apiOperator, times(1)).contractNegotiation(eq(recipient), eq(Util.asList(resourceId)),
            eq(Util.asList(artifactId)), eq(download), ruleCaptor.capture());

        final var rules = ruleCaptor.getValue();
        assertEquals(1, rules.size());

        final var rule = rules.get(0);
        assertTrue(rule instanceof PermissionImpl);
        assertEquals(1, rule.getAction().size());
        assertEquals(Action.USE, rule.getAction().get(0));
        assertThat(rule.getConstraint()).isEmpty();
        assertEquals(artifactId, rule.getTarget());
    }

    @Test
    @SneakyThrows
    void negotiateContract2_communicationFails_throwApiInteractionUnsuccessfulException() {
        // Arrange
        final var resourceId = URI.create("https://resource");
        final var contractId = URI.create("https://contract");
        final var artifactId = URI.create("https://artifact");

        when(apiOperator.descriptionRequest(recipient, resourceId))
            .thenThrow(ApiInteractionUnsuccessfulException.class);

        // Act & Assert
        assertThatThrownBy(() -> negotiator
            .negotiateContract(recipient, resourceId, artifactId, contractId, download))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class);
        verify(apiOperator, never()).contractNegotiation(any(URI.class), anyList(), anyList(),
            anyBoolean(), anyList());
    }

    @Test
    @SneakyThrows
    void negotiateContract2_resourceDescNotParsable_throwNoParsableResponseException() {
        // Arrange
        final var resourceId = URI.create("https://resource");
        final var contractId = URI.create("https://contract");
        final var artifactId = URI.create("https://artifact");

        when(apiOperator.descriptionRequest(recipient, resourceId))
            .thenThrow(NoParsableResponseException.class);

        // Act & Assert
        assertThatThrownBy(() -> negotiator
            .negotiateContract(recipient, resourceId, artifactId, contractId, download))
            .isInstanceOf(NoParsableResponseException.class);
        verify(apiOperator, never()).contractNegotiation(any(URI.class), anyList(), anyList(),
            anyBoolean(), anyList());
    }

    @Test
    @SneakyThrows
    void negotiateContract2_invalidContractId_throwInvalidInputException() {
        // Arrange
        final var resourceId = URI.create("https://resource");
        final var contractId = URI.create("https://contract");
        final var artifactId = URI.create("https://artifact");

        final var permission = getPermission();
        final var contract = getContractOffer(URI.create("https://invalid"),
            getArrayList(permission), null, null);
        final var artifact = getArtifact(artifactId);
        final var representation = getRepresentation(getArrayList(artifact));
        final var resource = getResource(resourceId, getArrayList(representation),
            getArrayList(contract));

        when(apiOperator.descriptionRequest(recipient, resourceId)).thenReturn(resource);

        // Act & Assert
        assertThatThrownBy(() -> negotiator
            .negotiateContract(recipient, resourceId, artifactId, contractId, download))
            .isInstanceOf(InvalidInputException.class);
        verify(apiOperator, never()).contractNegotiation(any(URI.class), anyList(), anyList(),
            anyBoolean(), anyList());
    }

    @Test
    @SneakyThrows
    void negotiateContract2_invalidArtifactId_throwInvalidInputException() {
        // Arrange
        final var resourceId = URI.create("https://resource");
        final var contractId = URI.create("https://contract");
        final var artifactId = URI.create("https://artifact");

        final var permission = getPermission();
        final var contract = getContractOffer(contractId, getArrayList(permission), null, null);
        final var artifact = getArtifact(URI.create("https://invalid"));
        final var representation = getRepresentation(getArrayList(artifact));
        final var resource = getResource(resourceId, getArrayList(representation),
            getArrayList(contract));

        when(apiOperator.descriptionRequest(recipient, resourceId)).thenReturn(resource);

        // Act & Assert
        assertThatThrownBy(() -> negotiator
            .negotiateContract(recipient, resourceId, artifactId, contractId, download))
            .isInstanceOf(InvalidInputException.class);
        verify(apiOperator, never()).contractNegotiation(any(URI.class), anyList(), anyList(),
            anyBoolean(), anyList());
    }

    @Test
    @SneakyThrows
    void negotiateContract1_valueInsteadOfArtifact_throwInvalidInputException() {
        final var resourceId = URI.create("https://resource");
        final var contractId = URI.create("https://contract");
        final var artifactId = URI.create("https://artifact");

        final var permission = getPermission();
        final var contract = getContractOffer(contractId, getArrayList(permission), null, null);
        final var value = new ValueBuilder(artifactId).build();
        final var representation = getRepresentation(getArrayList(value));
        final var resource = getResource(resourceId, getArrayList(representation),
            getArrayList(contract));

        when(apiOperator.descriptionRequest(recipient, resourceId)).thenReturn(resource);

        // Act & Assert
        assertThatThrownBy(() -> negotiator
            .negotiateContract(recipient, resourceId, artifactId, contractId, download))
            .isInstanceOf(InvalidInputException.class);
        verify(apiOperator, never()).contractNegotiation(any(URI.class), anyList(), anyList(),
            anyBoolean(), anyList());
    }

    /**********************************************************************************************
     * Single resource, all artifacts
     **********************************************************************************************/

    @Test
    @SneakyThrows
    void negotiateContract3_validInput_returnAgreement() {
        // Arrange
        final var resourceId = URI.create("https://resource");
        final var artifact1Id = URI.create("https://artifact1");
        final var artifact2Id = URI.create("https://artifact2");
        final var contractId = URI.create("https://contract");

        final var artifact1 = getArtifact(artifact1Id);
        final var artifact2 = getArtifact(artifact2Id);
        final var representation = getRepresentation(getArrayList(artifact1, artifact2));
        final var permission = getPermission();
        final var contract = getContractOffer(contractId, getArrayList(permission), null, null);
        final var resource = getResource(resourceId, getArrayList(representation),
            getArrayList(contract));

        when(apiOperator.descriptionRequest(recipient, resourceId)).thenReturn(resource);
        when(apiOperator.contractNegotiation(eq(recipient), any(), any(), eq(download), any()))
            .thenReturn(new AgreementOutput());

        // Act
        final var result = negotiator
            .negotiateContract(recipient, resourceId, contractId, download);

        // Assert
        verify(apiOperator, times(1)).contractNegotiation(eq(recipient), eq(Util.asList(resourceId)),
            eq(Util.asList(artifact1Id, artifact2Id)), eq(download), ruleCaptor.capture());

        final var rules = ruleCaptor.getValue();
        assertEquals(2, rules.size());

        rules.forEach(r -> {
            assertTrue(r instanceof PermissionImpl);
            assertEquals(1, r.getAction().size());
            assertEquals(Action.USE, r.getAction().get(0));
            assertThat(r.getConstraint()).isEmpty();
        });

        final var targets = rules.stream().map(Rule::getTarget).collect(Collectors.toSet());
        assertEquals(2, targets.size());
        assertTrue(targets.containsAll(Util.asList(artifact1Id, artifact2Id)));
    }

    @Test
    @SneakyThrows
    void negotiateContract3_communicationFails_throwApiInteractionUnsuccessfulException() {
        // Arrange
        final var resourceId = URI.create("https://resource");
        final var contractId = URI.create("https://contract");

        when(apiOperator.descriptionRequest(recipient, resourceId))
            .thenThrow(ApiInteractionUnsuccessfulException.class);

        // Act & Assert
        assertThatThrownBy(() -> negotiator
            .negotiateContract(recipient, resourceId, contractId, download))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class);
        verify(apiOperator, never()).contractNegotiation(any(URI.class), anyList(), anyList(),
            anyBoolean(), anyList());
    }

    @Test
    @SneakyThrows
    void negotiateContract3_resourceDescNotParsable_throwNoParsableResponseException() {
        // Arrange
        final var resourceId = URI.create("https://resource");
        final var contractId = URI.create("https://contract");

        when(apiOperator.descriptionRequest(recipient, resourceId))
            .thenThrow(NoParsableResponseException.class);

        // Act & Assert
        assertThatThrownBy(() -> negotiator
            .negotiateContract(recipient, resourceId, contractId, download))
            .isInstanceOf(NoParsableResponseException.class);
        verify(apiOperator, never()).contractNegotiation(any(URI.class), anyList(), anyList(),
            anyBoolean(), anyList());
    }

    @Test
    @SneakyThrows
    void negotiateContract3_invalidContractId_throwInvalidInputException() {
        // Arrange
        final var resourceId = URI.create("https://resource");
        final var contractId = URI.create("https://contract");
        final var artifactId = URI.create("https://artifact");

        final var permission = getPermission();
        final var contract = getContractOffer(URI.create("https://invalid"),
            getArrayList(permission), null, null);
        final var artifact = getArtifact(artifactId);
        final var representation = getRepresentation(getArrayList(artifact));
        final var resource = getResource(resourceId, getArrayList(representation),
            getArrayList(contract));

        when(apiOperator.descriptionRequest(recipient, resourceId)).thenReturn(resource);

        // Act & Assert
        assertThatThrownBy(() -> negotiator
            .negotiateContract(recipient, resourceId, contractId, download))
            .isInstanceOf(InvalidInputException.class);
        verify(apiOperator, never()).contractNegotiation(any(URI.class), anyList(), anyList(),
            anyBoolean(), anyList());
    }

    /**********************************************************************************************
     Utilities
     **********************************************************************************************/

    private <T> ArrayList<T> getArrayList(T... elements) {
        return new ArrayList<>(Util.asList(elements));
    }

    private Artifact getArtifact(final URI id) {
        return new ArtifactBuilder(id)
            ._checkSum_("checksum")
            ._fileName_("artifact")
            .build();
    }

    private Representation getRepresentation(final ArrayList<RepresentationInstance> artifacts) {
        return new RepresentationBuilder()
            ._instance_(artifacts)
            ._representationStandard_(URI.create("https://standard.com"))
            ._mediaType_(new IANAMediaTypeBuilder()
                ._filenameExtension_("json")
                .build())
            .build();
    }

    private Permission getPermission() {
        return new PermissionBuilder()
            ._action_(Util.asList(Action.USE))
            .build();
    }

    private Prohibition getProhibition() {
        return new ProhibitionBuilder()
            ._action_(Util.asList(Action.USE))
            .build();
    }

    private Duty getDuty() {
        return new DutyBuilder()
            ._action_(Util.asList(Action.USE))
            .build();
    }

    private ContractOffer getContractOffer(final URI id, final ArrayList<Permission> permissions,
                                           final ArrayList<Prohibition> prohibitions,
                                           final ArrayList<Duty> duties) {
        return new ContractOfferBuilder(id)
            ._permission_(permissions)
            ._prohibition_(prohibitions)
            ._obligation_(duties)
            .build();
    }

    private Resource getResource(final URI id, final ArrayList<Representation> representations,
                                 final ArrayList<ContractOffer> contracts) {
        return new ResourceBuilder(id)
            ._representation_(representations)
            ._contractOffer_(contracts)
            .build();
    }

}
