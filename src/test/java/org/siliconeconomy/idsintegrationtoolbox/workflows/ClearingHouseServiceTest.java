/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.workflows;

import java.net.URI;
import java.util.UUID;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.siliconeconomy.idsintegrationtoolbox.config.IntegrationConfig;
import org.siliconeconomy.idsintegrationtoolbox.core.ids.MessagesApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.model.output.ClearingHouseLogEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

/**
 * ClearingHouseServiceTest
 *
 * @author Ronja Quensel
 */
@SpringBootTest(classes = {ClearingHouseService.class, IntegrationConfig.class})
class ClearingHouseServiceTest {

    /** Class under test */
    @Autowired
    private ClearingHouseService clearingHouseService;

    @MockBean
    private MessagesApiOperator messagesApiOperator;

    @Value("${clearing-house.path.query:messages/query}")
    private String clearingHouseQueryPath;

    @Captor
    private ArgumentCaptor<URI> recipientCaptor;

    @Test
    @SneakyThrows
    void queryClearingHouse_successfulRequest_returnEntries() {
        // Arrange
        final var clearingHouseUrl = URI.create("https://ch-ids.aisec.fraunhofer.de");
        final var processId = UUID.randomUUID().toString();
        final var response = getClearingHouseResponse();
        final var logEntry = new ClearingHouseLogEntry();

        when(messagesApiOperator.query(recipientCaptor.capture(), eq(" "))).thenReturn(response);

        // Act
        final var result = clearingHouseService.query(clearingHouseUrl, processId);

        // Assert
        final var expectedRecipient = clearingHouseUrl
            + "/" + clearingHouseQueryPath
            + "/" + processId;

        final var recipient = recipientCaptor.getValue();
        assertThat(recipient).isEqualTo(URI.create(expectedRecipient));

        assertThat(result).isNotEmpty().hasSize(1);
        final var entry = result.get(0);
        assertThat(entry.getType()).isEqualTo("ids:LogMessage");
        assertThat(entry.getId()).isEqualTo(URI.create("https://w3id.org/idsa/autogen/logMessage"
            + "/f2223bd9-9f37-41ed-a228-9b7f1120c151"));
        assertThat(entry.getModelVersion()).isEqualTo("4.2.6");
        assertThat(entry.getPayloadType()).isEqualTo("application/octet-stream");

        final var connectorId = URI.create("https://w3id.org/idsa/autogen/baseConnector"
            + "/7b934432-a85e-41c5-9f65-669219dde4ea-1");
        assertThat(entry.getIssuerConnector()).isEqualTo(connectorId);
        assertThat(entry.getSenderAgent()).isEqualTo(connectorId);
    }

    /**********************************************************************************************
     Utilities
     **********************************************************************************************/

    private String getClearingHouseResponse() {
        return "["
            + "{\n"
            + "\t\t\"@context\":{\n"
            + "\t\t\t\"ids\":\"https://w3id.org/idsa/core/\",\n"
            + "\t\t\t\"idsc\":\"https://w3id.org/idsa/code/\"\n"
            + "\t\t},\n"
            + "\t\t\"@type\":\"ids:LogMessage\",\n"
            + "\t\t\"@id\":\"https://w3id.org/idsa/autogen/logMessage/f2223bd9-9f37-41ed-a228"
            + "-9b7f1120c151\",\n"
            + "\t\t\"ids:modelVersion\":\"4.2.6\",\n"
            + "\t\t\"ids:issued\":\"2022-01-12T08:51:11.876+00:00\",\n"
            + "\t\t\"ids:issuerConnector\":\"https://w3id"
            + ".org/idsa/autogen/baseConnector/7b934432-a85e-41c5-9f65-669219dde4ea-1\",\n"
            + "\t\t\"ids:senderAgent\":\"https://w3id"
            + ".org/idsa/autogen/baseConnector/7b934432-a85e-41c5-9f65-669219dde4ea-1\",\n"
            + "\t\t\"payload"
            +
            "\":\"ewogICJAY29udGV4dCIgOiB7CiAgICAiaWRzIiA6ICJodHRwczovL3czaWQub3JnL2lkc2EvY29yZS8iLAogICAgImlkc2MiIDogImh0dHBzOi8vdzNpZC5vcmcvaWRzYS9jb2RlLyIKICB9LAogICJAdHlwZSIgOiAiaWRzOkNvbnRyYWN0QWdyZWVtZW50IiwKICAiQGlkIiA6ICJodHRwczovL2xvY2FsaG9zdDo4MDgwL2FwaS9hZ3JlZW1lbnRzL2VkOGVmYTBjLTE2M2ItNDY5MC04MDFhLTBmNzZmYjA4MjA5MyIsCiAgImlkczpwZXJtaXNzaW9uIiA6IFsgewogICAgIkB0eXBlIiA6ICJpZHM6UGVybWlzc2lvbiIsCiAgICAiQGlkIiA6ICJodHRwczovL2xvY2FsaG9zdDo4MDgwL2FwaS9ydWxlcy81Njc1ZmU3NC02ZTMwLTRhNzktYjJkNC1iMmYwNTQ3MzNhZDkiLAogICAgImlkczp0YXJnZXQiIDogewogICAgICAiQGlkIiA6ICJodHRwczovL2xvY2FsaG9zdDo4MDgwL2FwaS9hcnRpZmFjdHMvMmZhYmRlZTYtZTM3NS00YzdjLTg3YTctMWZkMTczODRhN2ZhIgogICAgfSwKICAgICJpZHM6ZGVzY3JpcHRpb24iIDogWyB7CiAgICAgICJAdmFsdWUiIDogInByb3ZpZGUtYWNjZXNzIiwKICAgICAgIkB0eXBlIiA6ICJodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTFNjaGVtYSNzdHJpbmciCiAgICB9IF0sCiAgICAiaWRzOnRpdGxlIiA6IFsgewogICAgICAiQHZhbHVlIiA6ICJFeGFtcGxlIFVzYWdlIFBvbGljeSIsCiAgICAgICJAdHlwZSIgOiAiaHR0cDovL3d3dy53My5vcmcvMjAwMS9YTUxTY2hlbWEjc3RyaW5nIgogICAgfSBdLAogICAgImlkczphc3NpZ25lZSIgOiBbIHsKICAgICAgIkBpZCIgOiAiaHR0cHM6Ly93M2lkLm9yZy9pZHNhL2F1dG9nZW4vYmFzZUNvbm5lY3Rvci83YjkzNDQzMi1hODVlLTQxYzUtOWY2NS02NjkyMTlkZGU0ZWEtMiIKICAgIH0gXSwKICAgICJpZHM6YXNzaWduZXIiIDogWyB7CiAgICAgICJAaWQiIDogImh0dHBzOi8vdzNpZC5vcmcvaWRzYS9hdXRvZ2VuL2Jhc2VDb25uZWN0b3IvN2I5MzQ0MzItYTg1ZS00MWM1LTlmNjUtNjY5MjE5ZGRlNGVhLTEiCiAgICB9IF0sCiAgICAiaWRzOnBvc3REdXR5IiA6IFsgXSwKICAgICJpZHM6Y29uc3RyYWludCIgOiBbIF0sCiAgICAiaWRzOmFjdGlvbiIgOiBbIHsKICAgICAgIkBpZCIgOiAiaHR0cHM6Ly93M2lkLm9yZy9pZHNhL2NvZGUvVVNFIgogICAgfSBdLAogICAgImlkczpwcmVEdXR5IiA6IFsgXQogIH0gXSwKICAiaWRzOnByb3ZpZGVyIiA6IHsKICAgICJAaWQiIDogImh0dHBzOi8vdzNpZC5vcmcvaWRzYS9hdXRvZ2VuL2Jhc2VDb25uZWN0b3IvN2I5MzQ0MzItYTg1ZS00MWM1LTlmNjUtNjY5MjE5ZGRlNGVhLTEiCiAgfSwKICAiaWRzOmNvbnN1bWVyIiA6IHsKICAgICJAaWQiIDogImh0dHBzOi8vdzNpZC5vcmcvaWRzYS9hdXRvZ2VuL2Jhc2VDb25uZWN0b3IvN2I5MzQ0MzItYTg1ZS00MWM1LTlmNjUtNjY5MjE5ZGRlNGVhLTIiCiAgfSwKICAiaWRzOmNvbnRyYWN0RW5kIiA6IHsKICAgICJAdmFsdWUiIDogIjIwMjItMDYtMzBUMDg6NDY6MzkuODg4WiIsCiAgICAiQHR5cGUiIDogImh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hI2RhdGVUaW1lU3RhbXAiCiAgfSwKICAiaWRzOmNvbnRyYWN0U3RhcnQiIDogewogICAgIkB2YWx1ZSIgOiAiMjAyMi0wMS0xMlQwODo1MTowOC45ODJaIiwKICAgICJAdHlwZSIgOiAiaHR0cDovL3d3dy53My5vcmcvMjAwMS9YTUxTY2hlbWEjZGF0ZVRpbWVTdGFtcCIKICB9LAogICJpZHM6Y29udHJhY3REYXRlIiA6IHsKICAgICJAdmFsdWUiIDogIjIwMjItMDEtMTJUMDg6NTE6MDguOTgxWiIsCiAgICAiQHR5cGUiIDogImh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hI2RhdGVUaW1lU3RhbXAiCiAgfSwKICAiaWRzOnByb2hpYml0aW9uIiA6IFsgXSwKICAiaWRzOm9ibGlnYXRpb24iIDogWyBdCn0=\",\n"
            + "\t\t\"payload_type\":\"application/octet-stream\"\n"
            + "\t}"
            + "]";
    }

}
