/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.workflows;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.siliconeconomy.idsintegrationtoolbox.core.base.SubscriptionApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.ids.MessagesApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.model.input.SubscriptionInput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ArtifactLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.Link;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.RepresentationLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.RequestedResourceLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.AgreementOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ArtifactOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.RepresentationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.RequestedResourceOutput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.util.UriComponentsBuilder;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * SubscriberTest.
 *
 * @author Ronja Quensel
 */
@SpringBootTest(classes = {Subscriber.class})
class SubscriberTest {

    @MockBean
    private MessagesApiOperator messagesApiOperator;

    @MockBean
    private ContractNegotiator negotiator;

    @MockBean
    private SubscriptionApiOperator subscriptionApiOperator;

    @Autowired
    private Subscriber subscriber;

    @Value("${connector.url:http://localhost:8080}")
    protected String connectorUrl;

    @Captor
    private ArgumentCaptor<SubscriptionInput> captor;

    @Test
    @SneakyThrows
    void requestAndSubscribe_successfulRequest() {
        // Arrange
        final var resourceIds = new ArrayList<URI>() {{ add(URI.create("https://resource")); }};
        final var artifactIds = new ArrayList<URI>() {{ add(URI.create("https://artifact")); }};
        final var contractIds = new ArrayList<URI>() {{ add(URI.create("https://contract")); }};
        final URI recipient = URI.create("https://recipient");
        final var agreement = new AgreementOutput();
        final var connectorEndpoint = getIdsEndpointUrl();

        when(negotiator.negotiateContract(recipient, resourceIds, artifactIds, contractIds, true))
            .thenReturn(agreement);

        // Act
        subscriber.requestAndSubscribe(recipient, resourceIds, artifactIds, contractIds);

        // Assert
        verify(messagesApiOperator, times(2))
            .subscribe(eq(recipient), captor.capture());

        final var subscriptionInputs = captor.getAllValues();
        assertThat(subscriptionInputs).hasSize(2);

        final var resourceSubscription = subscriptionInputs.get(0);
        assertThat(resourceSubscription.getTarget()).isEqualTo(resourceIds.get(0));
        assertThat(resourceSubscription.getLocation()).isEqualTo(connectorEndpoint);
        assertThat(resourceSubscription.getSubscriber()).isEqualTo(connectorEndpoint);
        assertThat(resourceSubscription.isPushData()).isFalse();

        final var artifactSubscription = subscriptionInputs.get(1);
        assertThat(artifactSubscription.getTarget()).isEqualTo(artifactIds.get(0));
        assertThat(artifactSubscription.getLocation()).isEqualTo(connectorEndpoint);
        assertThat(artifactSubscription.getSubscriber()).isEqualTo(connectorEndpoint);
        assertThat(artifactSubscription.isPushData()).isTrue();
    }

    @Test
    @SneakyThrows
    void requestAndSubscribe_errorNegotiatingContract_propagateException() {
        // Arrange
        final var resourceIds = new ArrayList<URI>() {{ add(URI.create("https://resource")); }};
        final var artifactIds = new ArrayList<URI>() {{ add(URI.create("https://artifact")); }};
        final var contractIds = new ArrayList<URI>() {{ add(URI.create("https://contract")); }};
        final URI recipient = URI.create("https://recipient");

        doThrow(IOException.class).when(negotiator)
            .negotiateContract(recipient, resourceIds, artifactIds, contractIds, true);

        // Act & Assert
        assertThatThrownBy(() -> subscriber
            .requestAndSubscribe(recipient, resourceIds, artifactIds, contractIds))
            .isInstanceOf(IOException.class);
    }

    @Test
    @SneakyThrows
    void requestAndSubscribe_errorCreatingSubscription_propagateException() {
        // Arrange
        final var resourceIds = new ArrayList<URI>() {{ add(URI.create("https://resource")); }};
        final var artifactIds = new ArrayList<URI>() {{ add(URI.create("https://artifact")); }};
        final var contractIds = new ArrayList<URI>() {{ add(URI.create("https://contract")); }};
        final URI recipient = URI.create("https://recipient");
        final var agreement = new AgreementOutput();

        when(negotiator.negotiateContract(recipient, resourceIds, artifactIds, contractIds, true))
            .thenReturn(agreement);
        doThrow(IOException.class).when(messagesApiOperator)
            .subscribe(eq(recipient), any());

        // Act & Assert
        assertThatThrownBy(() -> subscriber
            .requestAndSubscribe(recipient, resourceIds, artifactIds, contractIds))
            .isInstanceOf(IOException.class);
    }

    @Test
    @SneakyThrows
    void subscribe_resource() {
        // Arrange
        final var resource = getResource();
        final var recipient = URI.create("https://recipient");

        final var connectorEndpoint = getIdsEndpointUrl();

        // Act
        subscriber.subscribe(recipient, resource);

        // Assert
        verify(messagesApiOperator, times(1))
            .subscribe(eq(recipient), captor.capture());

        final var subscriptionInput = captor.getValue();
        assertThat(subscriptionInput.getTarget()).isEqualTo(resource.getRemoteId());
        assertThat(subscriptionInput.getLocation()).isEqualTo(connectorEndpoint);
        assertThat(subscriptionInput.getSubscriber()).isEqualTo(connectorEndpoint);
        assertThat(subscriptionInput.isPushData()).isFalse();
    }

    @Test
    @SneakyThrows
    void subscribe_representation() {
        // Arrange
        final var representation = getRepresentation();
        final var recipient = URI.create("https://recipient");

        final var connectorEndpoint = getIdsEndpointUrl();

        // Act
        subscriber.subscribe(recipient, representation);

        // Assert
        verify(messagesApiOperator, times(1))
            .subscribe(eq(recipient), captor.capture());

        final var subscriptionInput = captor.getValue();
        assertThat(subscriptionInput.getTarget()).isEqualTo(representation.getRemoteId());
        assertThat(subscriptionInput.getLocation()).isEqualTo(connectorEndpoint);
        assertThat(subscriptionInput.getSubscriber()).isEqualTo(connectorEndpoint);
        assertThat(subscriptionInput.isPushData()).isFalse();
    }

    @Test
    @SneakyThrows
    void subscribe_artifact() {
        // Arrange
        final var artifact = getArtifact();
        final var recipient = URI.create("https://recipient");
        final var pushData = true;

        final var connectorEndpoint = getIdsEndpointUrl();

        // Act
        subscriber.subscribe(recipient, artifact, pushData);

        // Assert
        verify(messagesApiOperator, times(1))
            .subscribe(eq(recipient), captor.capture());

        final var subscriptionInput = captor.getValue();
        assertThat(subscriptionInput.getTarget()).isEqualTo(artifact.getRemoteId());
        assertThat(subscriptionInput.getLocation()).isEqualTo(connectorEndpoint);
        assertThat(subscriptionInput.getSubscriber()).isEqualTo(connectorEndpoint);
        assertThat(subscriptionInput.isPushData()).isEqualTo(pushData);
    }

    @Test
    @SneakyThrows
    void subscribeBackend_resource() {
        // Arrange
        final var resource = getResource();
        final var location = URI.create("https://backend");
        final var subscriberId = URI.create("https://backend");

        // Act
        subscriber.subscribeBackend(resource, location, subscriberId);

        // Assert
        verify(subscriptionApiOperator, times(1)).create(captor.capture());

        final var subscriptionInput = captor.getValue();
        assertThat(subscriptionInput.getTarget())
            .isEqualTo(URI.create(resource.getLinks().getSelf().getHref()));
        assertThat(subscriptionInput.getLocation()).isEqualTo(location);
        assertThat(subscriptionInput.getSubscriber()).isEqualTo(subscriberId);
        assertThat(subscriptionInput.isPushData()).isFalse();
    }

    @Test
    @SneakyThrows
    void subscribeBackend_representation() {
        // Arrange
        final var representation = getRepresentation();
        final var location = URI.create("https://backend");
        final var subscriberId = URI.create("https://backend");

        // Act
        subscriber.subscribeBackend(representation, location, subscriberId);

        // Assert
        verify(subscriptionApiOperator, times(1)).create(captor.capture());

        final var subscriptionInput = captor.getValue();
        assertThat(subscriptionInput.getTarget())
            .isEqualTo(URI.create(representation.getLinks().getSelf().getHref()));
        assertThat(subscriptionInput.getLocation()).isEqualTo(location);
        assertThat(subscriptionInput.getSubscriber()).isEqualTo(subscriberId);
        assertThat(subscriptionInput.isPushData()).isFalse();
    }

    @Test
    @SneakyThrows
    void subscribeBackend_artifact() {
        // Arrange
        final var artifact = getArtifact();
        final var location = URI.create("https://backend");
        final var subscriberId = URI.create("https://backend");
        final var pushData = true;

        // Act
        subscriber.subscribeBackend(artifact, location, subscriberId, pushData);

        // Assert
        verify(subscriptionApiOperator, times(1)).create(captor.capture());

        final var subscriptionInput = captor.getValue();
        assertThat(subscriptionInput.getTarget())
            .isEqualTo(URI.create(artifact.getLinks().getSelf().getHref()));
        assertThat(subscriptionInput.getLocation()).isEqualTo(location);
        assertThat(subscriptionInput.getSubscriber()).isEqualTo(subscriberId);
        assertThat(subscriptionInput.isPushData()).isEqualTo(pushData);
    }

    private RequestedResourceOutput getResource() {
        var link = new Link();
        ReflectionTestUtils.setField(link, "href", "https://self-link");
        var links = new RequestedResourceLinks();
        ReflectionTestUtils.setField(links, "self", link);

        var resource = new RequestedResourceOutput();
        ReflectionTestUtils.setField(resource, "title", "title");
        ReflectionTestUtils.setField(resource, "description", "description");
        ReflectionTestUtils.setField(resource, "remoteId", URI.create("https://remote-id"));
        ReflectionTestUtils.setField(resource, "links", links);
        return resource;
    }

    private RepresentationOutput getRepresentation() {
        var link = new Link();
        ReflectionTestUtils.setField(link, "href", "https://self-link");
        var links = new RepresentationLinks();
        ReflectionTestUtils.setField(links, "self", link);

        var representation = new RepresentationOutput();
        ReflectionTestUtils.setField(representation, "title", "title");
        ReflectionTestUtils.setField(representation, "description", "description");
        ReflectionTestUtils.setField(representation, "remoteId", URI.create("https://remote-id"));
        ReflectionTestUtils.setField(representation, "mediaType", "json");
        ReflectionTestUtils.setField(representation, "links", links);
        return representation;
    }

    private ArtifactOutput getArtifact() {
        var link = new Link();
        ReflectionTestUtils.setField(link, "href", "https://self-link");
        var links = new ArtifactLinks();
        ReflectionTestUtils.setField(links, "self", link);

        var artifact = new ArtifactOutput();
        ReflectionTestUtils.setField(artifact, "title", "title");
        ReflectionTestUtils.setField(artifact, "description", "description");
        ReflectionTestUtils.setField(artifact, "remoteId", URI.create("https://remote-id"));
        ReflectionTestUtils.setField(artifact, "links", links);
        return artifact;
    }

    private URI getIdsEndpointUrl() {
        return UriComponentsBuilder
            .fromUriString(this.connectorUrl)
            .path("/api/ids/data").build()
            .toUri();
    }

}
