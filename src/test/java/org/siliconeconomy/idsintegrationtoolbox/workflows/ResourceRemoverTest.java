/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.workflows;

import java.net.URI;
import java.util.List;

import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.siliconeconomy.idsintegrationtoolbox.core.base.ArtifactApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.base.CatalogApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.base.ContractApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.base.OfferedResourceApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.base.RepresentationApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.base.RuleApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.*;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.ArtifactEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.CatalogEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.ContractEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.ContractRuleEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.OfferedResourceEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.RepresentationEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ArtifactLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.CatalogLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ContractLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ContractRuleLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.Link;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.OfferedResourceLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.RepresentationLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.*;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ArtifactOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.CatalogOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ContractOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ContractRuleOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.OfferedResourceOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.RepresentationOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.UuidUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {ResourceRemover.class})
@ExtendWith(SpringExtension.class)
class ResourceRemoverTest {

    final String EXAMPLE_URI = "http://localhost:8080/api/example/06dc1644-f58d-4562-836f" +
        "-197c0a65c0d6";

    final String EXAMPLE_URI_2 = "http://localhost:8080/api/example/16dc1644-f58d-4562-836f" +
        "-197c0a65c0d6";

    @Autowired
    ResourceRemover resourceRemover;

    /** Operator for the connector's Catalog Api. */
    @MockBean
    CatalogApiOperator catalogApiOperator;

    /** Operator for the connector's Offered Resource Api. */
    @MockBean
    OfferedResourceApiOperator offeredResourceApiOperator;

    /** Operator for the connector's Representation Api. */
    @MockBean
    RepresentationApiOperator representationApiOperator;

    /** Operator for the connector's Artifact Api. */
    @MockBean
    ArtifactApiOperator artifactApiOperator;

    /** Operator for the connector's Contracts Api. */
    @MockBean
    ContractApiOperator contractApiOperator;

    /** Operator for the connector's Rules Api. */
    @MockBean
    RuleApiOperator ruleApiOperator;

    /** Operator for the connector's Offered Resource-Catalogs Relation Api. */
    @MockBean
    OfferedResourceCatalogsApiOperator offeredResourceCatalogsApiOperator;

    /** Operator for the connector's Offered Resource-Representation Relation Api. */
    @MockBean
    OfferedResourceRepresentationsApiOperator
        offeredResourceRepresentationsApiOperator;

    /** Operator for the connector's Representation-Offered Resource Relation Api. */
    @MockBean
    RepresentationOfferedResourcesApiOperator
        representationOfferedResourcesApiOperator;

    /** Operator for the connector's Catalog-Offered Resources Relation Api. */
    @MockBean
    CatalogOfferedResourcesApiOperator catalogOfferedResourcesApiOperator;

    /** Operator for the connector's Representation-Artifacts Relation Api. */
    @MockBean
    RepresentationArtifactsApiOperator representationArtifactsApiOperator;

    /** Operator for the connector's Artifact-Representations Relation Api. */
    @MockBean
    ArtifactRepresentationsApiOperator artifactRepresentationsApiOperator;

    /** Operator for the connector's Offered Resource-Contracts Api. */

    @MockBean
    OfferedResourceContractsApiOperator offeredResourceContractsApiOperator;

    /** Operator for the connector's Contracts-Offered Resource Api. */
    @MockBean
    ContractOfferedResourcesApiOperator contractOfferedResourcesApiOperator;

    /** Operator for the connector's Contracts-Rules Relation Api. */
    @MockBean
    ContractRulesApiOperator contractRulesApiOperator;

    /** Operator for the connector's Rules-Contracts Relation Api. */
    @MockBean
    RuleContractsApiOperator ruleContractsApiOperator;

    @BeforeEach
    @SneakyThrows
    void setUp() {
        var link = new Link();
        ReflectionTestUtils.setField(link, "href", EXAMPLE_URI);

        final var offeredResourceSingleOutput = new OfferedResourceOutput();
        final var offeredResourceLinks = new OfferedResourceLinks();
        ReflectionTestUtils.setField(offeredResourceLinks, "self", link);
        ReflectionTestUtils.setField(offeredResourceSingleOutput, "links", offeredResourceLinks);

        final var catalogSingleOutput = new CatalogOutput();
        final var catalogLinks = new CatalogLinks();
        ReflectionTestUtils.setField(catalogLinks, "self", link);
        ReflectionTestUtils.setField(catalogSingleOutput, "links", catalogLinks);

        final var ruleSingleOutput = new ContractRuleOutput();
        final var ruleLinks = new ContractRuleLinks();
        ReflectionTestUtils.setField(ruleLinks, "self", link);
        ReflectionTestUtils.setField(ruleSingleOutput, "links", ruleLinks);


        final var contractSingleOutput = new ContractOutput();
        final var contractLinks = new ContractLinks();
        ReflectionTestUtils.setField(contractLinks, "self", link);
        ReflectionTestUtils.setField(contractSingleOutput, "links", contractLinks);


        final var representationSingleOutput = new RepresentationOutput();
        final var representationLinks = new RepresentationLinks();
        ReflectionTestUtils.setField(representationLinks, "self", link);
        ReflectionTestUtils.setField(representationSingleOutput, "links", representationLinks);


        final var artifactSingleOutput = new ArtifactOutput();
        final var artifactLinks = new ArtifactLinks();
        ReflectionTestUtils.setField(artifactLinks, "self", link);
        ReflectionTestUtils.setField(artifactSingleOutput, "links", artifactLinks);

        final var offeredResourceRepresentationRelationOutput =
            new OfferedResourceRepresentationRelationOutput();
        final var representationEmbedded = new RepresentationEmbedded();
        ReflectionTestUtils.setField(representationEmbedded, "entries",
            List.of(representationSingleOutput));
        ReflectionTestUtils.setField(offeredResourceRepresentationRelationOutput,
            "embedded", representationEmbedded);
        when(offeredResourceRepresentationsApiOperator.getAll(any()))
            .thenReturn(offeredResourceRepresentationRelationOutput);

        final var representationArtifactRelationOutput = new RepresentationArtifactRelationOutput();
        final var artifactEmbedded = new ArtifactEmbedded();
        ReflectionTestUtils.setField(artifactEmbedded, "entries",
            List.of(artifactSingleOutput));
        ReflectionTestUtils.setField(representationArtifactRelationOutput,
            "embedded", artifactEmbedded);
        when(representationArtifactsApiOperator.getAll(any()))
            .thenReturn(representationArtifactRelationOutput);

        final var artifactRepresentationRelationOutput = new ArtifactRepresentationRelationOutput();
        ReflectionTestUtils.setField(artifactRepresentationRelationOutput,
            "embedded", artifactEmbedded);
        when(artifactRepresentationsApiOperator.getAll(any()))
            .thenReturn(artifactRepresentationRelationOutput);

        final var offeredResourceContractRelationOutput =
            new OfferedResourceContractRelationOutput();
        final var contractEmbedded = new ContractEmbedded();
        ReflectionTestUtils.setField(contractEmbedded, "entries",
            List.of(contractSingleOutput));
        ReflectionTestUtils.setField(offeredResourceContractRelationOutput,
            "embedded", contractEmbedded);
        when(offeredResourceContractsApiOperator.getAll(any()))
            .thenReturn(offeredResourceContractRelationOutput);

        final var contractRuleRelationOutput = new ContractContractRuleRelationOutput();
        final var contractRuleEmbedded = new ContractRuleEmbedded();
        ReflectionTestUtils.setField(contractRuleEmbedded, "entries",
            List.of(ruleSingleOutput));
        ReflectionTestUtils.setField(contractRuleRelationOutput,
            "embedded", contractRuleEmbedded);
        when(contractRulesApiOperator.getAll(any()))
            .thenReturn(contractRuleRelationOutput);

        final var offeredResourceCatalogRelationOutput = new OfferedResourceCatalogRelationOutput();
        final var catalogEmbedded = new CatalogEmbedded();
        ReflectionTestUtils.setField(catalogEmbedded, "entries",
            List.of(catalogSingleOutput));
        ReflectionTestUtils.setField(offeredResourceCatalogRelationOutput,
            "embedded", catalogEmbedded);
        when(offeredResourceCatalogsApiOperator.getAll(any()))
            .thenReturn(offeredResourceCatalogRelationOutput);

        // Mock catalog and its relation api operator in the deleteOrUnlink method.
        final var catalogOfferedResourceRelationOutput =
            new CatalogOfferedResourceRelationOutput();
        final var offeredResourceEmbedded = new OfferedResourceEmbedded();
        ReflectionTestUtils.setField(offeredResourceEmbedded, "entries",
            List.of(offeredResourceSingleOutput));
        ReflectionTestUtils.setField(catalogOfferedResourceRelationOutput,
            "embedded", offeredResourceEmbedded);

        when(catalogApiOperator.getOne(any())).thenReturn(catalogSingleOutput);
        when(catalogOfferedResourcesApiOperator.getAll(any()))
            .thenReturn(catalogOfferedResourceRelationOutput);

        // Mocking artifact.getOne() only. Artifact relation already mocked above.
        when(artifactApiOperator.getOne(any())).thenReturn(artifactSingleOutput);

        // Mock OfferedResourceApiOperator
        when(offeredResourceApiOperator.getOne(any())).thenReturn(offeredResourceSingleOutput);

        // Mocking representation
        final var representationOfferedResourceRelationOutput =
            new RepresentationOfferedResourceRelationOutput();

        // Reusing objects created above.
        ReflectionTestUtils.setField(representationOfferedResourceRelationOutput,
            "embedded", offeredResourceEmbedded);

        when(representationApiOperator.getOne(any())).thenReturn(representationSingleOutput);
        when(representationOfferedResourcesApiOperator.getAll(any()))
            .thenReturn(representationOfferedResourceRelationOutput);

        // Mock the contract and its offered resource relation api operators.
        final var contractOfferedResourceRelationOutput =
            new ContractOfferedResourceRelationOutput();
        // Reusing objects created above.
        ReflectionTestUtils.setField(contractOfferedResourceRelationOutput,
            "embedded", offeredResourceEmbedded);

        when(contractApiOperator.getOne(any())).thenReturn(contractSingleOutput);
        when(contractOfferedResourcesApiOperator.getAll(any()))
            .thenReturn(contractOfferedResourceRelationOutput);

        // Mock the rules and its contract relations api operators. Contract Embedded defined above.
        final var rulesContractRelationOutput = new ContractRuleContractRelationOutput();
        ReflectionTestUtils.setField(rulesContractRelationOutput,
            "embedded", contractEmbedded);

        when(ruleContractsApiOperator.getAll(any())).thenReturn(rulesContractRelationOutput);

    }

    @Test
    @SneakyThrows
    void deleteFullResource_numerousRepresentationRelations_RepresentationsUnlinked_TestPass() {
        // Arrange & Act
        // Mocking multiple resources connected to a representation
        var link = new Link();
        ReflectionTestUtils.setField(link, "href", EXAMPLE_URI);
        var link2 = new Link();
        ReflectionTestUtils.setField(link2, "href", EXAMPLE_URI_2);

        final var offeredResourceSingleOutput = new OfferedResourceOutput();
        final var offeredResourceLinks = new OfferedResourceLinks();
        ReflectionTestUtils.setField(offeredResourceLinks, "self", link);
        ReflectionTestUtils.setField(offeredResourceSingleOutput, "links", offeredResourceLinks);

        final var offeredResourceSingleOutput2 = new OfferedResourceOutput();
        final var offeredResourceLinks2 = new OfferedResourceLinks();
        ReflectionTestUtils.setField(offeredResourceLinks2, "self", link2);
        ReflectionTestUtils.setField(offeredResourceSingleOutput2, "links", offeredResourceLinks2);

        final var offeredResourceEmbedded = new OfferedResourceEmbedded();
        ReflectionTestUtils.setField(offeredResourceEmbedded, "entries",
            List.of(offeredResourceSingleOutput, offeredResourceSingleOutput2));

        final var representationOfferedResourceRelationOutput =
            new RepresentationOfferedResourceRelationOutput();

        // Reusing objects created above.
        ReflectionTestUtils.setField(representationOfferedResourceRelationOutput,
            "embedded", offeredResourceEmbedded);

        when(representationOfferedResourcesApiOperator.getAll(any()))
            .thenReturn(representationOfferedResourceRelationOutput);

        final var resourceURI = URI.create(EXAMPLE_URI);
        final var resourceUUID = UuidUtils.getUuidFromUri(resourceURI);
        resourceRemover.deleteFullResource(resourceURI);

        // Assert
        // Make sure the resource delete api is called with the correct UUID.
        verify(offeredResourceApiOperator, times(1)).delete(resourceUUID);

        verify(offeredResourceRepresentationsApiOperator, times(1)).getAll(resourceUUID);

        verify(representationArtifactsApiOperator, times(1)).getAll(any());

        verify(artifactRepresentationsApiOperator, times(1)).getAll(any());

        verify(offeredResourceContractsApiOperator, times(1)).getAll(any());

        verify(contractRulesApiOperator, times(1)).getAll(any());

        verify(offeredResourceCatalogsApiOperator, times(0)).getAll(any());

        verify(catalogOfferedResourcesApiOperator, times(0)).getAll(any());

        verify(representationOfferedResourcesApiOperator, times(1)).getAll(any());

        verify(contractOfferedResourcesApiOperator, times(1)).getAll(any());

        verify(ruleContractsApiOperator, times(1)).getAll(any());

        verify(representationOfferedResourcesApiOperator, times(1)).remove(any(), any());

    }

    @Test
    @SneakyThrows
    void deleteFullResource_UriCorrectInput_TestPass() {
        // Arrange & Act
        final var resourceURI = URI.create(EXAMPLE_URI);
        final var resourceUUID = UuidUtils.getUuidFromUri(resourceURI);
        resourceRemover.deleteFullResource(resourceURI);
        // Assert
        // Make sure the resource delete api is called with the correct uri.
        verify(offeredResourceApiOperator, times(1)).delete(resourceUUID);

        verify(offeredResourceRepresentationsApiOperator, times(1)).getAll(resourceUUID);

        verify(representationArtifactsApiOperator, times(1)).getAll(any());

        verify(artifactRepresentationsApiOperator, times(1)).getAll(any());

        verify(offeredResourceContractsApiOperator, times(1)).getAll(any());

        verify(contractRulesApiOperator, times(1)).getAll(any());

        verify(offeredResourceCatalogsApiOperator, times(0)).getAll(any());

        verify(catalogOfferedResourcesApiOperator, times(0)).getAll(any());

        verify(representationOfferedResourcesApiOperator, times(1)).getAll(any());

        verify(contractOfferedResourcesApiOperator, times(1)).getAll(any());

        verify(ruleContractsApiOperator, times(1)).getAll(any());

    }


    @Test
    @SneakyThrows
    void deleteFullResource_UuidCorrectInput_TestPass() {
        // Arrange & Act
        final var resourceURI = URI.create(EXAMPLE_URI);
        final var resourceUUID = UuidUtils.getUuidFromUri(resourceURI);
        resourceRemover.deleteFullResource(resourceUUID);
        // Assert
        // Make sure the resource delete api is called with the correct uri.
        verify(offeredResourceApiOperator, times(1)).delete(resourceUUID);

        verify(offeredResourceRepresentationsApiOperator, times(1)).getAll(resourceUUID);

        verify(representationArtifactsApiOperator, times(1)).getAll(any());

        verify(artifactRepresentationsApiOperator, times(1)).getAll(any());

        verify(offeredResourceContractsApiOperator, times(1)).getAll(any());

        verify(contractRulesApiOperator, times(1)).getAll(any());

        verify(offeredResourceCatalogsApiOperator, times(0)).getAll(any());

        verify(catalogOfferedResourcesApiOperator, times(0)).getAll(any());

        verify(representationOfferedResourcesApiOperator, times(1)).getAll(any());

        verify(contractOfferedResourcesApiOperator, times(1)).getAll(any());

        verify(ruleContractsApiOperator, times(1)).getAll(any());

    }

    @Test
    @SneakyThrows
    void deleteFullResource_OutputAsCorrectInput_TestPass() {
        // Arrange & Act
        final var resourceURI = URI.create(EXAMPLE_URI);
        final var resourceUUID = UuidUtils.getUuidFromUri(resourceURI);
        var link = new Link();
        ReflectionTestUtils.setField(link, "href", EXAMPLE_URI);

        final var offeredResourceSingleOutput = new OfferedResourceOutput();
        final var offeredResourceLinks = new OfferedResourceLinks();
        ReflectionTestUtils.setField(offeredResourceLinks, "self", link);
        ReflectionTestUtils.setField(offeredResourceSingleOutput, "links", offeredResourceLinks);

        resourceRemover.deleteFullResource(offeredResourceSingleOutput);
        // Assert

        // Make sure the resource delete api is called with the correct UUID.
        verify(offeredResourceApiOperator, times(1)).delete(resourceUUID);

        verify(offeredResourceRepresentationsApiOperator, times(1)).getAll(resourceUUID);

        verify(representationArtifactsApiOperator, times(1)).getAll(any());

        verify(artifactRepresentationsApiOperator, times(1)).getAll(any());

        verify(offeredResourceContractsApiOperator, times(1)).getAll(any());

        verify(contractRulesApiOperator, times(1)).getAll(any());

        verify(offeredResourceCatalogsApiOperator, times(0)).getAll(any());

        verify(catalogOfferedResourcesApiOperator, times(0)).getAll(any());

        verify(representationOfferedResourcesApiOperator, times(1)).getAll(any());

        verify(contractOfferedResourcesApiOperator, times(1)).getAll(any());

        verify(ruleContractsApiOperator, times(1)).getAll(any());

    }

    @Test
    @SneakyThrows
    void deleteResourceAndCatalog_UriCorrectInput_TestPass() {
        // Arrange & Act
        final var resourceURI = URI.create(EXAMPLE_URI);
        final var resourceUUID = UuidUtils.getUuidFromUri(resourceURI);
        resourceRemover.deleteResourceAndCatalog(resourceURI);
        // Assert
        // Make sure the resource delete api is called with the correct uri.
        verify(offeredResourceApiOperator, times(1)).delete(resourceUUID);

        verify(offeredResourceRepresentationsApiOperator, times(1)).getAll(resourceUUID);

        verify(representationArtifactsApiOperator, times(1)).getAll(any());

        verify(artifactRepresentationsApiOperator, times(1)).getAll(any());

        verify(offeredResourceContractsApiOperator, times(1)).getAll(any());

        verify(contractRulesApiOperator, times(1)).getAll(any());

        verify(offeredResourceCatalogsApiOperator, times(1)).getAll(any());

        verify(catalogOfferedResourcesApiOperator, times(1)).getAll(any());

        verify(representationOfferedResourcesApiOperator, times(1)).getAll(any());

        verify(contractOfferedResourcesApiOperator, times(1)).getAll(any());

        verify(ruleContractsApiOperator, times(1)).getAll(any());

        verify(catalogApiOperator, times(1)).delete(any());
    }

    @Test
    @SneakyThrows
    void deleteResourceAndCatalog_UuidCorrectInput_TestPass() {
        // Arrange & Act
        final var resourceURI = URI.create(EXAMPLE_URI);
        final var resourceUUID = UuidUtils.getUuidFromUri(resourceURI);
        resourceRemover.deleteResourceAndCatalog(resourceUUID);
        // Assert
        // Make sure the resource delete api is called with the correct uri.
        verify(offeredResourceApiOperator, times(1)).delete(resourceUUID);

        verify(offeredResourceRepresentationsApiOperator, times(1)).getAll(resourceUUID);

        verify(representationArtifactsApiOperator, times(1)).getAll(any());

        verify(artifactRepresentationsApiOperator, times(1)).getAll(any());

        verify(offeredResourceContractsApiOperator, times(1)).getAll(any());

        verify(contractRulesApiOperator, times(1)).getAll(any());

        verify(offeredResourceCatalogsApiOperator, times(1)).getAll(any());

        verify(catalogOfferedResourcesApiOperator, times(1)).getAll(any());

        verify(representationOfferedResourcesApiOperator, times(1)).getAll(any());

        verify(contractOfferedResourcesApiOperator, times(1)).getAll(any());

        verify(ruleContractsApiOperator, times(1)).getAll(any());

        verify(catalogApiOperator, times(1)).delete(any());
    }

    @Test
    @SneakyThrows
    void deleteResourceAndCatalog_OutputAsCorrectInput_TestPass() {
        // Arrange & Act
        final var resourceURI = URI.create(EXAMPLE_URI);
        final var resourceUUID = UuidUtils.getUuidFromUri(resourceURI);
        var link = new Link();
        ReflectionTestUtils.setField(link, "href", EXAMPLE_URI);

        final var offeredResourceSingleOutput = new OfferedResourceOutput();
        final var offeredResourceLinks = new OfferedResourceLinks();
        ReflectionTestUtils.setField(offeredResourceLinks, "self", link);
        ReflectionTestUtils.setField(offeredResourceSingleOutput, "links", offeredResourceLinks);

        resourceRemover.deleteResourceAndCatalog(offeredResourceSingleOutput);
        // Assert
        // Make sure the resource delete api is called with the correct uri.
        verify(offeredResourceApiOperator, times(1)).delete(resourceUUID);

        verify(offeredResourceRepresentationsApiOperator, times(1)).getAll(resourceUUID);

        verify(representationArtifactsApiOperator, times(1)).getAll(any());

        verify(artifactRepresentationsApiOperator, times(1)).getAll(any());

        verify(offeredResourceContractsApiOperator, times(1)).getAll(any());

        verify(contractRulesApiOperator, times(1)).getAll(any());

        verify(offeredResourceCatalogsApiOperator, times(1)).getAll(any());

        verify(catalogOfferedResourcesApiOperator, times(1)).getAll(any());

        verify(representationOfferedResourcesApiOperator, times(1)).getAll(any());

        verify(contractOfferedResourcesApiOperator, times(1)).getAll(any());

        verify(ruleContractsApiOperator, times(1)).getAll(any());

        verify(catalogApiOperator, times(1)).delete(any());
    }

    @Test
    @SneakyThrows
    void deleteCatalog_UuidCorrectInput_TestPass() {
        // Arrange & Act
        final var catalogURI = URI.create(EXAMPLE_URI);
        final var catalogUUID = UuidUtils.getUuidFromUri(catalogURI);
        resourceRemover.deleteCatalog(catalogUUID);
        // Assert
        // Make sure the resource delete api is called with the correct uri.
        verify(offeredResourceApiOperator, times(1)).delete(any());

        verify(offeredResourceRepresentationsApiOperator, times(1)).getAll(any());

        verify(representationArtifactsApiOperator, times(1)).getAll(any());

        verify(artifactRepresentationsApiOperator, times(1)).getAll(any());

        verify(offeredResourceContractsApiOperator, times(1)).getAll(any());

        verify(contractRulesApiOperator, times(1)).getAll(any());

        verify(offeredResourceCatalogsApiOperator, times(0)).getAll(any());

        verify(catalogOfferedResourcesApiOperator, times(1)).getAll(any());

        verify(representationOfferedResourcesApiOperator, times(1)).getAll(any());

        verify(contractOfferedResourcesApiOperator, times(1)).getAll(any());

        verify(ruleContractsApiOperator, times(1)).getAll(any());

        verify(catalogApiOperator, times(1)).delete(catalogUUID);
    }



    @Test
    @SneakyThrows
    void deleteCatalog_UriCorrectInput_TestPass() {
        // Arrange & Act
        final var catalogURI = URI.create(EXAMPLE_URI);
        final var catalogUUID = UuidUtils.getUuidFromUri(catalogURI);
        resourceRemover.deleteCatalog(catalogURI);
        // Assert
        // Make sure the resource delete api is called with the correct uri.
        verify(offeredResourceApiOperator, times(1)).delete(any());

        verify(offeredResourceRepresentationsApiOperator, times(1)).getAll(any());

        verify(representationArtifactsApiOperator, times(1)).getAll(any());

        verify(artifactRepresentationsApiOperator, times(1)).getAll(any());

        verify(offeredResourceContractsApiOperator, times(1)).getAll(any());

        verify(contractRulesApiOperator, times(1)).getAll(any());

        verify(offeredResourceCatalogsApiOperator, times(0)).getAll(any());

        verify(catalogOfferedResourcesApiOperator, times(1)).getAll(any());

        verify(representationOfferedResourcesApiOperator, times(1)).getAll(any());

        verify(contractOfferedResourcesApiOperator, times(1)).getAll(any());

        verify(ruleContractsApiOperator, times(1)).getAll(any());

        verify(catalogApiOperator, times(1)).delete(catalogUUID);

    }



    @Test
    @SneakyThrows
    void deleteCatalog_OutputAsCorrectInput_TestPass() {
        // Arrange & Act
        final var catalogURI = URI.create(EXAMPLE_URI);
        final var catalogUUID = UuidUtils.getUuidFromUri(catalogURI);
        var link = new Link();
        ReflectionTestUtils.setField(link, "href", EXAMPLE_URI);

        final var catalogSingleOutput = new CatalogOutput();
        final var catalogLinks = new CatalogLinks();
        ReflectionTestUtils.setField(catalogLinks, "self", link);
        ReflectionTestUtils.setField(catalogSingleOutput, "links", catalogLinks);

        resourceRemover.deleteCatalog(catalogSingleOutput);
        // Assert
        // Make sure the resource delete api is called with the correct uri.
        verify(offeredResourceApiOperator, times(1)).delete(any());

        verify(offeredResourceRepresentationsApiOperator, times(1)).getAll(any());

        verify(representationArtifactsApiOperator, times(1)).getAll(any());

        verify(artifactRepresentationsApiOperator, times(1)).getAll(any());

        verify(offeredResourceContractsApiOperator, times(1)).getAll(any());

        verify(contractRulesApiOperator, times(1)).getAll(any());

        verify(offeredResourceCatalogsApiOperator, times(0)).getAll(any());

        verify(catalogOfferedResourcesApiOperator, times(1)).getAll(any());

        verify(representationOfferedResourcesApiOperator, times(1)).getAll(any());

        verify(contractOfferedResourcesApiOperator, times(1)).getAll(any());

        verify(ruleContractsApiOperator, times(1)).getAll(any());

        verify(catalogApiOperator, times(1)).delete(catalogUUID);

    }
}
