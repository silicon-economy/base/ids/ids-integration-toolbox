/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.workflows;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.idsintegrationtoolbox.core.base.OfferedResourceApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.ids.MessagesApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.OfferedResourceEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.Link;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.OfferedResourceLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.OfferedResourceMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.OfferedResourceOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.util.ReflectionTestUtils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {BrokerService.class})
class BrokerServiceTest {

    /** Class under test */
    @Autowired
    private BrokerService brokerService;

    @MockBean
    private MessagesApiOperator messagesApiOperator;

    @MockBean
    private OfferedResourceApiOperator resourceApiOperator;

    private final URI brokerUrl = URI.create("https://my-broker:8080/infrastructure");

    @Test
    @SneakyThrows
    void register_successfulRequest() {
        // Arrange
        doNothing().when(messagesApiOperator).connectorUpdate(brokerUrl);

        // Act & Assert
        assertThatCode(() -> brokerService.register(brokerUrl)).doesNotThrowAnyException();
    }

    @Test
    @SneakyThrows
    void register_unsuccessfulRequest_throwException() {
        // Arrange
        doThrow(ApiInteractionUnsuccessfulException.class)
            .when(messagesApiOperator).connectorUpdate(brokerUrl);

        // Act && Assert
        assertThatThrownBy(() -> brokerService.register(brokerUrl))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class);
    }

    @Test
    @SneakyThrows
    void register_successfulRequestWithResourceIds() {
        // Arrange
        final var resourceIds = getResourceIds();

        doNothing().when(messagesApiOperator).connectorUpdate(brokerUrl);
        doNothing().when(messagesApiOperator).resourceUpdate(brokerUrl, eq(any()));

        // Act & Assert
        assertThatCode(() -> brokerService.register(brokerUrl, resourceIds))
            .doesNotThrowAnyException();
    }

    @Test
    @SneakyThrows
    void register_unsuccessfulRequestWithResourceIds_throwException() {
        // Arrange
        final var resourceIds = getResourceIds();

        doNothing().when(messagesApiOperator).connectorUpdate(brokerUrl);
        doThrow(ApiInteractionUnsuccessfulException.class)
            .when(messagesApiOperator).resourceUpdate(any(), any());

        // Act && Assert
        assertThatThrownBy(() -> brokerService.register(brokerUrl, resourceIds))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class);
    }

    @Test
    @SneakyThrows
    void registerResources_successfulRequest() {
        // Arrange
        final var resourceIds = getResourceIds();

        doNothing().when(messagesApiOperator).resourceUpdate(brokerUrl, eq(any()));

        // Act & Assert
        assertThatCode(() -> brokerService.registerResources(brokerUrl, resourceIds))
            .doesNotThrowAnyException();
    }

    @Test
    @SneakyThrows
    void registerResources_unsuccessfulRequest() {
        // Arrange
        final var resourceIds = getResourceIds();

        doThrow(ApiInteractionUnsuccessfulException.class)
            .when(messagesApiOperator).resourceUpdate(any(), any());

        // Act && Assert
        assertThatThrownBy(() -> brokerService.registerResources(brokerUrl, resourceIds))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class);
    }

    @Test
    @SneakyThrows
    void registerAllResources_successfulRequest() {
        // Arrange
        final var resources = getResources();

        doNothing().when(messagesApiOperator).connectorUpdate(brokerUrl);
        doNothing().when(messagesApiOperator).resourceUpdate(brokerUrl, eq(any()));
        when(resourceApiOperator.getAll()).thenReturn(resources);

        // Act & Assert
        assertThatCode(() -> brokerService.registerAllResources(brokerUrl))
            .doesNotThrowAnyException();
    }

    @Test
    @SneakyThrows
    void registerAllResources_unsuccessfulRequest() {
        // Arrange
        final var resources = getResources();

        doNothing().when(messagesApiOperator).connectorUpdate(brokerUrl);
        doThrow(ApiInteractionUnsuccessfulException.class)
            .when(messagesApiOperator).resourceUpdate(any(), any());
        when(resourceApiOperator.getAll()).thenReturn(resources);

        // Act && Assert
        assertThatThrownBy(() -> brokerService.registerAllResources(brokerUrl))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class);
    }

    @Test
    @SneakyThrows
    void unregister_successfulRequest() {
        // Arrange
        doNothing().when(messagesApiOperator).connectorUnavailable(brokerUrl);

        // Act & Assert
        assertThatCode(() -> brokerService.unregister(brokerUrl)).doesNotThrowAnyException();
    }

    @Test
    @SneakyThrows
    void unregister_unsuccessfulRequest_throwException() {
        // Arrange
        doThrow(ApiInteractionUnsuccessfulException.class)
            .when(messagesApiOperator).connectorUnavailable(brokerUrl);

        // Act && Assert
        assertThatThrownBy(() -> brokerService.unregister(brokerUrl))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class);
    }

    @Test
    @SneakyThrows
    void unregisterResources_successfulRequestWithResourceIds() {
        // Arrange
        final var resourceIds = getResourceIds();

        doNothing().when(messagesApiOperator).resourceUnavailable(brokerUrl, eq(any()));

        // Act & Assert
        assertThatCode(() -> brokerService.unregisterResources(brokerUrl, resourceIds))
            .doesNotThrowAnyException();
    }

    @Test
    @SneakyThrows
    void unregisterResources_unsuccessfulRequestWithResourceIds_throwException() {
        // Arrange
        final var resourceIds = getResourceIds();

        doThrow(ApiInteractionUnsuccessfulException.class)
            .when(messagesApiOperator).resourceUnavailable(any(), any());

        // Act && Assert
        assertThatThrownBy(() -> brokerService.unregisterResources(brokerUrl, resourceIds))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class);
    }

    @Test
    @SneakyThrows
    void search_successfulRequest_returnResult() {
        // Arrange
        final var searchTerm = "searchTerm";
        final var brokerResult = "result";

        when(messagesApiOperator.search(brokerUrl, searchTerm, 50, 0))
            .thenReturn(brokerResult);

        // Act
        var result = brokerService.search(brokerUrl, searchTerm);

        // Assert
        assertThat(result).isEqualTo(brokerResult);
    }

    @Test
    @SneakyThrows
    void search_unsuccessfulRequest_throwException() {
        // Arrange
        final var searchTerm = "searchTerm";
        final var brokerResult = "result";

        when(messagesApiOperator.search(brokerUrl, searchTerm, 50, 0))
            .thenThrow(ApiInteractionUnsuccessfulException.class);

        // Act && Assert
        assertThatThrownBy(() -> brokerService.search(brokerUrl, searchTerm))
            .isInstanceOf(ApiInteractionUnsuccessfulException.class);
    }

    private List<URI> getResourceIds() {
        return new ArrayList<>() {{
            add(URI.create("https://resource1"));
            add(URI.create("https://resource2"));
        }};
    }

    private OfferedResourceMultiOutput getResources() {
        var link = new Link();
        ReflectionTestUtils.setField(link, "href", "https://resource");

        var links = new OfferedResourceLinks();
        ReflectionTestUtils.setField(links, "self", link);

        var resource = new OfferedResourceOutput();
        ReflectionTestUtils.setField(resource, "links", links);

        var embedded = new OfferedResourceEmbedded();
        ReflectionTestUtils.setField(embedded, "entries",
            Collections.singletonList(resource));

        var output = new OfferedResourceMultiOutput();
        ReflectionTestUtils.setField(output, "embedded", embedded);
        ReflectionTestUtils.setField(output, "links", links);

        return output;
    }

}
