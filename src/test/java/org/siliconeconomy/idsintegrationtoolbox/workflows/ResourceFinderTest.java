/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.workflows;

import java.net.URI;
import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Stream;

import de.fraunhofer.iais.eis.BaseConnectorBuilder;
import de.fraunhofer.iais.eis.Connector;
import de.fraunhofer.iais.eis.ConnectorEndpointBuilder;
import de.fraunhofer.iais.eis.Resource;
import de.fraunhofer.iais.eis.ResourceBuilder;
import de.fraunhofer.iais.eis.ResourceCatalog;
import de.fraunhofer.iais.eis.ResourceCatalogBuilder;
import de.fraunhofer.iais.eis.SecurityProfile;
import de.fraunhofer.iais.eis.util.TypedLiteral;
import de.fraunhofer.iais.eis.util.Util;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.MockitoAnnotations;
import org.siliconeconomy.idsintegrationtoolbox.core.ids.MessagesApiOperator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

/**
 * ResourceFinderTest
 *
 * @author Haydar Qarawlus
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {ResourceFinder.class})
class ResourceFinderTest {

    // Class under test
    @Autowired
    private ResourceFinder resourceFinder;

    // Dependencies
    @MockBean
    MessagesApiOperator messagesApiOperator;

    private String resourceTitle = "Resource title";
    private String resourceKeyword = "Resource keyword";
    private String resourceDescription = "Resource Description";
    private URI resourceId;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
        resourceId = URI.create("https://example.com/offered-resource");
    }

    @Test
    @SneakyThrows
    void findResourceByTitleExactMatch_resourceExists_returnResourceUri() {
        // Arrange
        final var resource = getResource(resourceId);
        final var catalog = getResourceCatalog(resource);
        final var connector = getConnectorDesc(catalog);

        when(messagesApiOperator.descriptionRequest(any(URI.class)))
            .thenReturn(connector);
        when(messagesApiOperator.descriptionRequest(any(URI.class), any(URI.class)))
            .thenReturn(catalog);

        // Act
        final var results = resourceFinder.findResourceByTitle(
            URI.create("https://example.com/connector"),
            "Resource title", true);

        // Assert
        assertThat(results.size()).isEqualTo(1);
        assertThat(results.get(0)).isEqualTo(resourceId);
    }

    @Test
    @SneakyThrows
    void findResourceByTitleNotExactMatch_resourceExists_returnResourceUri() {
        // Arrange
        final var resource = getResource(resourceId);
        final var catalog = getResourceCatalog(resource);
        final var connector = getConnectorDesc(catalog);

        when(messagesApiOperator.descriptionRequest(any(URI.class)))
            .thenReturn(connector);
        when(messagesApiOperator.descriptionRequest(any(URI.class), any(URI.class)))
            .thenReturn(catalog);

        // Act
        final var results = resourceFinder.findResourceByTitle(
            URI.create("https://example.com/connector"),
            "Resource", false);

        // Assert
        assertThat(results.size()).isEqualTo(1);
        assertThat(results.get(0)).isEqualTo(resourceId);
    }

    @Test
    @SneakyThrows
    void findResourceByTitle_resourceDoesNotExist_returnEmptyList() {
        // Arrange
        final var resource = getResource(resourceId);
        final var catalog = getResourceCatalog(resource);
        final var connector = getConnectorDesc(catalog);

        when(messagesApiOperator.descriptionRequest(any(URI.class)))
            .thenReturn(connector);
        when(messagesApiOperator.descriptionRequest(any(URI.class), any(URI.class)))
            .thenReturn(catalog);

        // Act
        final var results = resourceFinder.findResourceByTitle(
            URI.create("https://example.com/connector"),
            "Incorrect Resource", true);

        // Assert
        assertThat(results).isEmpty();
    }

    @Test
    @SneakyThrows
    void findResourceByDescriptionExactMatch_resourceExists_returnResourceUri() {
        // Arrange
        final var resource = getResource(resourceId);
        final var catalog = getResourceCatalog(resource);
        final var connector = getConnectorDesc(catalog);

        when(messagesApiOperator.descriptionRequest(any(URI.class)))
            .thenReturn(connector);
        when(messagesApiOperator.descriptionRequest(any(URI.class), any(URI.class)))
            .thenReturn(catalog);

        // Act
        final var results = resourceFinder.findResourceByDescription(
            URI.create("https://example.com/connector"),
            "Resource Description", true);

        // Assert
        assertThat(results.size()).isEqualTo(1);
        assertThat(results.get(0)).isEqualTo(resourceId);
    }

    @Test
    @SneakyThrows
    void findResourceByDescriptionNotExactMatch_resourceExists_returnResourceUri() {
        // Arrange
        final var resource = getResource(resourceId);
        final var catalog = getResourceCatalog(resource);
        final var connector = getConnectorDesc(catalog);

        when(messagesApiOperator.descriptionRequest(any(URI.class)))
            .thenReturn(connector);
        when(messagesApiOperator.descriptionRequest(any(URI.class), any(URI.class)))
            .thenReturn(catalog);

        // Act
        final var results = resourceFinder.findResourceByDescription(
            URI.create("https://example.com/connector"),
            "Resource", false);

        // Assert
        assertThat(results.size()).isEqualTo(1);
        assertThat(results.get(0)).isEqualTo(resourceId);
    }

    @Test
    @SneakyThrows
    void findResourceByDescription_resourceDoesNotExist_returnEmptyList() {
        // Arrange
        final var resource = getResource(resourceId);
        final var catalog = getResourceCatalog(resource);
        final var connector = getConnectorDesc(catalog);

        when(messagesApiOperator.descriptionRequest(any(URI.class)))
            .thenReturn(connector);
        when(messagesApiOperator.descriptionRequest(any(URI.class), any(URI.class)))
            .thenReturn(catalog);

        // Act
        final var results = resourceFinder.findResourceByDescription(
            URI.create("https://example.com/connector"),
            "Incorrect Resource", true);

        // Assert
        assertThat(results).isEmpty();
    }

    @Test
    @SneakyThrows
    void findResourceByKeywordExactMatch_resourceExists_returnResourceUri() {
        // Arrange
        final var resource = getResource(resourceId);
        final var catalog = getResourceCatalog(resource);
        final var connector = getConnectorDesc(catalog);

        when(messagesApiOperator.descriptionRequest(any(URI.class)))
            .thenReturn(connector);
        when(messagesApiOperator.descriptionRequest(any(URI.class), any(URI.class)))
            .thenReturn(catalog);

        // Act
        final var results = resourceFinder.findResourceByKeyword(
            URI.create("https://example.com/connector"),
            "Resource keyword", true);

        // Assert
        assertThat(results.size()).isEqualTo(1);
        assertThat(results.get(0)).isEqualTo(resourceId);
    }

    @Test
    @SneakyThrows
    void findResourceByKeywordNotExactMatch_resourceExists_returnResourceUri() {
        // Arrange
        final var resource = getResource(resourceId);
        final var catalog = getResourceCatalog(resource);
        final var connector = getConnectorDesc(catalog);

        when(messagesApiOperator.descriptionRequest(any(URI.class)))
            .thenReturn(connector);
        when(messagesApiOperator.descriptionRequest(any(URI.class), any(URI.class)))
            .thenReturn(catalog);

        // Act
        final var results = resourceFinder.findResourceByKeyword(
            URI.create("https://example.com/connector"),
            "Resource", false);

        // Assert
        assertThat(results.size()).isEqualTo(1);
        assertThat(results.get(0)).isEqualTo(resourceId);
    }

    @Test
    @SneakyThrows
    void findResourceByKeyword_resourceDoesNotExist_returnEmptyList() {
        // Arrange
        final var resource = getResource(resourceId);
        final var catalog = getResourceCatalog(resource);
        final var connector = getConnectorDesc(catalog);

        when(messagesApiOperator.descriptionRequest(any(URI.class)))
            .thenReturn(connector);
        when(messagesApiOperator.descriptionRequest(any(URI.class), any(URI.class)))
            .thenReturn(catalog);

        // Act
        final var results = resourceFinder.findResourceByKeyword(
            URI.create("https://example.com/connector"),
            "Incorrect Resource", true);

        // Assert
        assertThat(results).isEmpty();
    }

    @ParameterizedTest
    @MethodSource("provideArgumentsForFindByAny")
    @SneakyThrows
    void findResourceByAny_resourceExists_returnResourceUri(URI connectorUri, String searchTerm,
                                                            boolean isExactMatch) {
        // Arrange
        final var resource = getResource(resourceId);
        final var catalog = getResourceCatalog(resource);
        final var connector = getConnectorDesc(catalog);

        when(messagesApiOperator.descriptionRequest(any(URI.class)))
            .thenReturn(connector);
        when(messagesApiOperator.descriptionRequest(any(URI.class), any(URI.class)))
            .thenReturn(catalog);

        // Act
        final var results = resourceFinder
            .findResourceByAny(connectorUri, searchTerm, isExactMatch);

        // Assert
        assertThat(results.size()).isEqualTo(1);
        assertThat(results.get(0)).isEqualTo(resourceId);
    }

    @Test
    @SneakyThrows
    void findResourceByAny_wKeyword_resourceDoesNotExist_returnEmptyList() {
        // Arrange
        final var resource = getResource(resourceId);
        final var catalog = getResourceCatalog(resource);
        final var connector = getConnectorDesc(catalog);

        when(messagesApiOperator.descriptionRequest(any(URI.class)))
            .thenReturn(connector);
        when(messagesApiOperator.descriptionRequest(any(URI.class), any(URI.class)))
            .thenReturn(catalog);

        // Act
        final var results = resourceFinder.findResourceByAny(
            URI.create("https://example.com/connector"),
            "Resource does not exist", true);

        // Assert
        assertThat(results).isEmpty();
    }

    @Test
    @SneakyThrows
    void findResourceByAny_nullValueInTypedLiteral_returnEmptyList() {
        // Arrange
        final var resource = getResourceWithNullValueInTypedLiteral();
        final var catalog = getResourceCatalog(resource);
        final var connector = getConnectorDesc(catalog);

        when(messagesApiOperator.descriptionRequest(any(URI.class)))
            .thenReturn(connector);
        when(messagesApiOperator.descriptionRequest(any(URI.class), any(URI.class)))
            .thenReturn(catalog);

        // Act
        final var results = resourceFinder.findResourceByAny(
            URI.create("https://example.com/connector"), "Resource", false);

        // Assert
        assertThat(results).isEmpty();
    }

    @Test
    @SneakyThrows
    void findResourceByTitle_multipleCatalogs_returnResourceId() {
        // Arrange
        var resourceId1 = URI.create("https://resource1");
        var resource1 = getResource(resourceId1);
        var catalogId1 = URI.create("https://catalog1");
        var catalog1 = getResourceCatalog(catalogId1, resource1);

        var resourceId2 = URI.create("https://resource2");
        var resource2 = getResource(resourceId2);
        var catalogId2 = URI.create("https://catalog2");
        var catalog2 = getResourceCatalog(catalogId2, resource2);

        var connector = getConnectorDesc(catalog1, catalog2);

        when(messagesApiOperator.descriptionRequest(any())).thenReturn(connector);
        when(messagesApiOperator.descriptionRequest(any(), eq(catalogId1))).thenReturn(catalog1);
        when(messagesApiOperator.descriptionRequest(any(), eq(catalogId2))).thenReturn(catalog2);

        // Act
        final var results = resourceFinder.findResourceByTitle(
            URI.create("https://example.com/connector"), "Resource", false);

        // Assert
        assertThat(results)
            .isNotEmpty()
            .hasSize(2)
            .containsExactly(resourceId1, resourceId2);
    }

    @Test
    @SneakyThrows
    void findResourceByKeyword_multipleCatalogs_returnResourceId() {
        // Arrange
        var resourceId1 = URI.create("https://resource1");
        var resource1 = getResource(resourceId1);
        var catalogId1 = URI.create("https://catalog1");
        var catalog1 = getResourceCatalog(catalogId1, resource1);

        var resourceId2 = URI.create("https://resource2");
        var resource2 = getResource(resourceId2);
        var catalogId2 = URI.create("https://catalog2");
        var catalog2 = getResourceCatalog(catalogId2, resource2);

        var connector = getConnectorDesc(catalog1, catalog2);

        when(messagesApiOperator.descriptionRequest(any())).thenReturn(connector);
        when(messagesApiOperator.descriptionRequest(any(), eq(catalogId1))).thenReturn(catalog1);
        when(messagesApiOperator.descriptionRequest(any(), eq(catalogId2))).thenReturn(catalog2);

        // Act
        final var results = resourceFinder.findResourceByKeyword(
            URI.create("https://example.com/connector"), "Resource", false);

        // Assert
        assertThat(results)
            .isNotEmpty()
            .hasSize(2)
            .containsExactly(resourceId1, resourceId2);
    }

    @Test
    @SneakyThrows
    void findResourceByDescription_multipleCatalogs_returnResourceId() {
        // Arrange
        var resourceId1 = URI.create("https://resource1");
        var resource1 = getResource(resourceId1);
        var catalogId1 = URI.create("https://catalog1");
        var catalog1 = getResourceCatalog(catalogId1, resource1);

        var resourceId2 = URI.create("https://resource2");
        var resource2 = getResource(resourceId2);
        var catalogId2 = URI.create("https://catalog2");
        var catalog2 = getResourceCatalog(catalogId2, resource2);

        var connector = getConnectorDesc(catalog1, catalog2);

        when(messagesApiOperator.descriptionRequest(any())).thenReturn(connector);
        when(messagesApiOperator.descriptionRequest(any(), eq(catalogId1))).thenReturn(catalog1);
        when(messagesApiOperator.descriptionRequest(any(), eq(catalogId2))).thenReturn(catalog2);

        // Act
        final var results = resourceFinder.findResourceByDescription(
            URI.create("https://example.com/connector"), "Resource", false);

        // Assert
        assertThat(results)
            .isNotEmpty()
            .hasSize(2)
            .containsExactly(resourceId1, resourceId2);
    }

    @Test
    @SneakyThrows
    void findResourceByAny_multipleCatalogs_returnResourceId() {
        // Arrange
        var resourceId1 = URI.create("https://resource1");
        var resource1 = getResource(resourceId1);
        var catalogId1 = URI.create("https://catalog1");
        var catalog1 = getResourceCatalog(catalogId1, resource1);

        var resourceId2 = URI.create("https://resource2");
        var resource2 = getResource(resourceId2);
        var catalogId2 = URI.create("https://catalog2");
        var catalog2 = getResourceCatalog(catalogId2, resource2);

        var connector = getConnectorDesc(catalog1, catalog2);

        when(messagesApiOperator.descriptionRequest(any())).thenReturn(connector);
        when(messagesApiOperator.descriptionRequest(any(), eq(catalogId1))).thenReturn(catalog1);
        when(messagesApiOperator.descriptionRequest(any(), eq(catalogId2))).thenReturn(catalog2);

        // Act
        final var results = resourceFinder.findResourceByAny(
            URI.create("https://example.com/connector"), "Resource", false);

        // Assert
        assertThat(results)
            .isNotEmpty()
            .hasSize(2)
            .containsExactly(resourceId1, resourceId2);
    }


    private static Stream<Arguments> provideArgumentsForFindByAny() {
        final var connectorUri = URI.create("https://example.com/connector");
        return Stream.of(
            Arguments.of(connectorUri, "Resource title", true),
            Arguments.of(connectorUri, "Resource Description", true),
            Arguments.of(connectorUri, "Resource keyword", true),
            Arguments.of(connectorUri, "title", false),
            Arguments.of(connectorUri, "Description", false),
            Arguments.of(connectorUri, "keyword", false));
    }

    Resource getResource(URI resourceId) {
        return new ResourceBuilder(resourceId)
            ._title_(Collections.singletonList(new TypedLiteral(this.resourceTitle)))
            ._keyword_(Collections.singletonList(new TypedLiteral(this.resourceKeyword)))
            ._description_(Collections.singletonList(new TypedLiteral(this.resourceDescription)))
            .build();
    }

    Resource getResourceWithNullValueInTypedLiteral() {
        return new ResourceBuilder(this.resourceId)
            ._title_(Collections
                .singletonList(new TypedLiteral(null, URI.create("https://type"))))
            ._keyword_(Collections
                .singletonList(new TypedLiteral(null, URI.create("https://type"))))
            ._description_(Collections
                .singletonList(new TypedLiteral(null, URI.create("https://type"))))
            .build();
    }

    ResourceCatalog getResourceCatalog(final URI id, final Resource resource) {
        return new ResourceCatalogBuilder(id)
            ._offeredResource_(Collections.singletonList(resource))
            .build();
    }

    ResourceCatalog getResourceCatalog(final Resource resource) {
        return getResourceCatalog(URI.create("https://example.com/resource-catalog"), resource);
    }

    Connector getConnectorDesc(final ResourceCatalog... catalogs) {
        return new BaseConnectorBuilder()
            ._securityProfile_(SecurityProfile.BASE_SECURITY_PROFILE)
            ._maintainer_(URI.create("https://some-url.com"))
            ._curator_(URI.create("https://some-url.com"))
            ._hasDefaultEndpoint_(new ConnectorEndpointBuilder()
                ._accessURL_(URI.create("https://some-url.com"))
                .build())
            ._inboundModelVersion_(Util.asList("4.0.0"))
            ._outboundModelVersion_("4.0.0")
            ._resourceCatalog_(Arrays.asList(catalogs))
            .build();
    }
}
