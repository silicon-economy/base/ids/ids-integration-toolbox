/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.workflows;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import lombok.SneakyThrows;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.AgreementArtifactsApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.ArtifactAgreementsApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.ContractOfferedResourcesApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.ContractRequestedResourcesApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.ContractRulesApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.OfferedResourceContractsApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.RelationApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.RequestedResourceContractsApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.RuleContractsApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.AbstractEntity;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.Embedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.AgreementLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ArtifactLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ContractLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ContractRuleLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.Link;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.Links;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.OfferedResourceLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.RequestedResourceLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.AgreementOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ArtifactOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ContractRuleOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ContractOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.EntityOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.OfferedResourceOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.RequestedResourceOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.OperationNotAllowedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.util.ReflectionTestUtils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * ContractNegotiatorTest
 *
 * @author Florian Zimmer
 */
@SpringBootTest(classes = RelationLinker.class)
class RelationLinkerTest {

    /** Class under test */
    @Autowired
    private RelationLinker relationLinker;

    @MockBean
    private List<RelationApiOperator<? extends AbstractEntity, ? extends Links<?>,
        ? extends AbstractEntity, ? extends EntityOutput<?>,
        ? extends Embedded<?, ?>>> relationApiOperators;

    @MockBean
    private ContractOfferedResourcesApiOperator contractOfferedResourcesApiOperator;
    @MockBean
    private OfferedResourceContractsApiOperator offeredResourceContractsApiOperator;
    @MockBean
    private ContractRequestedResourcesApiOperator contractRequestedResourcesApiOperator;
    @MockBean
    private RequestedResourceContractsApiOperator requestedResourceContractsApiOperator;
    @MockBean
    private ContractRulesApiOperator contractRulesApiOperator;
    @MockBean
    private RuleContractsApiOperator ruleContractsApiOperator;
    @MockBean
    private ArtifactAgreementsApiOperator artifactAgreementsApiOperator;
    @MockBean
    private AgreementArtifactsApiOperator agreementArtifactsApiOperator;

    @Captor
    private ArgumentCaptor<UUID> uuidArgumentCaptor;
    @Captor
    private ArgumentCaptor<List<URI>> urisArgumentCaptor;

    @Spy
    private Logger log;

    @BeforeEach
    @SneakyThrows
    void init() {
        relationApiOperators = new ArrayList<>();

        relationApiOperators.add(contractOfferedResourcesApiOperator);
        relationApiOperators.add(offeredResourceContractsApiOperator);
        relationApiOperators.add(contractRequestedResourcesApiOperator);
        relationApiOperators.add(requestedResourceContractsApiOperator);
        relationApiOperators.add(contractRulesApiOperator);
        relationApiOperators.add(ruleContractsApiOperator);
        relationApiOperators.add(artifactAgreementsApiOperator);
        relationApiOperators.add(agreementArtifactsApiOperator);

        ReflectionTestUtils.setField(relationLinker, "relationApiOperators", relationApiOperators);
    }

    @Test
    @SneakyThrows
    void linkEntities_linkableEntities_shouldGetLinked() {
        // Arrange
        var entity1 = new ContractOutput();
        var uuid1 = "e7f01ba7-0d2a-4709-88ce-c2e3e9a1cc83";
        var uri1 = "http://localhost:8080/api/representations/" + uuid1;
        var contractLinks = new ContractLinks();
        var link1 = new Link();
        ReflectionTestUtils.setField(link1, "href", uri1);
        ReflectionTestUtils.setField(contractLinks, "self", link1);
        ReflectionTestUtils.setField(entity1, "links", contractLinks);
        ReflectionTestUtils.setField(entity1, "title", "contract");

        var entity2 = new RequestedResourceOutput();
        var uuid2 = "e7f01ba7-0d2a-4709-88ce-c2e3e9a1cc84";
        var uri2 = "http://localhost:8080/api/representations/" + uuid2;
        var requestedResourceLinks = new RequestedResourceLinks();
        var link2 = new Link();
        ReflectionTestUtils.setField(link2, "href", uri2);
        ReflectionTestUtils.setField(requestedResourceLinks, "self", link2);
        ReflectionTestUtils.setField(entity2, "links", requestedResourceLinks);
        ReflectionTestUtils.setField(entity2, "title", "requestedResource");

        var entity3 = new OfferedResourceOutput();
        var uuid3 = "e7f01ba7-0d2a-4709-88ce-c2e3e9a1cc85";
        var uri3 = "http://localhost:8080/api/representations/" + uuid3;
        var offeredResourceLinks = new OfferedResourceLinks();
        var link3 = new Link();
        ReflectionTestUtils.setField(link3, "href", uri3);
        ReflectionTestUtils.setField(offeredResourceLinks, "self", link3);
        ReflectionTestUtils.setField(entity3, "links", offeredResourceLinks);
        ReflectionTestUtils.setField(entity3, "title", "offeredResource");

        var entity4 = new ContractRuleOutput();
        var uuid4 = "e7f01ba7-0d2a-4709-88ce-c2e3e9a1cc86";
        var uri4 = "http://localhost:8080/api/representations/" + uuid4;
        var contractRuleLinks = new ContractRuleLinks();
        var link4 = new Link();
        ReflectionTestUtils.setField(link4, "href", uri4);
        ReflectionTestUtils.setField(contractRuleLinks, "self", link4);
        ReflectionTestUtils.setField(entity4, "links", contractRuleLinks);
        ReflectionTestUtils.setField(entity4, "title", "contractRule");

        // Act
        relationLinker.linkEntities(entity1, entity2, entity3, entity4);

        // Assert
        verify(contractRequestedResourcesApiOperator, times(1)).add(uuidArgumentCaptor.capture(),
            urisArgumentCaptor.capture());
        verify(requestedResourceContractsApiOperator, times(0)).add(any(), any());
        verify(contractOfferedResourcesApiOperator, times(1)).add(uuidArgumentCaptor.capture(),
            urisArgumentCaptor.capture());
        verify(offeredResourceContractsApiOperator, times(0)).add(any(), any());
        verify(contractRulesApiOperator, times(1)).add(uuidArgumentCaptor.capture(),
            urisArgumentCaptor.capture());
        verify(ruleContractsApiOperator, times(0)).add(any(), any());

        var uuids = uuidArgumentCaptor.getAllValues();
        var uris = urisArgumentCaptor.getAllValues();

        assertThat(uuids.get(0)).hasToString(uuid1);
        assertThat(uuids.get(1)).hasToString(uuid1);
        assertThat(uuids.get(2)).hasToString(uuid1);

        assertThat(uris.get(0).get(0)).hasToString(uri2);
        assertThat(uris.get(1).get(0)).hasToString(uri3);
        assertThat(uris.get(2).get(0)).hasToString(uri4);
    }

    @Test
    @SneakyThrows
    void linkEntities_sameEntity_doNotLink() {
        // Arrange
        var entity = new ContractOutput();
        var uuid = "e7f01ba7-0d2a-4709-88ce-c2e3e9a1cc83";
        var uri = "http://localhost:8080/api/representations/" + uuid;
        var contractLinks = new ContractLinks();
        var link = new Link();
        ReflectionTestUtils.setField(link, "href", uri);
        ReflectionTestUtils.setField(contractLinks, "self", link);
        ReflectionTestUtils.setField(entity, "links", contractLinks);
        ReflectionTestUtils.setField(entity, "title", "contract");

        // Act
        relationLinker.linkEntities(entity, entity);

        // Assert
        relationApiOperators.forEach(Mockito::verifyNoInteractions);
    }

    @Test
    @SneakyThrows
    void linkEntities_onlyOneEntity_doNothing() {
        // Arrange
        var entity = new ContractOutput();
        var uuid = "e7f01ba7-0d2a-4709-88ce-c2e3e9a1cc83";
        var uri = "http://localhost:8080/api/representations/" + uuid;
        var contractLinks = new ContractLinks();
        var link = new Link();
        ReflectionTestUtils.setField(link, "href", uri);
        ReflectionTestUtils.setField(contractLinks, "self", link);
        ReflectionTestUtils.setField(entity, "links", contractLinks);
        ReflectionTestUtils.setField(entity, "title", "contract");

        // Act
        relationLinker.linkEntities(entity);

        // Assert
        relationApiOperators.forEach(Mockito::verifyNoInteractions);
    }

    @Test
    @SneakyThrows
    void linkEntities_unlinkableEntities_doNotThrowExceptionAndLogWarning() {
        // Arrange
        var entity1 = new ArtifactOutput();
        var artifactLinks = new ArtifactLinks();
        var link1 = new Link();
        ReflectionTestUtils.setField(link1, "href", "http://localhost:8080/api"
            + "/representations/e7f01ba7-0d2a-4709-88ce-c2e3e9a1cc83");
        ReflectionTestUtils.setField(artifactLinks, "self", link1);
        ReflectionTestUtils.setField(entity1, "links", artifactLinks);
        ReflectionTestUtils.setField(entity1, "title", "artifact");

        var entity2 = new AgreementOutput();
        var agreementLinks = new AgreementLinks();
        var link2 = new Link();
        ReflectionTestUtils.setField(link2, "href", "http://localhost:8080/api/representations"
            + "/e7f01ba7-0d2a-4709-88ce-c2e3e9a1cc84");
        ReflectionTestUtils.setField(agreementLinks, "self", link2);
        ReflectionTestUtils.setField(entity2, "links", agreementLinks);

        when(artifactAgreementsApiOperator.add(any(), any())).thenThrow(OperationNotAllowedException.class);
        when(agreementArtifactsApiOperator.add(any(), any())).thenThrow(OperationNotAllowedException.class);

        // Act & Assert
        assertDoesNotThrow(() -> relationLinker.linkEntities(entity1, entity2));
    }
}
