/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.workflows;


import java.net.URI;
import java.time.ZonedDateTime;

import lombok.SneakyThrows;
import org.assertj.core.util.Arrays;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.siliconeconomy.idsintegrationtoolbox.core.base.ArtifactApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.base.CatalogApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.base.ContractApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.base.OfferedResourceApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.base.RepresentationApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.base.RuleApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.ContractRulesApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.OfferedResourceCatalogsApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.OfferedResourceContractsApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.OfferedResourceRepresentationsApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.RepresentationArtifactsApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.model.input.ArtifactInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.CatalogInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.ContractInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.OfferedResourceInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.RepresentationInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.RuleInput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ArtifactLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.CatalogLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ContractLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ContractRuleLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.Link;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.OfferedResourceLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.RepresentationLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ArtifactOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.CatalogOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ContractRuleOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ContractOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.OfferedResourceOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.RepresentationOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * ResourceCreatorTest
 *
 * @author Haydar Qarawlus
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {ResourceCreator.class})
class ResourceCreatorTest {

    final String EXAMPLE_URI = "http://localhost:8080/api/example/06dc1644-f58d-4562-836f" +
        "-197c0a65c0d6";


    // Class under test
    @Autowired
    ResourceCreator resourceCreator;

    // Dependencies
    @MockBean
    RestTemplate restTemplate;
    @MockBean
    CatalogApiOperator catalogApiOperator;
    @MockBean
    OfferedResourceApiOperator offeredResourceApiOperator;
    @MockBean
    RepresentationApiOperator representationApiOperator;
    @MockBean
    ArtifactApiOperator artifactApiOperator;
    @MockBean
    ContractApiOperator contractApiOperator;
    @MockBean
    RuleApiOperator ruleApiOperator;
    @MockBean
    OfferedResourceCatalogsApiOperator offeredResourceCatalogsApiOperator;
    @MockBean
    OfferedResourceRepresentationsApiOperator
        offeredResourceRepresentationsApiOperator;
    @MockBean
    RepresentationArtifactsApiOperator representationArtifactsApiOperator;
    @MockBean
    OfferedResourceContractsApiOperator offeredResourceContractsApiOperator;
    @MockBean
    ContractRulesApiOperator contractRulesApiOperator;

    @BeforeEach
    @SneakyThrows
    void setMocks() {
        var link = new Link();
        ReflectionTestUtils.setField(link, "href", EXAMPLE_URI);

        final var offeredResourceSingleOutput = new OfferedResourceOutput();
        final var offeredResourceLinks = new OfferedResourceLinks();
        ReflectionTestUtils.setField(offeredResourceLinks, "self", link);
        ReflectionTestUtils.setField(offeredResourceSingleOutput, "links", offeredResourceLinks);
        when(offeredResourceApiOperator.create(any())).thenReturn(offeredResourceSingleOutput);

        final var catalogSingleOutput = new CatalogOutput();
        final var catalogLinks = new CatalogLinks();
        ReflectionTestUtils.setField(catalogLinks, "self", link);
        ReflectionTestUtils.setField(catalogSingleOutput, "links", catalogLinks);
        when(catalogApiOperator.create(any())).thenReturn(catalogSingleOutput);

        final var ruleSingleOutput = new ContractRuleOutput();
        final var ruleLinks = new ContractRuleLinks();
        ReflectionTestUtils.setField(ruleLinks, "self", link);
        ReflectionTestUtils.setField(ruleSingleOutput, "links", ruleLinks);
        when(ruleApiOperator.create(any())).thenReturn(ruleSingleOutput);

        final var contractSingleOutput = new ContractOutput();
        final var contractLinks = new ContractLinks();
        ReflectionTestUtils.setField(contractLinks, "self", link);
        ReflectionTestUtils.setField(contractSingleOutput, "links", contractLinks);
        when(contractApiOperator.create(any())).thenReturn(contractSingleOutput);

        final var representationSingleOutput = new RepresentationOutput();
        final var representationLinks = new RepresentationLinks();
        ReflectionTestUtils.setField(representationLinks, "self", link);
        ReflectionTestUtils.setField(representationSingleOutput, "links", representationLinks);
        when(representationApiOperator.create(any())).thenReturn(representationSingleOutput);

        final var artifactSingleOutput = new ArtifactOutput();
        final var artifactLinks = new ArtifactLinks();
        ReflectionTestUtils.setField(artifactLinks, "self", link);
        ReflectionTestUtils.setField(artifactSingleOutput, "links", artifactLinks);
        when(artifactApiOperator.create(any())).thenReturn(artifactSingleOutput);

    }

    @Test
    @SneakyThrows
    void createResource_contractUriInput_returnResource() {
        // Arrange
        final OfferedResourceInput offeredResourceInput = new OfferedResourceInput();
        final CatalogInput catalogInput = new CatalogInput();
        final URI contractUri = URI.create(EXAMPLE_URI);
        final RepresentationInput representationInput = new RepresentationInput();
        final ArtifactInput artifactInput = new ArtifactInput();

        // Act
        final OfferedResourceOutput offeredResourceSingleOutput =
            resourceCreator.createResource(offeredResourceInput, catalogInput,
                representationInput, artifactInput, contractUri);

        // Assert
        assertThat(offeredResourceSingleOutput.getLinks().getSelf().getHref()).isEqualTo(EXAMPLE_URI);
    }

    @Test
    @SneakyThrows
    void createResource_catalogContractUriInput_returnResource() {
        // Arrange
        final OfferedResourceInput offeredResourceInput = new OfferedResourceInput();
        final URI catalogUri = URI.create(EXAMPLE_URI);
        final URI contractUri = URI.create(EXAMPLE_URI);
        final RepresentationInput representationInput = new RepresentationInput();
        final ArtifactInput artifactInput = new ArtifactInput();

        // Act
        final OfferedResourceOutput offeredResourceSingleOutput =
            resourceCreator.createResource(offeredResourceInput, catalogUri,
                representationInput, artifactInput, contractUri);

        // Assert
        assertThat(offeredResourceSingleOutput.getLinks().getSelf().getHref()).isEqualTo(EXAMPLE_URI);
    }

    @Test
    @SneakyThrows
    void createResourceWithRuleInputs_catalogUriInput_returnResource() {
        // Arrange
        final OfferedResourceInput offeredResourceInput = new OfferedResourceInput();
        final URI catalogUri = URI.create(EXAMPLE_URI);
        final ContractInput contractInput = new ContractInput(ZonedDateTime.now());
        final RuleInput[] ruleInputs = Arrays.array(new RuleInput());
        final RepresentationInput representationInput = new RepresentationInput();
        final ArtifactInput artifactInput = new ArtifactInput();

        // Act
        final OfferedResourceOutput offeredResourceSingleOutput =
            resourceCreator.createResource(offeredResourceInput, catalogUri,
                representationInput, artifactInput, contractInput, ruleInputs);

        // Assert
        assertThat(offeredResourceSingleOutput.getLinks().getSelf().getHref()).isEqualTo(EXAMPLE_URI);
    }

    @Test
    @SneakyThrows
    void createResourceWithRuleUris_catalogUriRuleUris_returnResource() {
        // Arrange
        final OfferedResourceInput offeredResourceInput = new OfferedResourceInput();
        final URI catalogUri = URI.create(EXAMPLE_URI);
        final ContractInput contractInput = new ContractInput(ZonedDateTime.now());
        final URI[] ruleUris = Arrays.array(URI.create(EXAMPLE_URI));
        final RepresentationInput representationInput = new RepresentationInput();
        final ArtifactInput artifactInput = new ArtifactInput();

        // Act
        final OfferedResourceOutput offeredResourceSingleOutput =
            resourceCreator.createResource(offeredResourceInput, catalogUri,
                representationInput, artifactInput, contractInput, ruleUris);

        // Assert
        assertThat(offeredResourceSingleOutput.getLinks().getSelf().getHref()).isEqualTo(EXAMPLE_URI);
    }

    @Test
    @SneakyThrows
    void createResourceWithRuleInputs_fullInput_returnResource() {
        // Arrange
        final OfferedResourceInput offeredResourceInput = new OfferedResourceInput();
        final CatalogInput catalogInput = new CatalogInput();
        final ContractInput contractInput = new ContractInput(ZonedDateTime.now());
        final RuleInput[] ruleInputs = Arrays.array(new RuleInput());
        final RepresentationInput representationInput = new RepresentationInput();
        final ArtifactInput artifactInput = new ArtifactInput();

        // Act
        final OfferedResourceOutput offeredResourceSingleOutput =
            resourceCreator.createResource(offeredResourceInput, catalogInput,
                representationInput, artifactInput, contractInput, ruleInputs);

        // Assert
        assertThat(offeredResourceSingleOutput.getLinks().getSelf().getHref()).isEqualTo(EXAMPLE_URI);
    }

    @Test
    @SneakyThrows
    void createResourceWithRuleUris_fullInput_returnResource() {
        // Arrange
        final OfferedResourceInput offeredResourceInput = new OfferedResourceInput();
        final CatalogInput catalogInput = new CatalogInput();
        final ContractInput contractInput = new ContractInput(ZonedDateTime.now());
        final URI[] ruleUris = Arrays.array(URI.create(EXAMPLE_URI));
        final RepresentationInput representationInput = new RepresentationInput();
        final ArtifactInput artifactInput = new ArtifactInput();

        // Act
        final OfferedResourceOutput offeredResourceSingleOutput =
            resourceCreator.createResource(offeredResourceInput, catalogInput,
                representationInput, artifactInput, contractInput, ruleUris);

        // Assert
        assertThat(offeredResourceSingleOutput.getLinks().getSelf().getHref()).isEqualTo(EXAMPLE_URI);
    }

    @Test
    @SneakyThrows
    void createResourceWithRuleUris_listEmpty_returnResource() {
        // Arrange
        final OfferedResourceInput offeredResourceInput = new OfferedResourceInput();
        final CatalogInput catalogInput = new CatalogInput();
        final ContractInput contractInput = new ContractInput(ZonedDateTime.now());
        final URI[] ruleUris = new URI[0];
        final RepresentationInput representationInput = new RepresentationInput();
        final ArtifactInput artifactInput = new ArtifactInput();

        // Act
        final OfferedResourceOutput offeredResourceSingleOutput =
            resourceCreator.createResource(offeredResourceInput, catalogInput,
                representationInput, artifactInput, contractInput, ruleUris);

        // Assert
        assertThat(offeredResourceSingleOutput.getLinks().getSelf().getHref()).isEqualTo(EXAMPLE_URI);
    }

    @Test
    @SneakyThrows
    void createResource_invalidResourceUri_throwApiInteractionUnsuccessfulException() {
        // Arrange
        final OfferedResourceInput offeredResourceInput = new OfferedResourceInput();
        final CatalogInput catalogInput = new CatalogInput();
        final ContractInput contractInput = new ContractInput(ZonedDateTime.now());
        final RuleInput[] ruleInputs = Arrays.array(new RuleInput());
        final RepresentationInput representationInput = new RepresentationInput();
        final ArtifactInput artifactInput = new ArtifactInput();

        final var bad_uri = "https://example.com/connector/no-response";
        var bad_link = new Link();
        ReflectionTestUtils.setField(bad_link, "href", bad_uri);

        final var offeredResourceOutput = new OfferedResourceOutput();
        final var offeredResourceLinks = new OfferedResourceLinks();
        ReflectionTestUtils.setField(offeredResourceLinks, "self", bad_link);
        ReflectionTestUtils.setField(offeredResourceOutput, "links", offeredResourceLinks);
        when(offeredResourceApiOperator.create(any())).thenReturn(offeredResourceOutput);


        // Act & Assert
        assertThrows(ApiInteractionUnsuccessfulException.class, () ->
            resourceCreator.createResource(offeredResourceInput, catalogInput,
                representationInput, artifactInput, contractInput, ruleInputs));
    }

    @Test
    @SneakyThrows
    void createResource_invalidRepresentationUri_throwApiInteractionUnsuccessfulException() {
        // Arrange
        final OfferedResourceInput offeredResourceInput = new OfferedResourceInput();
        final CatalogInput catalogInput = new CatalogInput();
        final ContractInput contractInput = new ContractInput(ZonedDateTime.now());
        final RuleInput[] ruleInputs = Arrays.array(new RuleInput());
        final RepresentationInput representationInput = new RepresentationInput();
        final ArtifactInput artifactInput = new ArtifactInput();

        final var bad_uri = "https://example.com/connector/no-response";
        var bad_link = new Link();
        ReflectionTestUtils.setField(bad_link, "href", bad_uri);

        final var representationSingleOutput = new RepresentationOutput();
        final var representationLinks = new RepresentationLinks();
        ReflectionTestUtils.setField(representationLinks, "self", bad_link);
        ReflectionTestUtils.setField(representationSingleOutput, "links", representationLinks);
        when(representationApiOperator.create(any())).thenReturn(representationSingleOutput);


        // Act & Assert
        assertThrows(ApiInteractionUnsuccessfulException.class, () ->
            resourceCreator.createResource(offeredResourceInput, catalogInput,
                representationInput, artifactInput, contractInput, ruleInputs));
    }


    @Test
    @SneakyThrows
    void createResource_invalidContractUri_throwApiInteractionUnsuccessfulException() {
        // Arrange
        final OfferedResourceInput offeredResourceInput = new OfferedResourceInput();
        final CatalogInput catalogInput = new CatalogInput();
        final ContractInput contractInput = new ContractInput(ZonedDateTime.now());
        final RuleInput[] ruleInputs = Arrays.array(new RuleInput());
        final RepresentationInput representationInput = new RepresentationInput();
        final ArtifactInput artifactInput = new ArtifactInput();

        final var bad_uri = "https://example.com/connector/no-response";
        var bad_link = new Link();
        ReflectionTestUtils.setField(bad_link, "href", bad_uri);

        final var contractSingleOutput = new ContractOutput();
        final var contractLinks = new ContractLinks();
        ReflectionTestUtils.setField(contractLinks, "self", bad_link);
        ReflectionTestUtils.setField(contractSingleOutput, "links", contractLinks);
        when(contractApiOperator.create(any())).thenReturn(contractSingleOutput);


        // Act & Assert
        assertThrows(ApiInteractionUnsuccessfulException.class, () ->
            resourceCreator.createResource(offeredResourceInput, catalogInput,
                representationInput, artifactInput, contractInput, ruleInputs));
    }
}
