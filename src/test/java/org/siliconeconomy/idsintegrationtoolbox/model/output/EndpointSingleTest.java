/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output;

import org.junit.jupiter.api.Test;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.AppEndpointOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.GenericEndpointOutput;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * EndpointSingleOutputTest
 *
 * @author Ronja Quensel
 */
class EndpointSingleTest {

    @Test
    void getType_generic_returnType() {
        // Arrange
        final var output = new GenericEndpointOutput();

        // Act
        final var result = output.getType();

        // Assert
        assertThat(result).isEqualTo("GENERIC");
    }

    @Test
    void getType_app_returnType() {
        // Arrange
        final var output = new AppEndpointOutput();

        // Act
        final var result = output.getType();

        // Assert
        assertThat(result).isEqualTo("APP");
    }

}
