/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.embedded;

import java.util.ArrayList;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.AbstractEntity;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.*;
import org.springframework.test.util.ReflectionTestUtils;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests the getEntries method on all types of Embedded.
 */
class EmbeddedTest {

    @ParameterizedTest
    @MethodSource("provideAbstractEntityArguments")
    <T extends AbstractEntity, D extends EntityOutput<T>> void getEntries_AbstractEntityEmbedded(final ArrayList<D> entityList, D entity, Embedded<T, D> embeddedEntity) {
        // Arrange
        entityList.add(entity);
        ReflectionTestUtils.setField(embeddedEntity, "entries", entityList);

        // Act
        final var result = embeddedEntity.getEntries();

        // Assert
        assertThat(result.get(0)).isEqualTo(entityList.get(0));
    }

    @ParameterizedTest
    @MethodSource("provideEmptyAbstractEntityArguments")
    <T extends AbstractEntity, D extends EntityOutput<T>> void getEntries_AbstractEntityEmbedded_emptyList(final ArrayList<D> entityList, Embedded<T, D> embeddedEntity) {
        // Arrange
        ReflectionTestUtils.setField(embeddedEntity, "entries", entityList);

        // Act
        final var result = embeddedEntity.getEntries();

        // Assert
        assertThat(result).isEqualTo(embeddedEntity.getEntries());
    }

    private static Stream<Arguments> provideAbstractEntityArguments() {
        return Stream.of(
            Arguments.of(new ArrayList<AgreementOutput>(), new AgreementOutput(), new AgreementEmbedded()),
            Arguments.of(new ArrayList<AppOutput>(), new AppOutput(), new AppEmbedded()),
            Arguments.of(new ArrayList<AppStoreOutput>(), new AppStoreOutput(), new AppStoreEmbedded()),
            Arguments.of(new ArrayList<ArtifactOutput>(), new ArtifactOutput(), new ArtifactEmbedded()),
            Arguments.of(new ArrayList<BrokerOutput>(), new BrokerOutput(), new BrokerEmbedded()),
            Arguments.of(new ArrayList<CatalogOutput>(), new CatalogOutput(), new CatalogEmbedded()),
            Arguments.of(new ArrayList<ConfigurationOutput>(), new ConfigurationOutput(), new ConfigurationEmbedded()),
            Arguments.of(new ArrayList<ContractOutput>(), new ContractOutput(), new ContractEmbedded()),
            Arguments.of(new ArrayList<ContractRuleOutput>(), new ContractRuleOutput(), new ContractRuleEmbedded()),
            Arguments.of(new ArrayList<DataSourceOutput>(), new RestDataSourceOutput(), new DataSourceEmbedded()),
            Arguments.of(new ArrayList<EndpointOutput>(), new AppEndpointOutput(), new EndpointEmbedded()),
            Arguments.of(new ArrayList<OfferedResourceOutput>(), new OfferedResourceOutput(), new OfferedResourceEmbedded()),
            Arguments.of(new ArrayList<RepresentationOutput>(), new RepresentationOutput(), new RepresentationEmbedded()),
            Arguments.of(new ArrayList<RequestedResourceOutput>(), new RequestedResourceOutput(), new RequestedResourceEmbedded()),
            Arguments.of(new ArrayList<RouteOutput>(), new RouteOutput(), new RouteEmbedded()),
            Arguments.of(new ArrayList<SubscriptionOutput>(), new SubscriptionOutput(), new SubscriptionEmbedded())
        );
    }

    private static Stream<Arguments> provideEmptyAbstractEntityArguments() {
        return Stream.of(
            Arguments.of(new ArrayList<AgreementOutput>(), new AgreementEmbedded()),
            Arguments.of(new ArrayList<AppOutput>(), new AppEmbedded()),
            Arguments.of(new ArrayList<AppStoreOutput>(), new AppStoreEmbedded()),
            Arguments.of(new ArrayList<ArtifactOutput>(), new ArtifactEmbedded()),
            Arguments.of(new ArrayList<BrokerOutput>(), new BrokerEmbedded()),
            Arguments.of(new ArrayList<CatalogOutput>(), new CatalogEmbedded()),
            Arguments.of(new ArrayList<ConfigurationOutput>(), new ConfigurationEmbedded()),
            Arguments.of(new ArrayList<ContractOutput>(), new ContractEmbedded()),
            Arguments.of(new ArrayList<ContractRuleOutput>(), new ContractRuleEmbedded()),
            Arguments.of(new ArrayList<DataSourceOutput>(), new DataSourceEmbedded()),
            Arguments.of(new ArrayList<EndpointOutput>(), new EndpointEmbedded()),
            Arguments.of(new ArrayList<OfferedResourceOutput>(), new OfferedResourceEmbedded()),
            Arguments.of(new ArrayList<RepresentationOutput>(), new RepresentationEmbedded()),
            Arguments.of(new ArrayList<RequestedResourceOutput>(), new RequestedResourceEmbedded()),
            Arguments.of(new ArrayList<RouteOutput>(), new RouteEmbedded()),
            Arguments.of(new ArrayList<SubscriptionOutput>(), new SubscriptionEmbedded())
        );
    }

}
