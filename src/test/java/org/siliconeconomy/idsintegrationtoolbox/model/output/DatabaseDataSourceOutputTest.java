/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.DataSourceType;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.DatabaseDataSourceOutput;

class DatabaseDataSourceOutputTest {

    @Test
    void constructor_validInput_returnDatabaseDataSourceSingleOutput() {
        // Arrange
        var type = DataSourceType.DATABASE;

        // Act
        final var result = new DatabaseDataSourceOutput();

        // Assert
        assertNotNull(result);
        assertEquals(type, result.getType());
    }
}
