/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.multi;

import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.AbstractEntity;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.AgreementEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.AppEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.AppStoreEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.ArtifactEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.BrokerEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.CatalogEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.ConfigurationEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.ContractEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.ContractRuleEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.DataSourceEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.Embedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.EndpointEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.OfferedResourceEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.RepresentationEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.RequestedResourceEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.RouteEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.SubscriptionEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.AgreementLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.AppLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.AppStoreLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ArtifactLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.BrokerLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.CatalogLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ConfigurationLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ContractLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ContractRuleLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.DataSourceLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.EndpointLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.Links;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.OfferedResourceLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.RepresentationLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.RequestedResourceLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.RouteLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.SubscriptionLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.EntityOutput;
import org.springframework.test.util.ReflectionTestUtils;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests the getEmbedded and getLinks methods on all types of MultiOutput.
 */
class MultiOutputTest {

    @ParameterizedTest
    @MethodSource("provideArgumentsForGetEmbedded")
    <T extends AbstractEntity, O extends EntityOutput<T>, E extends Embedded<T, O>,
        L extends Links<T>> void getEmbedded_returnEmbedded(
        final Embedded<T, O> embedded, final EntityMultiOutput<T, O, E, L> output) {
        // Arrange
        ReflectionTestUtils.setField(output, "embedded", embedded);

        // Act
        final var result = output.getEmbedded();

        // Assert
        assertThat(result).isEqualTo(embedded);
    }

    private static Stream<Arguments> provideArgumentsForGetEmbedded() {
        return Stream.of(
            Arguments.of(new AgreementEmbedded(), new AgreementMultiOutput()),
            Arguments.of(new AppEmbedded(), new AppMultiOutput()),
            Arguments.of(new AppStoreEmbedded(), new AppStoreMultiOutput()),
            Arguments.of(new ArtifactEmbedded(), new ArtifactMultiOutput()),
            Arguments.of(new BrokerEmbedded(), new BrokerMultiOutput()),
            Arguments.of(new CatalogEmbedded(), new CatalogMultiOutput()),
            Arguments.of(new ConfigurationEmbedded(), new ConfigurationMultiOutput()),
            Arguments.of(new ContractEmbedded(), new ContractMultiOutput()),
            Arguments.of(new ContractRuleEmbedded(), new ContractRuleMultiOutput()),
            Arguments.of(new DataSourceEmbedded(), new DataSourceMultiOutput()),
            Arguments.of(new EndpointEmbedded(), new EndpointMultiOutput()),
            Arguments.of(new OfferedResourceEmbedded(), new OfferedResourceMultiOutput()),
            Arguments.of(new RepresentationEmbedded(), new RepresentationMultiOutput()),
            Arguments.of(new RequestedResourceEmbedded(), new RequestedResourceMultiOutput()),
            Arguments.of(new RouteEmbedded(), new RouteMultiOutput()),
            Arguments.of(new SubscriptionEmbedded(), new SubscriptionMultiOutput())
        );
    }

    @ParameterizedTest
    @MethodSource("provideArgumentsForGetLinks")
    <T extends AbstractEntity, O extends EntityOutput<T>, E extends Embedded<T, O>,
        L extends Links<T>> void getLinks_returnLinks(final Links<T> links,
                                                      final EntityMultiOutput<T, O, E, L> output) {
        // Arrange
        ReflectionTestUtils.setField(output, "links", links);

        // Act
        final var result = output.getLinks();

        // Assert
        assertThat(result).isEqualTo(links);
    }

    private static Stream<Arguments> provideArgumentsForGetLinks() {
        return Stream.of(
            Arguments.of(new AgreementLinks(), new AgreementMultiOutput()),
            Arguments.of(new AppLinks(), new AppMultiOutput()),
            Arguments.of(new AppStoreLinks(), new AppStoreMultiOutput()),
            Arguments.of(new ArtifactLinks(), new ArtifactMultiOutput()),
            Arguments.of(new BrokerLinks(), new BrokerMultiOutput()),
            Arguments.of(new CatalogLinks(), new CatalogMultiOutput()),
            Arguments.of(new ConfigurationLinks(), new ConfigurationMultiOutput()),
            Arguments.of(new ContractLinks(), new ContractMultiOutput()),
            Arguments.of(new ContractRuleLinks(), new ContractRuleMultiOutput()),
            Arguments.of(new DataSourceLinks(), new DataSourceMultiOutput()),
            Arguments.of(new EndpointLinks(), new EndpointMultiOutput()),
            Arguments.of(new OfferedResourceLinks(), new OfferedResourceMultiOutput()),
            Arguments.of(new RepresentationLinks(), new RepresentationMultiOutput()),
            Arguments.of(new RequestedResourceLinks(), new RequestedResourceMultiOutput()),
            Arguments.of(new RouteLinks(), new RouteMultiOutput()),
            Arguments.of(new SubscriptionLinks(), new SubscriptionMultiOutput())
        );
    }
}
