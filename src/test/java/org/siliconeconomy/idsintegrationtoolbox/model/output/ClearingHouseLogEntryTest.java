/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class ClearingHouseLogEntryTest {

    @Test
    void getDecodedPayload_returnDecodedPayload() {
        // Arrange
        final var payload = "the payload string";
        final var encodedPayload = Base64.getEncoder()
            .encodeToString(payload.getBytes(StandardCharsets.UTF_8));
        final var logEntry = new ClearingHouseLogEntry();
        logEntry.setPayload(encodedPayload);

        // Act
        final var result = logEntry.getDecodedPayload();

        // Assert
        assertThat(result).isEqualTo(payload);
    }

}
