/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.relation;

import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.AbstractEntity;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.AgreementEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.AppEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.ArtifactEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.BrokerEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.CatalogEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.ContractEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.ContractRuleEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.DataSourceEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.Embedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.EndpointEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.OfferedResourceEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.RepresentationEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.RequestedResourceEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.RouteEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.SubscriptionEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.AgreementLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.AppLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.AppStoreLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ArtifactLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.BrokerLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.CatalogLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ContractLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ContractRuleLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.EndpointLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.Links;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.OfferedResourceLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.RepresentationLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.RequestedResourceLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.RouteLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.EntityOutput;
import org.springframework.test.util.ReflectionTestUtils;

import static org.assertj.core.api.Assertions.assertThat;

class RelationOutputTest {

    @ParameterizedTest
    @MethodSource("provideArgumentsForGetEmbedded")
    <T extends AbstractEntity, D extends AbstractEntity, O extends EntityOutput<D>,
        E extends Embedded<D, O>, L extends Links<T>> void getEmbedded_returnEmbedded(
        final Embedded<D, O> embedded, final EntityRelationOutput<T, L, D, O, E> output) {
        // Arrange
        ReflectionTestUtils.setField(output, "embedded", embedded);

        // Act
        final var result = output.getEmbedded();

        // Assert
        assertThat(result).isEqualTo(embedded);
    }

    private static Stream<Arguments> provideArgumentsForGetEmbedded() {
        return Stream.of(
            Arguments.of(new ArtifactEmbedded(), new AgreementArtifactRelationOutput()),
            Arguments.of(new EndpointEmbedded(), new AppEndpointRelationOutput()),
            Arguments.of(new AppEmbedded(), new AppStoreAppRelationOutput()),
            Arguments.of(new AgreementEmbedded(), new ArtifactAgreementRelationOutput()),
            Arguments.of(new RepresentationEmbedded(), new ArtifactRepresentationRelationOutput()),
            Arguments.of(new SubscriptionEmbedded(), new ArtifactSubscriptionRelationOutput()),
            Arguments.of(new OfferedResourceEmbedded(), new BrokerOfferedResourceRelationOutput()),
            Arguments.of(new OfferedResourceEmbedded(), new CatalogOfferedResourceRelationOutput()),
            Arguments.of(new ContractRuleEmbedded(), new ContractContractRuleRelationOutput()),
            Arguments.of(new OfferedResourceEmbedded(),
                new ContractOfferedResourceRelationOutput()),
            Arguments.of(new RequestedResourceEmbedded(),
                new ContractRequestedResourceRelationOutput()),
            Arguments.of(new ContractEmbedded(), new ContractRuleContractRelationOutput()),
            Arguments.of(new DataSourceEmbedded(), new EndpointDatasourceRelationOutput()),
            Arguments.of(new CatalogEmbedded(), new OfferedResourceCatalogRelationOutput()),
            Arguments.of(new ContractEmbedded(), new OfferedResourceContractRelationOutput()),
            Arguments.of(new RepresentationEmbedded(),
                new OfferedResourceRepresentationRelationOutput()),
            Arguments.of(new SubscriptionEmbedded(),
                new OfferedResourceSubscriptionRelationOutput()),
            Arguments.of(new BrokerEmbedded(), new OfferedResourceBrokerRelationOutput()),
            Arguments.of(new ArtifactEmbedded(), new RepresentationArtifactRelationOutput()),
            Arguments.of(new OfferedResourceEmbedded(),
                new RepresentationOfferedResourceRelationOutput()),
            Arguments.of(new RequestedResourceEmbedded(),
                new RepresentationRequestedResourceRelationOutput()),
            Arguments.of(new SubscriptionEmbedded(),
                new RepresentationSubscriptionRelationOutput()),
            Arguments.of(new CatalogEmbedded(), new RequestedResourceCatalogRelationOutput()),
            Arguments.of(new ContractEmbedded(), new RequestedResourceContractRelationOutput()),
            Arguments.of(new RepresentationEmbedded(),
                new RequestedResourceRepresentationRelationOutput()),
            Arguments.of(new SubscriptionEmbedded(),
                new RequestedResourcesSubscriptionRelationOutput()),
            Arguments.of(new ArtifactEmbedded(), new RouteArtifactRelationOutput()),
            Arguments.of(new EndpointEmbedded(), new RouteEndpointRelationOutput()),
            Arguments.of(new RouteEmbedded(), new RouteRouteRelationOutput())
        );
    }

    @ParameterizedTest
    @MethodSource("provideArgumentsForGetLinks")
    <T extends AbstractEntity, D extends AbstractEntity, O extends EntityOutput<D>,
    E extends Embedded<D, O>, L extends Links<T>> void getLinks_returnLinks(
            final Links<T> links, final EntityRelationOutput<T, L, D, O, E> output) {
        // Arrange
        ReflectionTestUtils.setField(output, "links", links);

        // Act
        final var result = output.getLinks();

        // Assert
        assertThat(result).isEqualTo(links);
    }

    private static Stream<Arguments> provideArgumentsForGetLinks() {
        return Stream.of(
            Arguments.of(new AgreementLinks(), new AgreementArtifactRelationOutput()),
            Arguments.of(new AppLinks(), new AppEndpointRelationOutput()),
            Arguments.of(new AppStoreLinks(), new AppStoreAppRelationOutput()),
            Arguments.of(new ArtifactLinks(), new ArtifactAgreementRelationOutput()),
            Arguments.of(new ArtifactLinks(), new ArtifactRepresentationRelationOutput()),
            Arguments.of(new ArtifactLinks(), new ArtifactSubscriptionRelationOutput()),
            Arguments.of(new BrokerLinks(), new BrokerOfferedResourceRelationOutput()),
            Arguments.of(new CatalogLinks(), new CatalogOfferedResourceRelationOutput()),
            Arguments.of(new ContractLinks(), new ContractContractRuleRelationOutput()),
            Arguments.of(new ContractLinks(), new ContractOfferedResourceRelationOutput()),
            Arguments.of(new ContractLinks(), new ContractRequestedResourceRelationOutput()),
            Arguments.of(new ContractRuleLinks(), new ContractRuleContractRelationOutput()),
            Arguments.of(new EndpointLinks(), new EndpointDatasourceRelationOutput()),
            Arguments.of(new OfferedResourceLinks(), new OfferedResourceCatalogRelationOutput()),
            Arguments.of(new OfferedResourceLinks(), new OfferedResourceContractRelationOutput()),
            Arguments.of(new OfferedResourceLinks(), new OfferedResourceRepresentationRelationOutput()),
            Arguments.of(new OfferedResourceLinks(), new OfferedResourceSubscriptionRelationOutput()),
            Arguments.of(new OfferedResourceLinks(), new OfferedResourceBrokerRelationOutput()),
            Arguments.of(new RepresentationLinks(), new RepresentationArtifactRelationOutput()),
            Arguments.of(new RepresentationLinks(), new RepresentationOfferedResourceRelationOutput()),
            Arguments.of(new RepresentationLinks(), new RepresentationRequestedResourceRelationOutput()),
            Arguments.of(new RepresentationLinks(), new RepresentationSubscriptionRelationOutput()),
            Arguments.of(new RequestedResourceLinks(), new RequestedResourceCatalogRelationOutput()),
            Arguments.of(new RequestedResourceLinks(), new RequestedResourceContractRelationOutput()),
            Arguments.of(new RequestedResourceLinks(), new RequestedResourceRepresentationRelationOutput()),
            Arguments.of(new RequestedResourceLinks(), new RequestedResourcesSubscriptionRelationOutput()),
            Arguments.of(new RouteLinks(), new RouteArtifactRelationOutput()),
            Arguments.of(new RouteLinks(), new RouteEndpointRelationOutput()),
            Arguments.of(new RouteLinks(), new RouteRouteRelationOutput())
        );
    }

}
