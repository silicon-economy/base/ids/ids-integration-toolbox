/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * EndpointInputTest
 *
 * @author Ronja Quensel
 */
class EndpointInputTest {

    @Test
    void getType_generic_returnType() {
        // Arrange
        final var output = new GenericEndpointInput();

        // Act
        final var result = output.getType();

        // Assert
        assertThat(result).isEqualTo("GENERIC");
    }

}
