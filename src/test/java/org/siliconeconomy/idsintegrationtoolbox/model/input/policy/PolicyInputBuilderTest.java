/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input.policy;

import java.lang.reflect.Type;
import java.net.URI;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import lombok.SneakyThrows;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.PolicyPattern;
import org.siliconeconomy.idsintegrationtoolbox.model.input.AbstractBuilderTest;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test for all policy input builder classes.
 *
 * @author Ronja Quensel
 */
class PolicyInputBuilderTest extends AbstractBuilderTest {

    private static Stream<Arguments> providePolicyInputBuilderParameters() {
        return Stream.of(
            Arguments.of(ConnectorRestrictedPolicyInput.Builder.class,
                Map.of("type", PolicyPattern.CONNECTOR_RESTRICTED_USAGE)),
            Arguments.of(DurationRestrictedPolicyInput.Builder.class,
                Map.of("type", PolicyPattern.DURATION_USAGE)),
            Arguments.of(NotificationPolicyInput.Builder.class,
                Map.of("type", PolicyPattern.USAGE_NOTIFICATION)),
            Arguments.of(NumberRestrictedPolicyInput.Builder.class,
                Map.of("type", PolicyPattern.N_TIMES_USAGE)),
            Arguments.of(ProvideAccessPolicyInput.Builder.class,
                Map.of("type", PolicyPattern.PROVIDE_ACCESS)),
            Arguments.of(ProhibitAccessPolicyInput.Builder.class,
                Map.of("type", PolicyPattern.PROHIBIT_ACCESS)),
            Arguments.of(SecurityProfileRestrictedPolicyInput.Builder.class,
                Map.of("type", PolicyPattern.SECURITY_PROFILE_RESTRICTED_USAGE)),
            Arguments.of(UsageDuringIntervalPolicyInput.Builder.class,
                Map.of("type", PolicyPattern.USAGE_DURING_INTERVAL)),
            Arguments.of(LoggingPolicyInput.Builder.class,
                Map.of("type", PolicyPattern.USAGE_LOGGING)),
            Arguments.of(UsageUntilDeletionPolicyInput.Builder.class,
                Map.of("type", PolicyPattern.USAGE_UNTIL_DELETION))
        );
    }

    /**
     * Tests the builder class for a policy input class. Sets default values for each builder
     * methods, constructs the instance and verifies that all instance fields have been set
     * correctly. Values set for specific fields/methods can be overridden in the inputConfig
     * parameter.
     *
     * @param builderClass the corresponding builder class.
     * @param inputConfig map for overriding specific field values; format: field name -> field value
     * @param <I> the input class for the type.
     * @param <B> the builder class for the input class.
     */
    @ParameterizedTest
    @MethodSource("providePolicyInputBuilderParameters")
    @SneakyThrows
    <I extends PolicyInput, B extends PolicyInput.Builder<I,B>> void testBuilder(final Class<B> builderClass,
                                                                                 final Map<String, Object> inputConfig) {
        // Create a new builder instance
        var builder = (B) builderClass.getDeclaredConstructors()[0].newInstance();

        // Call all builder methods and set either config or default value
        builder = configureBuilder(builder, inputConfig);

        // Build instance
        var instance = builder.build();

        // Validate that instance fields have been set accordingly
        validateInstanceFields(instance, inputConfig);
    }

    @Override
    @SneakyThrows
    protected Object getDefaultParameter(Class<?> parameterType) {
        if (String.class.equals(parameterType)) {
            return "string";
        } else if (URI.class.equals(parameterType)) {
            return URI.create("https://uri");
        } else if (ZonedDateTime.class.equals(parameterType)) {
            return ZonedDateTime.of(2022, 1, 1, 1, 1, 1, 1, ZoneId.systemDefault());
        }

        return null;
    }

    @Override
    @SneakyThrows
    protected Object getGenericDefaultParameter(Type genericType) {
        var typeName = getTypeNameOfGenericType(genericType);
        if (List.class.getName().equals(typeName)) {
            // Get the default value for the parameter type of the generic type
            var parameterType = Class.forName(getParameterTypeNameOfGenericType(genericType));
            var parameterValue = getDefaultParameter(parameterType);

            // Return a list containing the default value
            assertThat(parameterValue).isNotNull();
            return List.of(parameterValue);
        } else if (Map.class.getName().equals(typeName)) {
            // Only map string -> string is used in builders
            return Map.of("key", "value");
        }

        return null;
    }

}
