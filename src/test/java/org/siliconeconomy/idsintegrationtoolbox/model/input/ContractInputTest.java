/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input;

import java.net.URI;
import java.time.ZonedDateTime;

import org.junit.jupiter.api.Test;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.InputValidationException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ContractInputTest {

    private final URI consumer = URI.create("https://consumer");

    private final URI provider = URI.create("https://provider");

    private final String title = "title";

    private final ZonedDateTime start = ZonedDateTime.now();

    private final ZonedDateTime end = ZonedDateTime.now().plusDays(365);

    @Test
    void constructor1_validInput_returnContractInput() {
        // Act
        final var result = new ContractInput(consumer, provider, title, start, end);

        // Assert
        assertNotNull(result);
        assertEquals(provider, result.getProvider());
        assertEquals(consumer, result.getConsumer());
        assertEquals(title, result.getTitle());
        assertEquals(start, result.getStart());
        assertEquals(end, result.getEnd());
    }

    @Test
    void constructor1_endDateNull_throwInputValidationException() {
        // Act & Assert
        assertThrows(InputValidationException.class,
            () -> new ContractInput(consumer, provider, title, start, null));
    }

    @Test
    void constructor2_validInput_returnContractInput() {
        // Act
        final var result = new ContractInput(consumer, provider, title,end);

        // Assert
        assertNotNull(result);
        assertEquals(provider, result.getProvider());
        assertEquals(consumer, result.getConsumer());
        assertEquals(title, result.getTitle());
        assertNull(result.getStart());
        assertEquals(end, result.getEnd());
    }

    @Test
    void constructor3_validInput_returnContractInput() {
        // Act
        final var result = new ContractInput(consumer, provider, start, end);

        // Assert
        assertNotNull(result);
        assertEquals(provider, result.getProvider());
        assertEquals(consumer, result.getConsumer());
        assertNull(result.getTitle());
        assertEquals(start, result.getStart());
        assertEquals(end, result.getEnd());
    }

    @Test
    void constructor4_validInput_returnContractInput() {
        // Act
        final var result = new ContractInput(consumer, title, start, end);

        // Assert
        assertNotNull(result);
        assertNull(result.getProvider());
        assertEquals(consumer, result.getConsumer());
        assertEquals(title, result.getTitle());
        assertEquals(start, result.getStart());
        assertEquals(end, result.getEnd());
    }

    @Test
    void constructor5_validInput_returnContractInput() {
        // Act
        final var result = new ContractInput(consumer, title, end);

        // Assert
        assertNotNull(result);
        assertNull(result.getProvider());
        assertEquals(consumer, result.getConsumer());
        assertEquals(title, result.getTitle());
        assertNull(result.getStart());
        assertEquals(end, result.getEnd());
    }

    @Test
    void constructor6_validInput_returnContractInput() {
        // Act
        final var result = new ContractInput(consumer, start, end);

        // Assert
        assertNotNull(result);
        assertNull(result.getProvider());
        assertEquals(consumer, result.getConsumer());
        assertNull(result.getTitle());
        assertEquals(start, result.getStart());
        assertEquals(end, result.getEnd());
    }

    @Test
    void constructor7_validInput_returnContractInput() {
        // Act
        final var result = new ContractInput(title, start, end);

        // Assert
        assertNotNull(result);
        assertNull(result.getProvider());
        assertNull(result.getConsumer());
        assertEquals(title, result.getTitle());
        assertEquals(start, result.getStart());
        assertEquals(end, result.getEnd());
    }

    @Test
    void constructor8_validInput_returnContractInput() {
        // Act
        final var result = new ContractInput(start, end);

        // Assert
        assertNotNull(result);
        assertNull(result.getProvider());
        assertNull(result.getConsumer());
        assertNull(result.getTitle());
        assertEquals(start, result.getStart());
        assertEquals(end, result.getEnd());
    }

    @Test
    void constructor9_validInput_returnContractInput() {
        // Act
        final var result = new ContractInput(title, end);

        // Assert
        assertNotNull(result);
        assertNull(result.getProvider());
        assertNull(result.getConsumer());
        assertEquals(title, result.getTitle());
        assertNull(result.getStart());
        assertEquals(end, result.getEnd());
    }

    @Test
    void constructor10_validInput_returnContractInput() {
        // Act
        final var result = new ContractInput(end);

        // Assert
        assertNotNull(result);
        assertNull(result.getProvider());
        assertNull(result.getConsumer());
        assertNull(result.getTitle());
        assertNull(result.getStart());
        assertEquals(end, result.getEnd());
    }

}
