/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.DataSourceType;

class DatabaseDataSourceInputTest {

    @Test
    void constructor1_validInput_returnDatabaseDataSourceInput() {
        // Arrange
        String driverClassName = "driverClassName";
        String url = "url";

        // Act
        final var result = new DatabaseDataSourceInput(url, driverClassName);

        // Assert
        assertNotNull(result);
        assertEquals(url, result.getUrl());
        assertEquals(driverClassName, result.getDriverClassName());
        assertEquals(DataSourceType.DATABASE, result.getType());
    }

    @Test
    void constructor2_validInput_returnDatabaseDataSourceInput() {
        // Act
        final var result = new DatabaseDataSourceInput();

        // Assert
        assertNotNull(result);
        assertEquals(DataSourceType.DATABASE, result.getType());
    }
}
