/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input;

import java.lang.reflect.Type;
import java.net.URI;
import java.net.URL;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import lombok.SneakyThrows;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.AbstractEntity;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.ConnectorStatus;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.DataSourceType;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.DeployMethod;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.DeployMode;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.LogLevel;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.PaymentMethod;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.PolicyPattern;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.RegistrationStatus;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.SecurityProfile;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.RouteOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.InputValidationException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

/**
 * Test for all input builder classes.
 *
 * @author Ronja Quensel
 */
class BuilderTest extends AbstractBuilderTest {

    private static Stream<Arguments> provideBuilderParameters() {
        return Stream.of(
            Arguments.of(AppInput.Builder.class, null, false),
            Arguments.of(AppStoreInput.Builder.class, null, false),
            Arguments.of(ArtifactInput.Builder.class, null, false),
            Arguments.of(BrokerInput.Builder.class, null, false),
            Arguments.of(CatalogInput.Builder.class, null, false),
            Arguments.of(ConfigurationInput.Builder.class, null, false),
            Arguments.of(ContractInput.Builder.class, getValidContractInputConfig(), false),
            Arguments.of(ContractInput.Builder.class, null, true), //should fail because start date = end date
            Arguments.of(ContractInput.Builder.class, getContractInputConfigWithEndNull(), true), //should fail because end date is null
            Arguments.of(DatabaseDataSourceInput.Builder.class, getDatabaseDataSourceInputConfig(), false),
            Arguments.of(GenericEndpointInput.Builder.class, null, false),
            Arguments.of(OfferedResourceInput.Builder.class, null, false),
            Arguments.of(RepresentationInput.Builder.class, null, false),
            Arguments.of(RestDataSourceInput.Builder.class, getRestDataSourceInputConfig(), false),
            Arguments.of(RouteInput.Builder.class, null, false),
            Arguments.of(RuleInput.Builder.class, null, false),
            Arguments.of(SubscriptionInput.Builder.class, null, false)
        );
    }

    /**
     * Creates an input configuration for a valid ContractInput. The validation requires the end
     * date to be after the start date, therefore the same default value can not be used for both
     * dates.
     *
     * @return the input configuration.
     */
    private static Map<String, Object> getValidContractInputConfig() {
        return new HashMap<>() {{
            put("start", ZonedDateTime.now());
            put("end", ZonedDateTime.now().plusDays(1));
        }};
    }

    /**
     * Creates an input configuration for an invalid ContractInput. Sets the end date to null.
     *
     * @return the input configuration.
     */
    private static Map<String, Object> getContractInputConfigWithEndNull() {
        return new HashMap<>() {{
            put("end", null);
        }};
    }

    /**
     * Creates the input configuration for DatabaseDataSourceInput. The type depends on the class
     * and is not set by the builder, therefore the correct value needs to be used when validating
     * the instance field.
     *
     * @return the input configuration.
     */
    private static Map<String, Object> getDatabaseDataSourceInputConfig() {
        return new HashMap<>() {{
            put("type", DataSourceType.DATABASE);
        }};
    }

    /**
     * Creates the input configuration for RestDataSourceInput. The type depends on the class
     * and is not set by the builder, therefore the correct value needs to be used when validating
     * the instance field.
     *
     * @return the input configuration.
     */
    private static Map<String, Object> getRestDataSourceInputConfig() {
        return new HashMap<>() {{
            put("type", DataSourceType.REST);
        }};
    }

    /**
     * Tests the builder class for an input class. Sets default values for each builder methods,
     * constructs the instance and verifies that all instance fields have been set correctly.
     * Values set for specific fields/methods can be overridden in the inputConfig parameter.
     *
     * @param builderClass the corresponding builder class.
     * @param inputConfig map for overriding specific field values; format: field name -> field value
     * @param shouldFail whether building the instance should fail with the supplied input config
     * @param <T> the type of abstract entity.
     * @param <I> the input class for the type.
     * @param <B> the builder class for the input class.
     */
    @ParameterizedTest
    @MethodSource("provideBuilderParameters")
    @SneakyThrows
    <T extends AbstractEntity, I extends EntityInput<T>, B extends EntityInput.Builder<T,B>> void testBuilder(final Class<B> builderClass,
                                                                                                              final Map<String, Object> inputConfig,
                                                                                                              final boolean shouldFail) {
        // Create a new builder instance
        var builder = (B) builderClass.getDeclaredConstructors()[0].newInstance();

        // Call all builder methods and set either config or default value
        builder = configureBuilder(builder, inputConfig);

        if (shouldFail) {
            // Check that exception thrown if invalid input is provided to the builder
            assertThatThrownBy(builder::build).isInstanceOf(InputValidationException.class);
        } else {
            // Build instance
            var instance = builder.build();

            // Validate that instance fields have been set accordingly
            validateInstanceFields(instance, inputConfig);
        }
    }

    @Override
    @SneakyThrows
    protected Object getDefaultParameter(Class<?> parameterType) {
        if (String.class.equals(parameterType)) {
            return "string";
        } else if (URI.class.equals(parameterType)) {
            return URI.create("https://uri");
        } else if (URL.class.equals(parameterType)) {
            return new URL("https://url");
        } else if (Boolean.class.equals(parameterType) || "boolean".equals(parameterType.getName())) {
            return true;
        } else if (RouteOutput.class.equals(parameterType)) {
            return new RouteOutput();
        } else if (AuthenticationInput.class.equals(parameterType)) {
            return new AuthenticationInput();
        } else if (TruststoreInput.class.equals(parameterType)) {
            return new TruststoreInput();
        } else if (KeystoreInput.class.equals(parameterType)) {
            return new KeystoreInput();
        } else if (ProxyInput.class.equals(parameterType)) {
            return new ProxyInput();
        } else if (SecurityProfile.class.equals(parameterType)) {
            return SecurityProfile.BASE_SECURITY;
        } else if (LogLevel.class.equals(parameterType)) {
            return LogLevel.INFO;
        } else if (DeployMode.class.equals(parameterType)) {
            return DeployMode.TEST;
        } else if (DeployMethod.class.equals(parameterType)) {
            return DeployMethod.NONE;
        }else if (PaymentMethod.class.equals(parameterType)) {
            return PaymentMethod.FREE;
        } else if (ConnectorStatus.class.equals(parameterType)) {
            return ConnectorStatus.ONLINE;
        } else if (PolicyPattern.class.equals(parameterType)) {
            return PolicyPattern.PROVIDE_ACCESS;
        } else if (RegistrationStatus.class.equals(parameterType)) {
            return RegistrationStatus.UNREGISTERED;
        }else if (ZonedDateTime.class.equals(parameterType)) {
            return ZonedDateTime.of(2022, 1, 1, 1, 1, 1, 1, ZoneId.systemDefault());
        }

        return null;
    }

    @Override
    @SneakyThrows
    protected Object getGenericDefaultParameter(Type genericType) {
        var typeName = getTypeNameOfGenericType(genericType);
        if (List.class.getName().equals(typeName)) {
            // Get the default value for the parameter type of the generic type
            var parameterType = Class.forName(getParameterTypeNameOfGenericType(genericType));
            var parameterValue = getDefaultParameter(parameterType);

            // Return a list containing the default value
            assertThat(parameterValue).isNotNull();
            return List.of(parameterValue);
        } else if (Map.class.getName().equals(typeName)) {
            // Only map string -> string is used in builders
            return Map.of("key", "value");
        }

        return null;
    }

}
