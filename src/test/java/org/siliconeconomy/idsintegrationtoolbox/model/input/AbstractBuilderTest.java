/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import lombok.SneakyThrows;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Abstract class for builder tests.
 *
 * @author Ronja Quensel
 */
public abstract class AbstractBuilderTest {

    /**
     * Returns a default value for the given type. Each call to this method for the same type must
     * return the same value (equals on all values must return true).
     *
     * @param parameterType the parameter type.
     * @return the default value for the type.
     */
    protected abstract Object getDefaultParameter(Class<?> parameterType);

    /**
     * Returns the default value for a parameter of a generic type. Each call to this method for
     * the same type must return the same value (equals on all values must return true).
     *
     * @param genericType the generic parameter type.
     * @return the default value for the type.
     */
    protected abstract Object getGenericDefaultParameter(Type genericType);

    /**
     * Configures a given builder. Calls every public method of the builder (except for the build
     * method) with a value for the respective parameter type. The value is taken from the
     * input config map, if present. Otherwise a default value as defined in the sub-class is used.
     *
     * @param builder the builder.
     * @param inputConfig the input config.
     * @param <B> the type of builder.
     * @return the configured builder.
     */
    @SneakyThrows
    protected <B> B configureBuilder(B builder, Map<String, Object> inputConfig) {
        // Loop over all public methods of the builder, omitting the "build" method
        var builderMethods = getAllMethods(builder.getClass());
        for (var method: filterBuilderMethods(builderMethods)) {

            // Verify that each method takes exactly one parameter
            var parameters = method.getParameterTypes();
            assertThat(parameters).hasSize(1);

            // Get name of field corresponding to builder method; here: field name = method name
            var fieldName = method.getName();

            // Check whether the parameter is of a generic type
            var parameterType = parameters[0];
            if (isGeneric(parameterType)) {
                // Get the field corresponding to the method
                var field = getField(builder.getClass(), fieldName);
                assertThat(field).isNotNull();

                // Use field's generic type information to get correct value and invoke method
                var value = getGenericValue(fieldName, field.getGenericType(), inputConfig);
                method.invoke(builder, value);
            } else {
                // Invoke method with either default value for type or value from input config
                var value = getValue(fieldName, parameterType, inputConfig);
                method.invoke(builder, value);
            }
        }

        return builder;
    }

    /**
     * Validates all fields of a given instance. Checks that a field is not null and whether it
     * is set to the correct value. The value is taken from the input config map, if present.
     * Otherwise a default value as defined in the sub-class is used.
     *
     * @param instance the instance.
     * @param inputConfig the input config.
     */
    @SneakyThrows
    protected void validateInstanceFields(Object instance, Map<String, Object> inputConfig) {
        // Loop over all fields of the constructed instance
        var classFields = getAllFields(instance.getClass());
        for (var field: classFields) {
            // Allow accessing private fields
            field.setAccessible(true);

            if (Modifier.isStatic(field.getModifiers())) {
                assertThat(field.get(instance)).isNotNull();
            } else {

                // Verify that the field has been set
                var fieldValue = field.get(instance);
                assertThat(fieldValue).isNotNull();

                // Verify the correct value was set for the field
                var fieldType = field.getType();
                if (isGeneric(fieldType)) {
                    assertThat(fieldValue)
                        .isEqualTo(getGenericValue(field.getName(), field.getGenericType(), inputConfig));
                } else {
                    assertThat(fieldValue).isEqualTo(getValue(field.getName(), fieldType, inputConfig));
                }
            }
        }
    }

    /**
     * Returns the value to insert into a field. If the field value is defined in the input config
     * map, that value is used. Otherwise, the default value for the field's type is returned.
     *
     * @param fieldName name of the field.
     * @param fieldType type of the field.
     * @param inputConfig input config for overriding specific field values.
     * @return the value for the field.
     */
    protected Object getValue(final String fieldName, final Class<?> fieldType, final Map<String, Object> inputConfig) {
        if (inputConfig == null) {
            return getDefaultParameter(fieldType);
        }

        return inputConfig.containsKey(fieldName) ? inputConfig.get(fieldName) : getDefaultParameter(fieldType);
    }

    /**
     * Returns the value to insert into a generic field. If the field value is defined in the input
     * config map, that value is used. Otherwise, the default value for the field's generic type is
     * returned.
     *
     * @param fieldName name of the field.
     * @param genericType generic type of the field.
     * @param inputConfig input config for overriding specific field values.
     * @return the value for the field.
     */
    protected Object getGenericValue(final String fieldName, final Type genericType, final Map<String, Object> inputConfig) {
        if (inputConfig == null) {
            return getGenericDefaultParameter(genericType);
        }

        return inputConfig.containsKey(fieldName) ? inputConfig.get(fieldName) : getGenericDefaultParameter(genericType);
    }

    /****************************** Reflection utility methods ******************************/

    protected boolean isGeneric(Class<?> clazz) {
        if (clazz == null) {
            return false;
        }

        return clazz.getTypeParameters().length > 0;
    }

    /**
     * Recursively gets all methods of a class and its super classes.
     *
     * @param clazz the class.
     * @return a list of all class methods.
     */
    protected List<Method> getAllMethods(Class<?> clazz) {
        if (clazz == null || Object.class.equals(clazz)) {
            return new ArrayList<>();
        }

        var inheritedMethods = new ArrayList<>(getAllMethods(clazz.getSuperclass()));
        var classMethods = Arrays.stream(clazz.getDeclaredMethods()).toList();

        return Stream.concat(inheritedMethods.stream(), classMethods.stream()).toList();
    }

    /**
     * Filters a builder class's methods and returns only the public methods, omitting the "build"
     * method.
     *
     * @param builderMethods the builder class's methods.
     * @return the filtered list.
     */
    protected List<Method> filterBuilderMethods(final List<Method> builderMethods) {
        return builderMethods.stream()
            .filter(m -> Modifier.isPublic(m.getModifiers()))
            .filter(m -> !m.getName().equals("build"))
            .collect(Collectors.toList());
    }

    /**
     * Recursively gets all fields of a class and its super classes.
     *
     * @param clazz the class.
     * @return a list of all class fields.
     */
    protected List<Field> getAllFields(Class<?> clazz) {
        if (clazz == null || Object.class.equals(clazz)) {
            return new ArrayList<>();
        }

        var inheritedFields = new ArrayList<>(getAllFields(clazz.getSuperclass()));
        var classFields = Arrays.stream(clazz.getDeclaredFields()).toList();

        return Stream.concat(inheritedFields.stream(), classFields.stream()).toList();
    }

    /**
     * Returns a class's field with a given name, or null if the field does not exist. Looks for
     * the field in the class itself and recursively in all super classes.
     *
     * @param clazz the class.
     * @param fieldName the field name.
     * @return the class's field with the given name; or null, if the field does not exist.
     */
    protected Field getField(Class<?> clazz, String fieldName) {
        if (clazz == null || Object.class.equals(clazz)) {
            return null;
        }

        Field field;
        try {
            field = clazz.getDeclaredField(fieldName);
        } catch (NoSuchFieldException e) {
            field = getField(clazz.getSuperclass(), fieldName);
        }

        return field;
    }

    /**
     * Returns the class name of a generic type.
     *
     * @param genericType the generic type.
     * @return the class name.
     */
    protected String getTypeNameOfGenericType(Type genericType) {
        // getTypeName() returns a string in format "java.util.List<java.net.URI>" => return "java.util.List"
        var genericTypeName = genericType.getTypeName();
        return genericTypeName.split("<")[0];
    }

    /**
     * Returns the class name of the parameter type of a generic type.
     *
     * @param genericType the generic type.
     * @return the parameter type class name.
     */
    protected String getParameterTypeNameOfGenericType(Type genericType) {
        // getTypeName() returns a string in format "java.util.List<java.net.URI>" => return "java.net.URI"
        var genericTypeName = genericType.getTypeName();
        var lastPart = genericTypeName.split("<")[1];
        return lastPart.substring(0, lastPart.length() - 1);
    }

}
