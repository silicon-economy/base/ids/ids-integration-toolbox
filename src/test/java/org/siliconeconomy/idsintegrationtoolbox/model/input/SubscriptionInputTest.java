/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.net.URI;

import org.junit.jupiter.api.Test;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.Link;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.RouteLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.RouteOutput;
import org.springframework.test.util.ReflectionTestUtils;

class SubscriptionInputTest {

    @Test
    void constructor1_validInput_returnSubscriptionInput() {
        // Arrange
        final var target = URI.create("https://target");
        final var location = "https://location";
        final var subscriber = URI.create("https://subscriber");

        final var route = new RouteOutput();
        final var routeLinks = new RouteLinks();
        final var routeLink = new Link();

        ReflectionTestUtils.setField(routeLink, "href", location);
        ReflectionTestUtils.setField(routeLinks, "self", routeLink);
        ReflectionTestUtils.setField(route, "links", routeLinks);

        // Act
        final var result = new SubscriptionInput(target, route, subscriber, true);

        // Assert
        assertNotNull(result);
        assertEquals(target, result.getTarget());
        assertEquals(URI.create(location), result.getLocation());
        assertEquals(subscriber, result.getSubscriber());
    }
}
