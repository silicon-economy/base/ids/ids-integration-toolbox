/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.DataSourceType;

class RestDataSourceInputTest {


    @Test
    void constructor1_validInput_returnRestDataSourceInput() {
        // Act
        final var result = new RestDataSourceInput();

        // Assert
        assertNotNull(result);
        assertEquals(DataSourceType.REST, result.getType());
    }
}
