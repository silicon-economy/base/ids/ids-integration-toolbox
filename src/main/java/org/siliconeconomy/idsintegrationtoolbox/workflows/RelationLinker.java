/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.workflows;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.URI;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.EntityRelationWorkflow;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.RelationApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.AbstractEntity;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.Embedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.Links;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.EntityOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.UuidUtils;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.EmptyFieldException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.OperationNotAllowedException;
import org.springframework.stereotype.Service;

/**
 * RelationLinker offers the ability to link any number of arbitrary {@link EntityOutput}
 * by using the respective {@link RelationApiOperator} to create a new relation.
 *
 * @author Florian Zimmer, Ronja Quensel
 */
@Service
@RequiredArgsConstructor
@Log4j2
public class RelationLinker implements EntityRelationWorkflow {

    private static final String NOT_LINKABLE_LOG_MESSAGE = "Entities of type {} and {} cannot be"
        + " linked with a relation. Therefore they have been ignored.";

    private static final String LINKING_NOT_ALLOWED_LOG_MESSAGE = "Linking entities of type {} and"
        + " {} is not allowed.";

    /** Includes all available {@link RelationApiOperator}'s. */
    private final @NonNull List<RelationApiOperator<? extends AbstractEntity, ? extends Links<?>,
        ? extends AbstractEntity, ? extends EntityOutput<?>, ? extends Embedded<?, ?>>>
        relationApiOperators;

    /**
     * {@inheritDoc}
     */
    @Override
    @SafeVarargs
    public final void linkEntities(EntityOutput<? extends AbstractEntity>... entities)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException, EmptyFieldException {
        if (entities.length <= 1) {
            return;
        }

        // for each element, try to link it to all following elements
        for (var i = 0; i < entities.length - 1; i++) {
            final var entity1 = entities[i];

            for (var j = i + 1; j < entities.length; j++) {
                final var entity2 = entities[j];
                if (!entity1.equals(entity2)) {
                    createRelation(entity1, entity2);
                }
            }
        }
    }

    /**
     * Given two entities, it creates a relation using the fitting relation operator.
     *
     * @param entity1 the first entity.
     * @param entity2 the second entity.
     * @throws ApiInteractionUnsuccessfulException if the DSC returns a non 2xx status code.
     * @throws JsonProcessingException if parsing the response of the request fails.
     * @throws EmptyFieldException if a required output field is missing.
     */
    private void createRelation(EntityOutput<? extends AbstractEntity> entity1,
                                EntityOutput<? extends AbstractEntity> entity2)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException, EmptyFieldException {
        final var entity1ParameterType = getParameterType(entity1);
        final var entity2ParameterType = getParameterType(entity2);

        // find operator that matches the parameter types for both entities
        final var relationApiOperator = relationApiOperators.stream()
            .filter(operator -> {
                final var operatorParameterTypes = getParameterTypes(operator);
                final var abstractEntityTypes = filterAbstractEntityTypes(operatorParameterTypes);

                return abstractEntityTypes
                    .containsAll(List.of(entity1ParameterType, entity2ParameterType));
            }).findFirst();

        if (relationApiOperator.isPresent()) {
            invokeOperator(relationApiOperator.get(), entity1, entity2);
        } else {
            log.info(NOT_LINKABLE_LOG_MESSAGE, entity1.getClass().getCanonicalName(),
                entity2.getClass().getCanonicalName());
        }
    }

    /**
     * Invokes a RelationApiOperator for two entities. Determines the entity owning the
     * relationship by comparing the entities' parameter types' indices in the operator's parameter
     * types before invoking the add operation.
     *
     * @param operator the operator.
     * @param entity1 the first entity.
     * @param entity2 the second entity.
     */
    private void invokeOperator(final RelationApiOperator<?, ?, ?, ?, ?> operator,
                                final EntityOutput<?> entity1, final EntityOutput<?> entity2)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException, EmptyFieldException {
        var operatorParameterTypes = Arrays.stream(getParameterTypes(operator)).toList();

        // Get indices of entities' parameter types in the operator's parameter type list
        var entity1TypeIndex = operatorParameterTypes.indexOf(getParameterType(entity1));
        var entity2TypeIndex = operatorParameterTypes.indexOf(getParameterType(entity2));

        // Determine entity owning the relationship; the owning entity has the lower index as
        // it is declared first
        EntityOutput<?> owningEntity;
        EntityOutput<?> nonOwningEntity;
        if (entity1TypeIndex < entity2TypeIndex) {
            owningEntity = entity1;
            nonOwningEntity = entity2;
        } else {
            owningEntity = entity2;
            nonOwningEntity = entity1;
        }

        try {
            var owningId = UuidUtils.getUuidFromOutput(owningEntity);
            var nonOwningUri = URI.create(nonOwningEntity.getLinks().getSelf().getHref());
            operator.add(owningId, List.of(nonOwningUri));
        } catch (OperationNotAllowedException e) {
            log.info(LINKING_NOT_ALLOWED_LOG_MESSAGE, owningEntity.getClass().getCanonicalName(),
                nonOwningEntity.getClass().getCanonicalName());
        }
    }

    /**
     * Returns the parameter type for an EntityOutput.
     *
     * @param entity the entity output.
     * @return the parameter type.
     */
    private Type getParameterType(final EntityOutput<? extends AbstractEntity> entity) {
        return ((ParameterizedType) entity.getClass().getGenericSuperclass())
            .getActualTypeArguments()[0];
    }

    /**
     * Returns the parameter types for a RelationApiOperator.
     *
     * @param relationApiOperator the operator.
     * @return the parameter types.
     */
    private Type[] getParameterTypes(final RelationApiOperator<?, ?, ?, ?, ?> relationApiOperator) {
        return ((ParameterizedType) relationApiOperator.getClass().getGenericSuperclass())
            .getActualTypeArguments();
    }

    /**
     * Removes all types from a type array that do not extend abstract entity.
     *
     * @param parameterTypes the types to filter.
     * @return the filtered types containing only abstract entity types.
     */
    @SuppressWarnings("java:S3958") // false positive, toList() is a terminal stream operation
    private List<Type> filterAbstractEntityTypes(final Type[] parameterTypes) {
        return Arrays.stream(parameterTypes)
            .filter(pt -> AbstractEntity.class.isAssignableFrom(classForType(pt)))
            .toList();
    }

    /**
     * Returns the class for a type, or Object.class if the corresponding class cannot be found.
     *
     * @param type the type.
     * @return the class representing the type.
     */
    private Class<?> classForType(Type type) {
        try {
            return Class.forName(type.getTypeName());
        } catch (ClassNotFoundException e) {
            return Object.class;
        }
    }
}
