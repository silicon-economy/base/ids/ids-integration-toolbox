/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.workflows;

import java.net.URI;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.ClearingHouseWorkflow;
import org.siliconeconomy.idsintegrationtoolbox.core.ids.MessagesApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.model.output.ClearingHouseLogEntry;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * Provides methods for querying the Clearing House.
 *
 * @author Ronja Quensel
 */
@Service
@RequiredArgsConstructor
@Log4j2
public class ClearingHouseService implements ClearingHouseWorkflow {

    /** Clearing House path segment for querying log entries. */
    @Value("${clearing-house.path.query:messages/query}")
    private String clearingHouseQueryPath;

    /** Sends IDS messages. */
    private final @NonNull MessagesApiOperator messagesApiOperator;

    /** Maps JSON from/to Java objects. */
    private final @NonNull ObjectMapper objectMapper;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ClearingHouseLogEntry> query(final URI clearingHouseUrl, final String processId)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException, RestClientException {
        // Construct query URL for Clearing House and process
        final var recipient = UriComponentsBuilder
            .fromHttpUrl(clearingHouseUrl.toString())
            .pathSegment(clearingHouseQueryPath, processId)
            .build()
            .toUri();

        // Send the QueryMessage with an empty query => query all entries for process ID
        final var response = messagesApiOperator.query(recipient, " ");

        // Map response to list of ClearingHouseLogEntry instances
        final var typeReference = new TypeReference<List<ClearingHouseLogEntry>>() {};
        return objectMapper.readValue(response, typeReference);
    }

}
