/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.workflows;

import java.net.URI;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.BrokerWorkflow;
import org.siliconeconomy.idsintegrationtoolbox.core.base.OfferedResourceApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.ids.MessagesApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.springframework.stereotype.Service;

/**
 * Offers methods for interacting with an IDS Broker.
 *
 * @author Ronja Quensel
 */
@Service
@RequiredArgsConstructor
@Log4j2
public class BrokerService implements BrokerWorkflow {

    /** Sends IDS messages. */
    private final @NonNull MessagesApiOperator messagesApiOperator;

    /** Operates the offered resources API of the DSC. */
    private final OfferedResourceApiOperator resourceApiOperator;

    private static final String RESOURCE_REGISTERED_LOG = "Registered resource ({}) at broker: {}";

    /**
     * {@inheritDoc}
     */
    @Override
    public void register(final URI broker) throws ApiInteractionUnsuccessfulException {
        messagesApiOperator.connectorUpdate(broker);
        log.info("Registered connector at broker: {}", broker);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void register(final URI broker, final List<URI> resources)
            throws ApiInteractionUnsuccessfulException {
        register(broker);
        for (var resource : resources) {
            messagesApiOperator.resourceUpdate(broker, resource);
            log.info(RESOURCE_REGISTERED_LOG, resource, broker);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void registerResources(final URI broker, final List<URI> resources)
        throws ApiInteractionUnsuccessfulException {
        for (var resource : resources) {
            messagesApiOperator.resourceUpdate(broker, resource);
            log.info(RESOURCE_REGISTERED_LOG, resource, broker);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void registerAllResources(final URI broker) throws ApiInteractionUnsuccessfulException,
            JsonProcessingException {
        register(broker);
        for (var resource : resourceApiOperator.getAll().getEmbedded().getEntries()) {
            var id = URI.create(resource.getLinks().getSelf().getHref());
            messagesApiOperator.resourceUpdate(broker, id);
            log.info(RESOURCE_REGISTERED_LOG, id, broker);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void unregister(final URI broker) throws ApiInteractionUnsuccessfulException {
        messagesApiOperator.connectorUnavailable(broker);
        log.info("Unregistered connector at broker: {}", broker);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void unregisterResources(final URI broker, final List<URI> resources)
            throws ApiInteractionUnsuccessfulException {
        for (var resource : resources) {
            messagesApiOperator.resourceUnavailable(broker, resource);
            log.info("Unregistered resource {} at broker: {}", resource, broker);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String search(final URI broker, final String searchTerm)
            throws ApiInteractionUnsuccessfulException {
        return search(broker, searchTerm, 50, 0);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String search(final URI broker, final String searchTerm, final int limit,
                       final int offset) throws ApiInteractionUnsuccessfulException {
        return messagesApiOperator.search(broker, searchTerm, limit, offset);
    }

}
