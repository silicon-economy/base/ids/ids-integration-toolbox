/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
/**
 * Contains different classes that offer methods for common workflows like e.g. creating resources
 * or negotiating contracts. These workflows require multiple interactions with the Dataspace
 * Connector, that are always executed in the same order. Therefore, the workflow classes aim to
 * make these workflows quick and simple to execute.
 */

package org.siliconeconomy.idsintegrationtoolbox.workflows;
