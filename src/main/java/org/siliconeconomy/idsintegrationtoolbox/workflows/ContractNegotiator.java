/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.workflows;

import static org.siliconeconomy.idsintegrationtoolbox.utils.ContractNegotiationUtils.addTargetToRules;
import static org.siliconeconomy.idsintegrationtoolbox.utils.ContractNegotiationUtils.findContractForResourceById;
import static org.siliconeconomy.idsintegrationtoolbox.utils.ContractNegotiationUtils.getAllArtifactsForResource;
import static org.siliconeconomy.idsintegrationtoolbox.utils.ContractNegotiationUtils.getAllRulesFromContract;
import static org.siliconeconomy.idsintegrationtoolbox.utils.ContractNegotiationUtils.getArtifactsForResourceById;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import de.fraunhofer.iais.eis.ArtifactImpl;
import de.fraunhofer.iais.eis.Resource;
import de.fraunhofer.iais.eis.Rule;
import de.fraunhofer.iais.eis.util.Util;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.ContractNegotiationWorkflow;
import org.siliconeconomy.idsintegrationtoolbox.core.ids.MessagesApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.AgreementOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.InvalidInputException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.NoParsableResponseException;
import org.springframework.stereotype.Service;

/**
 * Contains methods for negotiating a contract with another connector. The methods are designed to
 * require as little parameters for this as possible.
 *
 * @author Ronja Quensel
 */
@Service
@RequiredArgsConstructor
@Log4j2
public class ContractNegotiator implements ContractNegotiationWorkflow {

    /** Operators the (IDS) messages API of the DSC. */
    private final @NonNull MessagesApiOperator apiOperator;

    /**
     * {@inheritDoc}
     */
    @Override
    public AgreementOutput negotiateContract(final URI recipient, final List<URI> resourceIds,
                                             final List<URI> artifactIds,
                                             final List<URI> contractIds,
                                             final boolean download)
        throws IOException, ApiInteractionUnsuccessfulException, NoParsableResponseException,
        InvalidInputException {
        final var artifacts = new ArrayList<URI>();
        final var ruleInput = new ArrayList<Rule>();

        for (var resourceId : resourceIds) {
            final var resource = (Resource) apiOperator
                .descriptionRequest(recipient, resourceId);

            // Get list of all artifacts associated with resource
            final var artifactsForResource = getArtifactsForResourceById(resource, artifactIds);
            artifacts.addAll(artifactsForResource);

            // Get list of all rules associated with specified contract
            final var contract = findContractForResourceById(resource, contractIds);
            final var rules = getAllRulesFromContract(contract);

            ruleInput.addAll(addTargetToRules(rules, artifactsForResource));
        }

        final var invalidArtifacts = new ArrayList<>(artifactIds);
        invalidArtifacts.removeAll(artifacts);
        if (!invalidArtifacts.isEmpty()) {
            log.warn("Could not find all specified artifacts in the specified resources. "
                + "Will not request the following artifacts: {}", invalidArtifacts);
        }

        return apiOperator
            .contractNegotiation(recipient, resourceIds, artifacts, download, ruleInput);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AgreementOutput negotiateContract(final URI recipient, final List<URI> resourceIds,
                                             final List<URI> contractIds,
                                             final boolean download)
        throws IOException, ApiInteractionUnsuccessfulException, NoParsableResponseException,
        InvalidInputException {
        final var artifacts = new ArrayList<URI>();
        final var ruleInput = new ArrayList<Rule>();

        for (var resourceId : resourceIds) {
            final var resource = (Resource) apiOperator
                .descriptionRequest(recipient, resourceId);

            // Get list of all artifacts associated with resource
            final var artifactsForResource = getAllArtifactsForResource(resource);

            // Get the contract offer to use
            final var contract = findContractForResourceById(resource, contractIds);

            // Add target to rules
            final var rules = getAllRulesFromContract(contract);
            final var ruleInputForResource = addTargetToRules(rules, artifactsForResource);

            artifacts.addAll(artifactsForResource);
            ruleInput.addAll(ruleInputForResource);
        }

        return apiOperator
            .contractNegotiation(recipient, resourceIds, artifacts, download, ruleInput);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AgreementOutput negotiateContract(final URI recipient, final URI resourceId,
                                             final URI artifactId, final URI contractId,
                                             final boolean download) throws IOException,
        ApiInteractionUnsuccessfulException, NoParsableResponseException, InvalidInputException {
        final var resource = (Resource) apiOperator
            .descriptionRequest(recipient, resourceId);

        // Check if artifact is part of resource
        final var representations = resource.getRepresentation();
        var artifactPresent = false;
        for (var representation : representations) {
            for (var artifact : representation.getInstance()) {
                if (artifact instanceof ArtifactImpl && artifact.getId().equals(artifactId)) {
                    artifactPresent = true;
                }
            }
        }

        if (!artifactPresent) {
            throw new InvalidInputException("Artifact " + artifactId + " could not be found in "
                + "metadata of resource " + resourceId + ". Unable to negotiate contract.");
        }

        // Get the contract offer to use
        final var contract = findContractForResourceById(resource, contractId);

        // Add target to rules
        final var rules = getAllRulesFromContract(contract);
        final var artifacts = Util.asList(artifactId);
        final var ruleInput = addTargetToRules(rules, artifacts);

        return apiOperator.contractNegotiation(recipient, Util.asList(resourceId), artifacts,
            download, ruleInput);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AgreementOutput negotiateContract(final URI recipient, final URI resourceId,
                                             final URI contractId,
                                             final boolean download)
        throws IOException, ApiInteractionUnsuccessfulException, NoParsableResponseException,
        InvalidInputException {
        final var resource = (Resource) apiOperator
            .descriptionRequest(recipient, resourceId);

        // Get list of all artifacts associated with resource
        final var artifacts = getAllArtifactsForResource(resource);

        // Get the contract offer to use
        final var contract = findContractForResourceById(resource, contractId);

        // Add target to rules
        final var rules = getAllRulesFromContract(contract);
        final var ruleInput = addTargetToRules(rules, artifacts);

        return apiOperator.contractNegotiation(recipient, Util.asList(resourceId), artifacts,
            download, ruleInput);
    }

}
