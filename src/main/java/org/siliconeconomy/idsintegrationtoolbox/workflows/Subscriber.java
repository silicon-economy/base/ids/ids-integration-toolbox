/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.workflows;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.BackendSubscriptionWorkflow;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.IdsSubscriptionWorkflow;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.SubscriptionWorkflow;
import org.siliconeconomy.idsintegrationtoolbox.core.DscApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.base.SubscriptionApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.ids.MessagesApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Resource;
import org.siliconeconomy.idsintegrationtoolbox.model.input.SubscriptionInput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ArtifactOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.RepresentationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.RequestedResourceOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ResourceOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.NoParsableResponseException;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * Workflow class for subscriptions. Offers methods for requesting resources and artifacts and
 * subscribing automatically, subscribing to previously requested resources, representations or
 * artifacts, and subscribing backends for updates to resources, representations or artifacts.
 *
 * @author Ronja Quensel
 */
@Service
@RequiredArgsConstructor
public class Subscriber extends DscApiOperator implements SubscriptionWorkflow,
    IdsSubscriptionWorkflow, BackendSubscriptionWorkflow {

    /** Sends IDS messages. */
    private final @NonNull MessagesApiOperator messagesApiOperator;

    /** Initiates contract negotiations. */
    private final @NonNull ContractNegotiator negotiator;

    /** Provides CRUD operations for subscriptions. */
    private final @NonNull SubscriptionApiOperator subscriptionApiOperator;

    /**
     * {@inheritDoc}
     */
    @Override
    public void requestAndSubscribe(final URI recipient, final List<URI> resourceIds,
                                    final List<URI> artifactIds, final List<URI> contractIds)
            throws IOException, NoParsableResponseException, ApiInteractionUnsuccessfulException {
        // Negotiate agreement for all desired artifacts, including requesting the data
        negotiator.negotiateContract(recipient, resourceIds, artifactIds, contractIds, true);

        // Create a subscription for each requested resource
        for (var resourceId : resourceIds) {
            messagesApiOperator
                .subscribe(recipient, createSubscriptionInput(resourceId, false));
        }

        // Create a subscription for each requested artifact, including data push
        for (var artifactId : artifactIds) {
            messagesApiOperator
                .subscribe(recipient, createSubscriptionInput(artifactId, true));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void subscribe(final URI recipient, final RequestedResourceOutput resource)
            throws IOException, ApiInteractionUnsuccessfulException {
        final var input = createSubscriptionInput(resource.getRemoteId(), false);
        messagesApiOperator.subscribe(recipient, input);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void subscribe(final URI recipient, final RepresentationOutput representation)
            throws IOException, ApiInteractionUnsuccessfulException {
        final var input = createSubscriptionInput(representation.getRemoteId(), false);
        messagesApiOperator.subscribe(recipient, input);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void subscribe(final URI recipient, final ArtifactOutput artifact,
                          final boolean pushData)
            throws IOException, ApiInteractionUnsuccessfulException {
        final var input = createSubscriptionInput(artifact.getRemoteId(), pushData);
        messagesApiOperator.subscribe(recipient, input);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void subscribeBackend(final ResourceOutput<? extends Resource> resource,
                                 final URI backendLocation, final URI subscriber)
            throws ApiInteractionUnsuccessfulException, JsonProcessingException {
        final var target = URI.create(resource.getLinks().getSelf().getHref());
        final var input = new SubscriptionInput(target, backendLocation, subscriber, false);
        subscriptionApiOperator.create(input);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void subscribeBackend(final RepresentationOutput representation,
                                 final URI backendLocation, final URI subscriber)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException {
        final var target = URI.create(representation.getLinks().getSelf().getHref());
        final var input = new SubscriptionInput(target, backendLocation, subscriber, false);
        subscriptionApiOperator.create(input);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void subscribeBackend(final ArtifactOutput artifact, final URI backendLocation,
                                 final URI subscriber, final boolean pushData)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException {
        final var target = URI.create(artifact.getLinks().getSelf().getHref());
        final var input = new SubscriptionInput(target, backendLocation, subscriber, pushData);
        subscriptionApiOperator.create(input);
    }

    /**
     * Creates the {@link SubscriptionInput} for subscribing to an artifact.
     *
     * @param target the artifact ID.
     * @return the subscription input.
     */
    private SubscriptionInput createSubscriptionInput(final URI target, final boolean pushData) {
        final var uri = UriComponentsBuilder
            .fromUriString(this.connectorUrl)
            .path("/api/ids/data").build()
            .toUri();
        return new SubscriptionInput(target, uri, uri, pushData);
    }

}
