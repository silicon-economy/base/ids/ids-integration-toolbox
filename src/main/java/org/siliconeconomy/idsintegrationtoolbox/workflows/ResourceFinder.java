/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.workflows;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.fraunhofer.iais.eis.Connector;
import de.fraunhofer.iais.eis.Resource;
import de.fraunhofer.iais.eis.ResourceCatalog;
import de.fraunhofer.iais.eis.util.TypedLiteral;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.ResourceFindingWorkflow;
import org.siliconeconomy.idsintegrationtoolbox.core.ids.MessagesApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.NoParsableResponseException;
import org.springframework.stereotype.Service;

/**
 * Contains methods for finding a resource from a remote connector. The methods are designed to
 * require as little parameters for the task as possible.
 *
 * @author Haydar Qarawlus
 */

@RequiredArgsConstructor
@Log4j2
@Service
public class ResourceFinder implements ResourceFindingWorkflow {

    /** The messages api operator. */
    private final @NonNull MessagesApiOperator messagesApiOperator;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<URI> findResourceByTitle(final URI remoteConnectorUri, final String resourceTitle,
                                         final boolean isExactMatch)
        throws NoParsableResponseException,
        ApiInteractionUnsuccessfulException, IOException {

        final var results = new HashSet<URI>();
        final var catalogs = getResourceCatalogs(remoteConnectorUri);

        for (var catalog : catalogs) {
            for (Resource offeredResource : catalog.getOfferedResource()) {
                matchSearchTermAgainstList(offeredResource.getId(), offeredResource.getTitle(),
                    resourceTitle, results, isExactMatch);
            }
        }

        return new ArrayList<>(results);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<URI> findResourceByKeyword(final URI remoteConnectorUri,
                                           final String resourceKeyword,
                                           final boolean isExactMatch)
        throws NoParsableResponseException,
        ApiInteractionUnsuccessfulException, IOException {

        final var results = new HashSet<URI>();
        final var catalogs = getResourceCatalogs(remoteConnectorUri);

        for (var catalog : catalogs) {
            for (var offeredResource : catalog.getOfferedResource()) {
                matchSearchTermAgainstList(offeredResource.getId(), offeredResource.getKeyword(),
                    resourceKeyword, results, isExactMatch);
            }
        }

        return new ArrayList<>(results);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<URI> findResourceByDescription(final URI remoteConnectorUri,
                                               final String resourceDescription,
                                               final boolean isExactMatch)
        throws NoParsableResponseException,
        ApiInteractionUnsuccessfulException, IOException {

        final var results = new HashSet<URI>();
        final var catalogs = getResourceCatalogs(remoteConnectorUri);

        for (var catalog : catalogs) {
            for (Resource offeredResource : catalog.getOfferedResource()) {
                matchSearchTermAgainstList(offeredResource.getId(),
                    offeredResource.getDescription(), resourceDescription, results, isExactMatch);
            }
        }

        return new ArrayList<>(results);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<URI> findResourceByAny(final URI remoteConnectorUri, final String searchTerm,
                                       final boolean isExactMatch)
        throws NoParsableResponseException,
        ApiInteractionUnsuccessfulException, IOException {

        final var results = new HashSet<URI>();
        final var catalogs = getResourceCatalogs(remoteConnectorUri);

        for (var catalog : catalogs) {
            for (Resource offeredResource : catalog.getOfferedResource()) {
                // Match search keyword to the resource title. Add resource to results only if it is
                // not a duplicate.
                matchSearchTermAgainstList(offeredResource.getId(), offeredResource.getTitle(),
                    searchTerm, results, isExactMatch);

                // Match resource keywords.
                matchSearchTermAgainstList(offeredResource.getId(), offeredResource.getKeyword(),
                    searchTerm, results, isExactMatch);

                // Match resource description.
                matchSearchTermAgainstList(offeredResource.getId(),
                    offeredResource.getDescription(), searchTerm, results, isExactMatch);
            }
        }

        return new ArrayList<>(results);
    }

    /**
     * Checks whether a given search keyword is contained in a list of typed literals. If it is, the
     * given resource ID is added to the result set. Whether the search term should be an exact
     * match for a value in the list or just contained in any way, can be specified via the
     * parameter isExactMatch.
     *
     * @param resourceId the resource ID to add to the result set in case of a match.
     * @param list the list of typed literals to check.
     * @param searchTerm the search keyword.
     * @param results the result set.
     * @param isExactMatch whether the search term should be exactly matched or contained.
     */
    private void matchSearchTermAgainstList(final URI resourceId,
                                            final List<TypedLiteral> list,
                                            final String searchTerm,
                                            final Set<URI> results,
                                            final boolean isExactMatch) {
        for (TypedLiteral typedLiteral : list) {
            if (isExactMatch) {
                if (searchTerm.equals(typedLiteral.getValue())) {
                    results.add(resourceId);
                }
            } else {
                if (typedLiteral.getValue() != null
                    && typedLiteral.getValue().contains(searchTerm)) {
                    results.add(resourceId);
                }
            }
        }
    }

    /**
     * Retrieves the resource catalog of a remote connector.
     *
     * @param remoteConnectorUri the URI of the remote connector.
     *
     * @return the offered resource catalog of the remote resource
     * @throws NoParsableResponseException if invalid response.
     * @throws ApiInteractionUnsuccessfulException if unable to interact with the connector.
     * @throws IOException if output cannot be deserialized.
     * */
    private List<ResourceCatalog> getResourceCatalogs(final URI remoteConnectorUri)
        throws IOException, ApiInteractionUnsuccessfulException, NoParsableResponseException {
        final var catalogs = new ArrayList<ResourceCatalog>();

        // Fetch connector description
        final var connector =
            (Connector) messagesApiOperator.descriptionRequest(remoteConnectorUri);

        for (var catalog : connector.getResourceCatalog()) {
            // Fetch catalog
            catalogs.add((ResourceCatalog) messagesApiOperator.descriptionRequest(
                remoteConnectorUri, catalog.getId()));
        }

        return catalogs;
    }

}
