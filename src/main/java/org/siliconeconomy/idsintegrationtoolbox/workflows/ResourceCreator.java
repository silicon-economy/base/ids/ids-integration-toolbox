/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.workflows;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.ResourceCreationWorkflow;
import org.siliconeconomy.idsintegrationtoolbox.core.base.ArtifactApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.base.CatalogApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.base.ContractApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.base.OfferedResourceApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.base.RepresentationApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.base.RuleApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.ContractRulesApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.OfferedResourceCatalogsApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.OfferedResourceContractsApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.OfferedResourceRepresentationsApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.RepresentationArtifactsApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.model.input.ArtifactInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.CatalogInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.ContractInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.OfferedResourceInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.RepresentationInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.RuleInput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ContractRuleOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.OfferedResourceOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.UuidUtils;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.springframework.stereotype.Service;

/**
 * Contains methods for creating an offered resource at a connector. The methods are designed to
 * require as little parameters for this as possible.
 *
 * @author Haydar Qarawlus
 */

@RequiredArgsConstructor
@Service
@Log4j2
public class ResourceCreator implements ResourceCreationWorkflow {
    /** Operator for the connector's Catalog Api. */
    private final @NonNull CatalogApiOperator catalogApiOperator;

    /** Operator for the connector's Offered Resource Api. */
    private final @NonNull OfferedResourceApiOperator offeredResourceApiOperator;

    /** Operator for the connector's Representation Api. */
    private final @NonNull RepresentationApiOperator representationApiOperator;

    /** Operator for the connector's Artifact Api. */
    private final @NonNull ArtifactApiOperator artifactApiOperator;

    /** Operator for the connector's Contracts Api. */
    private final @NonNull ContractApiOperator contractApiOperator;

    /** Operator for the connector's Rules Api. */
    private final @NonNull RuleApiOperator ruleApiOperator;

    /** Operator for the connector's Offered Resource-Catalogs Relation Api. */
    private final @NonNull OfferedResourceCatalogsApiOperator offeredResourceCatalogsApiOperator;

    /** Operator for the connector's Offered Resource-Representation Relation Api. */
    private final @NonNull OfferedResourceRepresentationsApiOperator
        offeredResourceRepresentationsApiOperator;

    /** Operator for the connector's Representation-Artifacts Relation Api. */
    private final @NonNull RepresentationArtifactsApiOperator representationArtifactsApiOperator;

    /** Operator for the connector's Offered Resource-Contracts Api. */
    private final @NonNull OfferedResourceContractsApiOperator offeredResourceContractsApiOperator;

    /** Operator for the connector's Contracts-Rules Relation Api. */
    private final @NonNull ContractRulesApiOperator contractRulesApiOperator;

    /**
     * {@inheritDoc}
     */
    @Override
    public OfferedResourceOutput createResource(
        final OfferedResourceInput offeredResourceInput,
        final CatalogInput catalogInput,
        final RepresentationInput representationInput,
        final ArtifactInput artifactInput,
        final ContractInput contractInput,
        final RuleInput... ruleInputs)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException {

        // Create resource requirements items based on provided inputs
        final var catalogUri = createCatalog(catalogInput);
        final var artifactUri = createArtifact(artifactInput);
        final var representationUri = createRepresentation(representationInput);
        final var contractUri = createContract(contractInput);
        final var ruleUris = createRules(Arrays.stream(ruleInputs).collect(Collectors.toList()));

        return createResourceAndRelations(offeredResourceInput, catalogUri, representationUri,
            artifactUri, contractUri, ruleUris);

    }


    /**
     * {@inheritDoc}
     */
    @Override
    public OfferedResourceOutput createResource(
        final OfferedResourceInput offeredResourceInput,
        final URI catalogUri,
        final RepresentationInput representationInput,
        final ArtifactInput artifactInput,
        final ContractInput contractInput,
        final RuleInput... ruleInputs)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException {

        final var artifactUri = createArtifact(artifactInput);
        final var representationUri = createRepresentation(representationInput);
        final var contractUri = createContract(contractInput);
        final var ruleUris = createRules(Arrays.stream(ruleInputs).collect(Collectors.toList()));

        return createResourceAndRelations(offeredResourceInput, catalogUri, representationUri,
            artifactUri, contractUri, ruleUris);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OfferedResourceOutput createResource(
        final OfferedResourceInput offeredResourceInput,
        final URI catalogUri,
        final RepresentationInput representationInput,
        final ArtifactInput artifactInput,
        final URI contractUri)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException {

        final var artifactUri = createArtifact(artifactInput);
        final var representationUri = createRepresentation(representationInput);

        return createResourceAndRelations(offeredResourceInput, catalogUri, representationUri,
            artifactUri, contractUri, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OfferedResourceOutput createResource(
        final OfferedResourceInput offeredResourceInput,
        final CatalogInput catalogInput,
        final RepresentationInput representationInput,
        final ArtifactInput artifactInput,
        final URI contractUri)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException {

        final var catalogUri = createCatalog(catalogInput);
        final var artifactUri = createArtifact(artifactInput);
        final var representationUri = createRepresentation(representationInput);

        return createResourceAndRelations(offeredResourceInput, catalogUri, representationUri,
            artifactUri, contractUri, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OfferedResourceOutput createResource(
        final OfferedResourceInput offeredResourceInput,
        final URI catalogUri,
        final RepresentationInput representationInput,
        final ArtifactInput artifactInput,
        final ContractInput contractInput,
        final URI... ruleUris)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException {

        final var artifactUri = createArtifact(artifactInput);
        final var representationUri = createRepresentation(representationInput);
        final var contractUri = createContract(contractInput);

        return createResourceAndRelations(offeredResourceInput, catalogUri, representationUri,
            artifactUri, contractUri, Arrays.stream(ruleUris).collect(Collectors.toList()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OfferedResourceOutput createResource(
        final OfferedResourceInput offeredResourceInput,
        final CatalogInput catalogInput,
        final RepresentationInput representationInput,
        final ArtifactInput artifactInput,
        final ContractInput contractInput,
        final URI... ruleUris)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException {

        final var catalogUri = createCatalog(catalogInput);
        final var artifactUri = createArtifact(artifactInput);
        final var representationUri = createRepresentation(representationInput);
        final var contractUri = createContract(contractInput);

        return createResourceAndRelations(offeredResourceInput, catalogUri, representationUri,
            artifactUri, contractUri, Arrays.stream(ruleUris).collect(Collectors.toList()));
    }

    /**
     * Creates new rules at the connector.
     *
     * @param ruleInput the rule metadata.
     * @return the URIs of the created rules.
     * @throws ApiInteractionUnsuccessfulException if communication with the required endpoints
     *                                             in the connector cannot be made.
     * @throws JsonProcessingException             if unable to parse the inputs and the response.
     */
    private List<URI> createRules(final List<RuleInput> ruleInput)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException {
        List<URI> rules = new ArrayList<>();
        for (RuleInput rule : ruleInput) {
            ContractRuleOutput contractRule = ruleApiOperator.create(rule);
            final var uri = URI.create(contractRule.getLinks().getSelf().getHref());
            rules.add(uri);
        }
        return rules;
    }

    /**
     * Creates a new rule at the connector.
     *
     * @param contractInput the contract metadata.
     * @return the URI of the created contract.
     * @throws ApiInteractionUnsuccessfulException if communication with the required endpoints
     *                                             in the connector cannot be made.
     * @throws JsonProcessingException             if unable to parse the inputs and the response.
     */
    private URI createContract(final ContractInput contractInput)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException {
        // Create a contract at the connector
        final var contractSingleOutput = contractApiOperator.create(contractInput);
        return URI.create(contractSingleOutput.getLinks().getSelf().getHref());
    }

    /**
     * Creates a new rule at the connector.
     *
     * @param representationInput the representation metadata.
     * @return the URI of the created representation.
     * @throws ApiInteractionUnsuccessfulException if communication with the required endpoints
     *                                             in the connector cannot be made.
     * @throws JsonProcessingException             if unable to parse the inputs and the response.
     */
    private URI createRepresentation(final RepresentationInput representationInput)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException {
        // Create representation at connector
        final var representationOutput = representationApiOperator.create(representationInput);
        return URI.create(representationOutput.getLinks().getSelf().getHref());
    }

    /**
     * Creates a new rule at the connector.
     *
     * @param artifactInput the artifact metadata.
     * @return the URI of the created artifact.
     * @throws ApiInteractionUnsuccessfulException if communication with the required endpoints
     *                                             in the connector cannot be made.
     * @throws JsonProcessingException             if unable to parse the inputs and the response.
     */
    private URI createArtifact(final ArtifactInput artifactInput)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException {
        // Create the artifact for the offered resource
        final var artifactOutput = artifactApiOperator.create(artifactInput);
        return URI.create(artifactOutput.getLinks().getSelf().getHref());
    }

    /**
     * Creates a new rule at the connector.
     *
     * @param catalogInput the catalog metadata.
     * @return the URI of the created catalog.
     * @throws ApiInteractionUnsuccessfulException if communication with the required endpoints
     *                                             in the connector cannot be made.
     * @throws JsonProcessingException             if unable to parse the inputs and the response.
     */
    private URI createCatalog(final CatalogInput catalogInput)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException {
        // Create catalog at connector
        final var createdCatalog = catalogApiOperator.create(catalogInput);
        // Add offered resource to the resource catalog
        return URI.create(createdCatalog.getLinks().getSelf().getHref());
    }


    /**
     * Creates a resource at the connector and creates its relations to the other components.
     *
     * @param offeredResourceInput the resource metadata.
     * @param catalogUri           the URI of the catalog at the connector.
     * @param representationUri    the URI of the representation at the connector.
     * @param artifactUri          the URI of the artifact at the connector.
     * @param contractUri          the URI of the contract at the connector
     * @param ruleUris             the URI of the rules at the connector. Nullable if contract and
     *                             rule already
     *                             related.
     * @return the metadata received from the connector about the created resource.
     * @throws ApiInteractionUnsuccessfulException if communication with the required endpoints
     *                                             in the connector cannot be made.
     * @throws JsonProcessingException             if unable to parse the inputs and the responses.
     */
    private OfferedResourceOutput createResourceAndRelations(
        final OfferedResourceInput offeredResourceInput,
        final URI catalogUri,
        final URI representationUri,
        final URI artifactUri,
        final URI contractUri,
        final List<URI> ruleUris)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException {

        // Create offered resource at connector
        final var offeredResourceOutput = offeredResourceApiOperator.create(offeredResourceInput);


        /* Extract required UUIDs from provided URIs */

        // 1. Extract resource UUID from connector output.
        UUID resourceId;
        try {
            resourceId =
                UuidUtils.getUuidFromUri(URI.create(
                    offeredResourceOutput.getLinks().getSelf().getHref()));
        } catch (Exception e) {
            throw new ApiInteractionUnsuccessfulException("Unable to extract resource UUID from "
                + "connector response.");
        }

        // 2. Extract representation UUID from its URI
        UUID representationId;
        try {
            representationId =
                UuidUtils.getUuidFromUri(representationUri);
        } catch (Exception e) {
            throw new ApiInteractionUnsuccessfulException("Unable to extract representation UUID "
                + "from representation URI.");
        }

        // 3. Extract contract UUID from URI
        UUID contractId;
        try {
            contractId =
                UuidUtils.getUuidFromUri(contractUri);
        } catch (Exception e) {
            throw new ApiInteractionUnsuccessfulException("Unable to extract contract UUID "
                + "from connector response.");
        }

        /* Create relations between the various components */

        // 1. Create relation between the offered resource and catalog
        final List<URI> catalogList = Collections.singletonList(catalogUri);
        offeredResourceCatalogsApiOperator.add(resourceId, catalogList);

        // 2. Create relation between the offered resource and the resource representation
        final List<URI> representationList = Collections.singletonList(representationUri);
        offeredResourceRepresentationsApiOperator.add(resourceId, representationList);

        // 3. Create relation between the representation and the artifact
        final List<URI> artifactList = Collections.singletonList(artifactUri);
        representationArtifactsApiOperator.add(representationId, artifactList);

        // 4. Create a relation between the resource and the contracts
        final List<URI> contractList = Collections.singletonList(contractUri);
        offeredResourceContractsApiOperator.add(resourceId, contractList);

        // 5. Create a relation between the contract and the rule if rule is not null
        if (ruleUris != null && !ruleUris.isEmpty()) {
            contractRulesApiOperator.add(contractId, ruleUris);
        }

        // Return the metadata of the offered resource
        log.info("Successfully created the resource and relations at the connector.");
        return offeredResourceOutput;
    }


}
