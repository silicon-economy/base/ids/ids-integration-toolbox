/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.workflows;

import java.net.URI;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.ResourceRemovalWorkflow;
import org.siliconeconomy.idsintegrationtoolbox.core.base.ArtifactApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.base.BaseApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.base.CatalogApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.base.ContractApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.base.OfferedResourceApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.base.RepresentationApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.base.RuleApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.ArtifactRepresentationsApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.CatalogOfferedResourcesApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.ContractOfferedResourcesApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.ContractRulesApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.OfferedResourceCatalogsApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.OfferedResourceContractsApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.OfferedResourceRepresentationsApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.RelationApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.RepresentationArtifactsApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.RepresentationOfferedResourcesApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.RuleContractsApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.CatalogOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.OfferedResourceOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.UuidUtils;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.EmptyFieldException;
import org.springframework.stereotype.Service;

/**
 * Contains methods for removing an offered resource at a connector and its connected
 * representations and artifacts. The  methods are designed to require as little parameters for
 * this as possible.
 *
 * @author Haydar Qarawlus
 */

@RequiredArgsConstructor
@Service
@Log4j2
public class ResourceRemover implements ResourceRemovalWorkflow {
    /** Operator for the connector's Catalog Api. */
    private final @NonNull CatalogApiOperator catalogApiOperator;

    /** Operator for the connector's Offered Resource Api. */
    private final @NonNull OfferedResourceApiOperator offeredResourceApiOperator;

    /** Operator for the connector's Representation Api. */
    private final @NonNull RepresentationApiOperator representationApiOperator;

    /** Operator for the connector's Artifact Api. */
    private final @NonNull ArtifactApiOperator artifactApiOperator;

    /** Operator for the connector's Contracts Api. */
    private final @NonNull ContractApiOperator contractApiOperator;

    /** Operator for the connector's Rules Api. */
    private final @NonNull RuleApiOperator ruleApiOperator;

    /** Operator for the connector's Offered Resource-Catalogs Relation Api. */
    private final @NonNull OfferedResourceCatalogsApiOperator offeredResourceCatalogsApiOperator;

    /** Operator for the connector's Offered Resource-Representation Relation Api. */
    private final @NonNull OfferedResourceRepresentationsApiOperator
        offeredResourceRepresentationsApiOperator;

    /** Operator for the connector's Representation-Offered Resource Relation Api. */
    private final @NonNull RepresentationOfferedResourcesApiOperator
        representationOfferedResourcesApiOperator;

    /** Operator for the connector's Catalog-Offered Resources Relation Api. */
    private final @NonNull CatalogOfferedResourcesApiOperator catalogOfferedResourcesApiOperator;

    /** Operator for the connector's Representation-Artifacts Relation Api. */
    private final @NonNull RepresentationArtifactsApiOperator representationArtifactsApiOperator;

    /** Operator for the connector's Artifact-Representations Relation Api. */
    private final @NonNull ArtifactRepresentationsApiOperator artifactRepresentationsApiOperator;

    /** Operator for the connector's Offered Resource-Contracts Api. */
    private final @NonNull OfferedResourceContractsApiOperator offeredResourceContractsApiOperator;

    /** Operator for the connector's Contracts-Offered Resource Api. */
    private final @NonNull ContractOfferedResourcesApiOperator contractOfferedResourcesApiOperator;

    /** Operator for the connector's Contracts-Rules Relation Api. */
    private final @NonNull ContractRulesApiOperator contractRulesApiOperator;

    /** Operator for the connector's Rules-Contracts Relation Api. */
    private final @NonNull RuleContractsApiOperator ruleContractsApiOperator;


    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteFullResource(URI resourceUri) throws ApiInteractionUnsuccessfulException,
        EmptyFieldException, JsonProcessingException {
        safeDeleteResource(UuidUtils.getUuidFromUri(resourceUri), false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteFullResource(UUID resourceUuid) throws ApiInteractionUnsuccessfulException,
        EmptyFieldException, JsonProcessingException {
        safeDeleteResource(resourceUuid, false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteFullResource(OfferedResourceOutput resourceOutput)
        throws ApiInteractionUnsuccessfulException, EmptyFieldException, JsonProcessingException {
        safeDeleteResource(UuidUtils.getUuidFromOutput(resourceOutput), false);
    }

    /**
     * {@inheritDoc}
     */
    public void deleteResourceAndCatalog(URI resourceUri) throws EmptyFieldException,
        ApiInteractionUnsuccessfulException, JsonProcessingException {
        safeDeleteResource(UuidUtils.getUuidFromUri(resourceUri), true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteResourceAndCatalog(UUID resourceUuid)
        throws ApiInteractionUnsuccessfulException, EmptyFieldException, JsonProcessingException {
        safeDeleteResource(resourceUuid, true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteResourceAndCatalog(OfferedResourceOutput resourceOutput)
        throws ApiInteractionUnsuccessfulException, EmptyFieldException, JsonProcessingException {
        safeDeleteResource(UuidUtils.getUuidFromOutput(resourceOutput), true);
    }

    /**
     * Attempts to safely delete the resource by only removing related objects if they are only
     * linked to the resource.
     *
     * @param resourceUuid the UUID of the resource to be deleted.
     * @param deleteCatalog whether to additionally delete the catalog containing the resource.
     */
    private void safeDeleteResource(final UUID resourceUuid, final boolean deleteCatalog)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException, EmptyFieldException {
        final var resourceUri = URI.create(offeredResourceApiOperator.getOne(resourceUuid)
            .getLinks().getSelf().getHref());
        // Retrieve all the representations belonging to the resource
        final var resourceRepresentations = offeredResourceRepresentationsApiOperator
            .getAll(resourceUuid)
            .getEmbedded()
            .getEntries();

        // Iterate over all representations found under the resource
        for (var resourceRepresentation : resourceRepresentations) {
            final var representationUuid = UuidUtils.getUuidFromOutput(resourceRepresentation);

            // Retrieve all artifacts belonging to the resource representation.
            final var representationArtifacts = representationArtifactsApiOperator
                .getAll(representationUuid)
                .getEmbedded()
                .getEntries();

            // Iterate over all artifacts
            for (var artifactOutput : representationArtifacts) {
                final var artifactUuid = UuidUtils.getUuidFromOutput(artifactOutput);
                deleteOrUnlinkArtifact(artifactUuid,
                    URI.create(resourceRepresentation.getLinks().getSelf().getHref()));
            }

            // After all artifacts deleted, delete the representation
            deleteOrUnlinkRepresentation(representationUuid, resourceUri);
        }

        // Iterate over all contracts
        var resourceContracts = offeredResourceContractsApiOperator
            .getAll(resourceUuid)
            .getEmbedded()
            .getEntries();

        for (var contractOutput : resourceContracts) {
            final var contractUuid = UuidUtils.getUuidFromOutput(contractOutput);
            // Get all rules related to the contract
            var contractRules = contractRulesApiOperator
                .getAll(contractUuid)
                .getEmbedded()
                .getEntries();
            for (var ruleOutput : contractRules) {
                final var ruleUuid = UuidUtils.getUuidFromOutput(ruleOutput);
                deleteOrUnlinkRule(ruleUuid,
                    URI.create(contractOutput.getLinks().getSelf().getHref()));
            }
            deleteOrUnlinkContract(contractUuid, resourceUri);
        }

        // delete catalogs if deleteCatalog is desired
        if (deleteCatalog) {
            // get all catalogs related to the resource
            var resourceCatalogs =
                offeredResourceCatalogsApiOperator
                    .getAll(resourceUuid)
                    .getEmbedded()
                    .getEntries();

            // Iterate over all catalogs and delete
            for (var catalogOutput : resourceCatalogs) {
                final var catalogUuid = UuidUtils.getUuidFromOutput(catalogOutput);
                deleteOrUnlinkCatalog(catalogUuid, resourceUri);
            }
        }

        // Delete the resource
        offeredResourceApiOperator.delete(resourceUuid);
    }

    /** Delete a catalog if it does not contain any resource other than the provided one.
     *
     * @param catalogUuid the UUID of the catalog to be deleted.
     * @param resourceUri the URI of the resource connected to the catalog.
     */
    private void deleteOrUnlinkCatalog(final UUID catalogUuid, final URI resourceUri)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException {

        deleteOrUnlink(catalogOfferedResourcesApiOperator, catalogApiOperator,
            catalogUuid, resourceUri);
    }

    /** Delete an artifact if it is not connected to a representation other than the provided one.
     *
     * @param artifactUuid the UUID of the artifact to be deleted.
     * @param representationUri the URI of the representation connected to the artifact.
     */
    private void deleteOrUnlinkArtifact(final UUID artifactUuid, final URI representationUri)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException {

        deleteOrUnlink(artifactRepresentationsApiOperator, artifactApiOperator,
            artifactUuid, representationUri);
    }

    /** Attempts to delete a base component if not linked more than instance of the provided
     * related components. Otherwise, only the connection between them is removed
     *
     * @param relationApiOperator the relation api operator connecting both components
     * @param baseApiOperator the api operator that mainly interacts with the connectors
     *                        endpoints dealing with the base component.
     * @param baseUuid the UUID of the base component to be deleted.
     * @param relatedUri the URI of the component related to the base component.
     */
    private void deleteOrUnlink(final RelationApiOperator<?, ?, ?, ?, ?> relationApiOperator,
                                final BaseApiOperator<?, ?, ?, ?, ?> baseApiOperator,
                                final UUID baseUuid,
                                final URI relatedUri)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException {
        final var relatedOutputsList = relationApiOperator.getAll(baseUuid)
                .getEmbedded()
                .getEntries();

        // Can be replaced with stream.noneMatch(...), but with heavier performance impact.
        if (relatedOutputsList.size() > 1) {
            log.debug("Removing link between {} and {} using RelationApiOperator: {}.",
                baseUuid.toString(), relatedUri.toString(),
                RelationApiOperator.class.getSimpleName());
            relationApiOperator.remove(baseUuid, List.of(relatedUri));
        } else {
            log.debug("Deleting element from DSC with UUID: {} using BaseApiOperator: {}.",
                baseUuid.toString(), BaseApiOperator.class.getSimpleName());
            baseApiOperator.delete(baseUuid);
        }
    }

    /** Delete a representation if it is not connected to a resource other than the provided
     * one.
     *
     * @param representationUuid the UUID of the representation to be deleted.
     * @param resourceUri the URI of the resource connected to the representation.
     */
    private void deleteOrUnlinkRepresentation(final UUID representationUuid,
                                              final URI resourceUri)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException {

        deleteOrUnlink(representationOfferedResourcesApiOperator,
            representationApiOperator, representationUuid, resourceUri);
    }

    /** Delete a contract if it is not connected to a resource other than the provided
     * one.
     *
     * @param contractUuid the UUID of the contract to be deleted.
     * @param resourceUri the URI of the resource connected to the contract.
     */
    private void deleteOrUnlinkContract(final UUID contractUuid, final URI resourceUri)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException {

        deleteOrUnlink(contractOfferedResourcesApiOperator, contractApiOperator, contractUuid,
            resourceUri);

    }

    /** Delete a rule if it is not connected to a contract other than the provided
     * one.
     *
     * @param ruleUuid the UUID of the rule to be deleted.
     * @param contractUri the URI of the contract connected to the rule.
     */
    private void deleteOrUnlinkRule(final UUID ruleUuid, final URI contractUri)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException {

        deleteOrUnlink(ruleContractsApiOperator, ruleApiOperator, ruleUuid, contractUri);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteCatalog(final UUID catalogId) throws ApiInteractionUnsuccessfulException,
        JsonProcessingException, EmptyFieldException {
        var catalogResources = catalogOfferedResourcesApiOperator.getAll(catalogId)
            .getEmbedded().getEntries();

        for (var resource : catalogResources) {
            deleteFullResource(resource);
        }

        catalogApiOperator.delete(catalogId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteCatalog(final URI catalogUri) throws EmptyFieldException,
        ApiInteractionUnsuccessfulException, JsonProcessingException {
        deleteCatalog(UuidUtils.getUuidFromUri(catalogUri));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteCatalog(final CatalogOutput catalogOutput) throws EmptyFieldException,
        ApiInteractionUnsuccessfulException, JsonProcessingException {
        deleteCatalog(UuidUtils.getUuidFromOutput(catalogOutput));
    }

}
