/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.dsc;

/**
 * Represents the DataSource model class available in the DSC.
 *
 * @author Marc Peschke
 */
public class DataSource extends AbstractEntity {}
