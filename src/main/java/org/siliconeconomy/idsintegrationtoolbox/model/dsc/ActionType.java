/*
 * Copyright 2020 Fraunhofer Institute for Software and Systems Engineering
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.siliconeconomy.idsintegrationtoolbox.model.dsc;

/**
 * Enum class for action types.
 *
 * @author Dataspace-Connector Developers
 */
public enum ActionType {

    /**
     * Start an app.
     */
    START("START"),

    /**
     * Stop an app.
     */
    STOP("STOP"),

    /**
     * Delete an app.
     */
    DELETE("DELETE"),

    /**
     * Describes an app.
     */
    DESCRIBE("DESCRIBE");

    /**
     * Holds the enums string.
     */
    private final String value;

    /**
     * Constructor.
     *
     * @param message The msg of the action type.
     */
    ActionType(final String message) {
        this.value = message;
    }

    @Override
    public String toString() {
        return value;
    }

}
