/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
/**
 * Contains model classes copied from the DSC, as they are used as types for generic
 * implementations.
 */

package org.siliconeconomy.idsintegrationtoolbox.model.dsc;
