/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.dsc;

/**
 * Represents the Route model class available in the DSC.
 *
 * @author Steffen Biehs
 */
public class Route extends NamedEntity {}
