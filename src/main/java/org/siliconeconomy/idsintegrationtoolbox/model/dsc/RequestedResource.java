/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.dsc;

/**
 * Represents the RequestedResource model class available in the DSC.
 *
 * @author Haydar Qarawlus
 */
public class RequestedResource extends Resource {}
