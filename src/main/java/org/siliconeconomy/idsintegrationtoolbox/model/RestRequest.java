/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model;

import java.util.HashMap;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

/**
 * POJO for holding all information required to build/execute a REST request.
 *
 * @author Haydar Qarawlus
 */
@Data
@AllArgsConstructor
public class RestRequest {

    /** HTTP method of the request.  */
    private HttpMethod httpMethod;

    /** Base URL of the request's recipient. */
    private String baseUrl;

    /**
     * Path to the endpoint to be called, e.g. "/api/connector". Only one of this and
     * endpointPathSegment is required, as both can represent the same path.
     */
    private String endpointPath;

    /**
     * Path segments of the endpoint to be called, e.g. ["api", "connector"]. Only one of this and
     * endpointPath is required, as both can represent the same path.
     */
    private String[] endpointPathSegments;

    /**  Username for basic authentication. If set, password is also required. */
    private String username;

    /** Password for basic authentication. If set, username is also required. */
    private String password;

    /** Request body to send with the request. */
    private String body;

    /** HTTP headers to send with the request. */
    private HashMap<String, String> headers = new HashMap<>();

    /** Query parameters to send with the request. */
    private MultiValueMap<String, String> queryParameters = new LinkedMultiValueMap<>();

    /** Path variables to send with the request. */
    private HashMap<String, String> pathVariables = new HashMap<>();

}
