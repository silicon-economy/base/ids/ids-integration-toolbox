/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input;

import java.net.URI;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Subscription;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.RouteOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.InputValidationException;

/**
 * The input for creating and updating subscriptions.
 *
 * @author Florian Zimmer
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class SubscriptionInput extends NamedEntityInput<Subscription> {

    /**
     * Target of the subscription, may point to a resource, representation or artifact.
     */
    private URI target;

    /**
     * Location where updates should be pushed to.
     */
    private URI location;

    /**
     * Identifies the subscriber.
     */
    private URI subscriber;

    /**
     * Whether data should be pushed, only has impact for subscriptions on artifacts.
     */
    private boolean pushData;

    /**
     * Creates a new instance of a Subscription, based on a route object.
     *
     * @param target     the target subscription reference.
     * @param route      the route object which specifies the location to push updates to.
     * @param subscriber the subscriber uri.
     * @param pushData   a flag whether data should be pushed or not.
     *
     * @deprecated use the builder.
     */
    @Deprecated(forRemoval = true)
    public SubscriptionInput(final URI target, final RouteOutput route,
                             final URI subscriber, final boolean pushData) {
        this.target = target;
        this.location = URI.create(route.getLinks().getSelf().getHref());
        this.subscriber = subscriber;
        this.pushData = pushData;
    }

    protected SubscriptionInput(Builder builder) {
        super(builder);
        this.target = builder.target;
        this.location = builder.location;
        this.subscriber = builder.subscriber;
        this.pushData = builder.pushData;
    }

    /**
     * Builder for SubscriptionInputs.
     */
    public static class Builder extends NamedEntityInput.Builder<Subscription, Builder> {

        /**
         * Target of the subscription, may point to a resource, representation or artifact.
         */
        private URI target;

        /**
         * Location where updates should be pushed to.
         */
        private URI location;

        /**
         * Identifies the subscriber.
         */
        private URI subscriber;

        /**
         * Whether data should be pushed, only has impact for subscriptions on artifacts.
         */
        private boolean pushData;

        /**
         * Sets the target of the Subscription Input.
         *
         * @param target the target as a URI
         * @return Builder
         */
        public Builder target(URI target) {
            this.target = target;

            return this;
        }

        /**
         * Sets the location of the Subscription Input.
         *
         * @param location the location as a URI
         * @return Builder
         */
        public Builder location(URI location) {
            this.location = location;

            return this;
        }

        /**
         * Sets the subscriber of the Subscription Input.
         *
         * @param subscriber the subscriber as a URI
         * @return Builder
         */
        public Builder subscriber(URI subscriber) {
            this.subscriber = subscriber;

            return this;
        }

        /**
         * Set, whether the Subscription Input should push data.
         *
         * @param pushData true if data should be pushed
         * @return Builder
         */
        public Builder pushData(boolean pushData) {
            this.pushData = pushData;

            return this;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public SubscriptionInput build() throws InputValidationException {
            validate();
            return new SubscriptionInput(self());
        }

        /**
         * {@inheritDoc}
         */
        @Override
        protected Builder self() {
            return this;
        }
    }
}
