/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input;

import java.net.URI;
import java.time.ZonedDateTime;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Contract;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.InputValidationException;

/**
 * The input for creating and updating contracts.
 *
 * @author Sonia Nganfo
 */
@Data
@Getter
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@NoArgsConstructor

public class ContractInput extends NamedEntityInput<Contract> {

    /** Consumer of the contract. */
    protected URI consumer;

    /** Provider of the contract. */
    protected URI provider;

    /** Start date of the contract. */
    protected ZonedDateTime start;

    /** End date of the contract. */
    protected ZonedDateTime end;

    /**
     * Constructs a ContractInput using the supplied builder.
     *
     * @param builder the builder class.
     */
    protected ContractInput(Builder builder) {
        super(builder);
        this.consumer = builder.consumer;
        this.provider = builder.provider;
        this.title = builder.title;
        this.start = builder.start;
        this.end = builder.end;
    }

    /**
     * Constructs a ContractInput using the supplied parameters.
     *
     * @param consumer consumer of the contract
     * @param provider provider of the contract
     * @param title title of the contract
     * @param start start date of the contract
     * @param end end date of the contract
     *
     * @deprecated
     *     Please use the builder.
     */
    @Deprecated(forRemoval = true)
    public ContractInput(final URI consumer, final URI provider, final String title,
                         final ZonedDateTime start, final ZonedDateTime end) {
        this.consumer = consumer;
        this.provider = provider;
        this.title = title;
        this.start = start;
        this.end = end;
        validate();
    }

    /**
     * Constructs a ContractInput using the supplied parameters.
     *
     * @param consumer consumer of the contract
     * @param provider provider of the contract
     * @param title title of the contract
     * @param end end date of the contract
     *
     * @deprecated
     *     Please use the builder.
     */
    @Deprecated(forRemoval = true)
    public ContractInput(final URI consumer, final URI provider, final String title,
                         final ZonedDateTime end) {
        this.consumer = consumer;
        this.provider = provider;
        this.title = title;
        this.end = end;
        validate();
    }

    /**
     * Constructs a ContractInput using the supplied parameters.
     *
     * @param consumer consumer of the contract
     * @param provider provider of the contract
     * @param start start date of the contract
     * @param end end date of the contract
     *
     * @deprecated
     *     Please use the builder.
     */
    @Deprecated(forRemoval = true)
    public ContractInput(final URI consumer, final URI provider, final ZonedDateTime start,
                         final ZonedDateTime end) {
        this.consumer = consumer;
        this.provider = provider;
        this.start = start;
        this.end = end;
        validate();
    }

    /**
     * Constructs a ContractInput using the supplied parameters.
     *
     * @param consumer consumer of the contract
     * @param title title of the contract
     * @param start start date of the contract
     * @param end end date of the contract
     *
     * @deprecated
     *     Please use the builder.
     */
    @Deprecated(forRemoval = true)
    public ContractInput(final URI consumer, final String title, final ZonedDateTime start,
                         final ZonedDateTime end) {
        this.consumer = consumer;
        this.title = title;
        this.start = start;
        this.end = end;
        validate();
    }

    /**
     * Constructs a ContractInput using the supplied parameters.
     *
     * @param consumer consumer of the contract
     * @param title title of the contract
     * @param end end date of the contract
     *
     * @deprecated
     *     Please use the builder.
     */
    @Deprecated(forRemoval = true)
    public ContractInput(final URI consumer, final String title, final ZonedDateTime end) {
        this.consumer = consumer;
        this.title = title;
        this.end = end;
        validate();
    }

    /**
     * Constructs a ContractInput using the supplied parameters.
     *
     * @param consumer consumer of the contract
     * @param start start date of the contract
     * @param end end date of the contract
     *
     * @deprecated
     *     Please use the builder.
     */
    @Deprecated(forRemoval = true)
    public ContractInput(final URI consumer, final ZonedDateTime start, final ZonedDateTime end) {
        this.consumer = consumer;
        this.start = start;
        this.end = end;
        validate();
    }

    /**
     * Constructs a ContractInput using the supplied parameters.
     *
     * @param title title of the contract
     * @param start start date of the contract
     * @param end end date of the contract
     *
     * @deprecated
     *     Please use the builder.
     */
    @Deprecated(forRemoval = true)
    public ContractInput(final String title, final ZonedDateTime start, final ZonedDateTime end) {
        this.title = title;
        this.start = start;
        this.end = end;
        validate();
    }

    /**
     * Constructs a ContractInput using the supplied parameters.
     *
     * @param start start date of the contract
     * @param end end date of the contract
     *
     * @deprecated
     *     Please use the builder.
     */
    @Deprecated(forRemoval = true)
    public ContractInput(final ZonedDateTime start, final ZonedDateTime end) {
        this.start = start;
        this.end = end;
        validate();
    }

    /**
     * Constructs a ContractInput using the supplied parameters.
     *
     * @param title title of the contract
     * @param end end date of the contract
     *
     * @deprecated
     *     Please use the builder.
     */
    @Deprecated(forRemoval = true)
    public ContractInput(final String title, final ZonedDateTime end) {
        this.title = title;
        this.end = end;
        validate();
    }

    /**
     * Constructs a ContractInput using the supplied parameters.
     *
     * @param end end date of the contract
     *
     * @deprecated
     *     Please use the builder.
     */
    @Deprecated(forRemoval = true)
    public ContractInput(final ZonedDateTime end) {
        this.end = end;
        validate();
    }

    /**
     * Validates this ContractInput instance. The end date of a contract may not be null.
     *
     * @throws InputValidationException if validation fails.
     */
    private void validate() {
        if (this.end == null) {
            throw new InputValidationException("End date of contract must not be null.");
        }
    }


    /**
     * Builder for ContractInputs.
     */
    public static class Builder extends NamedEntityInput.Builder<Contract, Builder> {

        /** Consumer of the contract. */
        protected URI consumer;

        /** Provider of the contract. */
        protected URI provider;

        /** Start date of the contract. */
        protected ZonedDateTime start;

        /** End date of the contract. */
        protected ZonedDateTime end;

        /** Set {@link #consumer}. */
        public Builder consumer(URI consumer) {
            this.consumer = consumer;
            return this;
        }

        /**
         * Sets the provider.
         *
         * @param provider the provider.
         * @return the builder.
         */
        public Builder provider(URI provider) {
            this.provider = provider;

            return this;
        }

        /**
         * Sets the start date.
         *
         * @param start the start date.
         * @return the builder.
         */
        public Builder start(ZonedDateTime start) {
            this.start = start;

            return this;
        }

        /**
         * Sets the end date.
         *
         * @param end the end date.
         * @return the builder.
         */
        public Builder end(ZonedDateTime end) {
            this.end = end;
            return this;
        }

        /**
         * {@inheritDoc}
         */
        public ContractInput build() {
            validate();
            return new ContractInput(self());
        }

        /**
         * Validates the contract's end date. The contract end date must not be null and has to
         * be after the start date.
         */
        @Override
        protected void validate() throws InputValidationException {
            if (this.end == null || !this.end.isAfter(this.start)) {
                throw new InputValidationException(
                    "End time cannot be null or be before start time.");
            }
        }

        /**
         * {@inheritDoc}
         */
        @Override
        protected Builder self() {
            return this;
        }
    }
}
