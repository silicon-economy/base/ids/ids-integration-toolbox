/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input.policy;

import java.util.Objects;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.PolicyPattern;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.InputValidationException;

/**
 * The input to obtain a policy that provides duration-restricted data access. The duration the data
 * may be used for is defined in the ISO 8601 format.
 *
 * @author Malte Hellmeier, Ronja Quensel
 */
@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class DurationRestrictedPolicyInput extends PolicyInput {

    /** Duration of the policy. */
    private String duration;

    protected DurationRestrictedPolicyInput(Builder builder) {
        super(builder);
        this.duration = builder.duration;
    }

    /**
     * {@inheritDoc}
     */
    public static class Builder
        extends PolicyInput.Builder<DurationRestrictedPolicyInput, Builder> {

        /** Duration of the policy. */
        protected String duration;

        public Builder() {
            this.type = PolicyPattern.DURATION_USAGE;
        }

        /**
         * Sets the duration for the policy input.
         *
         * @param duration the duration.
         * @return the builder.
         */
        public Builder duration(String duration) {
            this.duration = duration;

            return this;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        protected Builder self() {
            return this;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        protected void validate() {
            super.validate();
            Objects.requireNonNull(this.duration);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public DurationRestrictedPolicyInput build() throws InputValidationException {
            validate();
            return new DurationRestrictedPolicyInput(self());
        }
    }
}
