/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Representation;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.InputValidationException;

/**
 * The input for creating and updating representations.
 *
 * @author Sonia Nganfo
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class RepresentationInput extends NamedEntityInput<Representation> {

    /** Media type of the representation. */
    private String mediaType;

    /** Language used by this representation. */
    private String language;

    /** Standard of the representation that governs the serialization of abstract content. */
    private String standard;

    protected RepresentationInput(Builder builder) {
        super(builder);
        this.mediaType = builder.mediaType;
        this.language = builder.language;
        this.standard = builder.standard;
    }

    /**
     * Builder for RepresentationInputs.
     */
    public static class Builder extends NamedEntityInput.Builder<Representation, Builder> {

        /** Media type of the representation. */
        private String mediaType;

        /** Language used by this representation. */
        private String language;

        /** Standard of the representation that governs the serialization of abstract content. */
        private String standard;

        /**
         * Sets the media Type of the Representation Input.
         *
         * @param mediaType the type in form a String
         * @return Builder
         */
        public Builder mediaType(String mediaType) {
            this.mediaType = mediaType;

            return this;
        }

        /**
         * Sets the language of the Representation Input.
         *
         * @param language the language as a String
         * @return Builder
         */
        public Builder language(String language) {
            this.language = language;

            return this;
        }

        /**
         * Sets the standard of the Representation Input.
         *
         * @param standard the standard as a String
         * @return Builder
         */
        public Builder standard(String standard) {
            this.standard = standard;

            return this;
        }

        @Override
        public RepresentationInput build() throws InputValidationException {
            validate();
            return new RepresentationInput(self());
        }

        @Override
        protected Builder self() {
            return this;
        }
    }
}
