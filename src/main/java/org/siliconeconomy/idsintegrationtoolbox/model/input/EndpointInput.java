/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input;

import java.net.URI;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Endpoint;

/**
 * The input for creating and updating endpoints.
 *
 * @param <T> the type of Endpoint.
 * @author Florian Zimmer
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@NoArgsConstructor
public abstract class EndpointInput<T extends Endpoint> extends EntityInput<T> {

    /**
     * The location information about the endpoint.
     */
    private String location;

    /**
     * The documentation url.
     */
    private URI docs;

    /**
     * The information about the endpoint.
     */
    private String info;

    protected <B extends EndpointInput.Builder<T, B>>
        EndpointInput(EndpointInput.Builder<T, B> builder) {
        super(builder);
        this.location = builder.location;
        this.docs = builder.docs;
        this.info = builder.info;
    }

    /**
     * Returns the type of the endpoint, which can either be GENERIC, CONNECTOR or APP.
     *
     * @return the type.
     */
    public abstract String getType();

    /**
     * Abstract builder for EndpointInputs.
     *
     * @param <T> the type of endpoint.
     * @param <B> the type of builder.
     */
    abstract static class Builder<T extends Endpoint, B extends Builder<T, B>>
        extends EntityInput.Builder<T, B> {

        /**
         * The location information about the endpoint.
         */
        private String location;

        /**
         * The documentation url.
         */
        private URI docs;

        /**
         * The information about the endpoint.
         */
        private String info;

        /**
         * Sets the location.
         *
         * @param location the location.
         * @return the builder.
         */
        public B location(String location) {
            this.location = location;
            return self();
        }

        /**
         * Sets the link to the documentation.
         *
         * @param docs the documentation URI.
         * @return the builder.
         */
        public B docs(URI docs) {
            this.docs = docs;
            return self();
        }

        /**
         * Sets the endpoint information.
         *
         * @param info the endpoint information.
         * @return the builder.
         */
        public B info(String info) {
            this.info = info;
            return self();
        }
    }
}
