/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input.policy;

import java.util.Objects;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.PolicyPattern;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.InputValidationException;

/**
 * The input to obtain a policy that provides access to the data a predefined number of times.
 * The number of allowed accesses is defined in the value field.
 *
 * @author Malte Hellmeier, Ronja Quensel
 */
@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class NumberRestrictedPolicyInput extends PolicyInput {

    /** Number of allowed data accesses. */
    private String value;

    protected NumberRestrictedPolicyInput(Builder builder) {
        super(builder);
        this.value = builder.value;
    }

    /**
     * {@inheritDoc}
     */
    public static class Builder extends PolicyInput.Builder<NumberRestrictedPolicyInput, Builder> {

        /** Value of the policy. */
        protected String value;

        public Builder() {
            this.type = PolicyPattern.N_TIMES_USAGE;
        }

        /**
         * Sets the number of allowed data accesses for the policy input.
         *
         * @param value the value.
         * @return the builder.
         */
        public Builder value(String value) {
            this.value = value;

            return this;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        protected Builder self() {
            return this;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        protected void validate() {
            super.validate();
            Objects.requireNonNull(this.value);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public NumberRestrictedPolicyInput build() throws InputValidationException {
            validate();
            return new NumberRestrictedPolicyInput(self());
        }
    }
}
