/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input.policy;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.PolicyPattern;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.InputValidationException;

/**
 * The input to obtain a policy that prohibits data access.
 *
 * @author Ronja Quensel
 */
@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class ProhibitAccessPolicyInput extends PolicyInput {

    protected ProhibitAccessPolicyInput(Builder builder) {
        super(builder);
    }

    /**
     * {@inheritDoc}
     */
    public static class Builder extends PolicyInput.Builder<ProhibitAccessPolicyInput, Builder> {

        public Builder() {
            this.type = PolicyPattern.PROHIBIT_ACCESS;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        protected Builder self() {
            return this;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public ProhibitAccessPolicyInput build() throws InputValidationException {
            validate();
            return new ProhibitAccessPolicyInput(self());
        }
    }

}
