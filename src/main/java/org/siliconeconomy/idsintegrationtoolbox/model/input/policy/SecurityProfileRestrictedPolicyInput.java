/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input.policy;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.PolicyPattern;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.InputValidationException;

/**
 * The input to obtain a policy that provides security-profile-restricted data access. Access will
 * be granted to connectors with the target security profile and connectors with a higher security
 * profile than the targeted one.
 *
 * @author Malte Hellmeier, Ronja Quensel
 */
@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class SecurityProfileRestrictedPolicyInput extends PolicyInput {

    /** The target security profile. */
    @JsonProperty("profile")
    private String securityProfile;

    protected SecurityProfileRestrictedPolicyInput(Builder builder) {
        super(builder);
        this.securityProfile = builder.securityProfile;
    }

    /**
     * {@inheritDoc}
     */
    public static class Builder
        extends PolicyInput.Builder<SecurityProfileRestrictedPolicyInput, Builder> {

        /** The target security profile. */
        protected String securityProfile;

        public Builder() {
            this.type = PolicyPattern.SECURITY_PROFILE_RESTRICTED_USAGE;
        }

        /**
         * Sets the security profile. Valid values are: idsc:BASE_SECURITY_PROFILE,
         * idsc:TRUST_SECURITY_PROFILE and idsc:TRUST_PLUS_SECURITY_PROFILE
         *
         * @param securityProfile the security profile.
         * @return the builder.
         */
        public Builder securityProfile(String securityProfile) {
            this.securityProfile = securityProfile;

            return this;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        protected Builder self() {
            return this;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        protected void validate() {
            super.validate();
            Objects.requireNonNull(this.securityProfile);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public SecurityProfileRestrictedPolicyInput build() throws InputValidationException {
            validate();
            return new SecurityProfileRestrictedPolicyInput(self());
        }
    }
}
