/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input.policy;

import java.time.ZonedDateTime;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.PolicyPattern;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.InputValidationException;

/**
 * The input to obtain a policy that provides data access during a predefined time interval with the
 * addition, that the data must be deleted afterwards.
 *
 * @author Malte Hellmeier, Ronja Quensel
 */
@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class UsageUntilDeletionPolicyInput extends IntervalPolicyInput {

    /** The deletion date of the policy. */
    @JsonProperty("date")
    private ZonedDateTime deletionDate;

    protected UsageUntilDeletionPolicyInput(Builder builder) {
        super(builder);
        this.deletionDate = builder.deletionData;
    }

    /**
     * {@inheritDoc}
     */
    public static class Builder
        extends IntervalPolicyInput.Builder<UsageUntilDeletionPolicyInput, Builder> {

        /** The deletion date of the policy. */
        private ZonedDateTime deletionData;

        public Builder() {
            this.type = PolicyPattern.USAGE_UNTIL_DELETION;
        }

        /**
         * Sets the deletion date for the policy input.
         *
         * @param deletionData the deletion date.
         * @return the builder.
         */
        public Builder deletionDate(ZonedDateTime deletionData) {
            this.deletionData = deletionData;

            return self();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        protected Builder self() {
            return this;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        protected void validate() {
            super.validate();
            Objects.requireNonNull(this.start);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public UsageUntilDeletionPolicyInput build() throws InputValidationException {
            validate();
            return new UsageUntilDeletionPolicyInput(self());
        }
    }
}
