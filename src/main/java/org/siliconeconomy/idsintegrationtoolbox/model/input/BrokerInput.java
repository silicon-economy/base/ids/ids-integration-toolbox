/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input;

import java.net.URI;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Broker;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.RegistrationStatus;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.InputValidationException;

/**
 * The input for creating and updating brokers.
 *
 * @author Steffen Biehs
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class BrokerInput extends NamedEntityInput<Broker> {

    /** The access url of the broker. */
    private URI location;

    /** The status of registration. */
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private RegistrationStatus status = RegistrationStatus.UNREGISTERED;

    protected BrokerInput(Builder builder) {
        super(builder);
        this.status = builder.registrationStatus;
        this.location = builder.location;
    }

    /**
     * Builder for BrokerInputs.
     */
    public static class Builder extends NamedEntityInput.Builder<Broker, Builder> {

        /** The access URL of the broker. */
        protected URI location;

        /** The registration status. */
        protected RegistrationStatus registrationStatus;

        /**
         * Sets the location.
         *
         * @param location the location.
         * @return the builder.
         */
        public Builder location(URI location) {
            this.location = location;

            return self();
        }

        /**
         * Sets the registration status. Should be set to true, if the connector is already
         * registered at the broker; false otherwise.
         *
         * @param registrationStatus the status.
         * @return the builder.
         */
        public Builder status(RegistrationStatus registrationStatus) {
            this.registrationStatus = registrationStatus;

            return self();
        }

        /**
         * {@inheritDoc}
         * */
        @Override
        protected Builder self() {
            return this;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public BrokerInput build() throws InputValidationException {
            validate();
            return new BrokerInput(self());
        }
    }
}
