/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input;

import java.net.MalformedURLException;
import java.net.URL;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Artifact;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.RouteOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.InputValidationException;


/**
 * The input for creating and updating artifacts.
 *
 * @author Ronja Quensel
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ArtifactInput extends NamedEntityInput<Artifact> {


    /**
     * Constructs the input for an artifact with a remote data source.
     *
     * @param builder the builder for the class.
     */
    protected ArtifactInput(Builder builder) {
        super(builder);
        this.accessUrl = builder.accessUrl;
        this.basicAuth = builder.basicAuth;
        this.apiKey = builder.apiKey;
        this.value = builder.value;
        this.automatedDownload = builder.automatedDownload;
    }

    /**
     * Constructs the input for an artifact with a remote data source.
     *
     * @param title             title of the artifact.
     * @param accessUrl         access URL of the data source, possibly of a route identifier.
     * @param basicAuth         basic auth for the data source.
     * @param apiKey            api key for the data source.
     * @param automatedDownload whether the data should be downloaded automatically.
     *
     * @deprecated
     *     Please use the builder. Will be removed in upcoming version.
     */
    @Deprecated(forRemoval = true)
    public ArtifactInput(final String title, final URL accessUrl,
                         final AuthenticationInput basicAuth, final AuthenticationInput apiKey,
                         final boolean automatedDownload) {
        this.title = title;
        this.accessUrl = accessUrl;
        this.basicAuth = basicAuth;
        this.apiKey = apiKey;
        this.automatedDownload = automatedDownload;
    }

    /**
     * Constructs the input for an artifact with a remote data source.
     *
     * @param title             title of the artifact.
     * @param route             access URL in form of a route output object.
     * @param basicAuth         basic auth for the data source.
     * @param apiKey            api key for the data source.
     * @param automatedDownload whether the data should be downloaded automatically.
     * @throws MalformedURLException in case the encapsulated self reference in the route object
     *                               is not a proper url.
     *
     * @deprecated
     *     Please use the builder. Will be removed in upcoming version.
     */
    @Deprecated(forRemoval = true)
    public ArtifactInput(final String title, final RouteOutput route,
                         final AuthenticationInput basicAuth, final AuthenticationInput apiKey,
                         final boolean automatedDownload) throws MalformedURLException {
        this.title = title;
        this.accessUrl = new URL(route.getLinks().getSelf().getHref());
        this.basicAuth = basicAuth;
        this.apiKey = apiKey;
        this.automatedDownload = automatedDownload;
    }

    /**
     * Constructs the input for an artifact with a local data source, i.e. the data will be stored
     * in the DSC's local database.
     *
     * @param title             title of the artifact.
     * @param value             the data to be stored.
     * @param automatedDownload whether the data should be downloaded automatically.
     *
     * @deprecated
     *     Please use the builder.
     */
    @Deprecated(forRemoval = true)
    public ArtifactInput(final String title, final String value, final boolean automatedDownload) {
        this.title = title;
        this.value = value;
        this.automatedDownload = automatedDownload;
    }

    /** URL of the data location. Only applicable for remote data. */
    private URL accessUrl;

    /** The username for authentication at the data location. */
    private AuthenticationInput basicAuth;

    /** The API key name used for authentication at the data location. */
    private AuthenticationInput apiKey;

    /** Data value stores in the local database. Only applicable for local data. */
    private String value;

    /** Whether the artifact should be downloaded automatically. */
    private boolean automatedDownload;

    /**
     * Builder for ArtifactInputs.
     */
    public static class Builder extends NamedEntityInput.Builder<Artifact, Builder> {

        /** URL of the data location. Only applicable for remote data. */
        URL accessUrl;

        /** The username for authentication at the data location. */
        AuthenticationInput basicAuth;

        /** The API key name used for authentication at the data location. */
        AuthenticationInput apiKey;

        /** Data value stores in the local database. Only applicable for local data. */
        String value;

        /** Whether the artifact should be downloaded automatically. */
        boolean automatedDownload;

        /**
         * Sets the access URL for a remote artifact.
         *
         * @param accessUrl the access URL.
         * @return the builder.
         */
        public Builder accessUrl(URL accessUrl) {
            this.accessUrl = accessUrl;

            return self();
        }

        /**
         * Sets the basic authentication for a remote artifact.
         *
         * @param basicAuth the basic auth input.
         * @return the builder.
         */
        public Builder basicAuth(AuthenticationInput basicAuth) {
            this.basicAuth = basicAuth;

            return self();
        }

        /**
         * Sets the API key authentication for a remote artifact.
         *
         * @param apiKey the API key auth input.
         * @return the builder.
         */
        public Builder apiKey(AuthenticationInput apiKey) {
            this.apiKey = apiKey;
            return self();
        }

        /**
         * Sets the data value for a local artifact.
         *
         * @param value the data.
         * @return the builder.
         */
        public Builder value(String value) {
            this.value = value;
            return self();
        }

        /**
         * Sets whether automated download should be enabled for the artifact. If set to true,
         * the data will be downloaded from the source on each access. If it is set to false, a
         * locally cached version may be returned on data access.
         *
         * @param automatedDownload whether automatic download should be enabled.
         * @return the builder.
         */
        public Builder automatedDownload(boolean automatedDownload) {
            this.automatedDownload = automatedDownload;
            return self();
        }

        /**
         * {@inheritDoc}
         * */
        @Override
        protected Builder self() {
            return this;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public ArtifactInput build() throws InputValidationException {
            validate();
            return new ArtifactInput(self());
        }
    }
}
