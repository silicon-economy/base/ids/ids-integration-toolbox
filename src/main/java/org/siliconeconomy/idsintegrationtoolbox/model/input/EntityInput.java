/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input;

import java.util.Map;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.AbstractEntity;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.InputValidationException;

/**
 * Abstract class for modeling the input expected by the Dataspace Connector when creating or
 * updating entities.
 *
 * @param <T> the {@link AbstractEntity} for which the input is modeled.
 * @author Ronja Quensel
 */
@Data
@EqualsAndHashCode
@ToString(callSuper = true)
@Log4j2
@NoArgsConstructor
@SuppressWarnings("squid:S2326")
public abstract class EntityInput<T extends AbstractEntity> {

    /** Additional properties. */
    protected Map<String, String> additional;

    protected <B extends Builder<T, B>> EntityInput(Builder<T, B> builder) {
        this.additional = builder.additional;
    }

    /**
     * Abstract builder for EntityInputs.
     *
     * @param <T> the type of entity.
     * @param <B> the type of builder.
     */
    abstract static class Builder<T extends AbstractEntity, B extends Builder<T, B>> {

        /** Additional properties. */
        protected Map<String, String> additional;

        /**
         * Sets the additional properties.
         *
         * @param additional the additional properties.
         * @return the builder.
         */
        public B additional(final Map<String, String> additional) {
            this.additional = additional;
            return self();
        }

        /**
         * Returns the instance of the builder class.
         */
        protected abstract B self();

        /**
         * Builds the input class and returns a new instance.
         *
         * @return the built input class.
         *
         * @throws InputValidationException if one of the fields in the class is deemed invalid.
         */
        public abstract EntityInput<T> build() throws InputValidationException;

        /**
         * Validates the inputs to the class to be created through the Builder.
         *
         * @throws InputValidationException if one of the fields in the class is deemed invalid.
         */
        protected void validate() throws InputValidationException {
            log.debug("Default validate method called. Method returns true by default.");
        }

    }
}
