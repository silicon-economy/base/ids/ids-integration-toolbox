/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input;

import java.net.URI;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.AppStore;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.InputValidationException;

/**
 * The input for creating and updating app stores.
 *
 * @author Steffen Biehs
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class AppStoreInput extends NamedEntityInput<AppStore> {

    /**
     * App store location.
     */
    private URI location;

    protected AppStoreInput(Builder builder) {
        super(builder);
        this.location = builder.location;
    }

    /**
     * Builder for AppStoreInputs.
     */
    public static class Builder extends NamedEntityInput.Builder<AppStore, Builder> {

        /**
         * App store location.
         */
        protected URI location;

        /**
         * Sets the location.
         *
         * @param location the location.
         * @return the builder.
         */
        public Builder location(URI location) {
            this.location = location;

            return self();
        }

        /**
         * {@inheritDoc}
         * */
        @Override
        protected Builder self() {
            return this;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public AppStoreInput build() throws InputValidationException {
            validate();
            return new AppStoreInput(self());
        }
    }
}
