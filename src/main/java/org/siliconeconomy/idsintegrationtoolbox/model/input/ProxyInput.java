/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input;

import java.net.URI;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * The input for creating and updating proxies.
 *
 * @author Steffen Biehs
 */
@Data
@EqualsAndHashCode
@ToString(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class ProxyInput {

    /**
     * The location information.
     */
    private URI location;

    /**
     * The list of exclusions.
     */
    private List<String> exclusions;

    /**
     * The authentication information.
     */
    private AuthenticationInput authentication;
}
