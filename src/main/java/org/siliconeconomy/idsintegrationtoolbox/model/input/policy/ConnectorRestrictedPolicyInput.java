/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input.policy;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.PolicyPattern;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.InputValidationException;

/**
 * The input to obtain a policy that provides connector-restricted data access. The connector ID
 * that may access the data is defined in the url field.
 *
 * @author Malte Hellmeier, Ronja Quensel
 */
@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class ConnectorRestrictedPolicyInput extends UrlPolicyInput {

    protected ConnectorRestrictedPolicyInput(Builder builder) {
        super(builder);
    }

    /**
     * {@inheritDoc}
     */
    public static class Builder
        extends UrlPolicyInput.Builder<ConnectorRestrictedPolicyInput, Builder> {

        public Builder() {
            this.type = PolicyPattern.CONNECTOR_RESTRICTED_USAGE;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        protected Builder self() {
            return this;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public ConnectorRestrictedPolicyInput build() throws InputValidationException {
            validate();
            return new ConnectorRestrictedPolicyInput(self());
        }
    }
}
