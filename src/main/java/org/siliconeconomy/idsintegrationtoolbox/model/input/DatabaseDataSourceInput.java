/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.DataSource;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.DataSourceType;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.InputValidationException;

/**
 * DatabaseDataSourceInput is a subclass of {@link DataSourceInput} and represents a database
 * datasource.
 *
 * @author Florian Zimmer
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class DatabaseDataSourceInput extends DataSourceInput<DataSource> {

    /**
     * JDBC URL of the database.
     */
    private String url;

    /**
     * Name of the driver class to use for connecting to the database.
     */
    private String driverClassName;

    /**
     * Instantiates a new DatabaseDataSourceInput.
     */
    protected DatabaseDataSourceInput(Builder builder) {
        super(builder);
        this.url = builder.url;
        this.driverClassName = builder.driverClassName;
        setType(DataSourceType.DATABASE);
    }

    /**
     * Instantiates a new DatabaseDataSourceInput.
     *
     * @deprecated use the builder.
     */
    @Deprecated(forRemoval = true)
    public DatabaseDataSourceInput() {
        setType(DataSourceType.DATABASE);
    }

    /**
     * Instantiates a new DatabaseDataSourceInput with the given parameters.
     *
     * @param url the url.
     * @param driverClassName name of the driver class.
     *
     * @deprecated use the builder.
     */
    @Deprecated(forRemoval = true)
    public DatabaseDataSourceInput(String url, String driverClassName) {
        setType(DataSourceType.DATABASE);
        this.url = url;
        this.driverClassName = driverClassName;
    }

    /**
     * Builder for DatabaseDataSourceInputs.
     */
    public static class Builder extends DataSourceInput.Builder<DataSource, Builder> {

        /**
         * JDBC URL of the database.
         */
        private String url;

        /**
         * Name of the driver class to use for connecting to the database.
         */
        private String driverClassName;

        /**
         * Sets the URL of the Database Data Source Input.
         *
         * @param url the URL as a String
         * @return Builder
         */
        public Builder url(String url) {
            this.url = url;

            return this;
        }

        /**
         * Sets the driver class name of the Database Data Source Input.
         *
         * @param driverClassName the driver class Name as a String
         * @return Builder
         */
        public Builder driverClassName(String driverClassName) {
            this.driverClassName = driverClassName;

            return this;
        }

        @Override
        protected Builder self() {
            return this;
        }

        @Override
        public DatabaseDataSourceInput build() throws InputValidationException {
            validate();
            return new DatabaseDataSourceInput(self());
        }
    }
}
