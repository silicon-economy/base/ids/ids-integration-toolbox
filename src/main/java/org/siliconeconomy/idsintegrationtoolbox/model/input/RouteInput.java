/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.DeployMethod;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Route;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.InputValidationException;

/**
 * The input for creating and updating routes.
 *
 * @author Steffen Biehs
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class RouteInput extends NamedEntityInput<Route> {

    /**
     * The route configuration.
     */
    private String configuration;

    protected RouteInput(Builder builder) {
        super(builder);
        this.configuration = builder.configuration;
    }

    /**
     * The deployment method of the route.
     */
    private DeployMethod deploy = DeployMethod.NONE;

    /**
     * Builder for RouteInputs.
     */
    public static class Builder extends NamedEntityInput.Builder<Route, Builder> {

        /** The route configuration. */
        protected String configuration;

        /**
         * Sets the configuration.
         *
         * @param configuration the configuration.
         * @return the builder.
         */
        public Builder configuration(String configuration) {
            this.configuration = configuration;
            return self();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        protected Builder self() {
            return this;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public RouteInput build() throws InputValidationException {
            validate();
            return new RouteInput(self());
        }
    }
}
