/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.DataSource;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.DataSourceType;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.InputValidationException;

/**
 * RestDataSourceInput is a subclass of {@link DataSourceInput} and represents a RESTful datasource.
 *
 * @author Florian Zimmer
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class RestDataSourceInput extends DataSourceInput<DataSource> {

    /**
     * Instantiates a new RestDataSourceInput.
     *
     * @deprecated use the builder
     */
    @Deprecated(forRemoval = true)
    public RestDataSourceInput() {
        setType(DataSourceType.REST);
    }

    protected RestDataSourceInput(Builder builder) {
        super(builder);
        setType(DataSourceType.REST);
    }

    /**
     * Builder for RestDataSourceInputs.
     */
    public static class Builder extends DataSourceInput.Builder<DataSource, Builder> {

        /**
         * {@inheritDoc}
         */
        @Override
        protected Builder self() {
            return this;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public RestDataSourceInput build() throws InputValidationException {
            validate();
            return new RestDataSourceInput(self());
        }
    }
}
