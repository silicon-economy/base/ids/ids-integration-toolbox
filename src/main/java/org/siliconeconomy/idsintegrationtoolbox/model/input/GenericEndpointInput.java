/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Endpoint;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.InputValidationException;

/**
 * The input for creating and updating generic endpoints.
 *
 * @author Florian Zimmer
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class GenericEndpointInput extends EndpointInput<Endpoint> {

    /** The endpoint type; GENERIC for all GenericEndpoints. */
    @JsonProperty("type")
    private static final String TYPE = "GENERIC";

    protected GenericEndpointInput(Builder builder) {
        super(builder);
    }

    /**
     * Returns the type.
     *
     * @return the type.
     */
    @Override
    public String getType() {
        return TYPE;
    }

    /**
     * Builder Pattern inner class.
     */
    public static class Builder extends EndpointInput.Builder<Endpoint, Builder> {

        @Override
        protected Builder self() {
            return this;
        }

        @Override
        public GenericEndpointInput build() throws InputValidationException {
            validate();
            return new GenericEndpointInput(self());
        }
    }
}
