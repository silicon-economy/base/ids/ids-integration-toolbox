/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.ContractRule;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.InputValidationException;

/**
 * The input for creating and updating rules.
 *
 * @author Sonia Nganfo
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class RuleInput extends NamedEntityInput<ContractRule> {

    /** Information Model representation of the rule (RDF). */
    private String value;

    protected RuleInput(Builder builder) {
        super(builder);
        this.value = builder.value;
    }

    /**
     * Builder for RuleInputs.
     */
    public static class Builder extends NamedEntityInput.Builder<ContractRule, Builder> {

        /** Information Model representation of the rule (RDF). */
        protected String value;

        /**
         * Sets the value for the rule. The value must be a {@link de.fraunhofer.iais.eis.Rule} in
         * JSON-LD format.
         *
         * @param value the location.
         * @return the builder.
         */
        public Builder value(String value) {
            this.value = value;

            return self();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        protected Builder self() {
            return this;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public RuleInput build() throws InputValidationException {
            validate();
            return new RuleInput(self());
        }
    }
}
