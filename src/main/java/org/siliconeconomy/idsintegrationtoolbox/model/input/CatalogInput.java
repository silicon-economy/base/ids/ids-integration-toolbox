/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Catalog;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.InputValidationException;

/**
 * The input for creating and updating catalogs.
 *
 * @author Sonia Nganfo
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@NoArgsConstructor
public class CatalogInput extends NamedEntityInput<Catalog> {

    /**
     * Constructs the input for a catalog from a builder.
     *
     * @param builder the builder.
     */
    protected CatalogInput(Builder builder) {
        super(builder);
    }

    /**
     * {@inheritDoc}
     */
    public static class Builder extends NamedEntityInput.Builder<Catalog, Builder> {

        /**
         * {@inheritDoc}
         */
        @Override
        public CatalogInput build() throws InputValidationException {
            validate();
            return new CatalogInput(this);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        protected Builder self() {
            return this;
        }
    }

}
