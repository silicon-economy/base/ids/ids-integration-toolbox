/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input.policy;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.PolicyPattern;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.InputValidationException;

/**
 * The input to obtain a policy that provides data access under the condition that a notification is
 * sent each time the data is accessed. Where the notification should be sent to is defined in the
 * url field.
 *
 * @author Malte Hellmeier, Ronja Quensel
 */
@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class NotificationPolicyInput extends UrlPolicyInput {

    protected NotificationPolicyInput(Builder builder) {
        super(builder);
    }

    /**
     * {@inheritDoc}
     */
    public static class Builder extends UrlPolicyInput.Builder<NotificationPolicyInput, Builder> {

        public Builder() {
            this.type = PolicyPattern.USAGE_NOTIFICATION;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        protected Builder self() {
            return this;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public NotificationPolicyInput build() throws InputValidationException {
            validate();
            return new NotificationPolicyInput(self());
        }
    }
}
