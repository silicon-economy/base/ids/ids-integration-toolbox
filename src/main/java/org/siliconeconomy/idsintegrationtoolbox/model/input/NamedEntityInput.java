/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.AbstractEntity;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.NamedEntity;

/**
 * Super class for {@link EntityInput}s that have a title and a description field.
 *
 * @param <T> the {@link AbstractEntity} for which the input modeled.
 * @author Ronja Quensel
 */
@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public abstract class NamedEntityInput<T extends NamedEntity> extends EntityInput<T> {

    /**
     * The title of the entity.
     */
    protected String title;

    /**
     * The description of the entity.
     */
    protected String description;

    protected <B extends NamedEntityInput.Builder<T, B>>
        NamedEntityInput(NamedEntityInput.Builder<T, B> builder) {
        super(builder);
        this.title = builder.title;
        this.description = builder.description;
    }

    /**
     * Abstract builder for NamedEntityInputs.
     *
     * @param <T> the type of entity.
     * @param <B> the type of builder.
     */
    abstract static class Builder<T extends NamedEntity, B extends Builder<T, B>>
        extends EntityInput.Builder<T, B> {

        /**
         * The title of the entity.
         */
        String title;

        /**
         * The description of the entity.
         */
        String description;

        /** Set {@link #title}. */
        public B title(String title) {
            this.title = title;
            return self();
        }

        /** Set {@link #description}. */
        public B description(String description) {
            this.description = description;
            return self();
        }
    }
}
