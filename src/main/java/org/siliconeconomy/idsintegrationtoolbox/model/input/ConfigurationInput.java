/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input;

import java.net.URI;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Configuration;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.ConnectorStatus;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.DeployMode;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.LogLevel;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.SecurityProfile;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.InputValidationException;

/**
 * The input for creating and updating configurations.
 *
 * @author Florian Zimmer
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class ConfigurationInput extends NamedEntityInput<Configuration> {

    /**
     * The keystore.
     */
    private KeystoreInput keystore;

    /**
     * The list of inbound model version.
     */
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private List<String> inboundModelVersion;

    /**
     * The id of the connector.
     */
    private URI connectorId;

    /**
     * The maintainer.
     */
    private URI maintainer;

    /**
     * The curator.
     */
    private URI curator;

    /**
     * The outbound model version.
     */
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String outboundModelVersion;

    /**
     * The project version.
     */
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String version;

    /**
     * The security profile.
     */
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private SecurityProfile securityProfile;

    /**
     * The log level.
     */
    private LogLevel logLevel;

    /**
     * The deployment mode of the connector.
     */
    private DeployMode deployMode;

    /**
     * The status of the connector.
     */
    private ConnectorStatus status;

    /**
     * The truststore.
     */
    private TruststoreInput truststore;

    /**
     * The access url of the connector.
     */
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private URI defaultEndpoint;

    /**
     * The proxy.
     */
    private ProxyInput proxy;


    protected ConfigurationInput(Builder builder) {
        super(builder);
        this.connectorId = builder.connectorId;
        this.defaultEndpoint = builder.defaultEndpoint;
        this.version = builder.version;
        this.curator = builder.curator;
        this.maintainer = builder.maintainer;
        this.inboundModelVersion = builder.inboundModelVersion;
        this.outboundModelVersion = builder.outboundModelVersion;
        this.securityProfile = builder.securityProfile;
        this.logLevel = builder.logLevel;
        this.status = builder.status;
        this.deployMode = builder.deployMode;
        this.truststore = builder.truststore;
        this.proxy = builder.proxy;
        this.keystore = builder.keystore;
    }

    /**
     * Builder for ConfigurationInputs.
     */
    public static class Builder extends NamedEntityInput.Builder<Configuration, Builder> {

        /**
         * The id of the connector.
         */
        private URI connectorId;

        /**
         * The access url of the connector.
         */
        @JsonProperty(access = JsonProperty.Access.READ_ONLY)
        private URI defaultEndpoint;

        /**
         * The project version.
         */
        @JsonProperty(access = JsonProperty.Access.READ_ONLY)
        private String version;

        /**
         * The curator.
         */
        private URI curator;

        /**
         * The maintainer.
         */
        private URI maintainer;

        /**
         * The list of inbound model version.
         */
        @JsonProperty(access = JsonProperty.Access.READ_ONLY)
        private List<String> inboundModelVersion;

        /**
         * The outbound model version.
         */
        @JsonProperty(access = JsonProperty.Access.READ_ONLY)
        private String outboundModelVersion;

        /**
         * The security profile.
         */
        @JsonProperty(access = JsonProperty.Access.READ_ONLY)
        private SecurityProfile securityProfile;

        /**
         * The log level.
         */
        private LogLevel logLevel;

        /**
         * The status of the connector.
         */
        private ConnectorStatus status;

        /**
         * The deployment mode of the connector.
         */
        private DeployMode deployMode;

        /**
         * The truststore.
         */
        private TruststoreInput truststore;

        /**
         * The proxy.
         */
        private ProxyInput proxy;

        /**
         * The keystore.
         */
        private KeystoreInput keystore;


        /**
         * Sets the connector ID of the Configuration Input.
         *
         * @param connectorId the URI of the connector
         * @return Builder
         */
        public Builder connectorId(URI connectorId) {
            this.connectorId = connectorId;

            return this;
        }

        /**
         * Sets the default endpoint of the Configuration Input.
         *
         * @param defaultEndpoint the URI of the default Endpoint
         * @return Builder
         */
        public Builder defaultEndpoint(URI defaultEndpoint) {
            this.defaultEndpoint = defaultEndpoint;

            return this;
        }

        /**
         * Sets the version of the Configuration Input.
         *
         * @param version the version as a String
         * @return Builder
         */
        public Builder version(String version) {
            this.version = version;

            return this;
        }

        /**
         * Sets the curator of the Configuration Input.
         *
         * @param curator the URI of the curator
         * @return Builder
         */
        public Builder curator(URI curator) {
            this.curator = curator;

            return this;
        }

        /**
         * Sets the maintainer of the Configuration Input.
         *
         * @param maintainer the URI of the maintainer
         * @return Builder
         */
        public Builder maintainer(URI maintainer) {
            this.maintainer = maintainer;

            return this;
        }

        /**
         * Sets the inbound model version of the Configuration Input.
         *
         * @param inboundModelVersion The String list of inbound model Versions
         * @return Builder
         */
        public Builder inboundModelVersion(List<String> inboundModelVersion) {
            this.inboundModelVersion = inboundModelVersion;

            return this;
        }

        /**
         * Sets the outbound model version of the Configuration Input.
         *
         * @param outboundModelVersion The outbound model version as String
         * @return Builder
         */
        public Builder outboundModelVersion(String outboundModelVersion) {
            this.outboundModelVersion = outboundModelVersion;

            return this;
        }

        /**
         * Sets the security profile of the Configuration Input.
         *
         * @param securityProfile The profile as a Security Profile
         * @return Builder
         */
        public Builder securityProfile(SecurityProfile securityProfile) {
            this.securityProfile = securityProfile;

            return this;
        }

        /**
         * Sets the log level of the Configuration Input.
         *
         * @param logLevel the level as a LogLevel
         * @return Builder
         */
        public Builder logLevel(LogLevel logLevel) {
            this.logLevel = logLevel;

            return this;
        }

        /**
         * Sets the connector status of the Configuration Input.
         *
         * @param status the status in form of a Connector Status
         * @return Builder
         */
        public Builder status(ConnectorStatus status) {
            this.status = status;

            return this;
        }

        /**
         * Sets the deployment method of the configuration Input.
         *
         * @param deployMode the mode in form of a Deploy Mode
         * @return Builder
         */
        public Builder deployMode(DeployMode deployMode) {
            this.deployMode = deployMode;

            return this;
        }

        /**
         * Sets the truststore of the Configuration Input.
         *
         * @param truststore the truststore as a Truststore Input
         * @return Builder
         */
        public Builder truststore(TruststoreInput truststore) {
            this.truststore = truststore;

            return this;
        }

        /**
         * Sets the proxy of the Configuration Input.
         *
         * @param proxy the proxy as a Proxy Input
         * @return Builder
         */
        public Builder proxy(ProxyInput proxy) {
            this.proxy = proxy;

            return this;
        }

        /**
         * Sets the keystore of the Configuration Input.
         *
         * @param keystore the keystore as a Keystore Input
         * @return Builder
         */
        public Builder keystore(KeystoreInput keystore) {
            this.keystore = keystore;

            return this;
        }

        @Override
        public ConfigurationInput build() throws InputValidationException {
            validate();
            return new ConfigurationInput(self());
        }

        @Override
        protected Builder self() {
            return this;
        }
    }
}
