/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input.policy;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.PolicyPattern;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.InputValidationException;

/**
 * The input to obtain a policy that provides data access during a predefined time interval.
 *
 * @author Malte Hellmeier, Ronja Quensel
 */
@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class UsageDuringIntervalPolicyInput extends IntervalPolicyInput {

    protected UsageDuringIntervalPolicyInput(Builder builder) {
        super(builder);
    }

    /**
     * {@inheritDoc}
     */
    public static class Builder
        extends IntervalPolicyInput.Builder<UsageDuringIntervalPolicyInput, Builder> {

        public Builder() {
            this.type = PolicyPattern.USAGE_DURING_INTERVAL;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        protected Builder self() {
            return this;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public UsageDuringIntervalPolicyInput build() throws InputValidationException {
            validate();
            return new UsageDuringIntervalPolicyInput(self());
        }
    }
}
