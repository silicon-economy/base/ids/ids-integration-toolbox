/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input;

import java.net.URI;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.OfferedResource;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.PaymentMethod;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.InputValidationException;

/**
 * The input for creating and updating offered resources.
 *
 * @author Sonia Nganfo
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class OfferedResourceInput extends NamedEntityInput<OfferedResource> {

    /** Keywords of the resource. */
    protected List<String> keywords;

    /** Publisher of the resource. */
    protected URI publisher;

    /** Language of the resource. */
    protected String language;

    /** License of the resource. */
    protected URI licence;

    /** Owner of the resource. */
    protected URI sovereign;

    /** Endpoint documentation for the backends of associated artifacts, if applicable. */
    protected URI endpointDocumentation;

    /** The payment modality. */
    protected PaymentMethod paymentMethod;

    /** A list of resource IDs pointing at sample resources. */
    protected List<URI> samples;

    protected OfferedResourceInput(Builder builder) {
        super(builder);
        this.keywords = builder.keywords;
        this.publisher = builder.publisher;
        this.language = builder.language;
        this.licence = builder.licence;
        this.sovereign = builder.sovereign;
        this.endpointDocumentation = builder.endpointDocumentation;
        this.paymentMethod = builder.paymentMethod;
        this.samples = builder.samples;
    }

    /**
     * Builder for OfferedResourceInputs.
     */
    public static class Builder extends NamedEntityInput.Builder<OfferedResource, Builder> {

        /** Keywords of the resource. */
        protected List<String> keywords;

        /** Publisher of the resource. */
        protected URI publisher;

        /** Language of the resource. */
        protected String language;

        /** License of the resource. */
        protected URI licence;

        /** Owner of the resource. */
        protected URI sovereign;

        /** Endpoint documentation for the backends of associated artifacts, if applicable. */
        protected URI endpointDocumentation;

        /** The payment modality. */
        protected PaymentMethod paymentMethod;

        /** A list of resource IDs pointing at sample resources. */
        protected List<URI> samples;

        /**
         * Sets the keywords of the Offered Resource Input.
         *
         * @param keywords a String list of keywords
         * @return Builder
         */
        public Builder keywords(List<String> keywords) {
            this.keywords = keywords;

            return this;
        }

        /**
         * Sets the publisher of the Offered Resource Input.
         *
         * @param publisher the publisher as a URI
         * @return Builder
         */
        public Builder publisher(URI publisher) {
            this.publisher = publisher;

            return this;
        }

        /**
         * Sets the language of the Offered Resource Input.
         *
         * @param language the language in form of a String
         * @return Builder
         */
        public Builder language(String language) {
            this.language = language;

            return this;
        }

        /**
         * Sets the license of the Offered Resource Input.
         *
         * @param licence the license as a URI
         * @return Builder
         */
        public Builder licence(URI licence) {
            this.licence = licence;

            return this;
        }

        /**
         * Sets the sovereign of the Offered Resource Input.
         *
         * @param sovereign the sovereign in form of a URI
         * @return Builder
         */
        public Builder sovereign(URI sovereign) {
            this.sovereign = sovereign;

            return this;
        }

        /**
         * Sets the endpoint documentation of the Offered Resource Input.
         *
         * @param endpointDocumentation the documentation as a URI
         * @return Builder
         */
        public Builder endpointDocumentation(URI endpointDocumentation) {
            this.endpointDocumentation = endpointDocumentation;

            return this;
        }

        /**
         * Sets the payment Method of the Offered Resource Input.
         *
         * @param paymentMethod the method as a Payment Method
         * @return Builder
         */
        public Builder paymentMethod(PaymentMethod paymentMethod) {
            this.paymentMethod = paymentMethod;

            return this;
        }

        /**
         * Sets the samples of the Offered Resource Input.
         *
         * @param samples a List of URI's
         * @return Builder
         */
        public Builder samples(List<URI> samples) {
            this.samples = samples;

            return this;
        }

        @Override
        public OfferedResourceInput build() throws InputValidationException {
            validate();
            return new OfferedResourceInput(self());
        }

        @Override
        protected Builder self() {
            return this;
        }
    }
}
