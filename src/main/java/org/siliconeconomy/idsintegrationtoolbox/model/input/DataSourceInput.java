/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.DataSource;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.DataSourceType;

/**
 * The input for datasource.
 *
 * @param <T> the type of DataSource.
 * @author Marc Peschke
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public abstract class DataSourceInput<T extends DataSource> extends EntityInput<T> {

    /**
     * The basic authentication for the data location.
     */
    private AuthenticationInput basicAuth;

    /**
     * The API key authentication for the data location.
     */
    private AuthenticationInput apiKey;

    /**
     * The type of the data source.
     */
    private DataSourceType type;

    protected <B extends DataSourceInput.Builder<T, B>>
        DataSourceInput(DataSourceInput.Builder<T, B> builder) {
        super(builder);
        this.basicAuth = builder.basicAuth;
        this.apiKey = builder.apiKey;
    }

    /**
     * Abstract builder for DataSourceInputs.
     *
     * @param <T> the type of entity.
     * @param <B> the type of builder.
     */
    abstract static class Builder<T extends DataSource, B extends Builder<T, B>>
        extends EntityInput.Builder<T, B> {

        /**
         * The basic authentication for the data location.
         */
        private AuthenticationInput basicAuth;

        /**
         * The API key authentication for the data location.
         */
        private AuthenticationInput apiKey;

        /**
         * Configures basic authentication for the data source.
         *
         * @param basicAuth the basic auth input.
         * @return the builder.
         */
        public B basicAuth(AuthenticationInput basicAuth) {
            this.basicAuth = basicAuth;
            return self();
        }

        /**
         * Configures API key authentication for the data source.
         *
         * @param apiKey the API key auth input.
         * @return the builder.
         */
        public B docs(AuthenticationInput apiKey) {
            this.apiKey = apiKey;
            return self();
        }
    }
}
