/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input;

import java.net.URI;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * The input for creating and updating truststores.
 *
 * @author Steffen Biehs
 */
@Data
@EqualsAndHashCode
@ToString(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class TruststoreInput {

    /**
     * The trust store.
     */
    private URI location;

    /**
     * The password of the trust store.
     */
    private String password;

    /**
     * Alias for the key store.
     */
    private String alias;
}
