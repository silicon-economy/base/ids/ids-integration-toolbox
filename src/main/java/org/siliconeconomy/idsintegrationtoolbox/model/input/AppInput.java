/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input;

import java.net.URI;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.App;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.PolicyPattern;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.InputValidationException;

/**
 * The input for creating and updating apps.
 *
 * @author Steffen Biehs
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class AppInput extends NamedEntityInput<App> {

    /***********************************************************************************************
     * Artifact attributes                                                                         *
     ***********************************************************************************************

     /**
     * Constructs the input for an artifact with a remote data source.
     *
     * @param builder the builder for the class.
     */
    protected AppInput(AppInput.Builder builder) {
        super(builder);
        this.remoteId = builder.remoteId;
        this.remoteAddress = builder.remoteAddress;
        this.value = builder.value;
        this.docs = builder.docs;
        this.envVariables = builder.envVariables;
        this.storageConfig = builder.storageConfig;
        this.supportedPolicies = builder.supportedPolicies;
        this.keywords = builder.keywords;
        this.publisher = builder.publisher;
        this.sovereign = builder.sovereign;
        this.language = builder.language;
        this.license = builder.license;
        this.endpointDocumentation = builder.endpointDocumentation;
        this.distributionService = builder.distributionService;
        this.runtimeEnvironment = builder.runtimeEnvironment;
    }

    /**
     * Some value for storing data locally.
     */
    private String value;

    /**
     * The provider's address for artifact request messages.
     */
    private URI remoteAddress;

    /**
     * The artifact id on provider side.
     */
    private URI remoteId;

    /***********************************************************************************************
     * Resource attributes                                                                         *
     ***********************************************************************************************

     /**
     * The keywords of the resource.
     */
    private List<String> keywords;

    /**
     * The owner of the resource.
     */
    private URI sovereign;

    /**
     * The publisher of the resource.
     */
    private URI publisher;

    /**
     * The license of the resource.
     */
    private URI license;

    /**
     * The language of the resource.
     */
    private String language;

    /**
     * The endpoint of the resource.
     */
    private URI endpointDocumentation;

    /***********************************************************************************************
     * App attributes                                                                              *
     ***********************************************************************************************

     /**
     * Text documentation of the data app.
     */
    private String docs;
    /**
     * Environment variables of the data app.
     */
    private String envVariables;

    /**
     * Usage policy patterns supported by the data app.
     */
    private List<PolicyPattern> supportedPolicies;

    /**
     * Storage configuration of the data app (e.g. path in the file system or volume name).
     */
    private String storageConfig;


    /***********************************************************************************************
     * Representation attributes                                                                   *
     ***********************************************************************************************

     /**
     * Distribution service, where the represented app can be downloaded.
     */
    private URI distributionService;

    /**
     * Runtime environment of a data app, e.g., software (or hardware) required to run the app.
     */
    private String runtimeEnvironment;

    /**
     * Builder for AppInputs.
     */
    public static class Builder extends NamedEntityInput.Builder<App, Builder> {

        /**
         * The artifact id on provider side.
         */
        private URI remoteId;

        /**
         * The provider's address for artifact request messages.
         */
        private URI remoteAddress;
        /**
         * Some value for storing data locally.
         */
        private String value;


        /*******************************************************************************************
         * App attributes                                                                          *
         *******************************************************************************************

         /**
         * Text documentation of the data app.
         */
        private String docs;

        /**
         * Environment variables of the data app.
         */
        private String envVariables;

        /**
         * Storage configuration of the data app (e.g. path in the file system or volume name).
         */
        private String storageConfig;

        /**
         * Usage policy patterns supported by the data app.
         */
        private List<PolicyPattern> supportedPolicies;

        /*******************************************************************************************
         * Resource attributes                                                                     *
         *******************************************************************************************

         /**
         * The keywords of the resource.
         */
        private List<String> keywords;

        /**
         * The publisher of the resource.
         */
        private URI publisher;

        /**
         * The owner of the resource.
         */
        private URI sovereign;

        /**
         * The language of the resource.
         */
        private String language;

        /**
         * The license of the resource.
         */
        private URI license;

        /**
         * The endpoint of the resource.
         */
        private URI endpointDocumentation;

        /*******************************************************************************************
         * Representation attributes                                                               *
         *******************************************************************************************

         /**
         * Distribution service, where the represented app can be downloaded.
         */
        private URI distributionService;

        /**
         * Runtime environment of a data app, e.g., software (or hardware) required to run the app.
         */
        private String runtimeEnvironment;


        /**
         * Sets the remote ID of the App Input.
         *
         * @param remoteId Uri of the remote ID
         * @return Builder
         */
        public Builder remoteId(URI remoteId) {
            this.remoteId = remoteId;

            return self();
        }

        /**
         * Sets the remote Address of the App Input.
         *
         * @param remoteAddress Uri of the remote Address
         * @return Builder
         */
        public Builder remoteAddress(URI remoteAddress) {
            this.remoteAddress = remoteAddress;

            return self();
        }

        /**
         * Sets the value of the App Input.
         *
         * @param value The value as a String
         * @return Builder
         */
        public Builder value(String value) {
            this.value = value;

            return self();
        }

        /**
         * Sets the docs of the App Input.
         *
         * @param docs The docs as a String
         * @return Builder
         */
        public Builder docs(String docs) {
            this.docs = docs;

            return self();
        }

        /**
         * Sets the environment variables of the App Input.
         *
         * @param envVariables The environment Variables as a String
         * @return Builder
         */
        public Builder envVariables(String envVariables) {
            this.envVariables = envVariables;

            return self();
        }

        /**
         * Sets the storage config of the App Input.
         *
         * @param storageConfig The storage config as a String
         * @return Builder
         */
        public Builder storageConfig(String storageConfig) {
            this.storageConfig = storageConfig;

            return self();
        }

        /**
         * Sets the supported Policies of the App Input.
         *
         * @param supportedPolicies A List of Policy Patterns
         * @return Builder
         */
        public Builder supportedPolicies(List<PolicyPattern> supportedPolicies) {
            this.supportedPolicies = supportedPolicies;

            return self();
        }

        /**
         * Sets the keywords of the App Input.
         *
         * @param keywords A String list of keywords
         * @return Builder
         */
        public Builder keywords(List<String> keywords) {
            this.keywords = keywords;

            return self();
        }

        /**
         * Sets the publisher of the App Input.
         *
         * @param publisher The URI of the publisher
         * @return Builder
         */
        public Builder publisher(URI publisher) {
            this.publisher = publisher;

            return self();
        }

        /**
         * Sets the sovereign of the App Input.
         *
         * @param sovereign The URI of the sovereign
         * @return Builder
         */
        public Builder sovereign(URI sovereign) {
            this.sovereign = sovereign;

            return self();
        }

        /**
         * Sets the language of the App Input.
         *
         * @param language The language as a String
         * @return Builder
         */
        public Builder language(String language) {
            this.language = language;

            return self();
        }

        /**
         * Sets the license of the App Input.
         *
         * @param license The license as a URI
         * @return Builder
         */
        public Builder license(URI license) {
            this.license = license;

            return self();
        }

        /**
         * Sets the endpoint documentation of the App Input.
         *
         * @param endpointDocumentation The URI of the endpoint
         * @return Builder
         */
        public Builder endpointDocumentation(URI endpointDocumentation) {
            this.endpointDocumentation = endpointDocumentation;

            return self();
        }

        /**
         * Sets the distribution Service of the App Input.
         *
         * @param distributionService The URI of the service
         * @return Builder
         */
        public Builder distributionService(URI distributionService) {
            this.distributionService = distributionService;

            return self();
        }

        /**
         * Sets the runtime environment of the App Input.
         *
         * @param runtimeEnvironment The runtime Environment as a String
         * @return Builder
         */
        public Builder runtimeEnvironment(String runtimeEnvironment) {
            this.runtimeEnvironment = runtimeEnvironment;

            return self();
        }

        @Override
        public AppInput build() throws InputValidationException {
            validate();
            return new AppInput(self());
        }

        @Override
        protected Builder self() {
            return this;
        }
    }
}
