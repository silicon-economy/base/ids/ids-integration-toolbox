/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input.policy;

import java.time.ZonedDateTime;
import java.util.Objects;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Super class for policy inputs that define a start and end date.
 *
 * @author Malte Hellmeier, Ronja Quensel
 */
@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public abstract class IntervalPolicyInput extends PolicyInput {

    /** Start date of the policy. */
    private ZonedDateTime start;

    /** End date of the policy. */
    private ZonedDateTime end;

    protected <T extends IntervalPolicyInput, B extends Builder<T, B>>
        IntervalPolicyInput(B builder) {
        super(builder);
        this.start = builder.start;
        this.end = builder.end;
    }

    /**
     * {@inheritDoc}
     *
     * @param <T> the type of policy input.
     * @param <B> The type of the child builder class.
     */
    public abstract static class Builder<T extends IntervalPolicyInput, B extends Builder<T, B>>
        extends PolicyInput.Builder<T, B> {

        /** Start date of the policy. */
        protected ZonedDateTime start;

        /** End date of the policy. */
        protected ZonedDateTime end;

        /**
         * Sets the start date for the policy input.
         *
         * @param start the start date.
         * @return the builder.
         */
        public B start(ZonedDateTime start) {
            this.start = start;

            return self();
        }

        /**
         * Sets the end date for the policy input.
         *
         * @param end the end date.
         * @return the builder.
         */
        public B end(ZonedDateTime end) {
            this.end = end;

            return self();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        protected void validate() {
            super.validate();
            Objects.requireNonNull(this.start);
            Objects.requireNonNull(this.end);
        }
    }
}
