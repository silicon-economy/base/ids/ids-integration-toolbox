/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input.policy;

import java.net.URI;
import java.util.Objects;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Super class for policy inputs that define a URL.
 *
 * @author Ronja Quensel
 */
@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public abstract class UrlPolicyInput extends PolicyInput {

    /** The URL of the policy. */
    private URI url;

    protected <T extends UrlPolicyInput, B extends Builder<T, B>> UrlPolicyInput(B builder) {
        super(builder);
        this.url = builder.url;
    }

    /**
     * {@inheritDoc}
     *
     * @param <T> the type of policy input.
     * @param <B> The type of the child builder class.
     */
    public abstract static class Builder<T extends UrlPolicyInput, B extends Builder<T, B>>
        extends PolicyInput.Builder<T, B> {

        /** URL of the policy. */
        protected URI url;

        /**
         * Sets the URL for the policy input.
         *
         * @param url the URL.
         * @return the builder.
         */
        public B url(URI url) {
            this.url = url;

            return self();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        protected void validate() {
            super.validate();
            Objects.requireNonNull(this.url);
        }
    }
}
