/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.input.policy;

import java.util.Objects;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.PolicyPattern;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.InputValidationException;

/**
 * Super class for inputs used to obtain an example policy from the DSC.
 *
 * @author Malte Hellmeier, Ronja Quensel
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode
public abstract class PolicyInput {

    /** The title for the policy. */
    protected String title;

    /** The description for the policy. */
    protected String description;

    /** The policy type. */
    private PolicyPattern type;

    protected <T extends PolicyInput, B extends Builder<T, B>> PolicyInput(Builder<T, B> builder) {
        this.title = builder.title;
        this.description = builder.description;
        this.type = builder.type;
    }

    /**
     * Builder class for policy inputs.
     *
     * @param <T> the type of policy input.
     * @param <B> The type of the child builder class.
     */
    public abstract static class Builder<T extends PolicyInput, B extends Builder<T, B>> {

        /** The title for the policy. */
        protected String title;

        /** The description for the policy. */
        protected String description;

        /** Policy type. */
        protected PolicyPattern type;

        /**
         * Sets the title for the policy input.
         *
         * @param title the title.
         * @return the builder.
         */
        public B title(String title) {
            this.title = title;
            return self();
        }

        /**
         * Sets the description for the policy input.
         *
         * @param description the description.
         * @return the builder.
         */
        public B description(String description) {
            this.description = description;
            return self();
        }

        /**
         * Builds the input class and returns a new instance.
         *
         * @return the input.
         * @throws InputValidationException if one of the fields in the class is deemed invalid.
         */
        public abstract T build() throws InputValidationException;

        /**
         * Returns the instance of the builder class.
         */
        protected abstract B self();

        /**
         * Validates the input to be created through the builder.
         *
         * @throws InputValidationException if one of the fields in the class is deemed invalid.
         */
        protected void validate() throws InputValidationException {
            Objects.requireNonNull(this.type);
        }
    }
}
