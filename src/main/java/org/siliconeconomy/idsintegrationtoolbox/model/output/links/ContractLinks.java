/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.links;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Contract;

/**
 * Models the collection of links that is present for every contract that is returned.
 *
 * @author Florian Zimmer
 */
@Getter
@NoArgsConstructor
@ToString(callSuper = true)
public class ContractLinks implements Links<Contract> {

    /** The self reference. */
    @Getter
    private Link self;

    /** The reference to associated rules. */
    private Link rules;

    /** The reference to associated offered resources. */
    private Link offers;

    /** The reference to associated requested resources. */
    private Link requests;

}
