/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.relation;

import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.OfferedResource;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Representation;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.OfferedResourceEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.RepresentationLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.OfferedResourceOutput;

/**
 * Models the response of the Dataspace Connector, when the offered resources related to a
 * representation are returned.
 *
 * @author Florian Zimmer
 */
@NoArgsConstructor
@ToString(callSuper = true)
public class RepresentationOfferedResourceRelationOutput
        extends EntityRelationOutput<Representation, RepresentationLinks,
    OfferedResource, OfferedResourceOutput, OfferedResourceEmbedded> {

}
