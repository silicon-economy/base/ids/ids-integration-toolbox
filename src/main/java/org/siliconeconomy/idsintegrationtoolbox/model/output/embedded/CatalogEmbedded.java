/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.embedded;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Catalog;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.CatalogOutput;

/**
 * Models the collection of single catalogs contained in the Dataspace Connector's responses,
 * when multiple catalogs are returned.
 *
 * @author Florian Zimmer
 */
@NoArgsConstructor
@ToString(callSuper = true)
public class CatalogEmbedded extends Embedded<Catalog, CatalogOutput> {

    /** List of catalogs. */
    @JsonProperty("catalogs")
    private List<CatalogOutput> entries;

    /**
     * Returns all entries of this embedded.
     *
     * @return the entries.
     */
    @Override
    public List<CatalogOutput> getEntries() {
        return getEntriesInternal(entries);
    }
}
