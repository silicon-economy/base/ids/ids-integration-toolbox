/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.embedded;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Agreement;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.AgreementOutput;

/**
 * Models the collection of single agreements contained in the Dataspace Connector's responses,
 * when multiple agreements are returned.
 *
 * @author Florian Zimmer
 */
@NoArgsConstructor
@ToString(callSuper = true)
public class AgreementEmbedded extends Embedded<Agreement, AgreementOutput> {

    /** List of agreements. */
    @JsonProperty("agreements")
    private List<AgreementOutput> entries;

    /**
     * Returns all entries of this embedded.
     *
     * @return the entries.
     */
    @Override
    public List<AgreementOutput> getEntries() {
        return getEntriesInternal(entries);
    }
}
