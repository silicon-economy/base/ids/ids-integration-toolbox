/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.multi;

import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.AppStore;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.AppStoreEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.AppStoreLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.AppStoreOutput;

/**
 * Models the response of the Dataspace Connector, when a collection of multiple app stores is
 * returned.
 *
 * @author Steffen Biehs
 */
@NoArgsConstructor
@ToString(callSuper = true)
public class AppStoreMultiOutput extends EntityMultiOutput<AppStore, AppStoreOutput,
    AppStoreEmbedded, AppStoreLinks> {

}
