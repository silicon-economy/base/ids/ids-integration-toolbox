/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.relation;

import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.DataSource;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Endpoint;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.DataSourceEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.EndpointLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.DataSourceOutput;

/**
 * Models the response of the Dataspace Connector, when the endpoints related to a datasource
 * are returned.
 *
 * @author Florian Zimmer
 */
@NoArgsConstructor
@ToString(callSuper = true)
public class EndpointDatasourceRelationOutput extends EntityRelationOutput<Endpoint, EndpointLinks,
    DataSource, DataSourceOutput, DataSourceEmbedded> {

}
