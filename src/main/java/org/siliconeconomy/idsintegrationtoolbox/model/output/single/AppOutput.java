/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.single;

import java.net.URI;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.App;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.PolicyPattern;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.AppLinks;

/**
 * Models the response of the Dataspace Connector, when an app rule is returned.
 *
 * @author Steffen Biehs
 */
@Getter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class AppOutput extends NamedEntityOutput<App> {

    /***********************************************************************************************
     * Artifact attributes                                                                         *
     ***********************************************************************************************

    /**
     * The provider's address for artifact request messages.
     */
    private URI remoteAddress;

    /***********************************************************************************************
     * App attributes                                                                              *
     ***********************************************************************************************

     /**
     * Text documentation of the data app.
     */
    private String docs;

    /**
     * Environment variables of the data app.
     */
    private String envVariables;

    /**
     * Storage configuration of the data app (e.g. path in the file system or volume name).
     */
    private String storageConfig;

    /**
     * Usage policy patterns supported by the data app.
     */
    private List<PolicyPattern> supportedPolicies;

    /***********************************************************************************************
     * Resource attributes                                                                         *
     ***********************************************************************************************

     /**
     * The keywords of the resource.
     */
    private List<String> keywords;

    /**
     * The publisher of the resource.
     */
    private URI publisher;

    /**
     * The owner of the resource.
     */
    private URI sovereign;

    /**
     * The language of the resource.
     */
    private String language;

    /**
     * The license of the resource.
     */
    private URI license;

    /**
     * The endpoint of the resource.
     */
    private URI endpointDocumentation;

    /***********************************************************************************************
     * Representation attributes                                                                   *
     ***********************************************************************************************

     /**
     * Distribution service, where the represented app can be downloaded.
     */
    private URI distributionService;

    /**
     * "Runtime environment of a data app, e.g., software (or hardware) required to run the app.
     */
    private String runtimeEnvironment;

    /** Contains the associated links. */
    @JsonProperty("_links")
    private AppLinks links;
}
