/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output;

import lombok.Data;

/**
 * Models the Dataspace Connector's response for endpoints requesting a status, e.g. the status for
 * policy negotiation or for ignoring unsupported patterns.
 *
 * @author Ronja Quensel
 */
@Data
public class StatusResponse {

    /** Indicates the current status. */
    private boolean status;

}
