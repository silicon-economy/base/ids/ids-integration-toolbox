/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.embedded;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Configuration;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ConfigurationOutput;

/**
 * Models the collection of single configurations contained in the Dataspace Connector's responses,
 * when multiple configurations are returned.
 *
 * @author Florian Zimmer
 */
@NoArgsConstructor
@ToString(callSuper = true)
public class ConfigurationEmbedded extends Embedded<Configuration, ConfigurationOutput> {

    /** List of configurations. */
    @JsonProperty("configurations")
    private List<ConfigurationOutput> entries;

    /**
     * Returns all entries of this embedded.
     *
     * @return the entries.
     */
    @Override
    public List<ConfigurationOutput> getEntries() {
        return getEntriesInternal(entries);
    }
}
