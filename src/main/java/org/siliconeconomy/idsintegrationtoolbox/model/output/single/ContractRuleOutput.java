/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.single;

import java.net.URI;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.ContractRule;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ContractRuleLinks;

/**
 * Models the response of the Dataspace Connector, when a single rule is returned.
 *
 * @author Florian Zimmer
 */
@Getter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class ContractRuleOutput extends NamedEntityOutput<ContractRule> {

    /** Information Model representation of the rule (RDF). */
    private String value;

    /** ID on the provider side, if applicable. Defaults to 'genesis' otherwise. */
    protected URI remoteId;

    /** Contains the associated links. */
    @JsonProperty("_links")
    private ContractRuleLinks links;

}
