/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.relation;

import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Contract;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.ContractRule;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.ContractRuleEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ContractLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ContractRuleOutput;

/**
 * Models the response of the Dataspace Connector, when the rules related to a contract
 * are returned.
 *
 * @author Florian Zimmer
 */
@NoArgsConstructor
@ToString(callSuper = true)
public class ContractContractRuleRelationOutput extends EntityRelationOutput<Contract,
    ContractLinks, ContractRule, ContractRuleOutput, ContractRuleEmbedded> {
}
