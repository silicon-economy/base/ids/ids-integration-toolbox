/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.relation;

import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Representation;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.RequestedResource;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.RepresentationEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.RequestedResourceLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.RepresentationOutput;

/**
 * Models the response of the Dataspace Connector, when the representations related to a requested
 * resource are returned.
 *
 * @author Florian Zimmer
 */
@NoArgsConstructor
@ToString(callSuper = true)
public class RequestedResourceRepresentationRelationOutput
        extends EntityRelationOutput<RequestedResource, RequestedResourceLinks,
    Representation, RepresentationOutput, RepresentationEmbedded> {

}
