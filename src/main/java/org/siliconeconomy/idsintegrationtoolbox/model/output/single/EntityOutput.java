/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.single;

import java.time.ZonedDateTime;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.AbstractEntity;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.Links;

/**
 * Models the response of the Dataspace Connector, when a single entity is returned.
 *
 * @param <T> the {@link AbstractEntity} which is returned.
 * @author Florian Zimmer
 */
@Data
@Getter
@ToString(callSuper = true)
@EqualsAndHashCode
@SuppressWarnings("squid:S2326")
public abstract class EntityOutput<T extends AbstractEntity> {

    /** The creation date. */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    protected ZonedDateTime creationDate;

    /** The last modification date. */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    protected ZonedDateTime modificationDate;

    /** Additional properties. */
    protected Map<String, String> additional;

    /**
     * Returns the links associated with the entity represented by this EntitySingleOutput.
     *
     * @return the associated links.
     */
    public abstract Links<T> getLinks();

}
