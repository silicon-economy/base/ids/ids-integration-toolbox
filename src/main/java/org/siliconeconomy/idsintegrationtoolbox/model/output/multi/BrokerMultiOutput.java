/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.multi;

import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Broker;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.BrokerEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.BrokerLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.BrokerOutput;

/**
 * Models the response of the Dataspace Connector, when a collection of multiple brokers is
 * returned.
 *
 * @author Steffen Biehs
 */
@NoArgsConstructor
@ToString(callSuper = true)
public class BrokerMultiOutput extends EntityMultiOutput<Broker, BrokerOutput,
    BrokerEmbedded, BrokerLinks> {

}
