/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
/**
 * Contains the classes used to model the links contained in the Dataspace Connector's responses.
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.links;
