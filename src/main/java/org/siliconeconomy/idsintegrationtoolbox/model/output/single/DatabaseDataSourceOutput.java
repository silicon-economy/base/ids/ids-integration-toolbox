/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.single;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.DataSourceType;

/**
 * Models the response of the Dataspace Connector, when a single database datasource is returned.
 *
 * @author Florian Zimmer
 */
@Getter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class DatabaseDataSourceOutput extends DataSourceOutput {

    public DatabaseDataSourceOutput() {
        this.type = DataSourceType.DATABASE;
    }

    /**
     * JDBC URL of the database.
     */
    private String url;

    /**
     * Name of the driver class to use for connecting to the database.
     */
    private String driverClassName;
}
