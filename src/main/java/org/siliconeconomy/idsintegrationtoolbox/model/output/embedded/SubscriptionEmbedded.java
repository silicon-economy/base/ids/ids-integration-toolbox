/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.embedded;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Subscription;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.SubscriptionOutput;

/**
 * Models the collection of single subscriptions contained in the Dataspace Connector's responses,
 * when multiple subscriptions are returned.
 *
 * @author Florian Zimmer
 */
@NoArgsConstructor
@ToString(callSuper = true)
public class SubscriptionEmbedded extends Embedded<Subscription, SubscriptionOutput> {

    /** List of subscriptions. */
    @JsonProperty("subscriptions")
    private List<SubscriptionOutput> entries;

    /**
     * Returns all entries of this embedded.
     *
     * @return the entries.
     */
    @Override
    public List<SubscriptionOutput> getEntries() {
        return getEntriesInternal(entries);
    }
}
