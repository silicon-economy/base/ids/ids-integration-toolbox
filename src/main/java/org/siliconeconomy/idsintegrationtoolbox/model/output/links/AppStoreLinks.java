/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.links;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.AppStore;

/**
 * Models the collection of links that is present for every app store that is returned.
 *
 * @author Steffen Biehs
 */
@Getter
@NoArgsConstructor
@ToString(callSuper = true)
public class AppStoreLinks implements Links<AppStore> {

    /** The self reference. */
    @Getter
    private Link self;

    /** The reference to associated apps. */
    private Link apps;
}
