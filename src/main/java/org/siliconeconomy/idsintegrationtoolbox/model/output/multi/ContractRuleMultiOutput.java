/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.multi;

import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.ContractRule;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.ContractRuleEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ContractRuleLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ContractRuleOutput;

/**
 * Models the response of the Dataspace Connector, when a collection of multiple rules is
 * returned.
 *
 * @author Florian Zimmer
 */
@NoArgsConstructor
@ToString(callSuper = true)
public class ContractRuleMultiOutput extends EntityMultiOutput<ContractRule, ContractRuleOutput,
    ContractRuleEmbedded, ContractRuleLinks> {

}
