/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.single;

import java.net.URI;
import java.util.List;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.PaymentMethod;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Resource;

/**
 * Models the response of the Dataspace Connector, when a single resource is returned. The resource
 * can either be an offered or a requested resource.
 *
 * @param <T> the {@link Resource} which is returned.
 * @author Ronja Quensel
 */
@Getter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public abstract class ResourceOutput<T extends Resource> extends NamedEntityOutput<T> {

    /** Keywords of the resource. */
    protected List<String> keywords;

    /** Publisher of the resource. */
    protected URI publisher;

    /** Language of the resource. */
    protected String language;

    /** License of the resource. */
    protected URI license;

    /** Version of the resource. */
    protected long version;

    /** Owner of the resource. */
    protected URI sovereign;

    /** Endpoint documentation for the backends of associated artifacts, if applicable. */
    protected URI endpointDocumentation;

    /** The payment modality. */
    protected PaymentMethod paymentModality;

    /** A list of resource IDs pointing at sample resources. */
    protected List<URI> samples;

}
