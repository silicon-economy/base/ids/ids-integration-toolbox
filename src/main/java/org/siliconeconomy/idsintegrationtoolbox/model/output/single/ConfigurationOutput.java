/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.single;

import java.net.URI;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.fraunhofer.iais.eis.ConnectorStatus;
import de.fraunhofer.iais.eis.SecurityProfile;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Configuration;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.DeployMode;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.LogLevel;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ConfigurationLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.property.KeystoreOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.property.ProxyOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.property.TruststoreOutput;

/**
 * Models the response of the Dataspace Connector, when a single configuration is returned.
 *
 * @author Florian Zimmer
 */
@Getter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class ConfigurationOutput extends NamedEntityOutput<Configuration> {

    /**
     * The id of the connector.
     */
    private URI connectorId;

    /**
     * The access url of the connector.
     */
    private URI defaultEndpoint;

    /**
     * The project version.
     */
    private String version;

    /**
     * The curator.
     */
    private URI curator;

    /**
     * The maintainer.
     */
    private URI maintainer;

    /**
     * The list of inbound model version.
     */
    private List<String> inboundModelVersion;

    /**
     * The outbound model version.
     */
    private String outboundModelVersion;

    /**
     * The security profile.
     */
    private SecurityProfile securityProfile;

    /**
     * The connector status.
     */
    private ConnectorStatus status;

    /**
     * The log level.
     */
    private LogLevel logLevel;

    /**
     * The deploy mode of the connector.
     */
    private DeployMode deployMode;

    /**
     * The proxy configuration.
     */
    private ProxyOutput proxy;

    /**
     * The trust store.
     */
    private TruststoreOutput trustStore;

    /**
     * The key store.
     */
    private KeystoreOutput keyStore;

    /** Contains the associated links. */
    @JsonProperty("_links")
    private ConfigurationLinks links;

}
