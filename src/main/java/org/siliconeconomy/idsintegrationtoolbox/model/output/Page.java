/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Models the page information contained in the Dataspace Connector's responses, when multiple
 * entities are returned.
 *
 * @author Florian Zimmer
 */
@Getter
@NoArgsConstructor
@ToString
public class Page {

    /** The page size. */
    private int size;

    /** Total number of elements. */
    private int totalElements;

    /** Total number of pages. */
    private int totalPages;

    /** The returned page number. */
    private int number;

}
