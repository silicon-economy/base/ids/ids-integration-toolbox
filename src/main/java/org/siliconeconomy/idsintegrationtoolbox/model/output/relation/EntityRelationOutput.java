/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.relation;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.AbstractEntity;
import org.siliconeconomy.idsintegrationtoolbox.model.output.Page;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.Embedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.Links;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.EntityOutput;

/**
 * Models the response of the Dataspace Connector, when a relation information is returned.
 *
 * @param <T> the entity that owns the relation.
 * @param <L> links type for the owning entity.
 * @param <D> the related entity.
 * @param <O> the output type for the related entity.
 * @param <E> the embedded type for the related entity.
 * @author Florian Zimmer
 */
@Getter
@ToString
public abstract class EntityRelationOutput<T extends AbstractEntity, L extends Links<T>,
    D extends AbstractEntity, O extends EntityOutput<D>, E extends Embedded<D, O>> {

    /** Holds pagination information for the response. */
    protected Page page;

    /** Contains the embedded entries. */
    @JsonProperty("_embedded")
    @Getter
    private E embedded;

    /** Contains the associated links. */
    @JsonProperty("_links")
    @Getter
    private L links;

}
