/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.multi;

import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Subscription;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.SubscriptionEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.SubscriptionLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.SubscriptionOutput;

/**
 * Models the response of the Dataspace Connector, when a collection of multiple subscriptions is
 * returned.
 *
 * @author Florian Zimmer
 */
@NoArgsConstructor
@ToString(callSuper = true)
public class SubscriptionMultiOutput extends EntityMultiOutput<Subscription, SubscriptionOutput,
    SubscriptionEmbedded, SubscriptionLinks> {

}
