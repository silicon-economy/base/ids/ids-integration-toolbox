/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.embedded;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.App;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.AppOutput;

/**
 * Models the collection of single apps contained in the Dataspace Connector's responses,
 * when multiple apps are returned.
 *
 * @author Steffen Biehs
 */
@NoArgsConstructor
@ToString(callSuper = true)
public class AppEmbedded extends Embedded<App, AppOutput> {

    /** List of apps. */
    @JsonProperty("apps")
    private List<AppOutput> entries;

    /**
     * Returns all entries of this embedded.
     *
     * @return the entries.
     */
    @Override
    public List<AppOutput> getEntries() {
        return getEntriesInternal(entries);
    }
}
