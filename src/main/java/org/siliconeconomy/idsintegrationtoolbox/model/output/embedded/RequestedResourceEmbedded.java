/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.embedded;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.RequestedResource;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.RequestedResourceOutput;

/**
 * Models the collection of single requested resources contained in the Dataspace Connector's
 * responses, when multiple requested resources are returned.
 *
 * @author Florian Zimmer
 */
@NoArgsConstructor
@ToString(callSuper = true)
public class RequestedResourceEmbedded extends Embedded<RequestedResource,
    RequestedResourceOutput> {

    /** List of requested resources. */
    @JsonProperty("resources")
    private List<RequestedResourceOutput> entries;

    /**
     * Returns all entries of this embedded.
     *
     * @return the entries.
     */
    @Override
    public List<RequestedResourceOutput> getEntries() {
        return getEntriesInternal(entries);
    }
}
