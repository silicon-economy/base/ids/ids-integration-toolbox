/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.relation;

import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Agreement;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Artifact;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.AgreementEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ArtifactLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.AgreementOutput;

/**
 * Models the response of the Dataspace Connector, when the agreements related to an artifact
 * are returned.
 *
 * @author Florian Zimmer
 */
@NoArgsConstructor
@ToString(callSuper = true)
public class ArtifactAgreementRelationOutput extends EntityRelationOutput<Artifact, ArtifactLinks,
    Agreement, AgreementOutput, AgreementEmbedded> {

}
