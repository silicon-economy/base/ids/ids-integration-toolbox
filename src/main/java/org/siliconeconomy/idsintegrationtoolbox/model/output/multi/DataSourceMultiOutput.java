/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.multi;

import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.DataSource;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.DataSourceEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.DataSourceLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.DataSourceOutput;

/**
 * Models the response of the Dataspace Connector, when a collection of multiple datasource is
 * returned.
 *
 * @author Marc Peschke
 */
@NoArgsConstructor
@ToString(callSuper = true)
public class DataSourceMultiOutput extends EntityMultiOutput<DataSource, DataSourceOutput,
    DataSourceEmbedded, DataSourceLinks> {

}
