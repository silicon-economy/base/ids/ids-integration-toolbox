/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.single;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Models the response of the Dataspace Connector, when a single app endpoint is returned.
 *
 * @author Florian Zimmer
 */
@Getter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class AppEndpointOutput extends EndpointOutput {

    /**
     * The type of endpoint.
     */
    @JsonProperty("type")
    private static final String TYPE = "APP";

    /**
     * Port of the Endpoint.
     */
    private int endpointPort;

    /**
     * Endpoint accepted media type.
     */
    private String mediaType;

    /**
     * Protocol used by endpoint.
     */
    private String protocol;

    /**
     * Language of the endpoint.
     */
    private String language;

    /**
     * Returns the type.
     *
     * @return the type.
     */
    @Override
    public String getType() {
        return TYPE;
    }
}
