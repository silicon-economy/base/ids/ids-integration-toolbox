/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.relation;


import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.App;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.AppStore;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.AppEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.AppStoreLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.AppOutput;

/**
 * Models the response of the Dataspace Connector, when the offered resources related to
 * an app store are returned.
 *
 * @author Steffen Biehs
 */
@NoArgsConstructor
@ToString(callSuper = true)
public class AppStoreAppRelationOutput
    extends EntityRelationOutput<AppStore, AppStoreLinks, App, AppOutput, AppEmbedded> {
}
