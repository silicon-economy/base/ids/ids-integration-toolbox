/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.multi;

import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.App;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.AppEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.AppLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.AppOutput;

/**
 * Models the response of the Dataspace Connector, when a collection of multiple apps is
 * returned.
 *
 * @author Steffen
 */
@NoArgsConstructor
@ToString(callSuper = true)
public class AppMultiOutput extends EntityMultiOutput<App, AppOutput, AppEmbedded, AppLinks> {

}
