/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.embedded;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Representation;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.RepresentationOutput;

/**
 * Models the collection of single representations descriptions contained in the Dataspace
 * Connector's responses, when multiple representations are returned.
 *
 * @author Florian Zimmer
 */
@NoArgsConstructor
@ToString(callSuper = true)
public class RepresentationEmbedded extends Embedded<Representation, RepresentationOutput> {

    /** List of representations. */
    @JsonProperty("representations")
    private List<RepresentationOutput> entries;

    /**
     * Returns all entries of this embedded.
     *
     * @return the entries.
     */
    @Override
    public List<RepresentationOutput> getEntries() {
        return getEntriesInternal(entries);
    }
}
