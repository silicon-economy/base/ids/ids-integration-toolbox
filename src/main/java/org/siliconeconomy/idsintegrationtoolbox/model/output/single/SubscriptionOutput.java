/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.single;

import java.net.URI;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Subscription;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.SubscriptionLinks;

/**
 * Models the response of the Dataspace Connector, when a single subscription is returned.
 *
 * @author Florian Zimmer
 */
@Getter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class SubscriptionOutput extends NamedEntityOutput<Subscription> {

    /**
     * The id of the resource or artifact that the subscriber subscribed to.
     */
    private URI target;

    /**
     * The URL to use when notifying the subscriber about updates to a resource.
     */
    private URI location;

    /**
     * A connector or backend system identifier.
     */
    private URI subscriber;

    /**
     * Indicates whether the subscriber wants the data to be pushed.
     */
    private boolean pushData;

    /**
     * Indicates whether the subscriber is an ids participant or not.
     */
    private boolean idsProtocol;

    /** Contains the associated links. */
    @JsonProperty("_links")
    private SubscriptionLinks links;

}
