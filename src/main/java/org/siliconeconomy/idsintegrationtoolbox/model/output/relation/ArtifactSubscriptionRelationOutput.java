/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.relation;

import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Artifact;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Subscription;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.SubscriptionEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ArtifactLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.SubscriptionOutput;

/**
 * Models the response of the Dataspace Connector, when the subscriptions related to an artifact
 * are returned.
 *
 * @author Florian Zimmer
 */
@NoArgsConstructor
@ToString(callSuper = true)
public class ArtifactSubscriptionRelationOutput extends EntityRelationOutput<Artifact,
    ArtifactLinks, Subscription, SubscriptionOutput, SubscriptionEmbedded> {
}
