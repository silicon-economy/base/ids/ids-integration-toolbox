/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.links;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Agreement;

/**
 * Models the collection of links that is present for every agreement that is returned.
 *
 * @author Florian Zimmer
 */
@Getter
@NoArgsConstructor
@ToString(callSuper = true)
public class AgreementLinks implements Links<Agreement> {

    /** The self reference. */
    @Getter
    private Link self;

    /** The reference to associated artifacts. */
    private Link artifacts;

}
