/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.embedded;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.ContractRule;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ContractRuleOutput;

/**
 * Models the collection of single rules contained in the Dataspace Connector's responses,
 * when multiple rules are returned.
 *
 * @author Florian Zimmer
 */
@NoArgsConstructor
@ToString(callSuper = true)
public class ContractRuleEmbedded extends Embedded<ContractRule, ContractRuleOutput> {

    /** List of contract rules. */
    @JsonProperty("rules")
    private List<ContractRuleOutput> entries;

    /**
     * Returns all entries of this embedded.
     *
     * @return the entries.
     */
    @Override
    public List<ContractRuleOutput> getEntries() {
        return getEntriesInternal(entries);
    }
}
