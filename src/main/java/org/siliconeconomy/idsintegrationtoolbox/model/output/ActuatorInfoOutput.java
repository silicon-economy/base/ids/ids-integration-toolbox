/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.ZonedDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * ActuatorInfoOutput encapsulates all relevant information of the actuator's info endpoint output.
 *
 * @author Florian Zimmer
 */
@Getter
@ToString
@NoArgsConstructor
public class ActuatorInfoOutput {

    /** The DSC title. */
    private String title;

    /** The DSC description. */
    private String description;

    /** The DSC version. */
    private String version;

    /** The DSC contact. */
    private Contact contact;

    /** The DSC license. */
    private License license;

    /** The DSC camel information. */
    private Camel camel;

    /** The DSC configuration. */
    private Configuration configuration;

    /** The DSC IDS information. */
    private Ids ids;

    /** The DSC update information. */
    private Update update;

    /**
     * Stores all relevant information for a DSC Contact.
     */
    @Getter
    @ToString
    @NoArgsConstructor
    public static class Contact {
        /** The contact's organization. */
        private String organization;

        /** The website of the contact. */
        private URL website;

        /** The contact's email address. */
        private String email;
    }

    /**
     * Stores all license related information.
     */
    @Getter
    @ToString
    @NoArgsConstructor
    public static class License {

        /** License name. */
        private String name;

        /** License location. */
        private String location;
    }

    /**
     * Stores all information regarding camel.
     */
    @Getter
    @ToString
    @NoArgsConstructor
    public static class Camel {

        /** The name of the camel instance. */
        private String name;

        /** The camel version. */
        private String version;

        /** The camel instance start date. */
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSz")
        protected ZonedDateTime startDate;

        /** The current camel uptime. */
        private String uptime;

        /** The camel status. */
        private String status;
    }

    /**
     * Stores all configuration information.
     */
    @Getter
    @ToString
    @NoArgsConstructor
    public static class Configuration {

        /** The configuration deploy mode. */
        private String deployMode;

        /** The current connector status. */
        private String connectorStatus;
    }

    /**
     * Stores the respective IDS information.
     */
    @Getter
    @ToString
    @NoArgsConstructor
    public static class Ids {

        /** The info model in use. */
        private InfoModel infoModel;

        /**
         * The Ids' dat stored as JsonNode since if the connector is in test mode the DAT is not
         * an object but rather a boolean, but in productive mode it's a DAT object. For the ease
         * of use please call either {@link #getDatObject()} or {@link #getDatBool()} respectively.
         */
        private JsonNode dat;

        /** The respective Ids certificate. */
        private Certificate certificate;

        /**
         * Returns the DAT property as an object, if the connector is in productive mode,
         * otherwise it will return null. For getting the DAT in test mode, please use
         * {@link #getDatBool()}.
         *
         * @return the DAT object.
         * @throws MalformedURLException if deserializing the DAT object fails.
         */
        public Dat getDatObject() throws MalformedURLException {
            if (dat.isBoolean()) {
                return null;
            } else {
                final var audience = dat.get("audience").asText();
                final var securityProfile = dat.get("securityProfile").asText();
                final var issuedAt = ZonedDateTime.parse(dat.get("issuedAt").asText());
                final var referringConnector = new URL(dat.get("referringConnector").asText());
                final var issuer = new URL(dat.get("issuer").asText());
                final var expirationDate = ZonedDateTime.parse(dat.get("expirationDate").asText());

                return new Dat(audience, securityProfile, issuedAt, referringConnector,
                    issuer, expirationDate);
            }
        }

        /**
         * Returns the DAT property as boolean, if the connector is in test mode, otherwise it
         * will return an exception. For getting the DAT in productive mode, please use
         * {@link #getDatObject()}.
         *
         * @return the DAT as boolean.
         * @throws NullPointerException if the DAT is an object, not a boolean.
         */
        public boolean getDatBool() throws NullPointerException {
            if (dat.isBoolean()) {
                return dat.asBoolean();
            } else {
                throw new NullPointerException("DAT is an object, not a boolean. Try "
                    + "'ActuatorInfoOutput$Ids.getDatObject()' instead.");
            }
        }
    }

    /**
     * Stores information about the used InfoModel.
     */
    @Getter
    @ToString
    @NoArgsConstructor
    public static class InfoModel {

        /** The info model inbound version. */
        private List<String> inboundVersion;

        /** The info model outbound version. */
        private String outboundVersion;
    }

    /**
     * Stores information about the DAT.
     */
    @Getter
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Dat {

        /** The audience. */
        private String audience;

        /** Dat's security profile. */
        private String securityProfile;

        /** The date when the DAT was issued. */
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSz")
        private ZonedDateTime issuedAt;

        /** The referring connector. */
        private URL referringConnector;

        /** The respective DAT issuer. */
        private URL issuer;

        /** DAT's expiration date. */
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSz")
        private ZonedDateTime expirationDate;
    }

    /**
     * Stores the certificate information.
     */
    @Getter
    @ToString
    @NoArgsConstructor
    public static class Certificate {

        /** The issuer date. */
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSz")
        private ZonedDateTime issuedAt;

        /** The certificate type. */
        private String type;

        /** The certificate version. */
        private int version;

        /** The certificate issuer. */
        private String issuer;

        /** The certificate expiration date. */
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSz")
        private ZonedDateTime expirationDate;

        /** The used signature algorithm name. */
        private String sigAlgName;
    }

    /**
     * Stores the respective Update information.
     */
    @Getter
    @ToString
    @NoArgsConstructor
    public static class Update {

        /** Whether or not an update is available. */
        private boolean available;

        /** The update location. */
        private URL location;

        /** The update type. */
        private String type;

        /** The update version. */
        private String version;
    }
}
