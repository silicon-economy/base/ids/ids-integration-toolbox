/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.single;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.AbstractEntity;

/**
 * Super class for {@link EntityOutput}s that have a title and a description field.
 *
 * @param <T> the {@link AbstractEntity} which is returned.
 * @author Ronja Quensel
 */
@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public abstract class NamedEntityOutput<T extends AbstractEntity>
    extends EntityOutput<T> {

    /**
     * The title of the entity.
     */
    protected String title;

    /**
     * The description of the entity.
     */
    protected String description;

}
