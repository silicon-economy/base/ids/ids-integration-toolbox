/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.links;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Route;

/**
 * Models the collection of links that is present for every route that is returned.
 *
 * @author Steffen Biehs
 */
@Getter
@NoArgsConstructor
@ToString(callSuper = true)
public class RouteLinks implements Links<Route> {

    /**
     * The self reference.
     */
    @Getter
    private Link self;

    /**
     * The reference to associated routes.
     */
    private Link routes;

    /**
     * The reference to the associated artifact, if any.
     */
    private Link output;
}
