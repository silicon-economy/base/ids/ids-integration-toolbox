/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.multi;

import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.OfferedResource;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.OfferedResourceEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.OfferedResourceLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.OfferedResourceOutput;

/**
 * Models the response of the Dataspace Connector, when a collection of multiple offered resources
 * is returned.
 *
 * @author Florian Zimmer
 */
@NoArgsConstructor
@ToString(callSuper = true)
public class OfferedResourceMultiOutput extends EntityMultiOutput<OfferedResource,
    OfferedResourceOutput, OfferedResourceEmbedded, OfferedResourceLinks> {

}
