/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Data object that holds data returned from the DSC.
 *
 * @author Haydar Qarawlus
 */
@Data
@AllArgsConstructor
public class ResourceData {

    /** Mime-type of returned data. */
    private String mimeType;

    /** Returned data as byte array. */
    private byte[] rawData;

}
