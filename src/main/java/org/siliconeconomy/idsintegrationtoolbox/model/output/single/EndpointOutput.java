/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.single;

import java.net.URI;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Endpoint;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.EndpointLinks;

/**
 * Models the response of the Dataspace Connector, when a single endpoint is returned.
 *
 * @author Florian Zimmer
 */
@Getter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({@JsonSubTypes.Type(value = AppEndpointOutput.class, name = "APP"),
    @JsonSubTypes.Type(value = GenericEndpointOutput.class, name = "GENERIC")})
public abstract class EndpointOutput extends EntityOutput<Endpoint> {

    /**
     * The location information.
     */
    protected String location;

    /**
     * The documentation of the endpoint.
     */
    protected URI docs;

    /**
     * The information of the endpoint.
     */
    protected String info;

    /** Contains the associated links. */
    @JsonProperty("_links")
    protected EndpointLinks links;

    /**
     * Returns the type of the endpoint, which can either be GENERIC, CONNECTOR or APP.
     *
     * @return the type.
     */
    public abstract String getType();

}
