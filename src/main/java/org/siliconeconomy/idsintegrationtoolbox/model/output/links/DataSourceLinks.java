/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.links;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.DataSource;

/**
 * Models the collection of links that is present for every datasource that is returned.
 *
 * @author Marc Peschke
 */
@Getter
@NoArgsConstructor
@ToString(callSuper = true)
public class DataSourceLinks implements Links<DataSource> {

    /** The self reference. */
    @Getter
    private Link self;

}
