/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output;

import java.net.URI;
import java.time.ZonedDateTime;
import java.util.Base64;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Models a log entry returned by the Clearing House.
 *
 * @author Ronja Quensel
 */
@Data
public class ClearingHouseLogEntry {

    /** The context reference. */
    @JsonProperty("@context")
    private Context context;

    /** The message type. */
    @JsonProperty("@type")
    private String type;

    /** The message ID. */
    @JsonProperty("@id")
    private URI id;

    /** The message's Infomodel version. */
    @JsonProperty("ids:modelVersion")
    private String modelVersion;

    /** When the message was issued. */
    @JsonProperty("ids:issued")
    private ZonedDateTime issued;

    /** The message's issuer connector. */
    @JsonProperty("ids:issuerConnector")
    private URI issuerConnector;

    /** The message's sender agent. */
    @JsonProperty("ids:senderAgent")
    private URI senderAgent;

    /** The Base64 encoded message payload. */
    private String payload;

    /** The payload type, e.g. "application/octet-stream". */
    @JsonProperty("payload_type")
    private String payloadType;

    /**
     * Returns the decoded payload.
     *
     * @return the decoded payload.
     */
    public String getDecodedPayload() {
        final var decodedBytes = Base64.getDecoder().decode(payload);
        return new String(decodedBytes);
    }

    /**
     * Holds the Infomodel context reference.
     */
    @Data
    static class Context {
        /** Reference to https://w3id.org/idsa/core/ */
        private URI ids;

        /** Reference to https://w3id.org/idsa/code/ */
        private URI idsc;
    }

}
