/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.embedded;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.DataSource;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.DataSourceOutput;

/**
 * Models the collection of single datasources contained in the Dataspace Connector's responses,
 * when multiple datasources are returned.
 *
 * @author Marc Peschke
 */
@NoArgsConstructor
@ToString(callSuper = true)
public class DataSourceEmbedded extends Embedded<DataSource, DataSourceOutput> {

    /** List of datasources. */
    @JsonProperty("datasources")
    private List<DataSourceOutput> entries;
    /**
     * Returns all entries of this embedded.
     *
     * @return the entries.
     */

    @Override
    public List<DataSourceOutput> getEntries() {
        return getEntriesInternal(entries);
    }
}
