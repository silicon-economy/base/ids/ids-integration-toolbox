/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.single;

import java.net.URI;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.AppStore;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.AppStoreLinks;

/**
 * Models the response of the Dataspace Connector, when a single app store is returned.
 *
 * @author Steffen Biehs
 */
@Getter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class AppStoreOutput extends NamedEntityOutput<AppStore> {

    /**
     * App store location.
     */
    private URI location;

    /** Contains the associated links. */
    @JsonProperty("_links")
    @Getter
    private AppStoreLinks links;
}
