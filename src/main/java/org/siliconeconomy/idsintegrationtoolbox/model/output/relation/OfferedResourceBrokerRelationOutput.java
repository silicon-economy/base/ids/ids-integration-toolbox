/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.relation;

import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Broker;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.OfferedResource;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.BrokerEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.OfferedResourceLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.BrokerOutput;

/**
 * Models the response of the Dataspace Connector, when the brokers related to an offered
 * resource are returned.
 *
 * @author Ronja Quensel
 */
public class OfferedResourceBrokerRelationOutput
    extends EntityRelationOutput<OfferedResource, OfferedResourceLinks,
    Broker, BrokerOutput, BrokerEmbedded> {

}
