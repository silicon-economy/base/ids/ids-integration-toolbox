/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
/**
 * Contains the classes used to model the Dataspace Connector's output, when related entities are
 * returned.
 *
 * <p>
 *     The output for related entities is almost identical to the one for multiple entities. If, in
 *     a later version, both outputs are identical, this package may be deleted and the classes in
 *     the package <i>model.output.multi</i> may be used instead.
 * </p>
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.relation;
