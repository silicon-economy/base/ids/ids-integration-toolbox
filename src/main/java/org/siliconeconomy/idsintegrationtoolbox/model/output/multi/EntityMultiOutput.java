/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.multi;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.AbstractEntity;
import org.siliconeconomy.idsintegrationtoolbox.model.output.Page;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.Embedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.Links;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.EntityOutput;

/**
 * Models the response of the Dataspace Connector, when a collection of multiple entities is
 * returned.
 *
 * @param <T> the {@link AbstractEntity} of which a collection is returned.
 * @param <O> the output type for the entity.
 * @param <E> the embedded type for the related entity.
 * @param <L> links type for the entity.
 * @author Florian Zimmer
 */
@Getter
@ToString
public abstract class EntityMultiOutput<T extends AbstractEntity, O extends EntityOutput<T>,
    E extends Embedded<T, O>, L extends Links<T>> {

    /** Holds pagination information for the response. */
    protected Page page;

    /**
     * The embedded entities.
     */
    @JsonProperty("_embedded")
    public E embedded;

    /**
     * The associated links.
     */
    @JsonProperty("_links")
    public L links;
}
