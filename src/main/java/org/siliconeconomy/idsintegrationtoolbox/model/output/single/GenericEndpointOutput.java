/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.single;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Models the response of the Dataspace Connector, when a single generic endpoint is returned.
 *
 * @author Florian Zimmer
 */
@Getter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class GenericEndpointOutput extends EndpointOutput {

    /**
     * The type of endpoint.
     */
    @JsonProperty("type")
    private static final String TYPE = "GENERIC";

    /**
     * Returns the type.
     *
     * @return the type.
     */
    @Override
    public String getType() {
        return TYPE;
    }
}
