/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.embedded;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Endpoint;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.EndpointOutput;

/**
 * Models the collection of single agreements contained in the Dataspace Connector's responses,
 * when multiple endpoints are returned.
 *
 * @author Florian Zimmer
 */
@NoArgsConstructor
@ToString(callSuper = true)
public class EndpointEmbedded extends Embedded<Endpoint, EndpointOutput> {

    /** List of endpoints. */
    @JsonProperty("endpoints")
    private List<EndpointOutput> entries;

    /**
     * Returns all entries of this embedded.
     *
     * @return the entries.
     */
    @Override
    public List<EndpointOutput> getEntries() {
        return getEntriesInternal(entries);
    }
}
