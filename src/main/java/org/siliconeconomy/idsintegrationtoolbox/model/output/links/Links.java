/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.links;

import org.siliconeconomy.idsintegrationtoolbox.model.dsc.AbstractEntity;

/**
 * Models the collection of links contained in the Dataspace Connector's responses, when one or
 * multiple entities are returned.
 *
 * @param <T> the {@link AbstractEntity} for which the associated links are modeled.
 * @author Florian Zimmer
 */
@SuppressWarnings("squid:S2326")
public interface Links<T extends AbstractEntity> {

    Link getSelf();
}
