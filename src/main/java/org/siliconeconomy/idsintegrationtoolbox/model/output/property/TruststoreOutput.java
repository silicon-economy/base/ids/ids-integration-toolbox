/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.property;

import java.net.URI;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Models the response of the Dataspace Connector, when a single truststore is returned.
 *
 * @author Steffen Biehs
 */
@Getter
@ToString
@NoArgsConstructor
public class TruststoreOutput {

    /**
     * The name of the trust store.
     */
    private URI location;

    /**
     * Alias for the key store.
     */
    private String alias;
}
