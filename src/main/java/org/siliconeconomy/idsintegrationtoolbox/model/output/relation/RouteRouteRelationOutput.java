/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.relation;

import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Route;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.RouteEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.RouteLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.RouteOutput;

/**
 * Models the response of the Dataspace Connector, when the routes related to a route are
 * returned.
 *
 * @author Florian Zimmer
 */
@NoArgsConstructor
@ToString(callSuper = true)
public class RouteRouteRelationOutput extends EntityRelationOutput<Route, RouteLinks,
    Route, RouteOutput, RouteEmbedded> {

}
