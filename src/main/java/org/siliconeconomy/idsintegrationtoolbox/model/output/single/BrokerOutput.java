/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.single;

import java.net.URI;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Broker;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.RegistrationStatus;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.BrokerLinks;

/**
 * Models the response of the Dataspace Connector, when a single broker is returned.
 *
 * @author Steffen Biehs
 */
@Getter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class BrokerOutput extends NamedEntityOutput<Broker> {

    /**
     * The access url of the broker.
     */
    private URI location;

    /** Contains the associated links. */
    @JsonProperty("_links")
    private BrokerLinks links;

    /**
     * The status of registration.
     */
    private RegistrationStatus status;

}
