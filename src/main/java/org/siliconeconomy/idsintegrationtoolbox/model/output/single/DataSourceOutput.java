/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.single;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.DataSource;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.DataSourceType;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.DataSourceLinks;

/**
 * Models the response of the Dataspace Connector, when a single datasource is returned.
 *
 * @author Marc Peschke
 */
@Getter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({@JsonSubTypes.Type(value = DatabaseDataSourceOutput.class, name = "Database"),
    @JsonSubTypes.Type(value = RestDataSourceOutput.class, name = "REST")})
public abstract class DataSourceOutput extends EntityOutput<DataSource> {

    /**
     * The id of the data source.
     */
    protected UUID id;

    /**
     * The type of the data source.
     */
    protected DataSourceType type;

    /** Contains the associated links. */
    @JsonProperty("_links")
    protected DataSourceLinks links;
}






