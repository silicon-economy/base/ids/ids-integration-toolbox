/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.links;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Artifact;

/**
 * Models the collection of links that is present for every artifact that is returned.
 *
 * @author Florian Zimmer
 */
@Getter
@NoArgsConstructor
@ToString(callSuper = true)
public class ArtifactLinks implements Links<Artifact> {

    /** The self reference. */
    @Getter
    private Link self;

    /** The reference to associated data. */
    private Link data;

    /** The reference to associated representations. */
    private Link representations;

    /** The reference to associated agreements. */
    private Link agreements;

    /** The reference to associated subscriptions. */
    private Link subscriptions;

    /** The reference to the associated route.  */
    private Link route;
}
