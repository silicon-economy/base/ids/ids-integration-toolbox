/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.embedded;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.siliconeconomy.idsintegrationtoolbox.model.dsc.AbstractEntity;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.EntityOutput;

/**
 * Models the collection of single entries contained in the Dataspace Connector's responses, when
 * multiple entities are returned.
 *
 * @param <T> the {@link AbstractEntity} represented by the single entries.
 * @param <O> the output type.
 * @author Florian Zimmer
 */
public abstract class Embedded<T extends AbstractEntity, O extends EntityOutput<T>> {

    /**
     * Returns all entries of this embedded.
     *
     * @return the entries.
     */
    public abstract List<O> getEntries();

    /**
     * Maps a List of a sub-type of EntitySingleOutput to a List of EntitySingleOutput.
     * Used by sub-classes for implementing {@link #getEntries()}.
     *
     * @param entries the list of entries to map.
     * @return the mapped list.
     */
    @SuppressWarnings("squid:S1612")
    protected List<O> getEntriesInternal(
            final List<? extends EntityOutput<T>> entries) {
        if (entries == null || entries.isEmpty()) {
            return new ArrayList<>();
        }
        return entries.stream()
            .map(r -> (O) r)
            .collect(Collectors.toList());
    }
}
