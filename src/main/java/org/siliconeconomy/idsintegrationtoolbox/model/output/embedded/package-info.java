/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
/**
 * Contains the classes used to model the embedded entities, when multiple entities are returned by
 * the Dataspace Connector.
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.embedded;
