/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.embedded;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Broker;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.BrokerOutput;

/**
 * Models the collection of single brokers contained in the Dataspace Connector's responses,
 * when multiple brokers are returned.
 *
 * @author Steffen Biehs
 */
@NoArgsConstructor
@ToString(callSuper = true)
public class BrokerEmbedded extends Embedded<Broker, BrokerOutput> {

    /** List of brokers. */
    @JsonProperty("brokers")
    private List<BrokerOutput> entries;

    /**
     * Returns all entries of this embedded.
     *
     * @return the entries.
     */
    @Override
    public List<BrokerOutput> getEntries() {
        return getEntriesInternal(entries);
    }
}
