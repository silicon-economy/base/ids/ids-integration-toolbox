/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.embedded;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Artifact;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ArtifactOutput;

/**
 * Models the collection of single artifacts contained in the Dataspace Connector's responses,
 * when multiple artifacts are returned.
 *
 * @author Florian Zimmer
 */
@NoArgsConstructor
@ToString(callSuper = true)
public class ArtifactEmbedded extends Embedded<Artifact, ArtifactOutput> {

    /** List of artifacts. */
    @JsonProperty("artifacts")
    private List<ArtifactOutput> entries;

    /**
     * Returns all entries of this embedded.
     *
     * @return the entries.
     */
    @Override
    public List<ArtifactOutput> getEntries() {
        return getEntriesInternal(entries);
    }
}
