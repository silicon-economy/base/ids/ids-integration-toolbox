/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.multi;

import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Endpoint;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.EndpointEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.EndpointLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.EndpointOutput;

/**
 * Models the response of the Dataspace Connector, when a collection of multiple endpoints is
 * returned.
 *
 * @author Florian Zimmer
 */
@NoArgsConstructor
@ToString(callSuper = true)
public class EndpointMultiOutput extends EntityMultiOutput<Endpoint, EndpointOutput,
    EndpointEmbedded, EndpointLinks> {

}
