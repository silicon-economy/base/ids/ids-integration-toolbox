/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
/**
 * Contains all classes used to model the output returned by the Dataspace Connector.
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output;
