/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.property;

import java.net.URI;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Models the response of the Dataspace Connector, when a single proxy is returned.
 *
 * @author Steffen Biehs
 */
@Getter
@ToString
@NoArgsConstructor
public class ProxyOutput {

    /**
     * The location information.
     */
    private URI location;

    /**
     * The list of exclusions.
     */
    private List<String> exclusions;

    /**
     * Boolean value indicating whether auth-credentials for the proxy are present.
     */
    private boolean authenticationSet;
}
