/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.single;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.DeployMethod;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Endpoint;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Route;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.RouteLinks;

/**
 * Models the response of the Dataspace Connector, when a single route is returned.
 *
 * @author Steffen Biehs
 */
@Getter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class RouteOutput extends NamedEntityOutput<Route> {

    /**
     * The route configuration.
     */
    private String configuration;

    /**
     * The deploy method of the route.
     */
    private DeployMethod deploy;

    /**
     * The start endpoint of the route.
     */
    private Endpoint start;

    /**
     * The last endpoint of the route.
     */
    private Endpoint end;

    /** Contains the associated links. */
    @JsonProperty("_links")
    private RouteLinks links;
}
