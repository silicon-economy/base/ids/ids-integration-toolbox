/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.embedded;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.AppStore;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.AppStoreOutput;

/**
 * Models the collection of single app stores contained in the Dataspace Connector's responses,
 * when multiple app stores are returned.
 *
 * @author Steffen Biehs
 */
@NoArgsConstructor
@ToString(callSuper = true)
public class AppStoreEmbedded extends Embedded<AppStore, AppStoreOutput> {

    /** List of catalogs. */
    @JsonProperty("catalogs")
    private List<AppStoreOutput> entries;

    /**
     * Returns all entries of this embedded.
     *
     * @return the entries.
     */
    @Override
    public List<AppStoreOutput> getEntries() {
        return getEntriesInternal(entries);
    }
}
