/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.links;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Models a link as returned by the Dataspace Connector. A link consists of the actual link and
 * the information whether the link is templated.
 *
 * @author Florian Zimmer
 */
@Getter
@NoArgsConstructor
@ToString
public class Link {

    /** The actual link. */
    private String href;

    /** Whether the link is templated. */
    private boolean templated;

}
