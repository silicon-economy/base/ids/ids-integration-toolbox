/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.model.output.relation;

import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Contract;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.RequestedResource;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.RequestedResourceEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ContractLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.RequestedResourceOutput;

/**
 * Models the response of the Dataspace Connector, when the requested resources related to a
 * contract are returned.
 *
 * @author Florian Zimmer
 */
@NoArgsConstructor
@ToString(callSuper = true)
public class ContractRequestedResourceRelationOutput
    extends EntityRelationOutput<Contract, ContractLinks, RequestedResource,
    RequestedResourceOutput, RequestedResourceEmbedded> {

}
