/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import org.siliconeconomy.idsintegrationtoolbox.api.ExamplesApi;
import org.siliconeconomy.idsintegrationtoolbox.api.camel.CamelApi;
import org.siliconeconomy.idsintegrationtoolbox.api.connector.ConnectorApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.EntityApi;
import org.siliconeconomy.idsintegrationtoolbox.api.ids.IdsApi;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.WorkflowApi;
import org.springframework.stereotype.Service;

/**
 * Entrypoint to all Toolbox functionalities. All classes operating endpoints of the DSC's API
 * can be obtained through chained calls starting on this class.
 *
 * @author Ronja Quensel
 */
@Service
@RequiredArgsConstructor
@Getter
@Accessors(fluent = true)
public class DscOperator {

    private final @NonNull EntityApi entities;

    private final @NonNull IdsApi ids;

    private final @NonNull WorkflowApi workflows;

    private final @NonNull ConnectorApi connector;

    private final @NonNull CamelApi camel;

    private final @NonNull ExamplesApi examples;

}
