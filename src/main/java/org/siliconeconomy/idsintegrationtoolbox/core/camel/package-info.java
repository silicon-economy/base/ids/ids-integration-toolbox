/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
/**
 * Provides the classes for interacting with the Camel related APIs of the Dataspace Connector.
 * This includes the APIs for managing routes and beans.
 */

package org.siliconeconomy.idsintegrationtoolbox.core.camel;
