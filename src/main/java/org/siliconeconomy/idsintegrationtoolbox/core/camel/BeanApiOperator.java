/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.camel;

import lombok.Getter;
import org.siliconeconomy.idsintegrationtoolbox.api.camel.BeanApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * Operates the beans API of the Dataspace Connector, that allows adding and removing beans at
 * runtime.
 *
 * @author Ronja Quensel
 */
@Component
public class BeanApiOperator extends FileUploadApiOperator implements BeanApi {

    /** The base path of the API for managing beans. */
    @Value("${connector.api.path.beans:/api/beans}")
    @Getter
    private String baseApiPath;

    /**
     * Constructs a BeanApiOperator.
     *
     * @param restTemplate the {@link RestTemplate} for sending HTTP requests.
     */
    public BeanApiOperator(final RestTemplate restTemplate) {
        super(restTemplate);
    }

    @Override
    protected String getManagedType() {
        return "bean";
    }
}
