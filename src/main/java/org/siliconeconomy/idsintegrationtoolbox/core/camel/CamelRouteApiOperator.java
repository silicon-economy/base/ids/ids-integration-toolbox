/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.camel;

import lombok.Getter;
import org.siliconeconomy.idsintegrationtoolbox.api.camel.CamelRouteApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * Provides methods for managing the Apache Camel routes of the Dataspace Connector.
 *
 * @author Haydar Qarawlus
 */
@Component
public class CamelRouteApiOperator extends FileUploadApiOperator implements CamelRouteApi {

    /** The base path of the API for managing Camel routes. */
    @Value("${connector.api.path.camel-routes:/api/camel/routes}")
    @Getter
    private String baseApiPath;

    /**
     * Constructs a CamelRouteApiOperator.
     *
     * @param restTemplate the {@link RestTemplate} for sending HTTP requests.
     */
    public CamelRouteApiOperator(final RestTemplate restTemplate) {
        super(restTemplate);
    }

    @Override
    protected String getManagedType() {
        return "route";
    }
}
