/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.camel;

import java.io.FileNotFoundException;
import java.net.URI;
import java.util.Collections;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.idsintegrationtoolbox.api.camel.FileUploadApi;
import org.siliconeconomy.idsintegrationtoolbox.core.DscApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.utils.ErrorCodeGenerator;
import org.siliconeconomy.idsintegrationtoolbox.utils.FileUtils;
import org.siliconeconomy.idsintegrationtoolbox.utils.HttpUtils;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.InvalidInputException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * Superclass for interacting with the DSC APIs that require a file upload. This includes the APIs
 * for managing Camel routes and for managing beans.
 *
 * @author Ronja Quensel
 */
@RequiredArgsConstructor
@Log4j2
public abstract class FileUploadApiOperator extends DscApiOperator
    implements ErrorCodeGenerator, FileUploadApi {

    /** Sends HTTP requests. */
    private final @NonNull RestTemplate restTemplate;

    /**
     * {@inheritDoc}
     */
    @Override
    public void upload(final String filePath)
        throws ApiInteractionUnsuccessfulException, InvalidInputException, FileNotFoundException {

        if (!filePath.endsWith(".xml")) {
            throw new InvalidInputException("File extension is not XML");
        }

        final var headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        addAuthHeader(headers);

        final var bodyContents = new LinkedMultiValueMap<String, Object>();
        bodyContents.add("file", FileUtils.readFile(filePath));

        final var request =
            new HttpEntity<MultiValueMap<String, Object>>(bodyContents, headers);

        final var response = restTemplate
            .exchange(buildEndpointUri(), HttpMethod.POST, request, String.class);

        checkHttpCode(response.getStatusCode());

        log.info("Successfully added {}(s) from [{}] to the Camel context.",
            getManagedType(), filePath);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(final String id) throws ApiInteractionUnsuccessfulException {
        final var headers = HttpUtils.getDefaultHttpHeaders();
        addAuthHeader(headers);
        final var request = new HttpEntity<String>(headers);

        final var response = restTemplate
            .exchange(buildEndpointUri() + "/" + id, HttpMethod.DELETE, request, String.class);

        checkHttpCode(response.getStatusCode());

        log.info("Deleted {} with ID [{}].", getManagedType(), id);
    }

    /**
     * Builds a URI object from the connector base URL and the base API path.
     *
     * @return the URI object containing the connector and the camel route endpoint.
     * */
    private URI buildEndpointUri() {
        return UriComponentsBuilder
            .fromHttpUrl(connectorUrl)
            .path(getBaseApiPath())
            .build()
            .toUri();
    }

    /**
     * Returns the base API to the API operated by the respective sub class.
     *
     * @return the base API path.
     */
    protected abstract String getBaseApiPath();

    /**
     * Returns the type managed by the respective sub class. Should either be "route" or "bean".
     *
     * @return the managed type.
     */
    protected abstract String getManagedType();

}
