/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.camel;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.idsintegrationtoolbox.core.DscApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.RouteError;
import org.siliconeconomy.idsintegrationtoolbox.utils.ErrorCodeGenerator;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * Operates the route error API of the Dataspace Connector.
 *
 * @author Ronja Quensel
 */
@Component
@RequiredArgsConstructor
@Log4j2
public class RouteErrorApiOperator extends DscApiOperator implements ErrorCodeGenerator {

    /** The base path of the API for managing beans. */
    @Value("${connector.api.path.route-errors:/api/camel/routes/error}")
    private String baseApiPath;

    /** Sends HTTP requests. */
    private final @NonNull RestTemplate restTemplate;

    /** Used to map the response from JSON. */
    private final @NonNull ObjectMapper objectMapper;

    /**
     * Fetches route error descriptions from the connector. Note, that only 100 route error
     * descriptions are cached in the connector. Therefore, this method should be called
     * regularly in order to not miss any route errors.
     *
     * @return list of route error descriptions.
     * @throws ApiInteractionUnsuccessfulException if the connector returns a non 2xx status code.
     * @throws IOException if the response cannot be parsed to a list of {@link RouteError}s.
     */
    public List<RouteError> getRouteErrors()
            throws ApiInteractionUnsuccessfulException, IOException {
        final var headers = new HttpHeaders();
        addAuthHeader(headers);
        final var request = new HttpEntity<String>(headers);

        final var uri = UriComponentsBuilder
            .fromHttpUrl(connectorUrl)
            .path(baseApiPath)
            .build()
            .toUri();

        final var response = restTemplate.exchange(uri, HttpMethod.GET, request, String.class);

        checkHttpCode(response.getStatusCode());

        return objectMapper.readValue(response.getBody(), new TypeReference<>() {});
    }

}
