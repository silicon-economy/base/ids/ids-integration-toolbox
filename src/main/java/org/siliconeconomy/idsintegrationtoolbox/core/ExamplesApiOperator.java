/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core;

import java.io.IOException;

import de.fraunhofer.iais.eis.Rule;
import de.fraunhofer.iais.eis.ids.jsonld.Serializer;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.json.JSONObject;
import org.siliconeconomy.idsintegrationtoolbox.api.ExamplesApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.PolicyPattern;
import org.siliconeconomy.idsintegrationtoolbox.model.input.policy.PolicyInput;
import org.siliconeconomy.idsintegrationtoolbox.utils.LoggingUtils;
import org.siliconeconomy.idsintegrationtoolbox.utils.PolicyUtils;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestBuilder;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;

/**
 * Provides methods for accessing the endpoints of the Dataspace Connector's messages API.
 *
 * @author Florian Zimmer
 */
@Component
@RequiredArgsConstructor
@Log4j2
public class ExamplesApiOperator extends DscApiOperator implements ExamplesApi {

    /** Handles the sending of REST requests. */
    private final @NonNull RestRequestHandler requestHandler;

    /** (De)serializes Information Model objects from/to JSON-LD. */
    private final @NonNull Serializer serializer;

    /** DSC endpoint for getting an example policy. */
    @Value("${connector.api.path.examples.policy:/api/examples/policy}")
    private String examplesPolicyEndpoint;

    /** DSC endpoint for getting an policy pattern. */
    @Value("${connector.api.path.examples.validation:/api/examples/validation}")
    private String examplesValidationEndpoint;

    /**
     * {@inheritDoc}
     */
    @Override
    public Rule policy(final PolicyInput policy) throws RestClientException,
        ApiInteractionUnsuccessfulException, IOException {
        final var request = new RestRequestBuilder(HttpMethod.POST, connectorUrl,
            examplesPolicyEndpoint)
            .body(serializer.serialize(policy))
            .basicAuth(connectorUsername, connectorPassword)
            .build();
        log.debug("Prepared request to get an example policy: [{}]", request);

        final var response = requestHandler.sendRequest(request);
        log.debug(LoggingUtils.RECEIVED_RESPONSE, response);

        return serializer.deserialize(response, Rule.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PolicyPattern policyValidation(final String policy) throws RestClientException,
        ApiInteractionUnsuccessfulException {
        final var request = new RestRequestBuilder(HttpMethod.POST, connectorUrl,
            examplesValidationEndpoint)
            .body(policy)
            .basicAuth(connectorUsername, connectorPassword)
            .build();
        log.debug("Prepared request to validate a policy: [{}]", request);

        final var response =
            PolicyUtils.removePolicyPatternSurroundings(requestHandler.sendRequest(request));
        log.debug(LoggingUtils.RECEIVED_RESPONSE, response);

        return PolicyPattern.valueOf(new JSONObject(response).get("value").toString());
    }
}
