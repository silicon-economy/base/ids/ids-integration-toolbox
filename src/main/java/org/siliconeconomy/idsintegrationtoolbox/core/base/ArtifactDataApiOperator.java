/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.base;

import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.DataApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.QueryInput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.ResourceData;
import org.siliconeconomy.idsintegrationtoolbox.utils.ErrorCodeGenerator;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestBuilder;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

/**
 * Provides methods for managing the data associated with an artifact at the Dataspace Connector.
 *
 * @author Ronja Quensel
 */
@Component
@RequiredArgsConstructor
public class ArtifactDataApiOperator implements ErrorCodeGenerator, DataApi {

    /** The base path of the API for managing artifacts. */
    @Value("${connector.api.path.artifacts:/api/artifacts}")
    private String baseApiPath;

    /** ID parameter path. */
    @Value("${connector.api.path.segment.id-parameter:/{id}}")
    private String idPathSegment;

    /** The relation path segment for data. */
    @Value("${connector.api.path.segment.relation.data:/data}")
    private String dataRelationPath;

    /** URL of the wrapped connector. */
    @Value("${connector.url:http://connector:8080}")
    private String connectorUrl;

    /** Username of the wrapped connector. */
    @Value("${connector.username:admin}")
    private String connectorUsername;

    /** Password of the wrapped connector. */
    @Value("${connector.password:password}")
    private String connectorPassword;

    /** Handles the sending of REST requests. */
    private final @NonNull RestRequestHandler restRequestHandler;

    /** Used to map the input to JSON. */
    private final @NonNull ObjectMapper objectMapper;

    /**
     * {@inheritDoc}
     */
    @Override
    public void upload(final UUID id, final String data)
        throws ApiInteractionUnsuccessfulException {
        final var request = new RestRequestBuilder(HttpMethod.PUT, connectorUrl, baseApiPath,
            idPathSegment, dataRelationPath)
            .pathVariable("id", id.toString())
            .basicAuth(connectorUsername, connectorPassword)
            .header("Content-Type", "application/octet-stream")
            .body(data)
            .build();

        restRequestHandler.sendRequest(request);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ResourceData get(final UUID id, final QueryInput queryInput,
                            final List<URI> routeIds) throws IOException,
        ApiInteractionUnsuccessfulException {
        final var requestBuilder =
            new RestRequestBuilder(HttpMethod.POST, connectorUrl, baseApiPath, idPathSegment,
                dataRelationPath)
                .pathVariable("id", id.toString())
                .basicAuth(connectorUsername, connectorPassword);

        if (routeIds != null) {
            routeIds.forEach(routeId -> requestBuilder
                .queryParameter("routeIds", routeId.toString()));
        }

        if (queryInput != null) {
            requestBuilder.body(objectMapper.writeValueAsString(queryInput));
        }

        final var request = requestBuilder.build();
        return restRequestHandler.sendDataRequest(request);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ResourceData get(final UUID id, final boolean download, final URI agreement,
                            final List<URI> routeIds, final Map<String, String> params,
                            final Map<String, String> headers, final String path)
                                throws ApiInteractionUnsuccessfulException {
        RestRequestBuilder requestBuilder;

        if (path != null) {
            requestBuilder = new RestRequestBuilder(HttpMethod.GET, connectorUrl, baseApiPath,
                idPathSegment, dataRelationPath, path);
        } else {
            requestBuilder = new RestRequestBuilder(HttpMethod.GET, connectorUrl, baseApiPath,
                idPathSegment, dataRelationPath);
        }

        requestBuilder
            .pathVariable("id", id.toString())
            .basicAuth(connectorUsername, connectorPassword)
            .queryParameter("download", String.valueOf(download));

        if (routeIds != null) {
            routeIds.forEach(routeId -> requestBuilder
                .queryParameter("routeIds", routeId.toString()));
        }

        if (agreement != null) {
            requestBuilder.queryParameter("agreementUri", agreement.toString());
        }

        if (params != null) {
            params.forEach(requestBuilder::queryParameter);
        }

        if (headers != null) {
            headers.forEach(requestBuilder::header);
        }

        final var request = requestBuilder.build();
        return restRequestHandler.sendDataRequest(request);
    }

}
