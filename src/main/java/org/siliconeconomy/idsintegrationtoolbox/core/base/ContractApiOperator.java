/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.base;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.Accessors;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.ContractApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.ContractResourcesApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.ContractRulesApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Contract;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.ContractEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ContractLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.ContractMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ContractOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Provides methods for managing contracts at the Dataspace Connector.
 *
 * @author Sonia Nganfo
 */
@Component
public class ContractApiOperator
    extends BaseApiOperator<Contract, ContractOutput, ContractEmbedded, ContractLinks,
    ContractMultiOutput> implements ContractApi {

    /** The base path of the API for managing contracts. */
    @Value("${connector.api.path.contracts:/api/contracts}")
    @Getter(value = AccessLevel.PROTECTED)
    private String baseApiPath;

    @Getter
    @Accessors(fluent = true)
    private final ContractResourcesApi resources;

    @Getter
    @Accessors(fluent = true)
    private final ContractRulesApi rules;

    /**
     * Constructs a ContractsApiOperator with the required parameters.
     *
     * @param restRequestHandler handles the sending of REST requests.
     */
    @Autowired
    public ContractApiOperator(final RestRequestHandler restRequestHandler,
                               final ObjectMapper objectMapper,
                               final ContractRulesApi rules,
                               final ContractResourcesApi resources) {
        super(restRequestHandler, objectMapper);
        this.rules = rules;
        this.resources = resources;
    }

    @Override
    protected Class<ContractOutput> getEntitySingleOutputClass() {
        return ContractOutput.class;
    }

    @Override
    protected Class<ContractMultiOutput> getEntityMultiOutputClass() {
        return ContractMultiOutput.class;
    }
}
