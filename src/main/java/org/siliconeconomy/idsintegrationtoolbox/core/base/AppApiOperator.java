/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.base;

import static org.siliconeconomy.idsintegrationtoolbox.utils.LoggingUtils.NOT_ALLOWED;

import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.Accessors;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.AppApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.AppEndpointsApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.ActionType;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.App;
import org.siliconeconomy.idsintegrationtoolbox.model.input.EntityInput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.AppEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.AppLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.AppMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.AppOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.LoggingUtils;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestBuilder;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.OperationNotAllowedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

/**
 * Provides methods for managing apps at the Dataspace Connector.
 *
 * @author Steffen Biehs
 */
@Component
@Log4j2
public class AppApiOperator extends BaseApiOperator<App, AppOutput, AppEmbedded, AppLinks,
    AppMultiOutput>
    implements AppApi {

    /**
     * The base path of the API for managing apps.
     */
    @Value("${connector.api.path.apps:/api/apps}")
    @Getter(value = AccessLevel.PROTECTED)
    private String baseApiPath;

    @Getter
    @Accessors(fluent = true)
    private final AppEndpointsApi endpoints;

    /**
     * The path for applying actions against apps.
     */
    @Value("${connector.api.path.segment.app.actions:/actions}")
    private String actionsPath;

    /**
     * The path for fetching an appstore.
     */
    @Value("${connector.api.path.segment.app.appstore:/appstore}")
    private String appstorePath;

    /**
     * Constructs an AppApiOperator with the required parameters.
     *
     * @param restRequestHandler handles the sending of REST requests.
     */
    @Autowired
    public AppApiOperator(final RestRequestHandler restRequestHandler,
                          final ObjectMapper objectMapper,
                          final AppEndpointsApi endpoints) {
        super(restRequestHandler, objectMapper);
        this.endpoints = endpoints;
    }


    @Override
    public void update(UUID id, EntityInput<App> input) throws ApiInteractionUnsuccessfulException,
        JsonProcessingException {
        throw new OperationNotAllowedException("Updating apps is " + NOT_ALLOWED);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void applyAction(final UUID id, final ActionType actionType)
        throws ApiInteractionUnsuccessfulException {
        final var request = new RestRequestBuilder(HttpMethod.PUT, connectorUrl,
            getBaseApiPath(), idPathSegment, actionsPath)
            .pathVariable("id", id.toString())
            .queryParameter("type", actionType.toString())
            .basicAuth(connectorUsername, connectorPassword)
            .build();
        log.debug("Prepared request to run app action: [{}]", request);

        final var response = requestHandler.sendRequest(request);
        log.debug(LoggingUtils.RECEIVED_RESPONSE, response);
        log.info("Applied an app action via [{}/{}/{}].", getBaseApiPath(), id, actionsPath);
    }

    /**
     * {@inheritDoc}
     */
    public void appstore(final UUID id)
        throws ApiInteractionUnsuccessfulException {
        final var request = new RestRequestBuilder(HttpMethod.GET, connectorUrl,
            getBaseApiPath(), idPathSegment, appstorePath)
            .pathVariable("id", id.toString())
            .basicAuth(connectorUsername, connectorPassword)
            .build();
        log.debug("Prepared request to fetch appstore for app: [{}]", request);

        final var response = requestHandler.sendRequest(request);
        log.debug(LoggingUtils.RECEIVED_RESPONSE, response);
        log.info("Fetched appstore via [{}/{}/{}].", getBaseApiPath(), id, appstorePath);
    }

    @Override
    protected Class<AppOutput> getEntitySingleOutputClass() {
        return AppOutput.class;
    }

    @Override
    protected Class<AppMultiOutput> getEntityMultiOutputClass() {
        return AppMultiOutput.class;
    }
}
