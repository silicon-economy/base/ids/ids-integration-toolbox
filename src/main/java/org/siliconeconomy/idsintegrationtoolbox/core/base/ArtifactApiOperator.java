/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.base;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.Accessors;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.ArtifactAgreementsApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.ArtifactApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.ArtifactRepresentationsApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.ArtifactSubscriptionsApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.DataApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Artifact;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.ArtifactEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ArtifactLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.ArtifactMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ArtifactOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Provides methods for managing artifacts at the Dataspace Connector.
 *
 * @author Ronja Quensel
 */
@Component
public class ArtifactApiOperator
    extends BaseApiOperator<Artifact, ArtifactOutput, ArtifactEmbedded, ArtifactLinks,
    ArtifactMultiOutput> implements ArtifactApi {

    /** The base path of the API for managing artifacts. */
    @Value("${connector.api.path.artifacts:/api/artifacts}")
    @Getter(value = AccessLevel.PROTECTED)
    private String baseApiPath;

    @Getter
    @Accessors(fluent = true)
    private final DataApi data;

    @Getter
    @Accessors(fluent = true)
    private final ArtifactAgreementsApi agreements;

    @Getter
    @Accessors(fluent = true)
    private final ArtifactRepresentationsApi representations;

    @Getter
    @Accessors(fluent = true)
    private final ArtifactSubscriptionsApi subscriptions;

    /**
     * Constructs an ArtifactApiOperator with the required parameters.
     *
     * @param restRequestHandler handles the sending of REST requests.
     */
    @Autowired
    public ArtifactApiOperator(final RestRequestHandler restRequestHandler,
                               final ObjectMapper objectMapper,
                               final DataApi data,
                               final ArtifactAgreementsApi agreements,
                               final ArtifactRepresentationsApi representations,
                               final ArtifactSubscriptionsApi subscriptions) {
        super(restRequestHandler, objectMapper);
        this.data = data;
        this.agreements = agreements;
        this.representations = representations;
        this.subscriptions = subscriptions;
    }

    @Override
    protected Class<ArtifactOutput> getEntitySingleOutputClass() {
        return ArtifactOutput.class;
    }

    @Override
    protected Class<ArtifactMultiOutput> getEntityMultiOutputClass() {
        return ArtifactMultiOutput.class;
    }

}
