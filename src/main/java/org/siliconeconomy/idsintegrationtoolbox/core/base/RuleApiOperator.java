/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.base;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.Accessors;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RuleApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RuleContractsApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.ContractRule;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.ContractRuleEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ContractRuleLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.ContractRuleMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ContractRuleOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Provides methods for managing rules at the Dataspace Connector.
 *
 * @author Sonia Nganfo
 */
@Component
public class RuleApiOperator
    extends BaseApiOperator<ContractRule, ContractRuleOutput, ContractRuleEmbedded,
    ContractRuleLinks, ContractRuleMultiOutput> implements RuleApi {

    /** The base path of the API for managing rules. */
    @Value("${connector.api.path.rules:/api/rules}")
    @Getter(value = AccessLevel.PROTECTED)
    private String baseApiPath;

    @Getter
    @Accessors(fluent = true)
    private final RuleContractsApi contracts;

    /**
     * Constructs a RuleApiOperator with the required parameters.
     *
     * @param restRequestHandler handles the sending of REST requests.
     */
    @Autowired
    public RuleApiOperator(final RestRequestHandler restRequestHandler,
                           final ObjectMapper objectMapper,
                           final RuleContractsApi contracts) {
        super(restRequestHandler, objectMapper);
        this.contracts = contracts;
    }

    @Override
    protected Class<ContractRuleOutput> getEntitySingleOutputClass() {
        return ContractRuleOutput.class;
    }

    @Override
    protected Class<ContractRuleMultiOutput> getEntityMultiOutputClass() {
        return ContractRuleMultiOutput.class;
    }
}
