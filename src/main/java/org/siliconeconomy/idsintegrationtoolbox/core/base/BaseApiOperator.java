/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.base;

import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.json.JSONObject;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.EntityCrudApi;
import org.siliconeconomy.idsintegrationtoolbox.core.DscApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.AbstractEntity;
import org.siliconeconomy.idsintegrationtoolbox.model.input.EntityInput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.Embedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.Links;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.EntityMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.EntityOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.LoggingUtils;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestBuilder;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.springframework.http.HttpMethod;

/**
 * Provides methods for CRUD operations for a single entity against the Dataspace Connector API.
 * To be implemented for each entity that the DSC offers a CRUD API for.
 *
 * @param <T> the type of the entity.
 * @param <O> the output type returned for a single entity.
 * @param <E> the model of the embedded entry in the dataspace connector.
 * @param <L> the model of links returned by the dataspace connector.
 * @param <M> the output type returned for multiple entities.
 * @author Haydar Qarawlus
 */
@RequiredArgsConstructor
@Log4j2
public abstract class BaseApiOperator<T extends AbstractEntity, O extends EntityOutput<T>,
    E extends Embedded<T, O>, L extends Links<T>, M extends EntityMultiOutput<T, O, E, L>>
    extends DscApiOperator implements EntityCrudApi<T, O, E, L, M> {

    /** Handles the sending of REST requests. */
    protected final @NonNull RestRequestHandler requestHandler;

    /** Used to map the DSC's responses to their object representation. */
    protected final @NonNull ObjectMapper objectMapper;

    /**
     * {@inheritDoc}
     */
    @Override
    public M getAll(final Integer page, final Integer size)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException {
        final var requestBuilder = new RestRequestBuilder(HttpMethod.GET, connectorUrl,
            getBaseApiPath())
                .basicAuth(connectorUsername, connectorPassword);

        if (page != null) {
            requestBuilder.queryParameter("page", String.valueOf(page));
        }

        if (size != null) {
            requestBuilder.queryParameter("size", String.valueOf(size));
        }

        final var request = requestBuilder.build();
        log.debug("Prepared request to query multiple entities: [{}]", request);

        final var response = requestHandler.sendRequest(request);
        log.debug(LoggingUtils.RECEIVED_RESPONSE, response);

        return objectMapper.readValue(response, getEntityMultiOutputClass());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public M getAll() throws ApiInteractionUnsuccessfulException,
        JsonProcessingException {
        return getAll(null, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public O getOne(final UUID id) throws ApiInteractionUnsuccessfulException,
            JsonProcessingException {
        final var request = new RestRequestBuilder(HttpMethod.GET, connectorUrl,
            getBaseApiPath() + idPathSegment)
                .pathVariable("id", id.toString())
                .basicAuth(connectorUsername, connectorPassword)
                .build();
        log.debug("Prepared request to query a single entity: [{}]", request);

        final var response = requestHandler.sendRequest(request);
        log.debug(LoggingUtils.RECEIVED_RESPONSE, response);

        return objectMapper.readValue(response, getEntitySingleOutputClass());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public O create(final EntityInput<T> input)
            throws ApiInteractionUnsuccessfulException, JsonProcessingException {
        final var request = new RestRequestBuilder(HttpMethod.POST, connectorUrl,
            getBaseApiPath())
                .body(serializeInputWithAdditional(input))
                .basicAuth(connectorUsername, connectorPassword)
                .build();
        log.debug("Prepared request to create an entity: [{}]", request);

        final var response = requestHandler.sendRequest(request);
        log.debug(LoggingUtils.RECEIVED_RESPONSE, response);
        log.info("Added an entity via [{}].", getBaseApiPath());

        return objectMapper.readValue(response, getEntitySingleOutputClass());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(final UUID id, final EntityInput<T> input)
            throws ApiInteractionUnsuccessfulException, JsonProcessingException {
        final var request = new RestRequestBuilder(HttpMethod.PUT, connectorUrl,
            getBaseApiPath() + idPathSegment)
                .pathVariable("id", id.toString())
                .body(serializeInputWithAdditional(input))
                .basicAuth(connectorUsername, connectorPassword)
                .build();
        log.debug("Prepared request to update an entity: [{}]", request);

        final var response = requestHandler.sendRequest(request);
        log.debug(LoggingUtils.RECEIVED_RESPONSE, response);
        log.info("Updated an entity via [{}/{}].", getBaseApiPath(), id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(final UUID id) throws ApiInteractionUnsuccessfulException {
        final var request = new RestRequestBuilder(HttpMethod.DELETE, connectorUrl,
            getBaseApiPath() + idPathSegment)
                .pathVariable("id", id.toString())
                .basicAuth(connectorUsername, connectorPassword)
                .build();
        log.debug("Prepared request to delete an entity: [{}]", request);

        final var response = requestHandler.sendRequest(request);
        log.debug(LoggingUtils.RECEIVED_RESPONSE, response);
        log.info("Deleted an entity via [{}/{}].", getBaseApiPath(), id);
    }

    /**
     * Serializes a map of additional key value pairs into a proper form for the DSC to understand.
     * Default serialization would end up in a normal map JSON representation,
     * rather than every key value pair being a top level property of the JSON object.
     *
     * @param input the input entity to serialize.
     * @return the serialized additional object.
     * @throws JsonProcessingException if parsing the input in a string fails.
     */
    private String serializeInputWithAdditional(final EntityInput<T> input)
        throws JsonProcessingException {
        final var jsonObject = new JSONObject(objectMapper.writeValueAsString(input));

        if (input.getAdditional() != null) {
            jsonObject.remove("additional");
            for (var entry : input.getAdditional().entrySet()) {
                jsonObject.put(entry.getKey(), entry.getValue());
            }
        }

        return jsonObject.toString();
    }

    /**
     * Returns the API base path to use for requests. To be implemented by subclasses to return
     * the path to the respective entity's API.
     *
     * @return the base path for the respective entity's API.
     */
    protected abstract String getBaseApiPath();

    /**
     * Returns the class that represents the information returned by the DSC when a single
     * entity is returned. To be implemented by subclasses.
     *
     * @return the class representing a single entity returned by the DSC.
     */
    protected abstract Class<O> getEntitySingleOutputClass();

    /**
     * Returns the class that represents the information returned by the DSC when multiple
     * entities are returned. To be implemented by subclasses.
     *
     * @return the class representing multiple entities returned by the DSC.
     */
    protected abstract Class<M> getEntityMultiOutputClass();
}
