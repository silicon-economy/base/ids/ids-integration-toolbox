/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.base;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.Accessors;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.OfferedResourceApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.OfferedResourceBrokersApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.OfferedResourceCatalogsApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.OfferedResourceContractsApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.OfferedResourceRepresentationsApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.OfferedResourceSubscriptionsApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.OfferedResource;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.OfferedResourceEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.OfferedResourceLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.OfferedResourceMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.OfferedResourceOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Provides methods for managing offered resources at the Dataspace Connector.
 *
 * @author Sonia Nganfo
 */
@Component
public class OfferedResourceApiOperator
    extends BaseApiOperator<OfferedResource, OfferedResourceOutput, OfferedResourceEmbedded,
    OfferedResourceLinks, OfferedResourceMultiOutput> implements OfferedResourceApi {

    /** The base path of the API for managing offered resources. */
    @Value("${connector.api.path.resource.offers:/api/offers}")
    @Getter(value = AccessLevel.PROTECTED)
    private String baseApiPath;

    @Getter
    @Accessors(fluent = true)
    private final OfferedResourceRepresentationsApi representations;

    @Getter
    @Accessors(fluent = true)
    private final OfferedResourceCatalogsApi catalogs;

    @Getter
    @Accessors(fluent = true)
    private final OfferedResourceContractsApi contracts;

    @Getter
    @Accessors(fluent = true)
    private final OfferedResourceSubscriptionsApi subscriptions;

    @Getter
    @Accessors(fluent = true)
    private final OfferedResourceBrokersApi brokers;

    /**
     * Constructs an OfferedResourceApiOperator with the required parameters.
     *
     * @param restRequestHandler handles the sending of REST requests.
     */
    @Autowired
    public OfferedResourceApiOperator(final RestRequestHandler restRequestHandler,
                                      final ObjectMapper objectMapper,
                                      final OfferedResourceRepresentationsApi representations,
                                      final OfferedResourceCatalogsApi catalogs,
                                      final OfferedResourceContractsApi contracts,
                                      final OfferedResourceSubscriptionsApi subscriptions,
                                      final OfferedResourceBrokersApi brokers) {
        super(restRequestHandler, objectMapper);
        this.representations = representations;
        this.catalogs = catalogs;
        this.contracts = contracts;
        this.subscriptions = subscriptions;
        this.brokers = brokers;
    }

    @Override
    protected Class<OfferedResourceOutput> getEntitySingleOutputClass() {
        return OfferedResourceOutput.class;
    }

    @Override
    protected Class<OfferedResourceMultiOutput> getEntityMultiOutputClass() {
        return OfferedResourceMultiOutput.class;
    }
}
