/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.base;

import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.ConfigurationApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Configuration;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.ConfigurationEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ConfigurationLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.ConfigurationMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ConfigurationOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.LoggingUtils;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestBuilder;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

/**
 * Provides methods for managing configurations at the Dataspace Connector.
 *
 * @author Florian Zimmer
 */
@Component
@Log4j2
public class ConfigurationApiOperator
    extends BaseApiOperator<Configuration, ConfigurationOutput, ConfigurationEmbedded,
    ConfigurationLinks, ConfigurationMultiOutput> implements ConfigurationApi {

    /** The base path of the API for managing configurations. */
    @Value("${connector.api.path.configurations:/api/configurations}")
    @Getter(value = AccessLevel.PROTECTED)
    private String baseApiPath;

    /** The active path segment. */
    @Value("${connector.api.path.segment.active:/active}")
    @Getter(value = AccessLevel.PROTECTED)
    private String activePathSegment;

    /**
     * Constructs an ConfigurationApiOperator with the required parameters.
     *
     * @param restRequestHandler handles the sending of REST requests.
     */
    @Autowired
    public ConfigurationApiOperator(final RestRequestHandler restRequestHandler,
                                    final ObjectMapper objectMapper) {
        super(restRequestHandler, objectMapper);
    }

    /**
     * Reads and returns the current configuration.
     *
     * @return the current configuration.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws JsonProcessingException if the response cannot be parsed.
     */
    public ConfigurationOutput active() throws ApiInteractionUnsuccessfulException,
        JsonProcessingException {
        final var request = new RestRequestBuilder(HttpMethod.GET, connectorUrl,
            getBaseApiPath() + activePathSegment)
            .basicAuth(connectorUsername, connectorPassword)
            .build();
        log.debug("Prepared request to query active configuration: [{}]", request);

        final var response = requestHandler.sendRequest(request);
        log.debug(LoggingUtils.RECEIVED_RESPONSE, response);

        return objectMapper.readValue(response, getEntitySingleOutputClass());
    }

    /**
     * Updates the active configuration by setting the active configuration to a registered
     * configuration.
     *
     * @param id UUID of the configuration that should be set to be the active one.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     */
    public void active(final UUID id) throws ApiInteractionUnsuccessfulException {
        final var request = new RestRequestBuilder(HttpMethod.PUT, connectorUrl,
            getBaseApiPath() + idPathSegment + activePathSegment)
            .pathVariable("id", id.toString())
            .basicAuth(connectorUsername, connectorPassword)
            .build();
        log.debug("Prepared request to update the active configuration: [{}]", request);

        final var response = requestHandler.sendRequest(request);
        log.debug(LoggingUtils.RECEIVED_RESPONSE, response);
        log.info("Updated the active configuration via [{}/{}].", getBaseApiPath(), id);
    }

    @Override
    protected Class<ConfigurationOutput> getEntitySingleOutputClass() {
        return ConfigurationOutput.class;
    }

    @Override
    protected Class<ConfigurationMultiOutput> getEntityMultiOutputClass() {
        return ConfigurationMultiOutput.class;
    }

}
