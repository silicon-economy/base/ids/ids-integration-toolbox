/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.base;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Getter;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.DataSourceApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.DataSource;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.DataSourceEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.DataSourceLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.DataSourceMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.DataSourceOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Provides methods for managing datasources at the Dataspace Connector.
 *
 * @author Marc Peschke
 */
@Component
public class DataSourceApiOperator
    extends BaseApiOperator<DataSource, DataSourceOutput, DataSourceEmbedded,
    DataSourceLinks, DataSourceMultiOutput>
    implements DataSourceApi {

    /** The base path of the API for managing datasources. */
    @Value("${connector.api.path.datasources:/api/datasources}")
    @Getter(value = AccessLevel.PROTECTED)
    private String baseApiPath;

    /**
     * Constructs a DataSourceApiOperator with the required parameters.
     *
     * @param restRequestHandler handles the sending of REST requests.
     */
    @Autowired
    public DataSourceApiOperator(final RestRequestHandler restRequestHandler,
                                 final ObjectMapper objectMapper) {
        super(restRequestHandler, objectMapper);
    }

    @Override
    protected Class<DataSourceOutput> getEntitySingleOutputClass() {
        return DataSourceOutput.class;
    }

    @Override
    protected Class<DataSourceMultiOutput> getEntityMultiOutputClass() {
        return DataSourceMultiOutput.class;
    }
}
