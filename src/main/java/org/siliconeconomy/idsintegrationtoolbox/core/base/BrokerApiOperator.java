/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.base;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.Accessors;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.BrokerApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.BrokerOfferedResourcesApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Broker;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.BrokerEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.BrokerLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.BrokerMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.BrokerOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Provides methods for managing brokers at the Dataspace Connector.
 *
 * @author Steffen Biehs
 */
@Component
public class BrokerApiOperator extends BaseApiOperator<Broker, BrokerOutput, BrokerEmbedded,
    BrokerLinks, BrokerMultiOutput> implements BrokerApi {

    /**
     * The base path of the API for managing brokers.
     */
    @Value("${connector.api.path.brokers:/api/brokers}")
    @Getter(value = AccessLevel.PROTECTED)
    private String baseApiPath;

    @Getter
    @Accessors(fluent = true)
    private final BrokerOfferedResourcesApi offers;

    /**
     * Constructs a BrokerApiOperator with the required parameters.
     *
     * @param restRequestHandler handles the sending of REST requests.
     */
    @Autowired
    public BrokerApiOperator(final RestRequestHandler restRequestHandler,
                             final ObjectMapper objectMapper,
                             final BrokerOfferedResourcesApi offers) {
        super(restRequestHandler, objectMapper);
        this.offers = offers;
    }

    @Override
    protected Class<BrokerOutput> getEntitySingleOutputClass() {
        return BrokerOutput.class;
    }

    @Override
    protected Class<BrokerMultiOutput> getEntityMultiOutputClass() {
        return BrokerMultiOutput.class;
    }
}
