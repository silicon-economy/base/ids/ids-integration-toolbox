/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.base;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.Accessors;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.CatalogApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.CatalogOfferedResourcesApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Catalog;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.CatalogEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.CatalogLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.CatalogMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.CatalogOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Provides methods for managing catalogs at the Dataspace Connector.
 *
 * @author Sonia Nganfo
 */
@Component
public class CatalogApiOperator extends BaseApiOperator<Catalog, CatalogOutput, CatalogEmbedded,
    CatalogLinks, CatalogMultiOutput> implements CatalogApi {

    /** The base path of the API for managing catalogs. */
    @Value("${connector.api.path.catalogs:/api/catalogs}")
    @Getter(value = AccessLevel.PROTECTED)
    private String baseApiPath;

    @Getter
    @Accessors(fluent = true)
    private final CatalogOfferedResourcesApi offers;

    /**
     * Constructs a CatalogApiOperator with the required parameters.
     *
     * @param restRequestHandler handles the sending of REST requests.
     */
    @Autowired
    public CatalogApiOperator(final RestRequestHandler restRequestHandler,
                              final ObjectMapper objectMapper,
                              final CatalogOfferedResourcesApi offers) {
        super(restRequestHandler, objectMapper);
        this.offers = offers;
    }

    @Override
    protected Class<CatalogOutput> getEntitySingleOutputClass() {
        return CatalogOutput.class;
    }

    @Override
    protected Class<CatalogMultiOutput> getEntityMultiOutputClass() {
        return CatalogMultiOutput.class;
    }
}
