/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.base;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.Accessors;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.EndpointApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.EndpointDataSourcesApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Endpoint;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.EndpointEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.EndpointLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.EndpointMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.EndpointOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Provides methods for managing endpoints at the Dataspace Connector.
 *
 * @author Florian Zimmer
 */
@Component
@Log4j2
public class EndpointApiOperator
    extends BaseApiOperator<Endpoint, EndpointOutput, EndpointEmbedded, EndpointLinks,
    EndpointMultiOutput> implements EndpointApi {

    /** The base path of the API for managing endpoints. */
    @Value("${connector.api.path.endpoints:/api/endpoints}")
    @Getter(value = AccessLevel.PROTECTED)
    private String baseApiPath;

    @Getter
    @Accessors(fluent = true)
    private final EndpointDataSourcesApi dataSources;

    /**
     * Constructs an EndpointApiOperator with the required parameters.
     *
     * @param restRequestHandler handles the sending of REST requests.
     */
    @Autowired
    public EndpointApiOperator(final RestRequestHandler restRequestHandler,
                               final ObjectMapper objectMapper,
                               final EndpointDataSourcesApi dataSources) {
        super(restRequestHandler, objectMapper);
        this.dataSources = dataSources;
    }

    @Override
    protected Class<EndpointOutput> getEntitySingleOutputClass() {
        return EndpointOutput.class;
    }

    @Override
    protected Class<EndpointMultiOutput> getEntityMultiOutputClass() {
        return EndpointMultiOutput.class;
    }

}
