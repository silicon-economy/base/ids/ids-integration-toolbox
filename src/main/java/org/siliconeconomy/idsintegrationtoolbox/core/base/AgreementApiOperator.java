/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.base;

import static org.siliconeconomy.idsintegrationtoolbox.utils.LoggingUtils.NOT_ALLOWED;

import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.Accessors;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.AgreementApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.AgreementArtifactsApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Agreement;
import org.siliconeconomy.idsintegrationtoolbox.model.input.EntityInput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.AgreementEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.AgreementLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.AgreementMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.AgreementOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.OperationNotAllowedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Provides methods for managing agreements at the Dataspace Connector.
 *
 * @author Sonia Nganfo
 */
@Component
public class AgreementApiOperator
    extends BaseApiOperator<Agreement, AgreementOutput, AgreementEmbedded,
    AgreementLinks, AgreementMultiOutput>
    implements AgreementApi {

    /** The base path of the API for managing agreements. */
    @Value("${connector.api.path.agreements:/api/agreements}")
    @Getter(value = AccessLevel.PROTECTED)
    private String baseApiPath;

    @Getter
    @Accessors(fluent = true)
    private final AgreementArtifactsApi artifacts;

    /**
     * Constructs an AgreementApiOperator with the required parameters.
     *
     * @param restRequestHandler handles the sending of REST requests.
     */
    @Autowired
    public AgreementApiOperator(final RestRequestHandler restRequestHandler,
                                final ObjectMapper objectMapper,
                                final AgreementArtifactsApi artifacts) {
        super(restRequestHandler, objectMapper);
        this.artifacts = artifacts;
    }

    @Override
    public AgreementOutput create(final EntityInput<Agreement> input) {
        throw new OperationNotAllowedException("Creating new agreements is " + NOT_ALLOWED);
    }

    @Override
    public void update(final UUID id, final EntityInput<Agreement> input) {
        throw new OperationNotAllowedException("Updating agreements is " + NOT_ALLOWED);
    }

    @Override
    public void delete(final UUID id) {
        throw new OperationNotAllowedException("Deleting agreements is " + NOT_ALLOWED);
    }

    @Override
    protected Class<AgreementOutput> getEntitySingleOutputClass() {
        return AgreementOutput.class;
    }

    @Override
    protected Class<AgreementMultiOutput> getEntityMultiOutputClass() {
        return AgreementMultiOutput.class;
    }

}
