/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.base;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.Accessors;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.AppStoreApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.AppStoreAppsApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.AppStore;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.AppStoreEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.AppStoreLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.AppStoreMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.AppStoreOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Provides methods for managing app stores at the Dataspace Connector.
 *
 * @author Steffen Biehs
 */
@Component
public class AppStoreApiOperator
    extends BaseApiOperator<AppStore, AppStoreOutput, AppStoreEmbedded, AppStoreLinks,
    AppStoreMultiOutput> implements AppStoreApi {

    /** The base path of the API for managing appstores. */
    @Value("${connector.api.path.appstores:/api/appstores}")
    @Getter(value = AccessLevel.PROTECTED)
    private String baseApiPath;

    @Getter
    @Accessors(fluent = true)
    private final AppStoreAppsApi apps;

    /**
     * Constructs a AppStoreApiOperator with the required parameters.
     *
     * @param restRequestHandler handles the sending of REST requests.
     */
    @Autowired
    public AppStoreApiOperator(final RestRequestHandler restRequestHandler,
                               final ObjectMapper objectMapper,
                               final AppStoreAppsApi apps) {
        super(restRequestHandler, objectMapper);
        this.apps = apps;
    }

    @Override
    protected Class<AppStoreOutput> getEntitySingleOutputClass() {
        return AppStoreOutput.class;
    }

    @Override
    protected Class<AppStoreMultiOutput> getEntityMultiOutputClass() {
        return AppStoreMultiOutput.class;
    }
}
