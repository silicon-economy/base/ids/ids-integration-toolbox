/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.base;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.Accessors;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RepresentationApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RepresentationArtifactsApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RepresentationResourcesApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RepresentationSubscriptionsApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Representation;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.RepresentationEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.RepresentationLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.RepresentationMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.RepresentationOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Provides methods for managing representations at the Dataspace Connector.
 *
 * @author Sonia Nganfo
 */
@Component
public class RepresentationApiOperator
    extends BaseApiOperator<Representation, RepresentationOutput, RepresentationEmbedded,
    RepresentationLinks, RepresentationMultiOutput> implements RepresentationApi {

    /** The base path of the API for managing representations. */
    @Value("${connector.api.path.representations:/api/representations}")
    @Getter(value = AccessLevel.PROTECTED)
    private String baseApiPath;

    @Getter
    @Accessors(fluent = true)
    private final RepresentationArtifactsApi artifacts;

    @Getter
    @Accessors(fluent = true)
    private final RepresentationResourcesApi resources;

    @Getter
    @Accessors(fluent = true)
    private final RepresentationSubscriptionsApi subscriptions;

    /**
     * Constructs a RepresentationApiOperator with the required parameters.
     *
     * @param restRequestHandler handles the sending of REST requests.
     */
    @Autowired
    public RepresentationApiOperator(final RestRequestHandler restRequestHandler,
                                     final ObjectMapper objectMapper,
                                     final RepresentationArtifactsApi artifacts,
                                     final RepresentationResourcesApi resources,
                                     final RepresentationSubscriptionsApi subscriptions) {
        super(restRequestHandler, objectMapper);
        this.artifacts = artifacts;
        this.resources = resources;
        this.subscriptions = subscriptions;
    }

    @Override
    protected Class<RepresentationOutput> getEntitySingleOutputClass() {
        return RepresentationOutput.class;
    }

    @Override
    protected Class<RepresentationMultiOutput> getEntityMultiOutputClass() {
        return RepresentationMultiOutput.class;
    }
}
