/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.base;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.SubscriptionApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Subscription;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.SubscriptionEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.SubscriptionLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.SubscriptionMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.SubscriptionOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.LoggingUtils;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestBuilder;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

/**
 * Provides methods for managing subscriptions at the Dataspace Connector.
 *
 * @author Florian Zimmer
 */
@Component
@Log4j2
public class SubscriptionApiOperator
    extends BaseApiOperator<Subscription, SubscriptionOutput, SubscriptionEmbedded,
    SubscriptionLinks, SubscriptionMultiOutput> implements SubscriptionApi {

    /** The base path of the API for managing subscriptions. */
    @Value("${connector.api.path.subscriptions:/api/subscriptions}")
    @Getter(value = AccessLevel.PROTECTED)
    private String baseApiPath;

    /** The owning path segment. */
    @Value("${connector.api.path.segment.owning:/owning}")
    @Getter(value = AccessLevel.PROTECTED)
    private String owningPathSegment;

    /**
     * Constructs an SubscriptionApiOperator with the required parameters.
     *
     * @param restRequestHandler handles the sending of REST requests.
     */
    @Autowired
    public SubscriptionApiOperator(final RestRequestHandler restRequestHandler,
                                   final ObjectMapper objectMapper) {
        super(restRequestHandler, objectMapper);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SubscriptionMultiOutput owned() throws ApiInteractionUnsuccessfulException,
        JsonProcessingException {
        final var request = new RestRequestBuilder(HttpMethod.GET, connectorUrl,
            getBaseApiPath() + owningPathSegment)
            .basicAuth(connectorUsername, connectorPassword)
            .build();
        log.debug("Prepared request to query owned entities: [{}]", request);

        final var response = requestHandler.sendRequest(request);
        log.debug(LoggingUtils.RECEIVED_RESPONSE, response);

        return objectMapper.readValue(response, getEntityMultiOutputClass());
    }

    @Override
    protected Class<SubscriptionOutput> getEntitySingleOutputClass() {
        return SubscriptionOutput.class;
    }

    @Override
    protected Class<SubscriptionMultiOutput> getEntityMultiOutputClass() {
        return SubscriptionMultiOutput.class;
    }

}
