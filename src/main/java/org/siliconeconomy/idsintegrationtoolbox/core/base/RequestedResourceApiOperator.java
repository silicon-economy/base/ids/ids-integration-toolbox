/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.base;

import static org.siliconeconomy.idsintegrationtoolbox.utils.LoggingUtils.NOT_ALLOWED;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.Accessors;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RequestedResourceApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RequestedResourceCatalogsApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RequestedResourceContractsApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RequestedResourceRepresentationsApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RequestedResourceSubscriptionsApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.RequestedResource;
import org.siliconeconomy.idsintegrationtoolbox.model.input.EntityInput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.RequestedResourceEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.RequestedResourceLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.RequestedResourceMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.RequestedResourceOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.OperationNotAllowedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Provides methods for managing requested resources at the Dataspace Connector.
 *
 * @author Sonia Nganfo
 */
@Component
public class RequestedResourceApiOperator extends BaseApiOperator<RequestedResource,
    RequestedResourceOutput, RequestedResourceEmbedded,
    RequestedResourceLinks, RequestedResourceMultiOutput> implements RequestedResourceApi {

    /** The base path of the API for managing requested resources. */
    @Value("${connector.api.path.resource.requests:/api/requests}")
    @Getter(value = AccessLevel.PROTECTED)
    private String baseApiPath;

    @Getter
    @Accessors(fluent = true)
    private final RequestedResourceCatalogsApi catalogs;

    @Getter
    @Accessors(fluent = true)
    private final RequestedResourceContractsApi contracts;

    @Getter
    @Accessors(fluent = true)
    private final RequestedResourceRepresentationsApi representations;

    @Getter
    @Accessors(fluent = true)
    private final RequestedResourceSubscriptionsApi subscriptions;

    /**
     * Constructs a RequestedResourceApiOperator with the required parameters.
     *
     * @param restRequestHandler handles the sending of REST requests.
     */
    @Autowired
    public RequestedResourceApiOperator(final RestRequestHandler restRequestHandler,
                                        final ObjectMapper objectMapper,
                                        final RequestedResourceCatalogsApi catalogs,
                                        final RequestedResourceContractsApi contracts,
                                        final RequestedResourceRepresentationsApi representations,
                                        final RequestedResourceSubscriptionsApi subscriptions) {
        super(restRequestHandler, objectMapper);
        this.catalogs = catalogs;
        this.contracts = contracts;
        this.representations = representations;
        this.subscriptions = subscriptions;
    }

    @Override
    public RequestedResourceOutput create(final EntityInput<RequestedResource> input) {
        throw new OperationNotAllowedException("Creating new requested resources is "
            + NOT_ALLOWED);
    }

    @Override
    protected Class<RequestedResourceOutput> getEntitySingleOutputClass() {
        return RequestedResourceOutput.class;
    }

    @Override
    protected Class<RequestedResourceMultiOutput> getEntityMultiOutputClass() {
        return RequestedResourceMultiOutput.class;
    }

}
