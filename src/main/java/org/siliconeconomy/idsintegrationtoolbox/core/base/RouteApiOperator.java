/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.base;

import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.Accessors;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RouteApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RouteEndpointsApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RouteSubRoutesApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Route;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.RouteEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.RouteLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.RouteMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ArtifactOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.RouteOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.LoggingUtils;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestBuilder;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

/**
 * Provides methods for managing routes at the Dataspace Connector.
 *
 * @author Steffen Biehs
 */
@Component
@Log4j2
public class RouteApiOperator extends BaseApiOperator<Route, RouteOutput, RouteEmbedded,
    RouteLinks, RouteMultiOutput> implements RouteApi {

    /**
     * The base path of the API for managing routes.
     */
    @Value("${connector.api.path.routes:/api/routes}")
    @Getter(value = AccessLevel.PROTECTED)
    private String baseApiPath;

    /** The relation path segment for artifacts as outputs. */
    @Value("${connector.api.path.segment.relation.output:/output}")
    @Getter(value = AccessLevel.PROTECTED)
    private String relationPath;

    @Getter
    @Accessors(fluent = true)
    private final RouteEndpointsApi endpoints;

    @Getter
    @Accessors(fluent = true)
    private final RouteSubRoutesApi subRoutes;

    /**
     * Constructs a RouteApiOperator with the required parameters.
     *
     * @param restRequestHandler handles the sending of REST requests.
     */
    @Autowired
    public RouteApiOperator(final RestRequestHandler restRequestHandler,
                            final ObjectMapper objectMapper,
                            final RouteEndpointsApi endpoints,
                            final RouteSubRoutesApi subRoutes) {
        super(restRequestHandler, objectMapper);
        this.endpoints = endpoints;
        this.subRoutes = subRoutes;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ArtifactOutput getOutput(final UUID routeId)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException {
        final var requestBuilder = new RestRequestBuilder(HttpMethod.GET, connectorUrl,
            getBaseApiPath() + idPathSegment + getRelationPath())
            .pathVariable("id", routeId.toString())
            .basicAuth(connectorUsername, connectorPassword);

        final var request = requestBuilder.build();
        log.debug("Prepared request to get artifact output: [{}]", request);

        final var response = requestHandler.sendRequest(request);
        log.debug(LoggingUtils.RECEIVED_RESPONSE, response);

        return objectMapper.readValue(response, ArtifactOutput.class);
    }

    @Override
    protected Class<RouteOutput> getEntitySingleOutputClass() {
        return RouteOutput.class;
    }

    @Override
    protected Class<RouteMultiOutput> getEntityMultiOutputClass() {
        return RouteMultiOutput.class;
    }
}
