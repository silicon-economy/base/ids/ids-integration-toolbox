/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.ids;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.fraunhofer.iais.eis.AppResource;
import de.fraunhofer.iais.eis.Artifact;
import de.fraunhofer.iais.eis.ContractOffer;
import de.fraunhofer.iais.eis.Duty;
import de.fraunhofer.iais.eis.InfrastructureComponent;
import de.fraunhofer.iais.eis.Permission;
import de.fraunhofer.iais.eis.Prohibition;
import de.fraunhofer.iais.eis.Representation;
import de.fraunhofer.iais.eis.Resource;
import de.fraunhofer.iais.eis.ResourceCatalog;
import de.fraunhofer.iais.eis.Rule;
import de.fraunhofer.iais.eis.ids.jsonld.Serializer;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.json.JSONObject;
import org.siliconeconomy.idsintegrationtoolbox.api.ids.MessagesApi;
import org.siliconeconomy.idsintegrationtoolbox.core.DscApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.model.input.SubscriptionInput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.AgreementOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.AppOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.LoggingUtils;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestBuilder;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.InvalidInputException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.NoParsableResponseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;

/**
 * Provides methods for accessing the endpoints of the Dataspace Connector's messages API.
 *
 * @author Florian Zimmer
 */
@Component
@RequiredArgsConstructor
@Log4j2
public class MessagesApiOperator extends DscApiOperator implements MessagesApi {

    /** Type literal used in Information Model JSON-LD representations. */
    private static final String TYPE_LITERAL = "@type";

    /** Name of the recipient parameter required for every request to the messages API. */
    private static final String RECIPIENT_QUERY_PARAM = "recipient";

    /** Name of the elementId parameter required for every request of the messages API. */
    private static final String ELEMENT_ID_QUERY_PARAM = "elementId";

    /** DSC endpoint for sending ConnectorUpdateMessages. */
    @Value("${connector.api.path.connector-update:/api/ids/connector/update}")
    private String connectorUpdateEndpoint;

    /** DSC endpoint for sending ConnectorUnavailableMessages. */
    @Value("${connector.api.path.connector-unavailable:/api/ids/connector/unavailable}")
    private String connectorUnavailableEndpoint;

    /** DSC endpoint for negotiating a contract. */
    @Value("${connector.api.path.contract-request:/api/ids/contract}")
    private String contractRequestEndpoint;

    /** DSC endpoint for sending DescriptionRequestMessages. */
    @Value("${connector.api.path.description-request:/api/ids/description}")
    private String descriptionRequestEndpoint;

    /** DSC endpoint for sending QueryMessages. */
    @Value("${connector.api.path.query:/api/ids/query}")
    private String queryEndpoint;

    /** DSC endpoint for sending ResourceUpdateMessages. */
    @Value("${connector.api.path.resource-update:/api/ids/resource/update/}")
    private String resourceUpdateEndpoint;

    /** DSC endpoint for sending ResourceUnavailableMessages. */
    @Value("${connector.api.path.resource-unavailable:/api/ids/resource/unavailable}")
    private String resourceUnavailableEndpoint;

    /** DSC endpoint for sending SearchMessages. */
    @Value("${connector.api.path.search:/api/ids/search}")
    private String searchEndpoint;

    /** DSC endpoint for sending SubscribeMessages. */
    @Value("${connector.api.path.subscribe:/api/ids/subscribe}")
    private String subscribeEndpoint;

    /** DSC endpoint for sending UnsubscribeMessages. */
    @Value("${connector.api.path.unsubscribe:/api/ids/unsubscribe}")
    private String unsubscribeEndpoint;

    /** DSC endpoint for sending NotifyMessages. */
    @Value("${connector.api.path.notify:/api/notify}")
    private String notifyEndpoint;

    /** DSC endpoint for sending AppDownloadMessages. */
    @Value("${connector.api.path.app:/api/ids/app}")
    private String appEndpoint;

    /** Maps JSON from/to Java objects. */
    private final @NonNull ObjectMapper objectMapper;

    /** (De)serializes Information Model objects from/to JSON-LD. */
    private final @NonNull Serializer serializer;

    /** Handles the sending of REST requests. */
    private final @NonNull RestRequestHandler requestHandler;

    /**
     * {@inheritDoc}
     */
    @Override
    public AppOutput appDownload(final URI recipient, final URI appId)
        throws RestClientException, ApiInteractionUnsuccessfulException, JsonProcessingException {
        final var request = new RestRequestBuilder(HttpMethod.POST, connectorUrl,
            appEndpoint)
            .queryParameter(RECIPIENT_QUERY_PARAM, recipient.toString())
            .queryParameter("appId", appId.toString())
            .basicAuth(connectorUsername, connectorPassword)
            .build();
        log.debug("Prepared request to send a DownloadAppMessage: [{}]", request);

        final var response = requestHandler.sendRequest(request);
        log.debug(LoggingUtils.RECEIVED_RESPONSE, response);

        return objectMapper.readValue(response, AppOutput.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void connectorUnavailable(final URI recipient)
        throws RestClientException, ApiInteractionUnsuccessfulException {
        final var request = new RestRequestBuilder(HttpMethod.POST, connectorUrl,
            connectorUnavailableEndpoint)
            .queryParameter(RECIPIENT_QUERY_PARAM, recipient.toString())
            .basicAuth(connectorUsername, connectorPassword)
            .build();
        log.debug("Prepared request to send a ConnectorUnavailableMessage: [{}]", request);

        final var response = requestHandler.sendRequest(request);
        log.debug(LoggingUtils.RECEIVED_RESPONSE, response);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void connectorUpdate(final URI recipient)
        throws RestClientException, ApiInteractionUnsuccessfulException {
        final var request = new RestRequestBuilder(HttpMethod.POST, connectorUrl,
            connectorUpdateEndpoint)
            .queryParameter(RECIPIENT_QUERY_PARAM, recipient.toString())
            .basicAuth(connectorUsername, connectorPassword)
            .build();
        log.debug("Prepared request to send a ConnectorUpdateMessage: [{}]", request);

        final var response = requestHandler.sendRequest(request);
        log.debug(LoggingUtils.RECEIVED_RESPONSE, response);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AgreementOutput contractNegotiation(final URI recipient,
                                               final List<URI> resourceIds,
                                               final List<URI> artifactIds,
                                               final boolean download,
                                               final List<Rule> rules)
        throws RestClientException, ApiInteractionUnsuccessfulException, IOException,
        InvalidInputException {
        checkInputForNegotiation(resourceIds, artifactIds, rules);

        // Convert rules to json list of rules
        final var bodyBuilder = new StringBuilder("[");
        for (Rule rule : rules) {
            final var ruleObject = new JSONObject(objectMapper.writeValueAsString(rule));

            if (rule instanceof Permission) {
                ruleObject.put(TYPE_LITERAL, "ids:Permission");
            } else if (rule instanceof Duty) {
                ruleObject.put(TYPE_LITERAL, "ids:Duty");
            } else {
                ruleObject.put(TYPE_LITERAL, "ids:Prohibition");
            }

            bodyBuilder.append(ruleObject).append(",");
        }
        bodyBuilder.deleteCharAt(bodyBuilder.length() - 1);
        bodyBuilder.append("]");

        final var body = bodyBuilder.toString();

        final var requestBuilder = new RestRequestBuilder(HttpMethod.POST, connectorUrl,
            contractRequestEndpoint)
            .queryParameter(RECIPIENT_QUERY_PARAM, recipient.toString())
            .queryParameter("download", String.valueOf(download))
            .body(body)
            .basicAuth(connectorUsername, connectorPassword);

        resourceIds.forEach(resourceId -> requestBuilder.queryParameter("resourceIds",
            resourceId.toString()));
        artifactIds.forEach(artifactId -> requestBuilder.queryParameter("artifactIds",
            artifactId.toString()));

        final var request = requestBuilder.build();
        log.debug("Prepared request to negotiate a contract: [{}]", request);

        final var response = requestHandler.sendRequest(request);
        log.debug(LoggingUtils.RECEIVED_RESPONSE, response);
        log.info("Negotiated a contract with [{}] via [{}]", recipient,
            contractRequestEndpoint);

        return objectMapper.readValue(response, AgreementOutput.class);
    }

    /**
     * Checks whether the resourceIds, artifactIds and rules used as input for the contract
     * negotiation are null or empty.
     *
     * @param resourceIds the resource IDs.
     * @param artifactIds the artifact IDs.
     * @param rules       the rules.
     * @throws InvalidInputException if any of the input parameters is null or empty.
     */
    private void checkInputForNegotiation(final List<URI> resourceIds, final List<URI> artifactIds,
                                          final List<Rule> rules) throws InvalidInputException {
        if (resourceIds == null || resourceIds.isEmpty()) {
            throw new InvalidInputException("Contract negotiation requires at least one "
                + " resource ID.");
        }

        if (artifactIds == null || artifactIds.isEmpty()) {
            throw new InvalidInputException("Contract negotiation requires at least one "
                + " artifact ID.");
        }

        if (rules == null || rules.isEmpty()) {
            throw new InvalidInputException("Contract negotiation requires at least one rule.");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InfrastructureComponent descriptionRequest(final URI recipient)
        throws ApiInteractionUnsuccessfulException, IOException {
        final var request = new RestRequestBuilder(HttpMethod.POST, connectorUrl,
            descriptionRequestEndpoint)
            .queryParameter(RECIPIENT_QUERY_PARAM, recipient.toString())
            .basicAuth(connectorUsername, connectorPassword)
            .build();
        log.debug("Prepared request to send a DescriptionRequestMessage: [{}]", request);

        final var response = requestHandler.sendRequest(request);
        log.debug(LoggingUtils.RECEIVED_RESPONSE, response);

        return serializer.deserialize(response, InfrastructureComponent.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object descriptionRequest(final URI recipient, final URI elementId)
        throws RestClientException, ApiInteractionUnsuccessfulException,
        NoParsableResponseException {
        final var request = new RestRequestBuilder(HttpMethod.POST, connectorUrl,
            descriptionRequestEndpoint)
            .queryParameter(RECIPIENT_QUERY_PARAM, recipient.toString())
            .queryParameter(ELEMENT_ID_QUERY_PARAM, elementId.toString())
            .basicAuth(connectorUsername, connectorPassword)
            .build();
        log.debug("Prepared request to send a DescriptionRequestMessage: [{}]", request);

        final var response = requestHandler.sendRequest(request);
        log.debug(LoggingUtils.RECEIVED_RESPONSE, response);

        return getInfoModelObject(response);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String query(final URI recipient, final String query)
        throws RestClientException, ApiInteractionUnsuccessfulException {
        final var request = new RestRequestBuilder(HttpMethod.POST, connectorUrl,
            queryEndpoint)
            .queryParameter(RECIPIENT_QUERY_PARAM, recipient.toString())
            .body(query)
            .basicAuth(connectorUsername, connectorPassword)
            .build();
        log.debug("Prepared request to send a QueryMessage: [{}]", request);

        final var response = requestHandler.sendRequest(request);
        log.debug(LoggingUtils.RECEIVED_RESPONSE, response);

        return response;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void resourceUnavailable(final URI recipient, final URI resourceId)
        throws RestClientException, ApiInteractionUnsuccessfulException {
        final var request = new RestRequestBuilder(HttpMethod.POST, connectorUrl,
            resourceUnavailableEndpoint)
            .queryParameter(RECIPIENT_QUERY_PARAM, recipient.toString())
            .queryParameter("resourceId", resourceId.toString())
            .basicAuth(connectorUsername, connectorPassword)
            .build();
        log.debug("Prepared request to send a ResourceUnavailableMessage: [{}]", request);

        final var response = requestHandler.sendRequest(request);
        log.debug(LoggingUtils.RECEIVED_RESPONSE, response);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void resourceUpdate(final URI recipient, final URI resourceId)
        throws RestClientException, ApiInteractionUnsuccessfulException {
        final var request = new RestRequestBuilder(HttpMethod.POST, connectorUrl,
            resourceUpdateEndpoint)
            .queryParameter(RECIPIENT_QUERY_PARAM, recipient.toString())
            .queryParameter("resourceId", resourceId.toString())
            .basicAuth(connectorUsername, connectorPassword)
            .build();
        log.debug("Prepared request to send a ResourceUpdateMessage: [{}]", request);

        final var response = requestHandler.sendRequest(request);
        log.debug(LoggingUtils.RECEIVED_RESPONSE, response);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String search(final URI recipient, final String body, final int limit,
                         final int offset)
        throws RestClientException, ApiInteractionUnsuccessfulException {
        final var request = new RestRequestBuilder(HttpMethod.POST, connectorUrl,
            searchEndpoint)
            .queryParameter(RECIPIENT_QUERY_PARAM, recipient.toString())
            .queryParameter("limit", String.valueOf(limit))
            .queryParameter("offset", String.valueOf(offset))
            .basicAuth(connectorUsername, connectorPassword)
            .body(body)
            .build();
        log.debug("Prepared request to send a DownloadAppMessage: [{}]", request);

        final var response = requestHandler.sendRequest(request);
        log.debug(LoggingUtils.RECEIVED_RESPONSE, response);

        return response;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String subscribe(final URI recipient, final SubscriptionInput input)
        throws RestClientException, ApiInteractionUnsuccessfulException, IOException {
        final var request = new RestRequestBuilder(HttpMethod.POST, connectorUrl,
            subscribeEndpoint)
            .queryParameter(RECIPIENT_QUERY_PARAM, recipient.toString())
            .basicAuth(connectorUsername, connectorPassword)
            .body(objectMapper.writeValueAsString(input))
            .build();
        log.debug("Prepared request to send a SubscribeMessage: [{}]", request);

        final var response = requestHandler.sendRequest(request);
        log.debug(LoggingUtils.RECEIVED_RESPONSE, response);

        return response;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String unsubscribe(final URI recipient, final URI elementId)
        throws RestClientException, ApiInteractionUnsuccessfulException {
        final var request = new RestRequestBuilder(HttpMethod.POST, connectorUrl,
            unsubscribeEndpoint)
            .queryParameter(RECIPIENT_QUERY_PARAM, recipient.toString())
            .queryParameter(ELEMENT_ID_QUERY_PARAM, elementId.toString())
            .basicAuth(connectorUsername, connectorPassword)
            .build();
        log.debug("Prepared request to send a UnsubscribeMessage: [{}]", request);

        final var response = requestHandler.sendRequest(request);
        log.debug(LoggingUtils.RECEIVED_RESPONSE, response);

        return response;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void notify(final URI elementId)
        throws RestClientException, ApiInteractionUnsuccessfulException {
        final var request = new RestRequestBuilder(HttpMethod.PUT, connectorUrl,
            notifyEndpoint)
            .queryParameter(ELEMENT_ID_QUERY_PARAM, elementId.toString())
            .basicAuth(connectorUsername, connectorPassword)
            .build();
        log.debug("Prepared request to send a NotifyMessage: [{}]", request);

        final var response = requestHandler.sendRequest(request);
        log.debug(LoggingUtils.RECEIVED_RESPONSE, response);
    }

    /**
     * Converts the response to a DescriptionRequestMessage targeting a specific element to a
     * corresponding Information Model object. Returns null, if the response was not of one of the
     * expected types (catalog, resource, representation, artifact, contract offer, permission,
     * prohibition, duty).
     *
     * @param descriptionResponse the response to the description request.
     * @return the Information Model object.
     * @throws NoParsableResponseException if deserializing the response fails.
     */
    private Object getInfoModelObject(final String descriptionResponse)
        throws NoParsableResponseException {
        final var json = new JSONObject(descriptionResponse);
        final var type = json.get(TYPE_LITERAL);

        Object infomodelObject;
        try {
            if ("ids:ResourceCatalog".equals(type)) {
                infomodelObject = serializer
                    .deserialize(descriptionResponse, ResourceCatalog.class);
            } else if ("ids:Resource".equals(type)) {
                infomodelObject = serializer.deserialize(descriptionResponse, Resource.class);
            } else if ("ids:ContractOffer".equals(type)) {
                infomodelObject = serializer.deserialize(descriptionResponse, ContractOffer.class);
            } else if ("ids:Permission".equals(type)) {
                infomodelObject = serializer.deserialize(descriptionResponse, Permission.class);
            } else if ("ids:Prohibition".equals(type)) {
                infomodelObject = serializer.deserialize(descriptionResponse, Prohibition.class);
            } else if ("ids:Duty".equals(type)) {
                infomodelObject = serializer.deserialize(descriptionResponse, Duty.class);
            } else if ("ids:Representation".equals(type)) {
                infomodelObject = serializer.deserialize(descriptionResponse, Representation.class);
            } else if ("ids:Artifact".equals(type)) {
                infomodelObject = serializer.deserialize(descriptionResponse, Artifact.class);
            } else if ("ids:AppResource".equals(type)) {
                infomodelObject = serializer.deserialize(descriptionResponse, AppResource.class);
            } else {
                infomodelObject = null;
            }
        } catch (IOException e) {
            log.warn("Unable to parse response to description request to valid Infomodel object."
                + " [response={}]", descriptionResponse);
            throw new NoParsableResponseException("Unable to parse response.", e);
        }

        return infomodelObject;
    }
}
