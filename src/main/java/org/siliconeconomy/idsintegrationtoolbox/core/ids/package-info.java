/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
/**
 * Contains the classes for operating the Dataspace Connector's endpoints for IDS communication.
 */

package org.siliconeconomy.idsintegrationtoolbox.core.ids;
