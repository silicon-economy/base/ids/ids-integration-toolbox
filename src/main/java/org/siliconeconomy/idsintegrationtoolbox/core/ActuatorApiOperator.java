/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.idsintegrationtoolbox.model.output.ActuatorInfoOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.LoggingUtils;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestBuilder;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;

/**
 * Offers methods for accessing actuator endpoints.
 *
 * @author Florian Zimmer
 */
@Component
@RequiredArgsConstructor
@Log4j2
public class ActuatorApiOperator extends DscApiOperator {

    /** DSC endpoint for getting connector info. */
    @Value("${connector.api.path.info:/actuator/info}")
    private String infoEndpoint;

    /** Handles the sending of REST requests. */
    private final @NonNull RestRequestHandler requestHandler;

    /** Used to map the DSC's responses to their object representation. */
    private final @NonNull ObjectMapper objectMapper;

    /**
     * Gets the connector info.
     *
     * @return the requested connector info.
     * @throws RestClientException if sending the request fails.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws JsonProcessingException if deserializing the response fails.
     */
    public ActuatorInfoOutput getInfo() throws RestClientException,
        ApiInteractionUnsuccessfulException, JsonProcessingException {
        final var request = new RestRequestBuilder(HttpMethod.GET, connectorUrl,
            infoEndpoint)
            .basicAuth(connectorUsername, connectorPassword)
            .build();
        log.debug("Prepared request to get the connector info: [{}]", request);

        final var response = requestHandler.sendRequest(request);
        log.debug(LoggingUtils.RECEIVED_RESPONSE, response);

        return objectMapper.readValue(response, ActuatorInfoOutput.class);
    }

}
