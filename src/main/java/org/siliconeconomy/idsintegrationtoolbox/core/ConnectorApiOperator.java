/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.fraunhofer.iais.eis.Connector;
import de.fraunhofer.iais.eis.ids.jsonld.Serializer;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.idsintegrationtoolbox.api.connector.ConnectorSettingsApi;
import org.siliconeconomy.idsintegrationtoolbox.api.connector.SelfDescriptionApi;
import org.siliconeconomy.idsintegrationtoolbox.model.output.StatusResponse;
import org.siliconeconomy.idsintegrationtoolbox.utils.LoggingUtils;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestBuilder;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;

/**
 * Provides methods for accessing the endpoints of the Dataspace Connector's connector API.
 *
 * @author Florian Zimmer
 */
@Component
@RequiredArgsConstructor
@Log4j2
public class ConnectorApiOperator extends DscApiOperator
    implements SelfDescriptionApi, ConnectorSettingsApi {

    /** DSC endpoint for getting the private self-description. */
    @Value("${connector.api.path.self-service:/api/connector}")
    private String selfServiceEndpoint;

    /** Key of the accept header. Overridden as connector API returns JSON-LD. */
    private static final String ACCEPT_HEADER_KEY = "accept";

    /** Value of the accept header. Overridden as connector API returns JSON-LD. */
    private static final String ACCEPT_HEADER_VALUE = "application/ld+json";

    /** Handles the sending of REST requests. */
    private final @NonNull RestRequestHandler requestHandler;

    /** (De)serializes Information Model objects from/to JSON-LD. */
    private final @NonNull Serializer serializer;

    /** Used to map the DSC's responses to their object representation. */
    private final @NonNull ObjectMapper objectMapper;

    /** DSC endpoint for managing unsupported patterns settings. */
    @Value("${connector.api.path.config.unsupported-patterns:/api/configuration/pattern}")
    private String unsupportedPatternsEndpoint;

    /** DSC endpoint for managing negotiation settings. */
    @Value("${connector.api.path.config.negotiation:/api/configuration/negotiation}")
    private String negotiationEndpoint;

    /**
     * {@inheritDoc}
     */
    @Override
    public Connector getPublic() throws RestClientException, ApiInteractionUnsuccessfulException,
            IOException {
        final var request = new RestRequestBuilder(HttpMethod.GET, connectorUrl, "/")
            .header(ACCEPT_HEADER_KEY, ACCEPT_HEADER_VALUE)
            .build();
        log.debug("Prepared request to get the public self-description: [{}]", request);

        final var response = requestHandler.sendRequest(request);
        log.debug(LoggingUtils.RECEIVED_RESPONSE, response);

        return serializer.deserialize(response, Connector.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Connector getPrivate() throws RestClientException,
        ApiInteractionUnsuccessfulException, IOException {
        final var request = new RestRequestBuilder(HttpMethod.GET, connectorUrl,
            selfServiceEndpoint)
            .header(ACCEPT_HEADER_KEY, ACCEPT_HEADER_VALUE)
            .basicAuth(connectorUsername, connectorPassword)
            .build();
        log.debug("Prepared request to get the private self-description: [{}]", request);

        final var response = requestHandler.sendRequest(request);
        log.debug(LoggingUtils.RECEIVED_RESPONSE, response);

        return serializer.deserialize(response, Connector.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean negotiation(final boolean status) throws RestClientException,
        ApiInteractionUnsuccessfulException, JsonProcessingException {
        final var request = new RestRequestBuilder(HttpMethod.PUT, connectorUrl,
            negotiationEndpoint)
            .queryParameter("status", String.valueOf(status))
            .basicAuth(connectorUsername, connectorPassword)
            .build();
        log.debug("Prepared request to set the status for contract negotiation: [{}]", request);

        final var response = requestHandler.sendRequest(request);
        log.debug(LoggingUtils.RECEIVED_RESPONSE, response);
        log.info("Changed status for contract negotiation to [{}] via [{}]", status,
            negotiationEndpoint);

        final var statusResponse = objectMapper.readValue(response, StatusResponse.class);
        return statusResponse.isStatus();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean negotiation() throws RestClientException,
        ApiInteractionUnsuccessfulException, JsonProcessingException {
        final var request = new RestRequestBuilder(HttpMethod.GET, connectorUrl,
            negotiationEndpoint)
            .basicAuth(connectorUsername, connectorPassword)
            .build();
        log.debug("Prepared request to get the status for contract negotiation: [{}]", request);

        final var response = requestHandler.sendRequest(request);
        log.debug(LoggingUtils.RECEIVED_RESPONSE, response);

        final var statusResponse = objectMapper.readValue(response, StatusResponse.class);
        return statusResponse.isStatus();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean unsupportedPatterns(final boolean status) throws RestClientException,
        ApiInteractionUnsuccessfulException, JsonProcessingException {
        final var request = new RestRequestBuilder(HttpMethod.PUT, connectorUrl,
            unsupportedPatternsEndpoint)
            .queryParameter("status", String.valueOf(status))
            .basicAuth(connectorUsername, connectorPassword)
            .build();
        log.debug("Prepared request to set the status for ignoring unsupported patterns: [{}]",
            request);

        final var response = requestHandler.sendRequest(request);
        log.debug(LoggingUtils.RECEIVED_RESPONSE, response);
        log.info("Changed status for ignoring unsupported patterns to [{}] via [{}]",
            status, unsupportedPatternsEndpoint);

        final var statusResponse = objectMapper.readValue(response, StatusResponse.class);
        return statusResponse.isStatus();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean unsupportedPatterns() throws RestClientException,
        ApiInteractionUnsuccessfulException, JsonProcessingException {
        final var request = new RestRequestBuilder(HttpMethod.GET, connectorUrl,
            unsupportedPatternsEndpoint)
            .basicAuth(connectorUsername, connectorPassword)
            .build();
        log.debug("Prepared request to get the status for ignoring unsupported patterns: [{}]",
            request);

        final var response = requestHandler.sendRequest(request);
        log.debug(LoggingUtils.RECEIVED_RESPONSE, response);

        final var statusResponse = objectMapper.readValue(response, StatusResponse.class);
        return statusResponse.isStatus();
    }

}
