/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.relation;

import static org.siliconeconomy.idsintegrationtoolbox.utils.LoggingUtils.NOT_ALLOWED;

import java.net.URI;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Getter;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.ContractRequestedResourcesApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Contract;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.RequestedResource;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.RequestedResourceEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ContractLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.ContractRequestedResourceRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.EntityRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.RequestedResourceOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.OperationNotAllowedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Provides methods for managing the relation between a contract and its associated requested
 * resources at the Dataspace Connector.
 *
 * @author Sonia Nganfo
 */
@Component
public class ContractRequestedResourcesApiOperator extends RelationApiOperator<Contract,
    ContractLinks, RequestedResource, RequestedResourceOutput, RequestedResourceEmbedded>
    implements ContractRequestedResourcesApi {

    /** The base path of the API for managing contracts. */
    @Value("${connector.api.path.contracts:/api/contracts}")
    @Getter(value = AccessLevel.PROTECTED)
    private String baseApiPath;

    /** The relation path segment for requested resources. */
    @Value("${connector.api.path.segment.relation.resource.requests:/request}")
    @Getter(value = AccessLevel.PROTECTED)
    private String relationPath;

    /**
     * Constructs a ContractRequestedResourcesApiOperator with the required parameters.
     *
     * @param restRequestHandler handles the sending of REST requests.
     */
    @Autowired
    public ContractRequestedResourcesApiOperator(final RestRequestHandler restRequestHandler,
                                                 final ObjectMapper objectMapper) {
        super(restRequestHandler, objectMapper);
    }

    @Override
    protected Class<? extends EntityRelationOutput<Contract, ContractLinks,
        RequestedResource, RequestedResourceOutput, RequestedResourceEmbedded>>
        getEntityRelationOutputClass() {
        return ContractRequestedResourceRelationOutput.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove(final UUID id, List<URI> links) {
        throw new OperationNotAllowedException("Removing requested resources from contracts is "
            + NOT_ALLOWED);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EntityRelationOutput<Contract, ContractLinks, RequestedResource, RequestedResourceOutput,
        RequestedResourceEmbedded> add(final UUID id, List<URI> links) {
        throw new OperationNotAllowedException("Adding requested resources to contracts is "
            + NOT_ALLOWED);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void replace(final UUID id, List<URI> links) {
        throw new OperationNotAllowedException("Replacing requested resources in contracts is "
            + NOT_ALLOWED);
    }
}
