/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.relation;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Getter;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RepresentationOfferedResourcesApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.OfferedResource;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Representation;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.OfferedResourceEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.RepresentationLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.EntityRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.RepresentationOfferedResourceRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.OfferedResourceOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Provides methods for managing the relation between a representation and its associated offered
 * resources at the Dataspace Connector.
 *
 * @author Sonia Nganfo
 */
@Component
public class RepresentationOfferedResourcesApiOperator extends RelationApiOperator<Representation,
    RepresentationLinks, OfferedResource, OfferedResourceOutput, OfferedResourceEmbedded>
    implements RepresentationOfferedResourcesApi {

    /** The base path of the API for managing representations. */
    @Value("${connector.api.path.representations:/api/representations}")
    @Getter(value = AccessLevel.PROTECTED)
    private String baseApiPath;

    /** The relation path segment for offered resources. */
    @Value("${connector.api.path.segment.relation.resource.offers:/offers}")
    @Getter(value = AccessLevel.PROTECTED)
    private String relationPath;

    /**
     * Constructs a RepresentationOfferedResourceApiOperator with the required parameters.
     *
     * @param restRequestHandler handles the sending of REST requests.
     */
    @Autowired
    public RepresentationOfferedResourcesApiOperator(final RestRequestHandler restRequestHandler,
                                                     final ObjectMapper objectMapper) {
        super(restRequestHandler, objectMapper);
    }

    @Override
    protected Class<? extends EntityRelationOutput<Representation, RepresentationLinks,
        OfferedResource, OfferedResourceOutput, OfferedResourceEmbedded>>
        getEntityRelationOutputClass() {
        return RepresentationOfferedResourceRelationOutput.class;
    }
}
