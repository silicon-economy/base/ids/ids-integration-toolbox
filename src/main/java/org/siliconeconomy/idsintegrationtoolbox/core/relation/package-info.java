/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
/**
 * Contains interfaces and implementations used to operate a relation API of the
 * Dataspace Connector.
 */

package org.siliconeconomy.idsintegrationtoolbox.core.relation;
