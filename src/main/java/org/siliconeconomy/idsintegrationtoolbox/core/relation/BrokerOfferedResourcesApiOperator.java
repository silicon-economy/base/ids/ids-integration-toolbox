/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.relation;

import static org.siliconeconomy.idsintegrationtoolbox.utils.LoggingUtils.NOT_ALLOWED;

import java.net.URI;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Getter;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.BrokerOfferedResourcesApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Broker;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.OfferedResource;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.OfferedResourceEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.BrokerLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.BrokerOfferedResourceRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.EntityRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.OfferedResourceOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.OperationNotAllowedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Provides methods for managing the relation between a broker and its associated offered resources
 * at the Dataspace Connector.
 *
 * @author Steffen Biehs
 */
@Component
public class BrokerOfferedResourcesApiOperator extends RelationApiOperator<Broker, BrokerLinks,
    OfferedResource, OfferedResourceOutput, OfferedResourceEmbedded>
    implements BrokerOfferedResourcesApi {

    /**
     * The base path of the API for managing brokers.
     */
    @Value("${connector.api.path.brokers:/api/brokers}")
    @Getter(value = AccessLevel.PROTECTED)
    private String baseApiPath;

    /**
     * The relation path segment for offered resources.
     */
    @Value("${connector.api.path.segment.relation.resource.offers:/offers}")
    @Getter(value = AccessLevel.PROTECTED)
    private String relationPath;

    /**
     * Constructs a BrokerOfferedResourcesApiOperator with the required parameters.
     *
     * @param restRequestHandler handles the sending of REST requests.
     */
    @Autowired
    public BrokerOfferedResourcesApiOperator(final RestRequestHandler restRequestHandler,
                                             final ObjectMapper objectMapper) {
        super(restRequestHandler, objectMapper);
    }

    @Override
    protected Class<? extends EntityRelationOutput<Broker, BrokerLinks, OfferedResource,
        OfferedResourceOutput, OfferedResourceEmbedded>> getEntityRelationOutputClass() {
        return BrokerOfferedResourceRelationOutput.class;
    }

    @Override
    public EntityRelationOutput<Broker, BrokerLinks, OfferedResource, OfferedResourceOutput,
        OfferedResourceEmbedded> add(final UUID id, List<URI> links) {
        throw new OperationNotAllowedException("Adding offered resources to broker is "
            + NOT_ALLOWED);
    }

    @Override
    public void replace(final UUID id, List<URI> links) throws OperationNotAllowedException {
        throw new OperationNotAllowedException("Updating offered resources for brokers is "
            + NOT_ALLOWED);
    }

    @Override
    public void remove(final UUID id, List<URI> links) {
        throw new OperationNotAllowedException("Removing offered resources from brokers is "
            + NOT_ALLOWED);
    }
}
