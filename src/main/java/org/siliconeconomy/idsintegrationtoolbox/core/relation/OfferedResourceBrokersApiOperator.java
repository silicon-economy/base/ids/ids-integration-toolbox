/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.relation;

import static org.siliconeconomy.idsintegrationtoolbox.utils.LoggingUtils.NOT_ALLOWED;

import java.net.URI;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Getter;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.OfferedResourceBrokersApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Broker;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.OfferedResource;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.BrokerEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.OfferedResourceLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.EntityRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.OfferedResourceBrokerRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.BrokerOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.OperationNotAllowedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Provides methods for managing the relation between an offered resource and its associated
 * brokers at the Dataspace Connector.
 *
 * @author Ronja Quensel
 */
@Component
public class OfferedResourceBrokersApiOperator extends RelationApiOperator<OfferedResource,
    OfferedResourceLinks, Broker, BrokerOutput, BrokerEmbedded>
    implements OfferedResourceBrokersApi {

    /** The base path of the API for managing offered resources. */
    @Value("${connector.api.path.resource.offers:/api/offers}")
    @Getter(value = AccessLevel.PROTECTED)
    private String baseApiPath;

    /** The relation path segment for subscriptions. */
    @Value("${connector.api.path.segment.relation.brokers:/brokers}")
    @Getter(value = AccessLevel.PROTECTED)
    private String relationPath;

    /**
     * Constructs an OfferedResourceBrokersApiOperator with the required parameters.
     *
     * @param restRequestHandler handles the sending of REST requests.
     * @param objectMapper maps between model classes and JSON.
     */
    @Autowired
    public OfferedResourceBrokersApiOperator(final RestRequestHandler restRequestHandler,
                                             final ObjectMapper objectMapper) {
        super(restRequestHandler, objectMapper);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EntityRelationOutput<OfferedResource, OfferedResourceLinks, Broker, BrokerOutput,
        BrokerEmbedded> add(final UUID id, List<URI> links) throws OperationNotAllowedException {
        throw new OperationNotAllowedException("Adding subscriptions to offered resource is "
            + NOT_ALLOWED);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void replace(final UUID id, List<URI> links) throws OperationNotAllowedException {
        throw new OperationNotAllowedException("Updating subscriptions for offered resource is "
            + NOT_ALLOWED);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove(final UUID id, List<URI> links) {
        throw new OperationNotAllowedException("Removing subscriptions from offered resource is "
            + NOT_ALLOWED);
    }

    @Override
    protected Class<? extends EntityRelationOutput<OfferedResource, OfferedResourceLinks, Broker,
        BrokerOutput, BrokerEmbedded>> getEntityRelationOutputClass() {
        return OfferedResourceBrokerRelationOutput.class;
    }
}
