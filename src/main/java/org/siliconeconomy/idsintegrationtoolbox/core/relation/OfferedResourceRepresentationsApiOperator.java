/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.relation;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Getter;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.OfferedResourceRepresentationsApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.OfferedResource;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Representation;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.RepresentationEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.OfferedResourceLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.EntityRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.OfferedResourceRepresentationRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.RepresentationOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Provides methods for managing the relation between an offered resource and its associated
 * representations at the Dataspace Connector.
 *
 * @author Sonia Nganfo
 */
@Component
public class OfferedResourceRepresentationsApiOperator extends RelationApiOperator<OfferedResource,
    OfferedResourceLinks, Representation, RepresentationOutput, RepresentationEmbedded>
    implements OfferedResourceRepresentationsApi {

    /** The base path of the API for managing offered resources. */
    @Value("${connector.api.path.resource.offers:/api/offers}")
    @Getter(value = AccessLevel.PROTECTED)
    private String baseApiPath;

    /** The relation path segment for representations. */
    @Value("${connector.api.path.segment.relation.representations:/representations}")
    @Getter(value = AccessLevel.PROTECTED)
    private String relationPath;

    /**
     * Constructs an OfferedResourceRepresentationApiOperator with the required parameters.
     *
     * @param restRequestHandler handles the sending of REST requests.
     */
    @Autowired
    public OfferedResourceRepresentationsApiOperator(final RestRequestHandler restRequestHandler,
                                                     final ObjectMapper objectMapper) {
        super(restRequestHandler, objectMapper);
    }

    @Override
    protected Class<? extends EntityRelationOutput<OfferedResource, OfferedResourceLinks,
        Representation, RepresentationOutput, RepresentationEmbedded>>
        getEntityRelationOutputClass() {
        return OfferedResourceRepresentationRelationOutput.class;
    }
}
