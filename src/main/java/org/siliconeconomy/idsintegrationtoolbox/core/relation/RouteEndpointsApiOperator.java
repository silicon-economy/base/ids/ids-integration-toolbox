/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.relation;

import java.net.URI;
import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RouteEndpointsApi;
import org.siliconeconomy.idsintegrationtoolbox.core.DscApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.utils.LoggingUtils;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestBuilder;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

/**
 * Provides methods for managing the relation between a route and its associated endpoints at the
 * Dataspace Connector.
 *
 * @author Steffen Biehs
 */
@RequiredArgsConstructor
@Log4j2
@Component
public class RouteEndpointsApiOperator extends DscApiOperator implements RouteEndpointsApi {

    /**
     * Handles the sending of REST requests.
     */
    private final RestRequestHandler requestHandler;

    /**
     * Used to map the DSC's responses to their object representation.
     */
    private final ObjectMapper objectMapper;

    /**
     * The base path of the API for managing routes.
     */
    @Value("${connector.api.path.routes:/api/routes}")
    @Getter(value = AccessLevel.PROTECTED)
    private String baseApiPath;

    /**
     * The relation path segment for routes.
     */
    @Value("${connector.api.path.segment.relation.endpoints:/endpoints}")
    @Getter(value = AccessLevel.PROTECTED)
    private String relationPath;

    /**
     * Path segment for managing the start endpoint of a route.
     */
    @Value("${connector.api.path.segment.relation.endpoints.start:/start}")
    private String startEndpointPath;

    /**
     * Path segment for managing the end endpoint of a route.
     */
    @Value("${connector.api.path.segment.relation.endpoints.end:/end}")
    private String endEndpointPath;

    /**
     * Position of the endpoint in a route, can either be start or end.
     */
    private enum EndpointPosition {
        START, END
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void start(UUID routeId, URI endpointId) throws ApiInteractionUnsuccessfulException,
        JsonProcessingException {
        if (endpointId == null) {
            remove(routeId, EndpointPosition.START);
        } else {
            set(routeId, endpointId, EndpointPosition.START);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void end(UUID routeId, URI endpointId) throws ApiInteractionUnsuccessfulException,
        JsonProcessingException {
        if (endpointId == null) {
            remove(routeId, EndpointPosition.END);
        } else {
            set(routeId, endpointId, EndpointPosition.END);
        }
    }

    /**
     * Sets a route endpoint. Depending on the value of the isStart parameter, will update
     * either the start or the end endpoint of the route.
     *
     * @param routeId ID of the route.
     * @param endpointId ID of the endpoint
     * @param type whether to set the endpoint as start or end
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws JsonProcessingException if the request body cannot be parsed.
     */
    private void set(final UUID routeId, final URI endpointId, final EndpointPosition type)
            throws ApiInteractionUnsuccessfulException, JsonProcessingException {
        String path;
        if (EndpointPosition.START.equals(type)) {
            path = startEndpointPath;
        } else {
            path = endEndpointPath;
        }

        final var request = new RestRequestBuilder(HttpMethod.PUT, connectorUrl,
            getBaseApiPath() + idPathSegment + getRelationPath() + path)
            .pathVariable("id", routeId.toString())
            .body(objectMapper.writeValueAsString(endpointId))
            .basicAuth(connectorUsername, connectorPassword)
            .build();
        log.debug("Prepared request to set route endpoint: [{}]", request);

        final var response = requestHandler.sendRequest(request);
        log.debug(LoggingUtils.RECEIVED_RESPONSE, response);
        log.info("Set route endpoint via [{}/{}/{}/{}].", getBaseApiPath(), routeId,
            getRelationPath(), path);
    }

    /**
     * Removes a route endpoint. Depending on the value of the isStart parameter, will remove
     * either the start or the end endpoint of the route.
     *
     * @param routeId ID of the route.
     * @param type whether to remove the start or end endpoint
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     */
    private void remove(final UUID routeId, final EndpointPosition type)
            throws ApiInteractionUnsuccessfulException {
        String path;
        if (EndpointPosition.START.equals(type)) {
            path = startEndpointPath;
        } else {
            path = endEndpointPath;
        }

        final var request = new RestRequestBuilder(HttpMethod.DELETE, connectorUrl,
            getBaseApiPath() + idPathSegment + getRelationPath(), path)
            .pathVariable("id", routeId.toString())
            .basicAuth(connectorUsername, connectorPassword)
            .build();
        log.debug("Prepared request to remove route endpoint: [{}]", request);

        final var response = requestHandler.sendRequest(request);
        log.debug(LoggingUtils.RECEIVED_RESPONSE, response);
        log.info("Removed route endpoint via [{}/{}/{}/{}].", getBaseApiPath(), routeId,
            getRelationPath(), path);
    }

}
