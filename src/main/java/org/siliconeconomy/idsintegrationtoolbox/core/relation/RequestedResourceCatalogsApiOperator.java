/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.relation;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Getter;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RequestedResourceCatalogsApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Catalog;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.RequestedResource;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.CatalogEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.RequestedResourceLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.EntityRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.RequestedResourceCatalogRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.CatalogOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Provides methods for managing the relation between a requested resource and its associated
 * catalogs at the Dataspace Connector.
 *
 * @author Sonia Nganfo
 */
@Component
public class RequestedResourceCatalogsApiOperator extends RelationApiOperator<RequestedResource,
    RequestedResourceLinks, Catalog, CatalogOutput, CatalogEmbedded>
    implements RequestedResourceCatalogsApi {

    /** The base path of the API for managing requested resources. */
    @Value("${connector.api.path.resource.requests:/api/requests}")
    @Getter(value = AccessLevel.PROTECTED)
    private String baseApiPath;

    /** The relation path segment for catalogs. */
    @Value("${connector.api.path.segment.relation.catalogs:/catalogs}")
    @Getter(value = AccessLevel.PROTECTED)
    private String relationPath;

    /**
     * Constructs a RequestedResourceCatalogsApiOperator with the required parameters.
     *
     * @param restRequestHandler handles the sending of REST requests.
     */
    @Autowired
    public RequestedResourceCatalogsApiOperator(final RestRequestHandler restRequestHandler,
                                                final ObjectMapper objectMapper) {
        super(restRequestHandler, objectMapper);
    }

    @Override
    protected Class<? extends EntityRelationOutput<RequestedResource, RequestedResourceLinks,
        Catalog, CatalogOutput, CatalogEmbedded>> getEntityRelationOutputClass() {
        return RequestedResourceCatalogRelationOutput.class;
    }
}
