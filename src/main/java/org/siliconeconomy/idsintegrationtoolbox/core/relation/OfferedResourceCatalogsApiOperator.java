/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.relation;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Getter;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.OfferedResourceCatalogsApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Catalog;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.OfferedResource;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.CatalogEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.OfferedResourceLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.EntityRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.OfferedResourceCatalogRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.CatalogOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Provides methods for managing the relation between an offered resource and its associated
 * catalogs at the Dataspace Connector.
 *
 * @author Sonia Nganfo
 */
@Component
public class OfferedResourceCatalogsApiOperator extends RelationApiOperator<OfferedResource,
    OfferedResourceLinks, Catalog, CatalogOutput, CatalogEmbedded>
    implements OfferedResourceCatalogsApi {

    /** The base path of the API for managing offered resources. */
    @Value("${connector.api.path.resource.offers:/api/offers}")
    @Getter(value = AccessLevel.PROTECTED)
    private String baseApiPath;

    /** The relation path segment for catalogs. */
    @Value("${connector.api.path.segment.relation.catalogs:/catalogs}")
    @Getter(value = AccessLevel.PROTECTED)
    private String relationPath;

    /**
     * Constructs an OfferedResourceCatalogsApiOperator with the required parameters.
     *
     * @param restRequestHandler handles the sending of REST requests.
     */
    @Autowired
    public OfferedResourceCatalogsApiOperator(final RestRequestHandler restRequestHandler,
                                              final ObjectMapper objectMapper) {
        super(restRequestHandler, objectMapper);
    }

    @Override
    protected Class<? extends EntityRelationOutput<OfferedResource, OfferedResourceLinks, Catalog,
        CatalogOutput, CatalogEmbedded>> getEntityRelationOutputClass() {
        return OfferedResourceCatalogRelationOutput.class;
    }
}
