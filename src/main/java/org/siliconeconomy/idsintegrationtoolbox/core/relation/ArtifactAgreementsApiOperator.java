/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.relation;

import static org.siliconeconomy.idsintegrationtoolbox.utils.LoggingUtils.NOT_ALLOWED;

import java.net.URI;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Getter;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.ArtifactAgreementsApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Agreement;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Artifact;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.AgreementEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ArtifactLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.ArtifactAgreementRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.EntityRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.AgreementOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.OperationNotAllowedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Provides methods for managing the relation between an artifact and its associated agreements at
 * the Dataspace Connector.
 *
 * @author Ronja Quensel
 */
@Component
public class ArtifactAgreementsApiOperator extends RelationApiOperator<Artifact, ArtifactLinks,
    Agreement, AgreementOutput, AgreementEmbedded> implements ArtifactAgreementsApi {

    /** The base path of the API for managing artifacts. */
    @Value("${connector.api.path.artifacts:/api/artifacts}")
    @Getter(value = AccessLevel.PROTECTED)
    private String baseApiPath;

    /** The relation path segment for agreements. */
    @Value("${connector.api.path.segment.relation.agreements:/agreements}")
    @Getter(value = AccessLevel.PROTECTED)
    private String relationPath;

    /**
     * Constructs an ArtifactAgreementsApiOperator with the required parameters.
     *
     * @param restRequestHandler handles the sending of REST requests.
     */
    @Autowired
    public ArtifactAgreementsApiOperator(final RestRequestHandler restRequestHandler,
                                         final ObjectMapper objectMapper) {
        super(restRequestHandler, objectMapper);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EntityRelationOutput<Artifact, ArtifactLinks, Agreement, AgreementOutput,
        AgreementEmbedded> add(final UUID id, List<URI> links) throws OperationNotAllowedException {
        throw new OperationNotAllowedException("Adding agreements to artifacts is " + NOT_ALLOWED);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void replace(final UUID id, List<URI> links) throws OperationNotAllowedException {
        throw new OperationNotAllowedException("Updating agreements for artifacts is "
            + NOT_ALLOWED);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove(final UUID id, List<URI> links) {
        throw new OperationNotAllowedException("Removing agreements from artifacts is "
            + NOT_ALLOWED);
    }

    @Override
    protected Class<? extends EntityRelationOutput<Artifact, ArtifactLinks, Agreement,
        AgreementOutput, AgreementEmbedded>> getEntityRelationOutputClass() {
        return ArtifactAgreementRelationOutput.class;
    }
}
