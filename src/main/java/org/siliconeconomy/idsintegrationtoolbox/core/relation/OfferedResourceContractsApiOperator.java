/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.relation;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Getter;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.OfferedResourceContractsApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Contract;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.OfferedResource;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.ContractEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.OfferedResourceLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.EntityRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.OfferedResourceContractRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ContractOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Provides methods for managing the relation between an offered resource and its associated
 * contracts at the Dataspace Connector.
 *
 * @author Sonia Nganfo
 */
@Component
public class OfferedResourceContractsApiOperator extends RelationApiOperator<OfferedResource,
    OfferedResourceLinks, Contract, ContractOutput, ContractEmbedded>
    implements OfferedResourceContractsApi {

    /** The base path of the API for managing offered resources. */
    @Value("${connector.api.path.resource.offers:/api/offers}")
    @Getter(value = AccessLevel.PROTECTED)
    private String baseApiPath;

    /** The relation path segment for caontracts. */
    @Value("${connector.api.path.segment.relation.contracts:/contracts}")
    @Getter(value = AccessLevel.PROTECTED)
    private String relationPath;

    /**
     * Constructs an OfferedResourceContractsApiOperator with the required parameters.
     *
     * @param restRequestHandler handles the sending of REST requests.
     */
    @Autowired
    public OfferedResourceContractsApiOperator(final RestRequestHandler restRequestHandler,
                                               final ObjectMapper objectMapper) {
        super(restRequestHandler, objectMapper);
    }

    @Override
    protected Class<? extends EntityRelationOutput<OfferedResource, OfferedResourceLinks, Contract,
        ContractOutput, ContractEmbedded>> getEntityRelationOutputClass() {
        return OfferedResourceContractRelationOutput.class;
    }
}
