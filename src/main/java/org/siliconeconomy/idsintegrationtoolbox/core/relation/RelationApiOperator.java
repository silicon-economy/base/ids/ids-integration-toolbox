/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.relation;

import java.net.URI;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.EntityRelationApi;
import org.siliconeconomy.idsintegrationtoolbox.core.DscApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.AbstractEntity;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.Embedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.Links;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.EntityRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.EntityOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.LoggingUtils;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestBuilder;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.springframework.http.HttpMethod;

/**
 * Provides methods for CRUD operations on relations between entities against the Dataspace
 * Connector API. The relation is always from the first type to the second type. To be implemented
 * for each entity relation that the DSC offers an API for.
 *
 * @param <T> the entity that owns the relation.
 * @param <L> links type for the owning entity.
 * @param <D> the related entity.
 * @param <O> the output type for the related entity.
 * @param <E> the embedded type for the related entity.
 * @author Haydar Qarawlus
 */
@RequiredArgsConstructor
@Log4j2
public abstract class RelationApiOperator<T extends AbstractEntity, L extends Links<T>,
    D extends AbstractEntity, O extends EntityOutput<D>, E extends Embedded<D, O>>
    extends DscApiOperator implements EntityRelationApi<T, L, D, O, E> {

    /** Handles the sending of REST requests. */
    private final @NonNull RestRequestHandler requestHandler;

    /** Used to map the DSC's responses to their object representation. */
    private final @NonNull ObjectMapper objectMapper;

    /**
     * {@inheritDoc}
     */
    @Override
    public EntityRelationOutput<T, L, D, O, E> getAll(final UUID id, final Integer page,
                                                      final Integer size)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException {
        final var requestBuilder = new RestRequestBuilder(HttpMethod.GET, connectorUrl,
            getBaseApiPath() + idPathSegment + getRelationPath())
                .pathVariable("id", id.toString())
                .basicAuth(connectorUsername, connectorPassword);

        if (page != null) {
            requestBuilder.queryParameter("page", String.valueOf(page));
        }

        if (size != null) {
            requestBuilder.queryParameter("size", String.valueOf(size));
        }

        final var request = requestBuilder.build();
        log.debug("Prepared request to query all related entities: [{}]", request);

        final var response = requestHandler.sendRequest(request);
        log.debug(LoggingUtils.RECEIVED_RESPONSE, response);

        return objectMapper.readValue(response, getEntityRelationOutputClass());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EntityRelationOutput<T, L, D, O, E> getAll(final UUID id)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException {
        return getAll(id, null, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EntityRelationOutput<T, L, D, O, E> add(final UUID id, List<URI> links)
            throws ApiInteractionUnsuccessfulException, JsonProcessingException {
        final var request = new RestRequestBuilder(HttpMethod.POST, connectorUrl,
            getBaseApiPath() + idPathSegment + getRelationPath())
                .pathVariable("id", id.toString())
                .body(objectMapper.writeValueAsString(links))
                .basicAuth(connectorUsername, connectorPassword)
                .build();
        log.debug("Prepared request to add related entities: [{}]", request);

        final var response = requestHandler.sendRequest(request);
        log.debug(LoggingUtils.RECEIVED_RESPONSE, response);
        log.info("Added entity relations via [{}/{}{}].", getBaseApiPath(), id, getRelationPath());

        return objectMapper.readValue(response, getEntityRelationOutputClass());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void replace(final UUID id, List<URI> links) throws ApiInteractionUnsuccessfulException,
            JsonProcessingException {
        remove(id, links);
        add(id, links);
        log.info("Replaced entity relations for [{}/{}{}].", getBaseApiPath(), id,
            getRelationPath());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove(final UUID id, List<URI> links) throws ApiInteractionUnsuccessfulException,
            JsonProcessingException {
        final var request = new RestRequestBuilder(HttpMethod.DELETE, connectorUrl,
            getBaseApiPath() + idPathSegment + getRelationPath())
                .pathVariable("id", id.toString())
                .body(objectMapper.writeValueAsString(links))
                .basicAuth(connectorUsername, connectorPassword)
                .build();
        log.debug("Prepared request to remove related entities: [{}]", request);

        final var response = requestHandler.sendRequest(request);
        log.debug(LoggingUtils.RECEIVED_RESPONSE, response);
        log.info("Removed entity relations via [{}/{}{}].", getBaseApiPath(), id,
            getRelationPath());
    }

    /**
     * Returns the API base path to use for requests. This base path identifies the first entity.
     * To be implemented by subclasses to return the path to the respective first entity's API.
     *
     * @return the base path for the first entity's API.
     */
    protected abstract String getBaseApiPath();

    /**
     * Returns the API path segment to use in addition to the base path. This path segment
     * identifies the second entity. To be implemented by subclasses to return the relation path
     * for the respective second entity.
     *
     * @return the path segment for the second entity.
     */
    protected abstract String getRelationPath();

    /**
     * Returns the class that represents the information returned by the DSC when any relation
     * operation is performed. To be implemented by subclasses.
     *
     * @return the class representing relations returned by the DSC.
     */
    protected abstract Class<? extends EntityRelationOutput<T, L, D, O, E>>
        getEntityRelationOutputClass();
}
