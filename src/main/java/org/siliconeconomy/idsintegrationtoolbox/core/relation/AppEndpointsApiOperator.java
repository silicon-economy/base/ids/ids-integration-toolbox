/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.relation;

import static org.siliconeconomy.idsintegrationtoolbox.utils.LoggingUtils.NOT_ALLOWED;

import java.net.URI;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Getter;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.AppEndpointsApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.App;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Endpoint;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.EndpointEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.AppLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.AppEndpointRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.EntityRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.EndpointOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.OperationNotAllowedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Provides methods for managing the relation between a catalog and its associated offered resources
 * at the Dataspace Connector.
 *
 * @author Steffen
 */
@Component
public class AppEndpointsApiOperator extends RelationApiOperator<App, AppLinks, Endpoint,
    EndpointOutput, EndpointEmbedded> implements AppEndpointsApi {

    /**
     * The base path of the API for managing apps.
     */
    @Value("${connector.api.path.apps:/api/apps}")
    @Getter(value = AccessLevel.PROTECTED)
    private String baseApiPath;

    /**
     * The relation path segment for endpoints.
     */
    @Value("${connector.api.path.segment.relation.endpoints:/endpoints}")
    @Getter(value = AccessLevel.PROTECTED)
    private String relationPath;

    /**
     * Constructs a AppEndpointsApiOperator with the required parameters.
     *
     * @param restRequestHandler handles the sending of REST requests.
     */
    @Autowired
    public AppEndpointsApiOperator(final RestRequestHandler restRequestHandler,
                                   final ObjectMapper objectMapper) {
        super(restRequestHandler, objectMapper);
    }

    @Override
    public EntityRelationOutput<App, AppLinks, Endpoint, EndpointOutput, EndpointEmbedded>
        add(final UUID id, List<URI> links) {
        throw new OperationNotAllowedException("Adding endpoints to apps is " + NOT_ALLOWED);
    }

    @Override
    public void replace(final UUID id, List<URI> links) throws OperationNotAllowedException {
        throw new OperationNotAllowedException("Updating endpoints for apps is " + NOT_ALLOWED);
    }

    @Override
    public void remove(final UUID id, List<URI> links) {
        throw new OperationNotAllowedException("Removing endpoints from apps is " + NOT_ALLOWED);
    }

    @Override
    protected Class<? extends EntityRelationOutput<App, AppLinks, Endpoint, EndpointOutput,
        EndpointEmbedded>> getEntityRelationOutputClass() {
        return AppEndpointRelationOutput.class;
    }
}
