/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.relation;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Getter;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.ContractRulesApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Contract;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.ContractRule;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.ContractRuleEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ContractLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.ContractContractRuleRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.EntityRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ContractRuleOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Provides methods for managing the relation between a contract and its associated rules at the
 * Dataspace Connector.
 *
 * @author Sonia Nganfo
 */
@Component
public class ContractRulesApiOperator extends RelationApiOperator<Contract, ContractLinks,
    ContractRule, ContractRuleOutput, ContractRuleEmbedded> implements ContractRulesApi {

    /** The base path of the API for managing contracts. */
    @Value("${connector.api.path.contracts:/api/contracts}")
    @Getter(value = AccessLevel.PROTECTED)
    private String baseApiPath;

    /** The relation path segment for rules. */
    @Value("${connector.api.path.segment.relation.rules:/rules}")
    @Getter(value = AccessLevel.PROTECTED)
    private String relationPath;

    /**
     * Constructs a ContractRulesApiOperator with the required parameters.
     *
     * @param restRequestHandler handles the sending of REST requests.
     */
    @Autowired
    public ContractRulesApiOperator(final RestRequestHandler restRequestHandler,
                                    final ObjectMapper objectMapper) {
        super(restRequestHandler, objectMapper);
    }

    @Override
    protected Class<? extends EntityRelationOutput<Contract, ContractLinks, ContractRule,
        ContractRuleOutput, ContractRuleEmbedded>> getEntityRelationOutputClass() {
        return ContractContractRuleRelationOutput.class;
    }
}
