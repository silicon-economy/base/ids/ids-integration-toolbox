/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.relation;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Getter;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RouteSubRoutesApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Route;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.RouteEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.RouteLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.EntityRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.RouteRouteRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.RouteOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Provides methods for managing the relation between a route and its associated routes at the
 * Dataspace Connector.
 *
 * @author Florian Zimmer
 */
@Component
public class RouteRoutesApiOperator
    extends RelationApiOperator<Route, RouteLinks, Route, RouteOutput, RouteEmbedded>
    implements RouteSubRoutesApi {

    /** The base path of the API for managing routes. */
    @Value("${connector.api.path.routes:/api/routes}")
    @Getter(value = AccessLevel.PROTECTED)
    private String baseApiPath;

    /** The relation path segment for steps as routes. */
    @Value("${connector.api.path.segment.relation.steps:/steps}")
    @Getter(value = AccessLevel.PROTECTED)
    private String relationPath;

    /**
     * Constructs an RouteRoutesApiOperator with the required parameters.
     *
     * @param restRequestHandler handles the sending of REST requests.
     */
    @Autowired
    public RouteRoutesApiOperator(final RestRequestHandler restRequestHandler,
                                  final ObjectMapper objectMapper) {
        super(restRequestHandler, objectMapper);
    }

    @Override
    protected Class<? extends EntityRelationOutput<Route, RouteLinks, Route, RouteOutput,
        RouteEmbedded>> getEntityRelationOutputClass() {
        return RouteRouteRelationOutput.class;
    }
}
