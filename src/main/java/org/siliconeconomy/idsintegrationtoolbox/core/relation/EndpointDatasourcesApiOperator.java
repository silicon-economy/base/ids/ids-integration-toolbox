/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.relation;

import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.EndpointDataSourcesApi;
import org.siliconeconomy.idsintegrationtoolbox.core.DscApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestBuilder;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

/**
 * Provides methods for managing the relation between an endpoint and its associated datasources at
 * the Dataspace Connector.
 *
 * @author Florian Zimmer
 */
@Component
@RequiredArgsConstructor
@Log4j2
public class EndpointDatasourcesApiOperator extends DscApiOperator
    implements EndpointDataSourcesApi {

    /** The base path of the API for managing endpoints. */
    @Value("${connector.api.path.endpoints:/api/endpoints}")
    @Getter(value = AccessLevel.PROTECTED)
    private String baseApiPath;

    /** The relation path segment for datasource. */
    @Value("${connector.api.path.segment.relation.datasources:/datasource}")
    @Getter(value = AccessLevel.PROTECTED)
    private String relationPath;

    /** Handles the sending of REST requests. */
    private final @NonNull RestRequestHandler requestHandler;

    /**
     * {@inheritDoc}
     */
    @Override
    public void add(final UUID endpointId, final UUID datasourceId)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException {
        final var request = new RestRequestBuilder(HttpMethod.PUT, connectorUrl,
            getBaseApiPath() + idPathSegment + getRelationPath() + "/{dataSourceId}")
            .pathVariable("id", endpointId.toString())
            .pathVariable("dataSourceId", datasourceId.toString())
            .basicAuth(connectorUsername, connectorPassword)
            .build();
        log.debug("Prepared request to add related entities: [{}]", request);

        requestHandler.sendRequest(request);
        log.info("Added entity relations via [{}/{}{}/{}].", getBaseApiPath(), endpointId,
            getRelationPath(), datasourceId);
    }
}
