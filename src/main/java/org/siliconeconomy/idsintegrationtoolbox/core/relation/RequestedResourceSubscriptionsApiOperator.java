/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.relation;

import static org.siliconeconomy.idsintegrationtoolbox.utils.LoggingUtils.NOT_ALLOWED;

import java.net.URI;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Getter;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RequestedResourceSubscriptionsApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.RequestedResource;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Subscription;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.SubscriptionEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.RequestedResourceLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.EntityRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.RequestedResourcesSubscriptionRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.SubscriptionOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.OperationNotAllowedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Provides methods for managing the relation between a requested resource and its associated
 * subscriptions at the Dataspace Connector.
 *
 * @author Florian Zimmer
 */
@Component
public class RequestedResourceSubscriptionsApiOperator
    extends RelationApiOperator<RequestedResource, RequestedResourceLinks, Subscription,
    SubscriptionOutput, SubscriptionEmbedded> implements RequestedResourceSubscriptionsApi {

    /** The base path of the API for managing requested resource. */
    @Value("${connector.api.path.resource.requests:/api/requests}")
    @Getter(value = AccessLevel.PROTECTED)
    private String baseApiPath;

    /** The relation path segment for subscriptions. */
    @Value("${connector.api.path.segment.relation.subscriptions:/subscriptions}")
    @Getter(value = AccessLevel.PROTECTED)
    private String relationPath;

    /**
     * Constructs an RequestedResourceSubscriptionsApiOperator with the required parameters.
     *
     * @param restRequestHandler handles the sending of REST requests.
     */
    @Autowired
    public RequestedResourceSubscriptionsApiOperator(final RestRequestHandler restRequestHandler,
                                                     final ObjectMapper objectMapper) {
        super(restRequestHandler, objectMapper);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EntityRelationOutput<RequestedResource, RequestedResourceLinks, Subscription,
        SubscriptionOutput, SubscriptionEmbedded> add(final UUID id, List<URI> links)
        throws OperationNotAllowedException {
        throw new OperationNotAllowedException("Adding subscriptions to requested resources is "
            + NOT_ALLOWED);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void replace(final UUID id, List<URI> links) throws OperationNotAllowedException {
        throw new OperationNotAllowedException("Updating subscriptions for requested resource is "
            + NOT_ALLOWED);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove(final UUID id, List<URI> links) {
        throw new OperationNotAllowedException("Removing subscriptions from requested resource is "
            + NOT_ALLOWED);
    }

    @Override
    protected Class<? extends EntityRelationOutput<RequestedResource, RequestedResourceLinks,
        Subscription, SubscriptionOutput, SubscriptionEmbedded>> getEntityRelationOutputClass() {
        return RequestedResourcesSubscriptionRelationOutput.class;
    }
}
