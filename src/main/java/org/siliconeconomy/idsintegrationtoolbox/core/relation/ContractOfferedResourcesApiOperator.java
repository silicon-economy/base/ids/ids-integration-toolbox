/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.relation;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Getter;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.ContractOfferedResourcesApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Contract;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.OfferedResource;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.OfferedResourceEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ContractLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.ContractOfferedResourceRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.EntityRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.OfferedResourceOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Provides methods for managing the relation between a contract and its associated offered
 * resources at the Dataspace Connector.
 *
 * @author Sonia Nganfo
 */
@Component
public class ContractOfferedResourcesApiOperator extends RelationApiOperator<Contract,
    ContractLinks, OfferedResource, OfferedResourceOutput, OfferedResourceEmbedded>
    implements ContractOfferedResourcesApi {

    /** The base path of the API for managing contracts. */
    @Value("${connector.api.path.contracts:/api/contracts}")
    @Getter(value = AccessLevel.PROTECTED)
    private String baseApiPath;

    /** The relation path segment for offered resources. */
    @Value("${connector.api.path.segment.relation.resource.offers:/offers}")
    @Getter(value = AccessLevel.PROTECTED)
    private String relationPath;

    /**
     * Constructs a ContractOfferedResourcesApiOperator with the required parameters.
     *
     * @param restRequestHandler handles the sending of REST requests.
     */
    @Autowired
    public ContractOfferedResourcesApiOperator(final RestRequestHandler restRequestHandler,
                                               final ObjectMapper objectMapper) {
        super(restRequestHandler, objectMapper);
    }

    @Override
    protected Class<? extends EntityRelationOutput<Contract, ContractLinks, OfferedResource,
        OfferedResourceOutput, OfferedResourceEmbedded>> getEntityRelationOutputClass() {
        return ContractOfferedResourceRelationOutput.class;
    }

}
