/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.relation;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Getter;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.ArtifactRepresentationsApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Artifact;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Representation;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.RepresentationEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ArtifactLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.ArtifactRepresentationRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.EntityRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.RepresentationOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Provides methods for managing the relation between an artifact and its associated representations
 * at the Dataspace Connector.
 *
 * @author Sonia Nganfo
 */
@Component
public class ArtifactRepresentationsApiOperator extends RelationApiOperator<Artifact, ArtifactLinks,
    Representation, RepresentationOutput, RepresentationEmbedded>
    implements ArtifactRepresentationsApi {

    /** The base path of the API for managing artifacts. */
    @Value("${connector.api.path.artifacts:/api/artifacts}")
    @Getter(value = AccessLevel.PROTECTED)
    private String baseApiPath;

    /** The relation path segment for representations. */
    @Value("${connector.api.path.segment.relation.representations:/representations}")
    @Getter(value = AccessLevel.PROTECTED)
    private String relationPath;

    /**
     * Constructs an ArtifactRepresentationsApiOperator with the required parameters.
     *
     * @param restRequestHandler handles the sending of REST requests.
     */
    @Autowired
    public ArtifactRepresentationsApiOperator(final RestRequestHandler restRequestHandler,
                                              final ObjectMapper objectMapper) {
        super(restRequestHandler, objectMapper);
    }

    @Override
    protected Class<? extends EntityRelationOutput<Artifact, ArtifactLinks, Representation,
        RepresentationOutput, RepresentationEmbedded>> getEntityRelationOutputClass() {
        return ArtifactRepresentationRelationOutput.class;
    }
}
