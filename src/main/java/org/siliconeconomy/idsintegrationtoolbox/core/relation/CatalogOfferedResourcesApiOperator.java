/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.relation;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Getter;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.CatalogOfferedResourcesApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Catalog;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.OfferedResource;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.OfferedResourceEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.CatalogLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.CatalogOfferedResourceRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.EntityRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.OfferedResourceOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Provides methods for managing the relation between a catalog and its associated offered resources
 * at the Dataspace Connector.
 *
 * @author Sonia Nganfo
 */
@Component
public class CatalogOfferedResourcesApiOperator extends RelationApiOperator<Catalog, CatalogLinks,
    OfferedResource, OfferedResourceOutput, OfferedResourceEmbedded>
    implements CatalogOfferedResourcesApi {

    /** The base path of the API for managing catalogs. */
    @Value("${connector.api.path.catalogs:/api/catalogs}")
    @Getter(value = AccessLevel.PROTECTED)
    private String baseApiPath;

    /** The relation path segment for offered resources. */
    @Value("${connector.api.path.segment.relation.resource.offers:/offers}")
    @Getter(value = AccessLevel.PROTECTED)
    private String relationPath;

    /**
     * Constructs a CatalogOfferedResourcesApiOperator with the required parameters.
     *
     * @param restRequestHandler handles the sending of REST requests.
     */
    @Autowired
    public CatalogOfferedResourcesApiOperator(final RestRequestHandler restRequestHandler,
                                              final ObjectMapper objectMapper) {
        super(restRequestHandler, objectMapper);
    }

    @Override
    protected Class<? extends EntityRelationOutput<Catalog, CatalogLinks, OfferedResource,
        OfferedResourceOutput, OfferedResourceEmbedded>> getEntityRelationOutputClass() {
        return CatalogOfferedResourceRelationOutput.class;
    }
}
