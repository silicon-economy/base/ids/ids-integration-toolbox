/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.relation;

import static org.siliconeconomy.idsintegrationtoolbox.utils.LoggingUtils.NOT_ALLOWED;

import java.net.URI;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Getter;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.AgreementArtifactsApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Agreement;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Artifact;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.ArtifactEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.AgreementLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.AgreementArtifactRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.EntityRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ArtifactOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.OperationNotAllowedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Provides methods for managing the relation between an agreement and its associated artifacts at
 * the Dataspace Connector.
 *
 * @author Sonia Nganfo
 */
@Component
public class AgreementArtifactsApiOperator extends RelationApiOperator<Agreement, AgreementLinks,
    Artifact, ArtifactOutput, ArtifactEmbedded> implements AgreementArtifactsApi {

    /** The base path of the API for managing agreements. */
    @Value("${connector.api.path.agreements:/api/agreements}")
    @Getter(value = AccessLevel.PROTECTED)
    private String baseApiPath;

    /** The relation path segment for artifacts. */
    @Value("${connector.api.path.segment.relation.artifacts:/artifacts}")
    @Getter(value = AccessLevel.PROTECTED)
    private String relationPath;

    /**
     * Constructs an AgreementArtifactsApiOperator with the required parameters.
     *
     * @param restRequestHandler handles the sending of REST requests.
     */
    @Autowired
    public AgreementArtifactsApiOperator(final RestRequestHandler restRequestHandler,
                                         final ObjectMapper objectMapper) {
        super(restRequestHandler, objectMapper);
    }

    @Override
    public EntityRelationOutput<Agreement, AgreementLinks, Artifact, ArtifactOutput,
        ArtifactEmbedded> add(final UUID id, List<URI> links) {
        throw new OperationNotAllowedException("Adding artifacts to agreements is " + NOT_ALLOWED);
    }

    @Override
    public void replace(final UUID id, List<URI> links) throws OperationNotAllowedException {
        throw new OperationNotAllowedException("Updating artifacts for agreements is "
            + NOT_ALLOWED);
    }

    @Override
    public void remove(final UUID id, List<URI> links) {
        throw new OperationNotAllowedException("Removing artifacts from agreements is "
            + NOT_ALLOWED);
    }

    @Override
    protected Class<? extends EntityRelationOutput<Agreement, AgreementLinks, Artifact,
        ArtifactOutput, ArtifactEmbedded>> getEntityRelationOutputClass() {
        return AgreementArtifactRelationOutput.class;
    }

}
