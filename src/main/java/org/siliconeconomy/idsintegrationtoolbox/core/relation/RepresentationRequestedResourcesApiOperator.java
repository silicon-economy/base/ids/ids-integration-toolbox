/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.relation;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Getter;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RepresentationRequestedResourcesApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Representation;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.RequestedResource;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.RequestedResourceEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.RepresentationLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.EntityRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.RepresentationRequestedResourceRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.RequestedResourceOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Provides methods for managing the relation between a representation and its associated requested
 * resources at the Dataspace Connector.
 *
 * @author Sonia Nganfo
 */
@Component
public class RepresentationRequestedResourcesApiOperator extends RelationApiOperator<Representation,
    RepresentationLinks, RequestedResource, RequestedResourceOutput, RequestedResourceEmbedded>
    implements RepresentationRequestedResourcesApi {

    /** The base path of the API for managing representations. */
    @Value("${connector.api.path.representations:/api/representations}")
    @Getter(value = AccessLevel.PROTECTED)
    private String baseApiPath;

    /** The relation path segment for requested resources. */
    @Value("${connector.api.path.segment.relation.resource.requests:/requests}")
    @Getter(value = AccessLevel.PROTECTED)
    private String relationPath;

    /**
     * Constructs a RepresentationRequestedResourcesApiOperator with the required parameters.
     *
     * @param restRequestHandler handles the sending of REST requests.
     */
    @Autowired
    public RepresentationRequestedResourcesApiOperator(final RestRequestHandler restRequestHandler,
                                                       final ObjectMapper objectMapper) {
        super(restRequestHandler, objectMapper);
    }

    @Override
    protected Class<? extends EntityRelationOutput<Representation, RepresentationLinks,
        RequestedResource, RequestedResourceOutput, RequestedResourceEmbedded>>
        getEntityRelationOutputClass() {
        return RepresentationRequestedResourceRelationOutput.class;
    }
}
