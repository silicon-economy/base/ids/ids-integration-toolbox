/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.relation;

import static org.siliconeconomy.idsintegrationtoolbox.utils.LoggingUtils.NOT_ALLOWED;

import java.net.URI;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Getter;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.AppStoreAppsApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.App;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.AppStore;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.AppEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.AppStoreLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.AppStoreAppRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.EntityRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.AppOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.OperationNotAllowedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Provides methods for managing the relation between an AppStore and its associated apps
 * at the Dataspace Connector.
 *
 * @author Steffen Biehs
 */
@Component
public class AppStoreAppApiOperator extends RelationApiOperator<AppStore, AppStoreLinks, App,
    AppOutput, AppEmbedded> implements AppStoreAppsApi {

    /** The base path of the API for managing appstores. */
    @Value("${connector.api.path.appstores:/api/appstores}")
    @Getter(value = AccessLevel.PROTECTED)
    private String baseApiPath;

    /** The relation path segment for offered resources. */
    @Value("${connector.api.path.segment.relation.apps:/apps}")
    @Getter(value = AccessLevel.PROTECTED)
    private String relationPath;

    /**
     * Constructs an AppStoreAppApiOperator with the required parameters.
     *
     * @param restRequestHandler handles the sending of REST requests.
     */
    @Autowired
    public AppStoreAppApiOperator(final RestRequestHandler restRequestHandler,
                                              final ObjectMapper objectMapper) {
        super(restRequestHandler, objectMapper);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void replace(final UUID id, List<URI> links) throws OperationNotAllowedException {
        throw new OperationNotAllowedException("Updating apps for appstores is " + NOT_ALLOWED);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove(final UUID id, List<URI> links) {
        throw new OperationNotAllowedException("Removing apps from appstores is " + NOT_ALLOWED);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EntityRelationOutput<AppStore, AppStoreLinks, App, AppOutput, AppEmbedded>
        add(final UUID id, List<URI> links) throws OperationNotAllowedException {
        throw new OperationNotAllowedException("Adding apps to appstores is " + NOT_ALLOWED);
    }

    @Override
    protected Class<? extends EntityRelationOutput<AppStore, AppStoreLinks, App, AppOutput,
        AppEmbedded>> getEntityRelationOutputClass() {
        return AppStoreAppRelationOutput.class;
    }
}
