/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core.relation;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Getter;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RuleContractsApi;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Contract;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.ContractRule;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.ContractEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ContractRuleLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.ContractRuleContractRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.EntityRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ContractOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Provides methods for managing the relation between a rule and its associated contracts at the
 * Dataspace Connector.
 *
 * @author Sonia Nganfo
 */
@Component
public class RuleContractsApiOperator extends RelationApiOperator<ContractRule, ContractRuleLinks,
    Contract, ContractOutput, ContractEmbedded> implements RuleContractsApi {

    /** The base path of the API for managing rules. */
    @Value("${connector.api.path.rules:/api/rules}")
    @Getter(value = AccessLevel.PROTECTED)
    private String baseApiPath;

    /** The relation path segment for catalogs. */
    @Value("${connector.api.path.segment.relation.contracts:/contracts}")
    @Getter(value = AccessLevel.PROTECTED)
    private String relationPath;

    /**
     * Constructs a RuleContractsApiOperator with the required parameters.
     *
     * @param restRequestHandler handles the sending of REST requests.
     */
    @Autowired
    public RuleContractsApiOperator(final RestRequestHandler restRequestHandler,
                                    final ObjectMapper objectMapper) {
        super(restRequestHandler, objectMapper);
    }

    @Override
    protected Class<? extends EntityRelationOutput<ContractRule, ContractRuleLinks, Contract,
        ContractOutput, ContractEmbedded>> getEntityRelationOutputClass() {
        return ContractRuleContractRelationOutput.class;
    }
}
