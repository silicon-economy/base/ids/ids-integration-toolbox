/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.core;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;

/**
 * Superclass for API operators, that provides the commonly required parameters (e.g. the URL of the
 * connector).
 *
 * @author Ronja Quensel
 */
public abstract class DscApiOperator {

    /** URL of the wrapped connector. */
    @Value("${connector.url:http://localhost:8080}")
    protected String connectorUrl;

    /** Username of the wrapped connector. */
    @Value("${connector.username:admin}")
    protected String connectorUsername;

    /** Password of the wrapped connector. */
    @Value("${connector.password:password}")
    protected String connectorPassword;

    /** ID parameter path segment. */
    @Value("${connector.api.path.segment.id-parameter:/{id}}")
    protected String idPathSegment;

    /**
     * Adds the connector's authorization information to an {@link HttpHeaders} instance.
     *
     * @param headers the headers.
     */
    protected void addAuthHeader(final HttpHeaders headers) {
        if (connectorUsername != null && connectorPassword != null) {
            headers.setBasicAuth(connectorUsername, connectorPassword);
        }
    }

}
