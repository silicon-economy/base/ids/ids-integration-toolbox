/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
/**
 * Provides the classes for interacting with the Dataspace Connector's API.
 */

package org.siliconeconomy.idsintegrationtoolbox.core;
