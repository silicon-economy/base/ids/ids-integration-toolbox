/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.camel;

/**
 * Provides methods for uploading Camel routes from an XML file and deleting Camel routes by their
 * ID.
 *
 * @author Ronja Quensel
 */
public interface CamelRouteApi extends FileUploadApi {
}
