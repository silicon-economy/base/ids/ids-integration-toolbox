/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.camel;

import java.io.FileNotFoundException;

import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.InvalidInputException;

/**
 * Interface that is extended by both the {@link CamelRouteApi} and the {@link BeanApi}, as
 * both APIs offer the same methods for uploading routes/beans from an XML file and deleting
 * them by ID.
 *
 * @author Ronja Quensel
 */
public interface FileUploadApi {

    /**
     * Uploads an XML file to the Dataspace Connector. Depending on the implementation, this file
     * can either contain route or bean definitions.
     *
     * @param filePath the path to the XML file.
     * @throws ApiInteractionUnsuccessfulException if the connector returns a non 2xx status code.
     * @throws InvalidInputException if the file path does not point to an XML file.
     * @throws FileNotFoundException if the file cannot be found.
     */
    void upload(final String filePath)
        throws ApiInteractionUnsuccessfulException, InvalidInputException, FileNotFoundException;

    /**
     * Deletes a route or bean (depending on the implementation) by its ID.
     *
     * @param id the ID.
     * @throws ApiInteractionUnsuccessfulException if the connector returns a non 2xx status code.
     */
    void delete(final String id) throws ApiInteractionUnsuccessfulException;

}
