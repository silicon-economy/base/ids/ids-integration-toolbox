/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.camel;

/**
 * Intermediary interface for the DSC's Camel API, that delegates to the respective interfaces
 * for managing Camel routes and beans.
 *
 * @author Ronja Quensel
 */
public interface CamelApi {

    /**
     * The API for managing Camel routes.
     *
     * @return an API implementation.
     */
    CamelRouteApi routes();

    /**
     * The API for managing beans.
     *
     * @return an API implementation.
     */
    BeanApi beans();

}
