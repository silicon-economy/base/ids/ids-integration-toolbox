/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.camel.implementation;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import org.siliconeconomy.idsintegrationtoolbox.api.camel.BeanApi;
import org.siliconeconomy.idsintegrationtoolbox.api.camel.CamelApi;
import org.siliconeconomy.idsintegrationtoolbox.api.camel.CamelRouteApi;
import org.springframework.stereotype.Service;

/**
 * Implementation of the {@link CamelApi} that exposes the APIs for managing Camel routes and beans.
 *
 * @author Ronja Quensel
 */
@Service
@RequiredArgsConstructor
@Getter
@Accessors(fluent = true)
public class CamelApiImpl implements CamelApi {

    /** The API for managing Camel routes. */
    private final @NonNull CamelRouteApi routes;

    /** The API for managing beans. */
    private final @NonNull BeanApi beans;

}
