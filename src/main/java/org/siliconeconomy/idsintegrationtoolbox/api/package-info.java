/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
/**
 * Provides interfaces for the API exposed through the
 * {@link org.siliconeconomy.idsintegrationtoolbox.DscOperator}. It also provides implementations
 * for interfaces that just delegate to other interfaces.
 */

package org.siliconeconomy.idsintegrationtoolbox.api;
