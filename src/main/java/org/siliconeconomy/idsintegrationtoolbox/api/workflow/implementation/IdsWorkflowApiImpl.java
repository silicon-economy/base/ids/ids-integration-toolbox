/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.workflow.implementation;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.BrokerWorkflow;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.ClearingHouseWorkflow;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.ContractNegotiationWorkflow;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.IdsSubscriptionWorkflow;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.IdsWorkflowApi;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.ResourceFindingWorkflow;
import org.springframework.stereotype.Service;

/**
 * Implementation of the {@link IdsWorkflowApi} that exposes the APIs for using different IDS
 * workflows.
 *
 * @author Ronja Quensel
 */
@Service
@RequiredArgsConstructor
@Getter
@Accessors(fluent = true)
public class IdsWorkflowApiImpl implements IdsWorkflowApi {

    /** The API for broker operations. */
    private final @NonNull BrokerWorkflow broker;

    /** The API for contract negotiation. */
    private final ContractNegotiationWorkflow negotiation;

    /** The API for finding specific resources at other connectors. */
    private final @NonNull ResourceFindingWorkflow resourceFinding;

    /** The API for querying the Clearing House. */
    private final @NonNull ClearingHouseWorkflow clearingHouse;

    /** The API for IDS subscriptions. */
    private final @NonNull IdsSubscriptionWorkflow subscriber;

}
