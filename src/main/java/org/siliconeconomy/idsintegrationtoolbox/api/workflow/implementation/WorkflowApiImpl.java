/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.workflow.implementation;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.BrokerWorkflow;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.ClearingHouseWorkflow;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.ContractNegotiationWorkflow;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.EntityRelationWorkflow;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.ResourceCreationWorkflow;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.ResourceFindingWorkflow;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.ResourceRemovalWorkflow;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.SubscriptionWorkflow;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.WorkflowApi;
import org.springframework.stereotype.Service;

/**
 * Implementation of the {@link WorkflowApi} that exposes the APIs for using different workflows.
 *
 * @author Ronja Quensel
 */
@Service
@RequiredArgsConstructor
@Getter
@Accessors(fluent = true)
public class WorkflowApiImpl implements WorkflowApi {

    /** The API for broker operations. */
    private final @NonNull BrokerWorkflow broker;

    /** The API for contract negotiation. */
    private final ContractNegotiationWorkflow negotiation;

    /** The API for linking entities. */
    private final @NonNull EntityRelationWorkflow entityRelations;

    /** The API for creating a complete resource. */
    private final @NonNull ResourceCreationWorkflow resourceCreation;

    /** The API for finding specific resources at other connectors. */
    private final @NonNull ResourceFindingWorkflow resourceFinding;

    /** The API for deleting an offered resource. */
    private final @NonNull ResourceRemovalWorkflow resourceRemoval;

    /** The API for IDS and backend subscriptions. */
    private final @NonNull SubscriptionWorkflow subscriber;

    /** The API for querying the Clearing House. */
    private final @NonNull ClearingHouseWorkflow clearingHouse;

}
