/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.workflow;

import java.net.URI;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;

/**
 * Provides methods for registering and unregistering at an IDS broker.
 *
 * @author Ronja Quensel
 */
public interface BrokerWorkflow {

    /**
     * Registers or updates the connector at the specified broker, but does not register any
     * resources. Note, that an update of the connector may cause previously registered
     * resources to be unregistered.
     *
     * @param broker URL of the broker.
     * @throws ApiInteractionUnsuccessfulException if the DSC returns a non 2xx status code.
     */
    void register(URI broker) throws ApiInteractionUnsuccessfulException;

    /**
     * Registers or updates the connector and all specified resources at the specified broker.
     * Note, that an update of the connector may cause previously registered resources to be
     * unregistered.
     *
     * @param broker URL of the broker.
     * @param resources list of resource IDs to be registered.
     * @throws ApiInteractionUnsuccessfulException if the DSC returns a non 2xx status code.
     */
    void register(URI broker, List<URI> resources) throws ApiInteractionUnsuccessfulException;

    /**
     * Registers or updates all specified resources at the specified broker. This does not include
     * registration of the connector itself.
     *
     * @param broker URL of the broker.
     * @param resources list of resource IDs to be registered.
     * @throws ApiInteractionUnsuccessfulException if the DSC returns a non 2xx status code.
     */
    void registerResources(URI broker, List<URI> resources)
        throws ApiInteractionUnsuccessfulException;

    /**
     * Registers or updates the connector and all of its offered resources at the specified broker.
     *
     * @param broker URL of the broker.
     * @throws ApiInteractionUnsuccessfulException if the DSC returns a non 2xx status code.
     * @throws JsonProcessingException if the offered resources can not be parsed.
     */
    void registerAllResources(URI broker) throws ApiInteractionUnsuccessfulException,
        JsonProcessingException;

    /**
     * Unregisters the connector at the broker. When the connector is unregistered, this includes
     * all registered resources.
     *
     * @param broker  URL of the broker.
     * @throws ApiInteractionUnsuccessfulException if the DSC returns a non 2xx status code.
     */
    void unregister(URI broker) throws ApiInteractionUnsuccessfulException;

    /**
     * Unregisters all specified resources at the broker, but does not unregister the connector
     * itself.
     *
     * @param broker URL of the broker.
     * @param resources list of resource IDs to be unregistered.
     * @throws ApiInteractionUnsuccessfulException if the DSC returns a non 2xx status code.
     */
    void unregisterResources(URI broker, List<URI> resources)
        throws ApiInteractionUnsuccessfulException;

    /**
     * Searches connectors and resources registered at the broker for matches to the given
     * search term. Internally calls {@link #search(URI, String, int, int)} using the DSC's
     * default values for limit (50) and offset (0).
     *
     * @param broker URL of the broker.
     * @param searchTerm list of resource IDs to be unregistered.
     * @return the broker's response.
     * @throws ApiInteractionUnsuccessfulException if the DSC returns a non 2xx status code.
     */
    String search(URI broker, String searchTerm) throws ApiInteractionUnsuccessfulException;

    /**
     * Searches connectors and resources registered at the broker for matches to the given
     * search term.
     *
     * @param broker URL of the broker.
     * @param searchTerm the search term to match.
     * @param limit limit for the number of returned entries.
     * @param offset offset for the returned entries.
     * @return the broker's response.
     * @throws ApiInteractionUnsuccessfulException if the DSC returns a non 2xx status code.
     */
    String search(URI broker, String searchTerm, int limit, int offset)
        throws ApiInteractionUnsuccessfulException;

}
