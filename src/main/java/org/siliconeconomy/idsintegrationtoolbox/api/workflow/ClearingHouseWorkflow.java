/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.workflow;

import java.net.URI;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.siliconeconomy.idsintegrationtoolbox.model.output.ClearingHouseLogEntry;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.springframework.web.client.RestClientException;

/**
 * Provides methods for querying the Clearing House.
 *
 * @author Ronja Quensel
 */
public interface ClearingHouseWorkflow {

    /**
     * Sends a QueryMessage to the specified Clearing House to obtain all entries that have been
     * logged under the specified process ID. The response is parsed to a list of
     * {@link ClearingHouseLogEntry}.
     *
     * @param clearingHouseUrl base URL of the Clearing House (should not contain the query path).
     * @param processId the process ID.
     * @return the list of log entries.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws JsonProcessingException if the response cannot be parsed.
     * @throws RestClientException if sending the request fails.
     */
    List<ClearingHouseLogEntry> query(URI clearingHouseUrl, String processId)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException, RestClientException;

}
