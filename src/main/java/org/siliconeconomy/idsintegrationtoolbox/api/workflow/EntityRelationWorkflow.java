/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.workflow;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.siliconeconomy.idsintegrationtoolbox.core.relation.RelationApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.AbstractEntity;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.EntityOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.EmptyFieldException;

/**
 * Provides methods for linking entities.
 *
 * @author Ronja Quensel
 */
public interface EntityRelationWorkflow {

    /**
     * Accepts an arbitrary number of entites which will then be linked to each other by applying
     * the respective {@link RelationApiOperator} if it exists.
     *
     * @param entities arbitraty entities to be linked.
     * @throws ApiInteractionUnsuccessfulException if the DSC returns a non 2xx status code.
     * @throws JsonProcessingException if parsing the response of the request fails.
     * @throws EmptyFieldException if a required output field is missing.
     */
    void linkEntities(EntityOutput<? extends AbstractEntity>... entities)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException, EmptyFieldException;

}
