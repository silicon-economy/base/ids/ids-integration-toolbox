/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.workflow.implementation;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.BackendSubscriptionWorkflow;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.EntityRelationWorkflow;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.EntityWorkflowApi;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.ResourceCreationWorkflow;
import org.springframework.stereotype.Service;

/**
 * Implementation of the {@link EntityWorkflowApi} that exposes the APIs for using different
 * entity workflows.
 *
 * @author Ronja Quensel
 */
@Service
@RequiredArgsConstructor
@Getter
@Accessors(fluent = true)
public class EntityWorkflowApiImpl implements EntityWorkflowApi {

    /** The API for creating a complete resource. */
    private final @NonNull ResourceCreationWorkflow resourceCreation;

    /** The API for linking entities. */
    private final @NonNull EntityRelationWorkflow entityRelations;

    /** The API for backend subscriptions. */
    private final @NonNull BackendSubscriptionWorkflow subscriber;

}
