/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.workflow;

/**
 * Intermediary interface for the IDS workflow API, that delegates to the interfaces for specific
 * IDS workflows.
 *
 * @author Ronja Quensel
 */
public interface IdsWorkflowApi {

    /**
     * The API for broker operations.
     *
     * @return an API implementation.
     */
    BrokerWorkflow broker();

    /**
     * The API for contract negotiation.
     *
     * @return an API implementation.
     */
    ContractNegotiationWorkflow negotiation();

    /**
     * The API for finding specific resources at other connectors.
     *
     * @return an API implementation.
     */
    ResourceFindingWorkflow resourceFinding();

    /**
     * The API for querying the Clearing House.
     *
     * @return an API implementation.
     */
    ClearingHouseWorkflow clearingHouse();

    /**
     * The API for IDS subscriptions.
     *
     * @return an API implementation.
     */
    IdsSubscriptionWorkflow subscriber();

}
