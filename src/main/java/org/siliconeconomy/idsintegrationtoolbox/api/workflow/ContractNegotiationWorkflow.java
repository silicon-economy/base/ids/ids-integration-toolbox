/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.workflow;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import org.siliconeconomy.idsintegrationtoolbox.model.output.single.AgreementOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.InvalidInputException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.NoParsableResponseException;

/**
 * Provides methods for initiating a contract negotiation sequence with another connector.
 *
 * @author Ronja Quensel
 */
public interface ContractNegotiationWorkflow {

    /**
     * Initiates the contract negotiation sequence with another connector. The contract will be
     * negotiated for the specified artifacts using the specified contract offers for each resource.
     * If an artifact is specified that cannot be found in any of the resources, it will not be
     * part of the contract request.
     *
     * @param recipient the recipient of the contract request.
     * @param resourceIds the resource IDs.
     * @param artifactIds the artifact IDs.
     * @param contractIds the contract IDs.
     * @param download whether the data should be downloaded after the negotiation was successful.
     * @return the agreement.
     * @throws IOException if the request or response body cannot be parsed.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws NoParsableResponseException if the response to a description request cannot be
     *                                     parsed to a valid Information Model object.
     * @throws InvalidInputException if no contract with one of the given IDs exists for a resource.
     */
    AgreementOutput negotiateContract(URI recipient, List<URI> resourceIds, List<URI> artifactIds,
                                      List<URI> contractIds, boolean download)
        throws IOException, ApiInteractionUnsuccessfulException, NoParsableResponseException,
        InvalidInputException;

    /**
     * Initiates the contract negotiation sequence with another connector. The contract will be
     * negotiated for all artifacts that are part of the given resources using the specified
     * contract offers for each resource.
     *
     * @param recipient the recipient of the contract request.
     * @param resourceIds the resource IDs.
     * @param contractIds the contract IDs.
     * @param download whether the data should be downloaded after the negotiation was successful.
     * @return the agreement.
     * @throws IOException if the request or response body cannot be parsed.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws NoParsableResponseException if the response to a description request cannot be
     *                                     parsed to a valid Information Model object.
     * @throws InvalidInputException if no contract with one of the given IDs exists for a resource.
     */
    AgreementOutput negotiateContract(URI recipient, List<URI> resourceIds, List<URI> contractIds,
                                      boolean download) throws IOException,
        ApiInteractionUnsuccessfulException, NoParsableResponseException, InvalidInputException;

    /**
     * Initiates the contract negotiation sequence with another connector. The contract will be
     * negotiated for the specified artifact using the specified contract offer.
     *
     * @param recipient the recipient of the contract request.
     * @param resourceId the resource ID.
     * @param artifactId the artifact ID.
     * @param contractId the contract ID.
     * @param download whether the data should be downloaded after the negotiation was successful.
     * @return the agreement.
     * @throws IOException if the request or response body cannot be parsed.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws NoParsableResponseException if the response to a description request cannot be
     *                                     parsed to a valid Information Model object.
     * @throws InvalidInputException if no contract or artifact with the given IDs exist for the
     *                               given resource.
     */
    AgreementOutput negotiateContract(URI recipient, URI resourceId, URI artifactId, URI contractId,
                                      boolean download) throws IOException,
        ApiInteractionUnsuccessfulException, NoParsableResponseException, InvalidInputException;

    /**
     * Initiates the contract negotiation sequence with another connector. The contract will be
     * negotiated for all artifacts that are part of a given resource using the specified contract
     * offer.
     *
     * @param recipient the recipient of the contract request.
     * @param resourceId the resource ID.
     * @param contractId the contract ID.
     * @param download whether the data should be downloaded after the negotiation was successful.
     * @return the agreement.
     * @throws IOException if the request or response body cannot be parsed.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws NoParsableResponseException if the response to a description request cannot be
     *                                     parsed to a valid Information Model object.
     * @throws InvalidInputException if no contract with the given ID exists for the given resource.
     */
    AgreementOutput negotiateContract(URI recipient, URI resourceId, URI contractId,
                                      boolean download) throws IOException,
        ApiInteractionUnsuccessfulException, NoParsableResponseException, InvalidInputException;

}
