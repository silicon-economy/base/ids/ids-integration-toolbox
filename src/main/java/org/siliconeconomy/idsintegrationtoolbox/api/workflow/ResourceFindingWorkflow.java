/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.workflow;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.NoParsableResponseException;

/**
 * Provides methods for finding resources offered by another connector that match certain criteria.
 *
 * @author Ronja Quensel
 */
public interface ResourceFindingWorkflow {

    /**
     * Finds a resource at a remote connector by its title.
     *
     * @param remoteConnectorUri the URI of the remote connector.
     * @param resourceTitle the title to be found.
     * @param isExactMatch whether the search term should be exactly matched or contained.
     *
     * @return a list of matched resources
     * @throws NoParsableResponseException if invalid response.
     * @throws ApiInteractionUnsuccessfulException if unable to interact with the connector.
     * @throws IOException if output cannot be deserialized.
     * */
    List<URI> findResourceByTitle(URI remoteConnectorUri, String resourceTitle,
                                  boolean isExactMatch) throws NoParsableResponseException,
        ApiInteractionUnsuccessfulException, IOException;

    /**
     * Finds a resource at a remote connector by its keywords.
     *
     * @param remoteConnectorUri the URI of the remote connector.
     * @param resourceKeyword the resource keyword to be found.
     * @param isExactMatch whether the search term should be exactly matched or contained.
     *
     * @return a list of matched resources
     * @throws NoParsableResponseException if invalid response.
     * @throws ApiInteractionUnsuccessfulException if unable to interact with the connector.
     * @throws IOException if output cannot be deserialized.
     * */
    List<URI> findResourceByKeyword(URI remoteConnectorUri, String resourceKeyword,
                                    boolean isExactMatch) throws NoParsableResponseException,
        ApiInteractionUnsuccessfulException, IOException;

    /**
     * Finds a resource at a remote connector by its description.
     *
     * @param remoteConnectorUri the URI of the remote connector.
     * @param resourceDescription the resource description to be found.
     * @param isExactMatch whether the search term should be exactly matched or contained.
     *
     * @return a list of matched resources
     * @throws NoParsableResponseException if invalid response.
     * @throws ApiInteractionUnsuccessfulException if unable to interact with the connector.
     * @throws IOException if output cannot be deserialized.
     * */
    List<URI> findResourceByDescription(URI remoteConnectorUri, String resourceDescription,
                                        boolean isExactMatch) throws NoParsableResponseException,
        ApiInteractionUnsuccessfulException, IOException;

    /**
     * Finds a resource at a remote connector by matching numerous fields. Searches through
     * title, description, keywords, labels of offered resources at remote connectors.
     *
     * @param remoteConnectorUri the URI of the remote connector.
     * @param searchTerm the search term to be matched against.
     * @param isExactMatch whether the search term should be exactly matched or contained.
     *
     * @return a list of matched resources
     * @throws NoParsableResponseException if invalid response.
     * @throws ApiInteractionUnsuccessfulException if unable to interact with the connector.
     * @throws IOException if output cannot be deserialized.
     * */
    List<URI> findResourceByAny(URI remoteConnectorUri, String searchTerm,
                                boolean isExactMatch) throws NoParsableResponseException,
        ApiInteractionUnsuccessfulException, IOException;



}
