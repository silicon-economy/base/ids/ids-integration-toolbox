/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.workflow;

/**
 * Intermediary interface for the workflow API, that delegates to the interfaces for specific
 * workflows.
 *
 * @author Ronja Quensel
 */
public interface WorkflowApi {

    /**
     * The API for broker operations.
     *
     * @return an API implementation.
     */
    BrokerWorkflow broker();

    /**
     * The API for contract negotiation.
     *
     * @return an API implementation.
     */
    ContractNegotiationWorkflow negotiation();

    /**
     * The API for linking entities.
     *
     * @return an API implementation.
     */
    EntityRelationWorkflow entityRelations();

    /**
     * The API for creating a complete resource.
     *
     * @return an API implementation.
     */
    ResourceCreationWorkflow resourceCreation();

    /**
     * The API for finding specific resources at other connectors.
     *
     * @return an API implementation.
     */
    ResourceFindingWorkflow resourceFinding();

    /**
     * The API for removing offered resources.
     *
     * @return an API implementation.
     */
    ResourceRemovalWorkflow resourceRemoval();

    /**
     * The API for IDS and backend subscriptions.
     *
     * @return an API implementation.
     */
    SubscriptionWorkflow subscriber();

    /**
     * The API for querying the Clearing House.
     *
     * @return an API implementation.
     */
    ClearingHouseWorkflow clearingHouse();

}
