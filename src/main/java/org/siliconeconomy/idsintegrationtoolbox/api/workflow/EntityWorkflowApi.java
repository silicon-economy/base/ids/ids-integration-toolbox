/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.workflow;

/**
 * Intermediary interface for the entity workflow API, that delegates to the interfaces for specific
 * entity workflows.
 *
 * @author Ronja Quensel
 */
public interface EntityWorkflowApi {

    /**
     * The API for creating a complete resource.
     *
     * @return an API implementation.
     */
    ResourceCreationWorkflow resourceCreation();

    /**
     * The API for linking entities.
     *
     * @return an API implementation.
     */
    EntityRelationWorkflow entityRelations();

    /**
     * The API for backend subscriptions.
     *
     * @return an API implementation.
     */
    BackendSubscriptionWorkflow subscriber();

}
