/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.workflow;

import java.net.URI;
import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.CatalogOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.OfferedResourceOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.EmptyFieldException;

/**
 * Provides methods for deleting a complete resource. A complete resource is linked to at least one
 * catalog, at least one contract and at least one representation. At least one of the contracts is
 * also linked to at least one rule and at least one of the representations is linked to at least
 * one artifact. If these components are not connected to other resources, they are deleted as
 * well, otherwise only the link between them and the resource is removed.
 *
 * @author Haydar Qarawlus
 */
public interface ResourceRemovalWorkflow {

    /**
     * Deletes an offered resource at the connector. The resource's linked components
     * (representations, contracts, artifacts) are deleted as well if not linked to other
     * resources. Otherwise, only the link between them is removed.
     *
     * @param resourceUri the URI of the resource to be deleted.

     * @throws ApiInteractionUnsuccessfulException if communication with the required endpoints
     *                                             in the connector cannot be made.
     */
    void deleteFullResource(URI resourceUri)
        throws ApiInteractionUnsuccessfulException, EmptyFieldException, JsonProcessingException;

    /**
     * Deletes an offered resource at the connector. The resource's linked components
     * (representations, contracts, artifacts) are deleted as well if not linked to other
     * resources. Otherwise, only the link between them is removed.
     *
     * @param resourceUuid the UUID of the resource to be deleted.

     * @throws ApiInteractionUnsuccessfulException if communication with the required endpoints
     *                                             in the connector cannot be made.
     */
    void deleteFullResource(UUID resourceUuid)
        throws ApiInteractionUnsuccessfulException, EmptyFieldException, JsonProcessingException;

    /**
     * Deletes an offered resource at the connector. The resource's linked components
     * (representations, contracts, artifacts) are deleted as well if not linked to other
     * resources. Otherwise, only the link between them is removed.
     *
     * @param resourceOutput the model class of the offered resource to be deleted.

     * @throws ApiInteractionUnsuccessfulException if communication with the required endpoints
     *                                             in the connector cannot be made.
     */
    void deleteFullResource(OfferedResourceOutput resourceOutput)
        throws ApiInteractionUnsuccessfulException, EmptyFieldException, JsonProcessingException;

    /**
     * Deletes an offered resource at the connector and attempts to remove catalogs that only
     * contain this resource. The resource's linked components (representations, contracts,
     * artifacts) are deleted as well if not linked to other resources. Otherwise, only the link
     * between them is removed.
     *
     * @param resourceUri the URI of the resource to be deleted.

     * @throws ApiInteractionUnsuccessfulException if communication with the required endpoints
     *                                             in the connector cannot be made.
     */
    void deleteResourceAndCatalog(URI resourceUri)
        throws ApiInteractionUnsuccessfulException, EmptyFieldException, JsonProcessingException;

    /**
     * Deletes an offered resource at the connector and attempts to remove catalogs that only
     * contain this resource. The resource's linked components (representations, contracts,
     * artifacts) are deleted as well if not linked to other resources. Otherwise, only the link
     * between them is removed.
     *
     * @param resourceUuid the UUID of the resource to be deleted.

     * @throws ApiInteractionUnsuccessfulException if communication with the required endpoints
     *                                             in the connector cannot be made.
     */
    void deleteResourceAndCatalog(UUID resourceUuid)
        throws ApiInteractionUnsuccessfulException, EmptyFieldException, JsonProcessingException;

    /**
     * Deletes an offered resource at the connector and attempts to remove catalogs that only
     * contain this resource. The resource's linked components (representations, contracts,
     * artifacts) are deleted as well if not linked to other resources. Otherwise, only the link
     * between them is removed.
     *
     * @param resourceOutput the model class of the offered resource to be deleted.

     * @throws ApiInteractionUnsuccessfulException if communication with the required endpoints
     *                                             in the connector cannot be made.
     */
    void deleteResourceAndCatalog(OfferedResourceOutput resourceOutput)
        throws ApiInteractionUnsuccessfulException, EmptyFieldException, JsonProcessingException;

    /**
     * Deletes a catalog and all resources it contains. Each resource is deleted using
     * {@link #deleteFullResource(UUID)}.
     *
     * @param catalogUri URI of the catalog.
     * @throws EmptyFieldException if the UUID cannot be extracted from the URI.
     * @throws ApiInteractionUnsuccessfulException if the DSC returns a non 2xx status code.
     * @throws JsonProcessingException if parsing a response fails.
     */
    void deleteCatalog(URI catalogUri) throws EmptyFieldException,
        ApiInteractionUnsuccessfulException, JsonProcessingException;

    /**
     * Deletes a catalog and all resources it contains. Each resource is deleted using
     * {@link #deleteFullResource(UUID)}.
     *
     * @param catalogId UUID of the catalog.
     * @throws EmptyFieldException if the UUID cannot be extracted from the output.
     * @throws ApiInteractionUnsuccessfulException if the DSC returns a non 2xx status code.
     * @throws JsonProcessingException if parsing a response fails.
     */
    void deleteCatalog(UUID catalogId) throws ApiInteractionUnsuccessfulException,
        JsonProcessingException, EmptyFieldException;

    /**
     * Deletes a catalog and all resources it contains. Each resource is deleted using
     * {@link #deleteFullResource(UUID)}.
     *
     * @param catalogOutput the catalog.
     * @throws EmptyFieldException if the UUID cannot be extracted from the output.
     * @throws ApiInteractionUnsuccessfulException if the DSC returns a non 2xx status code.
     * @throws JsonProcessingException if parsing a response fails.
     */
    void deleteCatalog(CatalogOutput catalogOutput) throws EmptyFieldException,
        ApiInteractionUnsuccessfulException, JsonProcessingException;

}
