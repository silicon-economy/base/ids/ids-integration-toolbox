/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.workflow;

import java.net.URI;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.siliconeconomy.idsintegrationtoolbox.model.input.ArtifactInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.CatalogInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.ContractInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.OfferedResourceInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.RepresentationInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.RuleInput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.OfferedResourceOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;

/**
 * Provides methods for creating a complete resource. A complete resource is linked to at least one
 * catalog, at least one contract and at least one representation. At least one of the contracts is
 * also linked to at least one rule and at least one of the representations is linked to at least
 * one artifact.
 *
 * @author Ronja Quensel
 */
public interface ResourceCreationWorkflow {

    /**
     * Creates an offered resource at the connector sequence with another connector. The resource
     * is created based on the provided inputs. The relations between the various components are
     * created and handled automatically.
     *
     * @param offeredResourceInput the metadata information to be placed in the resource.
     * @param catalogInput         the catalog which will contain the offered resource.
     * @param representationInput  the representation metadata.
     * @param artifactInput        the artifact information. This includes reference to the data
     *                             or its
     *                             location.
     * @param contractInput        the contract information and metadata.
     * @param ruleInputs           the access and permission information.
     * @return the created offered resource.
     * @throws ApiInteractionUnsuccessfulException if communication with the required endpoints
     *                                             in the connector cannot be made.
     * @throws JsonProcessingException             if unable to parse the inputs and the responses.
     */
    OfferedResourceOutput createResource(OfferedResourceInput offeredResourceInput,
                                         CatalogInput catalogInput,
                                         RepresentationInput representationInput,
                                         ArtifactInput artifactInput, ContractInput contractInput,
                                         RuleInput... ruleInputs)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException;

    /**
     * Creates an offered resource at the connector sequence with another connector. The resource
     * is created based on the provided inputs. The relations between the various components are
     * created and handled automatically.
     *
     * @param offeredResourceInput the metadata information to be placed in the resource.
     * @param catalogUri           the URI of the existing catalog which will contain the offered
     *                             resource.
     * @param representationInput  the representation metadata.
     * @param artifactInput        the artifact information. This includes reference to the data
     *                             or its
     *                             location.
     * @param contractInput        the contract information and metadata.
     * @param ruleInputs           the access and permission information.
     * @return the created offered resource.
     * @throws ApiInteractionUnsuccessfulException if communication with the required endpoints
     *                                             in the connector cannot be made.
     * @throws JsonProcessingException             if unable to parse the inputs and the responses.
     */
    OfferedResourceOutput createResource(OfferedResourceInput offeredResourceInput, URI catalogUri,
                                         RepresentationInput representationInput,
                                         ArtifactInput artifactInput, ContractInput contractInput,
                                         RuleInput... ruleInputs)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException;

    /**
     * Creates an offered resource at the connector sequence with another connector. The resource
     * is created based on the provided inputs. The relations between the various components are
     * created and handled automatically.
     *
     * @param offeredResourceInput the metadata information to be placed in the resource.
     * @param catalogUri           the URI of the existing catalog which will contain the offered
     *                             resource.
     * @param representationInput  the representation metadata.
     * @param artifactInput        the artifact information. This includes reference to the data
     *                             or its
     *                             location.
     * @param contractUri          the URI of the existing contract to be added to the resource
     * @return the created offered resource.
     * @throws ApiInteractionUnsuccessfulException if communication with the required endpoints
     *                                             in the connector cannot be made.
     * @throws JsonProcessingException             if unable to parse the inputs and the responses.
     */
    OfferedResourceOutput createResource(OfferedResourceInput offeredResourceInput, URI catalogUri,
                                         RepresentationInput representationInput,
                                         ArtifactInput artifactInput, URI contractUri)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException;

    /**
     * Creates an offered resource at the connector sequence with another connector. The resource
     * is created based on the provided inputs. The relations between the various components are
     * created and handled automatically.
     *
     * @param offeredResourceInput the metadata information to be placed in the resource.
     * @param catalogInput         the catalog which will contain the offered resource.
     * @param representationInput  the representation metadata.
     * @param artifactInput        the artifact information. This includes reference to the data
     *                             or its
     *                             location.
     * @param contractUri          the URI of the existing contract to be added to the resource
     * @return the created offered resource.
     * @throws ApiInteractionUnsuccessfulException if communication with the required endpoints
     *                                             in the connector cannot be made.
     * @throws JsonProcessingException             if unable to parse the inputs and the responses.
     */
    OfferedResourceOutput createResource(OfferedResourceInput offeredResourceInput,
                                         CatalogInput catalogInput,
                                         RepresentationInput representationInput,
                                         ArtifactInput artifactInput, URI contractUri)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException;

    /**
     * Creates an offered resource at the connector sequence with another connector. The resource is
     * created based on the provided inputs. The relations between the various components are
     * created and handled automatically.
     *
     * @param offeredResourceInput the metadata information to be placed in the resource.
     * @param catalogUri           the URI of the existing catalog which will contain the offered
     *                             resource.
     * @param representationInput  the representation metadata.
     * @param artifactInput        the artifact information. This includes reference to the data
     *                             or its location.
     * @param contractInput        the contract information and metadata.
     * @param ruleUris             the access and permission information as URIs of the existing
     *                             rules.
     * @return the created offered resource.
     * @throws ApiInteractionUnsuccessfulException if communication with the required endpoints
     *                                             in the connector cannot be made.
     * @throws JsonProcessingException             if unable to parse the inputs and the responses.
     */
    OfferedResourceOutput createResource(OfferedResourceInput offeredResourceInput, URI catalogUri,
                                         RepresentationInput representationInput,
                                         ArtifactInput artifactInput, ContractInput contractInput,
                                         URI... ruleUris)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException;

    /**
     * Creates an offered resource at the connector sequence with another connector. The resource is
     * created based on the provided inputs. The relations between the various components are
     * created and handled automatically.
     *
     * @param offeredResourceInput the metadata information to be placed in the resource.
     * @param catalogInput         the catalog which will contain the offered resource.
     * @param representationInput  the representation metadata.
     * @param artifactInput        the artifact information. This includes reference to the data
     *                             or its location.
     * @param contractInput        the contract information and metadata.
     * @param ruleUris             the access and permission information as URIs of the existing
     *                             rules.
     * @return the created offered resource.
     * @throws ApiInteractionUnsuccessfulException if communication with the required endpoints
     *                                             in the connector cannot be made.
     * @throws JsonProcessingException             if unable to parse the inputs and the responses.
     */
    OfferedResourceOutput createResource(OfferedResourceInput offeredResourceInput,
                                         CatalogInput catalogInput,
                                         RepresentationInput representationInput,
                                         ArtifactInput artifactInput, ContractInput contractInput,
                                         URI... ruleUris)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException;

}
