/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.workflow;

import java.net.URI;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Resource;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ArtifactOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.RepresentationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ResourceOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;

/**
 * Provides methods for managing backend subscriptions.
 *
 * @author Ronja Quensel
 */
public interface BackendSubscriptionWorkflow {

    /**
     * Subscribes a backend for updates to a resource.
     *
     * @param resource the resource.
     * @param backendLocation URL of the backend where updates will be sent to.
     * @param subscriber identifies the subscriber.
     * @throws ApiInteractionUnsuccessfulException if parsing the request body fails.
     * @throws JsonProcessingException if the connector returns a non 2xx status code.
     */
    void subscribeBackend(ResourceOutput<? extends Resource> resource, URI backendLocation,
                          URI subscriber) throws ApiInteractionUnsuccessfulException,
        JsonProcessingException;

    /**
     * Subscribes a backend for updates to a representation.
     *
     * @param representation the representation.
     * @param backendLocation URL of the backend where updates will be sent to.
     * @param subscriber identifies the subscriber.
     * @throws ApiInteractionUnsuccessfulException if parsing the request body fails.
     * @throws JsonProcessingException if the connector returns a non 2xx status code.
     */
    void subscribeBackend(RepresentationOutput representation, URI backendLocation, URI subscriber)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException;

    /**
     * Subscribes a backend for updates to an artifact.
     *
     * @param artifact the artifact.
     * @param backendLocation URL of the backend where updates will be sent to.
     * @param subscriber identifies the subscriber.
     * @param pushData whether the data should be pushed on update.
     * @throws ApiInteractionUnsuccessfulException if parsing the request body fails.
     * @throws JsonProcessingException if the connector returns a non 2xx status code.
     */
    void subscribeBackend(ArtifactOutput artifact, URI backendLocation, URI subscriber,
                          boolean pushData) throws ApiInteractionUnsuccessfulException,
        JsonProcessingException;

}
