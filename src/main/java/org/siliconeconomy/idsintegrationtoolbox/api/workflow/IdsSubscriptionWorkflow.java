/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.workflow;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ArtifactOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.RepresentationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.RequestedResourceOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.NoParsableResponseException;

/**
 * Provides methods for managing IDS subscriptions.
 *
 * @author Ronja Quensel
 */
public interface IdsSubscriptionWorkflow {

    /**
     * Requests all specified resources and artifacts and subscribes to updates for both, including
     * pushing new data for artifacts.
     *
     * @param recipient the provider connector.
     * @param resourceIds IDs of the resources to request.
     * @param artifactIds IDs of the artifacts to request.
     * @param contractIds IDs of the contract to use for the contract negotiation, should contain
     *                    one applicable contract offer for each specified resource.
     * @throws IOException if parsing the request body fails.
     * @throws NoParsableResponseException if the received metadata cannot be parsed.
     * @throws ApiInteractionUnsuccessfulException if the connector returns a non 2xx status code.
     */
    void requestAndSubscribe(URI recipient, List<URI> resourceIds, List<URI> artifactIds,
                             List<URI> contractIds) throws IOException, NoParsableResponseException,
        ApiInteractionUnsuccessfulException;

    /**
     * Subscribes to updates for a previously requested resource.
     *
     * @param recipient the provider of the resource.
     * @param resource the requested resource.
     * @throws IOException if parsing the request body fails.
     * @throws ApiInteractionUnsuccessfulException if the connector returns a non 2xx status code.
     */
    void subscribe(URI recipient, RequestedResourceOutput resource) throws IOException,
        ApiInteractionUnsuccessfulException;

    /**
     * Subscribes to updates for a previously requested representation.
     *
     * @param recipient the provider of the representation.
     * @param representation the requested representation.
     * @throws IOException if parsing the request body fails.
     * @throws ApiInteractionUnsuccessfulException if the connector returns a non 2xx status code.
     */
    void subscribe(URI recipient, RepresentationOutput representation) throws IOException,
        ApiInteractionUnsuccessfulException;

    /**
     * Subscribes to updates for a previously requested artifact.
     *
     * @param recipient the provider of the artifact.
     * @param artifact the requested artifact.
     * @param pushData whether the data should be pushed on update.
     * @throws IOException if parsing the request body fails.
     * @throws ApiInteractionUnsuccessfulException if the connector returns a non 2xx status code.
     */
    void subscribe(URI recipient, ArtifactOutput artifact, boolean pushData) throws IOException,
        ApiInteractionUnsuccessfulException;

}
