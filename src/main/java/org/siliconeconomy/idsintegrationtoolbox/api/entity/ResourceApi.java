/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.entity;

/**
 * Intermediary interface for the resource API, that delegates to the respective interfaces
 * for managing offered and requested resources.
 *
 * @author Ronja Quensel
 */
public interface ResourceApi {

    /**
     * The API for managing offered resources.
     *
     * @return an API implementation.
     */
    OfferedResourceApi offers();

    /**
     * The API for managing requested resources.
     *
     * @return an API implementation.
     */
    RequestedResourceApi requests();

}
