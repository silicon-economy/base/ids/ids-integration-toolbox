/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.entity;

/**
 * Intermediary interface for managing the relation between contracts and resources, that delegates
 * to the respective interfaces for managing the relation to offered and requested resources.
 *
 * @author Ronja Quensel
 */
public interface ContractResourcesApi {

    /**
     * The API for managing the relation to offered resources.
     *
     * @return an API implementation.
     */
    ContractOfferedResourcesApi offers();

    /**
     * The API for managing the relation to requested resources.
     *
     * @return an API implementation.
     */
    ContractRequestedResourcesApi requests();

}
