/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.entity;

import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Endpoint;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.EndpointEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.EndpointLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.EndpointMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.EndpointOutput;

/**
 * Provides methods for managing endpoints and their relations.
 *
 * @author Ronja Quensel
 */
public interface EndpointApi extends EntityCrudApi<Endpoint, EndpointOutput, EndpointEmbedded,
    EndpointLinks, EndpointMultiOutput> {

    /**
     * Returns the API for managing the relation to data sources.
     *
     * @return an API implementation.
     */
    EndpointDataSourcesApi dataSources();

}
