/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.entity;

import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.ActionType;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.AppMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.AppOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;

/**
 * Provides methods for managing apps and their relations.
 *
 * @author Ronja Quensel
 */
public interface AppApi {

    /**
     * Gets all entities using the default pagination settings of the DSC.
     *
     * @return all entities in the associated class format.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws JsonProcessingException if the response cannot be parsed.
     */
    AppMultiOutput getAll() throws ApiInteractionUnsuccessfulException,
        JsonProcessingException;

    /**
     * Gets all entities using custom pagination information. The page number and size can be
     * specified.
     *
     * @param page the page number to get.
     * @param size the page size to use for pagination.
     * @return all entities in the associated class format.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws JsonProcessingException if the response cannot be parsed.
     */
    AppMultiOutput getAll(Integer page, Integer size)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException;

    /**
     * Gets a single entity.
     *
     * @param id UUID for the requested entity.
     * @return a single entity in the associated class format.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws JsonProcessingException if the response cannot be parsed.
     */
    AppOutput getOne(UUID id) throws ApiInteractionUnsuccessfulException,
        JsonProcessingException;

    /**
     * Deletes a specific entity by ID.
     *
     * @param id UUID of the entity that should be deleted.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     */
    void delete(UUID id) throws ApiInteractionUnsuccessfulException;

    /**
     * Applies an action against a saved app.
     *
     * @param id         UUID of the app that the action is applied to.
     * @param actionType the action type to apply.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     */
    void applyAction(UUID id, ActionType actionType) throws ApiInteractionUnsuccessfulException;

    /**
     * Fetches and returns the app store, from which an app was downloaded.
     *
     * @param appId UUID of the app.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     */
    void appstore(UUID appId) throws ApiInteractionUnsuccessfulException;

    /**
     * Returns the API for managing the relation to endpoints.
     *
     * @return an API implementation.
     */
    AppEndpointsApi endpoints();

}
