/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.entity;

import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Route;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.RouteEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.RouteLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.RouteMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ArtifactOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.RouteOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;

/**
 * Provides methods for managing routes and their relations.
 *
 * @author Ronja Quensel
 */
public interface RouteApi extends EntityCrudApi<Route, RouteOutput, RouteEmbedded, RouteLinks,
    RouteMultiOutput> {

    /**
     * Retrieves the Artifact associated with a route.
     *
     * @param routeId the ID of the route.
     * @return The related Artifact.
     */
    ArtifactOutput getOutput(final UUID routeId) throws ApiInteractionUnsuccessfulException,
        JsonProcessingException;

    /**
     * Returns the API for managing the relation to endpoints.
     *
     * @return an API implementation.
     */
    RouteEndpointsApi endpoints();

    /**
     * Returns the API for managing the relation to sub routes.
     *
     * @return an API implementation.
     */
    RouteSubRoutesApi subRoutes();

}
