/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.entity;

import org.siliconeconomy.idsintegrationtoolbox.model.dsc.RequestedResource;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Subscription;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.SubscriptionEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.RequestedResourceLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.SubscriptionOutput;

/**
 * Provides methods for managing the relation between requested resources and subscriptions.
 *
 * @author Ronja Quensel
 */
public interface RequestedResourceSubscriptionsApi
    extends RestrictedEntityRelationApi<RequestedResource, RequestedResourceLinks, Subscription,
    SubscriptionOutput, SubscriptionEmbedded> {
}
