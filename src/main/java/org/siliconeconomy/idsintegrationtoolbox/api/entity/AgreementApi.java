/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.entity;

import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.AgreementMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.AgreementOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;

/**
 * Provides methods for managing agreements and their relations.
 *
 * @author Ronja Quensel
 */
public interface AgreementApi {

    /**
     * Gets all entities using the default pagination settings of the DSC.
     *
     * @return all entities in the associated class format.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws JsonProcessingException if the response cannot be parsed.
     */
    AgreementMultiOutput getAll() throws ApiInteractionUnsuccessfulException,
        JsonProcessingException;

    /**
     * Gets all entities using custom pagination information. The page number and size can be
     * specified.
     *
     * @param page the page number to get.
     * @param size the page size to use for pagination.
     * @return all entities in the associated class format.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws JsonProcessingException if the response cannot be parsed.
     */
    AgreementMultiOutput getAll(Integer page, Integer size)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException;

    /**
     * Gets a single entity.
     *
     * @param id UUID for the requested entity.
     * @return a single entity in the associated class format.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws JsonProcessingException if the response cannot be parsed.
     */
    AgreementOutput getOne(UUID id) throws ApiInteractionUnsuccessfulException,
        JsonProcessingException;

    /**
     * Returns the API for managing the relation to artifacts.
     *
     * @return an API implementation.
     */
    AgreementArtifactsApi artifacts();

}
