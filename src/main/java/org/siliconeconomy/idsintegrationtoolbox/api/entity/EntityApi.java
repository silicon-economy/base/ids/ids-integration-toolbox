/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.entity;

import org.siliconeconomy.idsintegrationtoolbox.api.workflow.EntityWorkflowApi;

/**
 * Intermediary interface for the entity API, that delegates to the respective interfaces
 * for managing different entities.
 *
 * @author Ronja Quensel
 */
public interface EntityApi {

    /**
     * The API for managing resources.
     *
     * @return an API implementation.
     */
    ResourceApi resources();

    /**
     * The API for managing representations.
     *
     * @return an API implementation.
     */
    RepresentationApi representations();

    /**
     * The API for managing artifacts.
     *
     * @return an API implementation.
     */
    ArtifactApi artifacts();

    /**
     * The API for managing contracts.
     *
     * @return an API implementation.
     */
    ContractApi contracts();

    /**
     * The API for managing rules.
     *
     * @return an API implementation.
     */
    RuleApi rules();

    /**
     * The API for managing catalogs.
     *
     * @return an API implementation.
     */
    CatalogApi catalogs();

    /**
     * The API for managing agreements.
     *
     * @return an API implementation.
     */
    AgreementApi agreements();

    /**
     * The API for managing apps.
     *
     * @return an API implementation.
     */
    AppApi apps();

    /**
     * The API for managing app stores.
     *
     * @return an API implementation.
     */
    AppStoreApi appStores();

    /**
     * The API for managing brokers.
     *
     * @return an API implementation.
     */
    BrokerApi brokers();

    /**
     * The API for managing configurations.
     *
     * @return an API implementation.
     */
    ConfigurationApi configurations();

    /**
     * The API for managing data sources.
     *
     * @return an API implementation.
     */
    DataSourceApi dataSources();

    /**
     * The API for managing endpoints.
     *
     * @return an API implementation.
     */
    EndpointApi endpoints();

    /**
     * The API for managing routes.
     *
     * @return an API implementation.
     */
    RouteApi routes();

    /**
     * The API for managing subscriptions.
     *
     * @return an API implementation.
     */
    SubscriptionApi subscriptions();

    /**
     * The API for entity workflows.
     *
     * @return an API implementation.
     */
    EntityWorkflowApi workflows();

}
