/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.entity;

import java.net.URI;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.AbstractEntity;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.Embedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.Links;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.EntityRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.EntityOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;

/**
 * Interface that is extended by all interfaces for managing a specific entity relation, if all
 * CRUD operations are allowed for that relation.
 *
 * @param <T> the entity that owns the relation.
 * @param <L> links type for the owning entity.
 * @param <D> the related entity.
 * @param <O> the output type for the related entity.
 * @param <E> the embedded type for the related entity.
 * @author Ronja Quensel
 */
public interface EntityRelationApi<T extends AbstractEntity, L extends Links<T>,
    D extends AbstractEntity, O extends EntityOutput<D>, E extends Embedded<D, O>> {

    /**
     * Gets all related entities using the default pagination settings of the DSC.
     *
     * @param id ID of an entity of the first type.
     * @return all linked entities in the associated class format.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws JsonProcessingException if the response cannot be parsed.
     */
    EntityRelationOutput<T, L, D, O, E> getAll(UUID id)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException;

    /**
     * Gets all related entities using custom pagination information. The page number and size
     * can be specified.
     *
     * @param id ID of an entity of the first type.
     * @param page the page number to get.
     * @param size the page size to use for pagination.
     * @return all linked entities in the associated class format.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws JsonProcessingException if the response cannot be parsed.
     */
    EntityRelationOutput<T, L, D, O, E> getAll(UUID id, Integer page, Integer size)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException;

    /**
     * Adds one or more relations.
     *
     * @param id ID of an entity of the first type.
     * @param links list of URIs pointing to the entities that should be linked.
     * @return the entity relation output in the associated class format.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws JsonProcessingException if the response cannot be parsed.
     */
    EntityRelationOutput<T, L, D, O, E> add(UUID id, List<URI> links)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException;

    /**
     * Updates existing relations.
     *
     * @param id ID of an entity of the first type.
     * @param links list of URIs pointing to the entities that should be linked.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws JsonProcessingException if the response cannot be parsed.
     */
    void replace(UUID id, List<URI> links) throws ApiInteractionUnsuccessfulException,
        JsonProcessingException;

    /**
     * Removes relations.
     *
     * @param id ID of an entity of the first type.
     * @param links list of URIs pointing to the entities that should be linked.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws JsonProcessingException if the response cannot be parsed.
     */
    void remove(UUID id, List<URI> links) throws ApiInteractionUnsuccessfulException,
        JsonProcessingException;

}
