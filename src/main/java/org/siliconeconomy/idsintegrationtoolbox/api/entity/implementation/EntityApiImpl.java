/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.entity.implementation;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.AgreementApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.AppApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.AppStoreApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.ArtifactApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.BrokerApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.CatalogApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.ConfigurationApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.ContractApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.DataSourceApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.EndpointApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.EntityApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RepresentationApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.ResourceApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RouteApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RuleApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.SubscriptionApi;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.EntityWorkflowApi;
import org.springframework.stereotype.Service;

/**
 * Implementation of the {@link EntityApi} that exposes the APIs for managing different entities.
 *
 * @author Ronja Quensel
 */
@Service
@RequiredArgsConstructor
@Getter
@Accessors(fluent = true)
public class EntityApiImpl implements EntityApi {

    /** The API for managing resources. */
    private final @NonNull ResourceApi resources;

    /** The API for managing representations. */
    private final @NonNull RepresentationApi representations;

    /** The API for managing artifacts. */
    private final @NonNull ArtifactApi artifacts;

    /** The API for managing agreements. */
    private final @NonNull AgreementApi agreements;

    /** The API for managing contracts. */
    private final @NonNull ContractApi contracts;

    /** The API for managing rules. */
    private final @NonNull RuleApi rules;

    /** The API for managing catalogs. */
    private final @NonNull CatalogApi catalogs;

    /** The API for managing app stores. */
    private final @NonNull AppStoreApi appStores;

    /** The API for managing apps. */
    private final @NonNull AppApi apps;

    /** The API for managing brokers. */
    private final @NonNull BrokerApi brokers;

    /** The API for managing configurations. */
    private final @NonNull ConfigurationApi configurations;

    /** The API for managing data sources. */
    private final @NonNull DataSourceApi dataSources;

    /** The API for managing endpoints. */
    private final @NonNull EndpointApi endpoints;

    /** The API for managing routes. */
    private final @NonNull RouteApi routes;

    /** The API for managing subscriptions. */
    private final @NonNull SubscriptionApi subscriptions;

    /** The API for entity workflows. */
    private final @NonNull EntityWorkflowApi workflows;

}
