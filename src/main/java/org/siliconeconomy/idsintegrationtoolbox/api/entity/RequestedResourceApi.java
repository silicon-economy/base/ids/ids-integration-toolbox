/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.entity;

import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.RequestedResource;
import org.siliconeconomy.idsintegrationtoolbox.model.input.EntityInput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.RequestedResourceMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.RequestedResourceOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;

/**
 * Provides methods for managing requested resources and their relations.
 *
 * @author Ronja Quensel
 */
public interface RequestedResourceApi {

    /**
     * Gets all entities using the default pagination settings of the DSC.
     *
     * @return all entities in the associated class format.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws JsonProcessingException if the response cannot be parsed.
     */
    RequestedResourceMultiOutput getAll() throws ApiInteractionUnsuccessfulException,
        JsonProcessingException;

    /**
     * Gets all entities using custom pagination information. The page number and size can be
     * specified.
     *
     * @param page the page number to get.
     * @param size the page size to use for pagination.
     * @return all entities in the associated class format.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws JsonProcessingException if the response cannot be parsed.
     */
    RequestedResourceMultiOutput getAll(Integer page, Integer size)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException;

    /**
     * Gets a single entity.
     *
     * @param id UUID for the requested entity.
     * @return a single entity in the associated class format.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws JsonProcessingException if the response cannot be parsed.
     */
    RequestedResourceOutput getOne(UUID id) throws ApiInteractionUnsuccessfulException,
        JsonProcessingException;

    /**
     * Updates a specific, existing entity using the given input. All previous information stored
     * with the entity will be overridden.
     *
     * @param id UUID of the entity that should be updated.
     * @param input input for updating the entity.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws JsonProcessingException if the response cannot be parsed.
     */
    void update(UUID id, EntityInput<RequestedResource> input)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException;

    /**
     * Deletes a specific entity by ID.
     *
     * @param id UUID of the entity that should be deleted.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     */
    void delete(UUID id) throws ApiInteractionUnsuccessfulException;

    /**
     * Returns the API for managing the relation to catalogs.
     *
     * @return an API implementation.
     */
    RequestedResourceCatalogsApi catalogs();

    /**
     * Returns the API for managing the relation to contracts.
     *
     * @return an API implementation.
     */
    RequestedResourceContractsApi contracts();

    /**
     * Returns the API for managing the relation to representations.
     *
     * @return an API implementation.
     */
    RequestedResourceRepresentationsApi representations();

    /**
     * Returns the API for managing the relation to subscriptions.
     *
     * @return an API implementation.
     */
    RequestedResourceSubscriptionsApi subscriptions();

}
