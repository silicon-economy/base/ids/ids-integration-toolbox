/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.entity;

import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Agreement;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Artifact;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.ArtifactEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.AgreementLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ArtifactOutput;

/**
 * Provides methods for managing the relation between agreements and artifacts.
 *
 * @author Ronja Quensel
 */
public interface AgreementArtifactsApi extends RestrictedEntityRelationApi<Agreement,
    AgreementLinks, Artifact, ArtifactOutput, ArtifactEmbedded> {
}
