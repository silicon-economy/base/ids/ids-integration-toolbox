/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.entity.implementation;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RepresentationOfferedResourcesApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RepresentationRequestedResourcesApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RepresentationResourcesApi;
import org.springframework.stereotype.Service;

/**
 * Implementation of the {@link RepresentationResourcesApi} that exposes the APIs for managing the
 * relations between representations and offered resources and representations and requested
 * resources.
 *
 * @author Ronja Quensel
 */
@Service
@RequiredArgsConstructor
@Getter
@Accessors(fluent = true)
public class RepresentationResourcesApiImpl implements RepresentationResourcesApi {

    /** The API for managing the relation to offered resources. */
    private final @NonNull RepresentationOfferedResourcesApi offers;

    /** The API for managing the relation to requested resources. */
    private final @NonNull RepresentationRequestedResourcesApi requests;

}
