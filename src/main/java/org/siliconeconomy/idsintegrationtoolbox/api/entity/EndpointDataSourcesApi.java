/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.entity;

import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;

/**
 * Provides methods for managing the relation between endpoints and data sources.
 *
 * @author Ronja Quensel
 */
public interface EndpointDataSourcesApi {

    /**
     * Adds an endpoint datasource relation.
     *
     * @param endpointId ID of an endpoint.
     * @param datasourceId the id of a datasource to be linked.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws JsonProcessingException if the response cannot be parsed.
     */
    void add(UUID endpointId, UUID datasourceId) throws ApiInteractionUnsuccessfulException,
        JsonProcessingException;
}
