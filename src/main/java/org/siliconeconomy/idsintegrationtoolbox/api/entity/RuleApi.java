/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.entity;

import org.siliconeconomy.idsintegrationtoolbox.model.dsc.ContractRule;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.ContractRuleEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ContractRuleLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.ContractRuleMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ContractRuleOutput;

/**
 * Provides methods for managing rules and their relations.
 *
 * @author Ronja Quensel
 */
public interface RuleApi
    extends EntityCrudApi<ContractRule, ContractRuleOutput, ContractRuleEmbedded,
    ContractRuleLinks, ContractRuleMultiOutput> {

    /**
     * Returns the API for managing the relation to contracts.
     *
     * @return an API implementation.
     */
    RuleContractsApi contracts();

}
