/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.entity;

import org.siliconeconomy.idsintegrationtoolbox.model.dsc.OfferedResource;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Representation;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.OfferedResourceEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.RepresentationLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.OfferedResourceOutput;

/**
 * Provides methods for managing the relation between representations and offered resources.
 *
 * @author Ronja Quensel
 */
public interface RepresentationOfferedResourcesApi extends EntityRelationApi<Representation,
    RepresentationLinks, OfferedResource, OfferedResourceOutput, OfferedResourceEmbedded> {
}
