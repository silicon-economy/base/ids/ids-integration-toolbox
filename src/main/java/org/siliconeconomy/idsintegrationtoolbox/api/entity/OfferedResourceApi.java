/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.entity;

import org.siliconeconomy.idsintegrationtoolbox.model.dsc.OfferedResource;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.OfferedResourceEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.OfferedResourceLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.OfferedResourceMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.OfferedResourceOutput;

/**
 * Provides methods for managing offered resources and their relations.
 *
 * @author Ronja Quensel
 */
public interface OfferedResourceApi
    extends EntityCrudApi<OfferedResource, OfferedResourceOutput, OfferedResourceEmbedded,
    OfferedResourceLinks, OfferedResourceMultiOutput> {

    /**
     * Returns the API for managing the relation to catalogs.
     *
     * @return an API implementation.
     */
    OfferedResourceCatalogsApi catalogs();

    /**
     * Returns the API for managing the relation to contracts.
     *
     * @return an API implementation.
     */
    OfferedResourceContractsApi contracts();

    /**
     * Returns the API for managing the relation to representations.
     *
     * @return an API implementation.
     */
    OfferedResourceRepresentationsApi representations();

    /**
     * Returns the API for managing the relation to subscriptions.
     *
     * @return an API implementation.
     */
    OfferedResourceSubscriptionsApi subscriptions();

    /**
     * Returns the API for managing the relation to brokers.
     *
     * @return an API implementation.
     */
    OfferedResourceBrokersApi brokers();

}
