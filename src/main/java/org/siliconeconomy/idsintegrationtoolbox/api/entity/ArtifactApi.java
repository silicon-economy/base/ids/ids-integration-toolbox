/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.entity;

import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Artifact;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.ArtifactEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ArtifactLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.ArtifactMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ArtifactOutput;

/**
 * Provides methods for managing artifacts and their relations.
 *
 * @author Ronja Quensel
 */
public interface ArtifactApi extends EntityCrudApi<Artifact, ArtifactOutput, ArtifactEmbedded,
    ArtifactLinks, ArtifactMultiOutput> {

    /**
     * Returns the API for managing data.
     *
     * @return an API implementation.
     */
    DataApi data();

    /**
     * Returns the API for managing the relation to agreements.
     *
     * @return an API implementation.
     */
    ArtifactAgreementsApi agreements();

    /**
     * Returns the API for managing the relation to representations.
     *
     * @return an API implementation.
     */
    ArtifactRepresentationsApi representations();

    /**
     * Returns the API for managing the relation to subscriptions.
     *
     * @return an API implementation.
     */
    ArtifactSubscriptionsApi subscriptions();

}
