/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.entity;

import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.AbstractEntity;
import org.siliconeconomy.idsintegrationtoolbox.model.input.EntityInput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.Embedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.Links;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.EntityMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.EntityOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;

/**
 * Interface that is extended by all interfaces for managing a specific entity, if all CRUD
 * operations are allowed for that entity.
 *
 * @param <T> the type of entity.
 * @param <O> the output type returned for a single entity.
 * @param <E> the model of the embedded entry in the dataspace connector.
 * @param <L> the model of links returned by the dataspace connector.
 * @param <M> the output type returned for multiple entities.
 * @author Ronja Quensel
 */
public interface EntityCrudApi<T extends AbstractEntity, O extends EntityOutput<T>,
    E extends Embedded<T, O>, L extends Links<T>, M extends EntityMultiOutput<T, O, E, L>> {

    /**
     * Gets all entities using the default pagination settings of the DSC.
     *
     * @return all entities in the associated class format.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws JsonProcessingException if the response cannot be parsed.
     */
    M getAll() throws ApiInteractionUnsuccessfulException,
        JsonProcessingException;

    /**
     * Gets all entities using custom pagination information. The page number and size can be
     * specified.
     *
     * @param page the page number to get.
     * @param size the page size to use for pagination.
     * @return all entities in the associated class format.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws JsonProcessingException if the response cannot be parsed.
     */
    M getAll(Integer page, Integer size)
        throws ApiInteractionUnsuccessfulException, JsonProcessingException;

    /**
     * Gets a single entity.
     *
     * @param id UUID for the requested entity.
     * @return a single entity in the associated class format.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws JsonProcessingException if the response cannot be parsed.
     */
    O getOne(UUID id) throws ApiInteractionUnsuccessfulException,
        JsonProcessingException;

    /**
     * Creates a new entity.
     *
     * @param input input for creating the entity.
     * @return a representation of the created resource, if successful.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws JsonProcessingException if the response cannot be parsed.
     */
    O create(EntityInput<T> input) throws ApiInteractionUnsuccessfulException,
        JsonProcessingException;

    /**
     * Updates a specific, existing entity using the given input. All previous information stored
     * with the entity will be overridden.
     *
     * @param id UUID of the entity that should be updated.
     * @param input input for updating the entity.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws JsonProcessingException if the response cannot be parsed.
     */
    void update(UUID id, EntityInput<T> input) throws ApiInteractionUnsuccessfulException,
        JsonProcessingException;

    /**
     * Deletes a specific entity by ID.
     *
     * @param id UUID of the entity that should be deleted.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     */
    void delete(UUID id) throws ApiInteractionUnsuccessfulException;

}
