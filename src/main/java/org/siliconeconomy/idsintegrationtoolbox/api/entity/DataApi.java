/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.entity;

import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.QueryInput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.ResourceData;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;

/**
 * Provides methods for managing data related to artifacts.
 *
 * @author Ronja Quensel
 */
public interface DataApi {

    /**
     * Uploads data for a specific artifact.
     *
     * @param artifactId the artifact ID.
     * @param data the data string.
     * @throws ApiInteractionUnsuccessfulException if a response code other than 2xx is returned.
     */
    void upload(UUID artifactId, String data) throws ApiInteractionUnsuccessfulException;

    /**
     * Gets the data of a specific artifact, optionally using query parameters, path variables
     * and headers from the {@link QueryInput} in case of remote data.
     *
     * @param artifactId the artifact ID.
     * @param queryInput the QueryInput.
     * @param routeIds a list of route ids.
     * @return the data.
     * @throws JsonProcessingException if the query input cannot be parsed to JSON.
     * @throws IOException if the response body cannot be read or the query input cannot be parsed
     *                     to JSON.
     * @throws ApiInteractionUnsuccessfulException if a response code other than 2xx is returned.
     */
    ResourceData get(UUID artifactId, QueryInput queryInput, List<URI> routeIds) throws IOException,
        ApiInteractionUnsuccessfulException;

    /**
     * Gets the data of a specific artifact. If the artifact is part of a requested resource, a
     * contract agreement URI can be specified that will be used when the data is requested from
     * the owning connector. If download is set to false, the Dataspace Connector will try to find
     * the data locally; if download is set to true, the data will always be downloaded from the
     * backend/owning connector, even if it is already present in the local database.
     *
     * @param artifactId the artifact ID.
     * @param download whether the data should be downloaded.
     * @param agreement URI of the contract agreement to use when requesting the data (optional).
     * @param routeIds a list of route ids.
     * @param params map of query parameters to use for the request.
     * @param headers map of headers to use for the request.
     * @param path additional path to use for the request.
     * @return the data.
     * @throws ApiInteractionUnsuccessfulException if a response code other than 2xx is returned.
     */
    ResourceData get(UUID artifactId, boolean download, URI agreement, List<URI> routeIds,
                     Map<String, String> params, Map<String, String> headers, String path)
        throws ApiInteractionUnsuccessfulException;

}
