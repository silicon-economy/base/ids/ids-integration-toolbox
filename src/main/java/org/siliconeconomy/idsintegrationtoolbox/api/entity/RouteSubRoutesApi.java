/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.entity;

import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Route;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.RouteEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.RouteLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.RouteOutput;

/**
 * Provides methods for managing the relation between routes and their sub routes.
 *
 * @author Ronja Quensel
 */
public interface RouteSubRoutesApi
    extends EntityRelationApi<Route, RouteLinks, Route, RouteOutput, RouteEmbedded> {
}
