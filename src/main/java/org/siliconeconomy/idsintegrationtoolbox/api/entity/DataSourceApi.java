/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.entity;

import org.siliconeconomy.idsintegrationtoolbox.model.dsc.DataSource;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.DataSourceEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.DataSourceLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.DataSourceMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.DataSourceOutput;

/**
 * Provides methods for managing data sources.
 *
 * @author Ronja Quensel
 */
public interface DataSourceApi
    extends EntityCrudApi<DataSource, DataSourceOutput, DataSourceEmbedded,
    DataSourceLinks, DataSourceMultiOutput> {
}
