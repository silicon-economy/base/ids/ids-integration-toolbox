/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.entity;

import org.siliconeconomy.idsintegrationtoolbox.model.dsc.App;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Endpoint;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.EndpointEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.AppLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.EndpointOutput;

/**
 * Provides methods for managing the relation between apps and endpoints.
 *
 * @author Ronja Quensel
 */
public interface AppEndpointsApi extends RestrictedEntityRelationApi<App, AppLinks, Endpoint,
    EndpointOutput, EndpointEmbedded> {
}
