/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.entity;

import org.siliconeconomy.idsintegrationtoolbox.model.dsc.AppStore;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.AppStoreEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.AppStoreLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.AppStoreMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.AppStoreOutput;

/**
 * Provides methods for managing app stores and their relations.
 *
 * @author Ronja Quensel
 */
public interface AppStoreApi extends EntityCrudApi<AppStore, AppStoreOutput, AppStoreEmbedded,
    AppStoreLinks, AppStoreMultiOutput> {

    /**
     * Returns the API for managing the relation to apps.
     *
     * @return an API implementation.
     */
    AppStoreAppsApi apps();

}
