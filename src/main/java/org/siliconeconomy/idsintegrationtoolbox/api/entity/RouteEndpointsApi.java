/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.entity;

import java.net.URI;
import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;

/**
 * Provides methods for managing the relation between routes and endpoints.
 *
 * @author Ronja Quensel
 */
public interface RouteEndpointsApi {

    /**
     * Sets the route start to the given endpoint, or removes the start if endpointId is null.
     *
     * @param routeId the route ID.
     * @param endpointId the endpoint ID, may be null.
     * @throws ApiInteractionUnsuccessfulException if the DSC returns a non 2xx status code.
     * @throws JsonProcessingException if parsing the response fails.
     */
    void start(UUID routeId, URI endpointId) throws ApiInteractionUnsuccessfulException,
        JsonProcessingException;

    /**
     * Sets the route end to the given endpoint, or removes the end if endpointId is null.
     *
     * @param routeId the route ID.
     * @param endpointId the endpoint ID, may be null.
     * @throws ApiInteractionUnsuccessfulException if the DSC returns a non 2xx status code.
     * @throws JsonProcessingException if parsing the response fails.
     */
    void end(UUID routeId, URI endpointId) throws ApiInteractionUnsuccessfulException,
        JsonProcessingException;

}
