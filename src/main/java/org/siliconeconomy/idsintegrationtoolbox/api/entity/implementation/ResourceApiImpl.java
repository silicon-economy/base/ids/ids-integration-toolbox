/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.entity.implementation;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.OfferedResourceApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RequestedResourceApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.ResourceApi;
import org.springframework.stereotype.Service;

/**
 * Implementation of the {@link ResourceApi} that exposes the APIs for offered and requested
 * resources.
 *
 * @author Ronja Quensel
 */
@Service
@RequiredArgsConstructor
@Getter
@Accessors(fluent = true)
public class ResourceApiImpl implements ResourceApi {

    /** The API for managing offered resources. */
    private final @NonNull OfferedResourceApi offers;

    /** The API for managing requested resources. */
    private final @NonNull RequestedResourceApi requests;

}
