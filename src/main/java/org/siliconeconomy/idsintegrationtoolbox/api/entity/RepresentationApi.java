/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.entity;

import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Representation;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.RepresentationEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.RepresentationLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.RepresentationMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.RepresentationOutput;

/**
 * Provides methods for managing representations and their relations.
 *
 * @author Ronja Quensel
 */
public interface RepresentationApi
    extends EntityCrudApi<Representation, RepresentationOutput, RepresentationEmbedded,
    RepresentationLinks, RepresentationMultiOutput> {

    /**
     * Returns the API for managing the relation to artifacts.
     *
     * @return an API implementation.
     */
    RepresentationArtifactsApi artifacts();

    /**
     * Returns the API for managing the relation to resources.
     *
     * @return an API implementation.
     */
    RepresentationResourcesApi resources();

    /**
     * Returns the API for managing the relation to subscriptions.
     *
     * @return an API implementation.
     */
    RepresentationSubscriptionsApi subscriptions();

}
