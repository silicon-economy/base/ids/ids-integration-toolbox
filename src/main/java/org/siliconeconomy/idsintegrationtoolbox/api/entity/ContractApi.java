/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.entity;

import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Contract;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.ContractEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ContractLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.ContractMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ContractOutput;

/**
 * Provides methods for managing contracts and their relations.
 *
 * @author Ronja Quensel
 */
public interface ContractApi extends EntityCrudApi<Contract, ContractOutput, ContractEmbedded,
    ContractLinks, ContractMultiOutput> {

    /**
     * Returns the API for managing the relation to resources.
     *
     * @return an API implementation.
     */
    ContractResourcesApi resources();

    /**
     * Returns the API for managing the relation to rules.
     *
     * @return an API implementation.
     */
    ContractRulesApi rules();

}
