/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.entity;

import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Broker;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.BrokerEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.BrokerLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.BrokerMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.BrokerOutput;

/**
 * Provides methods for managing brokers and their relations.
 *
 * @author Ronja Quensel
 */
public interface BrokerApi extends EntityCrudApi<Broker, BrokerOutput, BrokerEmbedded, BrokerLinks,
    BrokerMultiOutput> {

    /**
     * Returns the API for managing the relation to offered resources.
     *
     * @return an API implementation.
     */
    BrokerOfferedResourcesApi offers();

}
