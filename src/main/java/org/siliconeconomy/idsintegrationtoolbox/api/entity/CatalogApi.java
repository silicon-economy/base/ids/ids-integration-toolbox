/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.entity;

import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Catalog;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.CatalogEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.CatalogLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.CatalogMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.CatalogOutput;

/**
 * Provides methods for managing catalogs and their relations.
 *
 * @author Ronja Quensel
 */
public interface CatalogApi extends EntityCrudApi<Catalog, CatalogOutput,
    CatalogEmbedded, CatalogLinks, CatalogMultiOutput> {

    /**
     * Returns the API for managing the relation to resources.
     *
     * @return an API implementation.
     */
    CatalogOfferedResourcesApi offers();

}
