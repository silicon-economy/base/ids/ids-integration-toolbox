/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.entity;

import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Configuration;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.ConfigurationEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ConfigurationLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.ConfigurationMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ConfigurationOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;

/**
 * Provides methods for managing configurations.
 *
 * @author Ronja Quensel
 */
public interface ConfigurationApi
    extends EntityCrudApi<Configuration, ConfigurationOutput, ConfigurationEmbedded,
    ConfigurationLinks, ConfigurationMultiOutput> {

    ConfigurationOutput active() throws ApiInteractionUnsuccessfulException,
        JsonProcessingException;

    void active(UUID id) throws ApiInteractionUnsuccessfulException;

}
