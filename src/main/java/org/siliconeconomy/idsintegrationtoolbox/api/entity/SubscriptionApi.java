/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.entity;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Subscription;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.SubscriptionEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.SubscriptionLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.SubscriptionMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.SubscriptionOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;

/**
 * Provides methods for managing subscriptions and their relations.
 *
 * @author Ronja Quensel
 */
public interface SubscriptionApi
    extends EntityCrudApi<Subscription, SubscriptionOutput, SubscriptionEmbedded,
    SubscriptionLinks, SubscriptionMultiOutput> {

    /**
     * Returns all subscriptions owned by this connector.
     *
     * @return all owned subscriptions.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws JsonProcessingException if the response cannot be parsed.
     */
    SubscriptionMultiOutput owned() throws ApiInteractionUnsuccessfulException,
        JsonProcessingException;

}
