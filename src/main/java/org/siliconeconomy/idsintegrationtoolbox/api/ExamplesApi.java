/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api;

import java.io.IOException;

import de.fraunhofer.iais.eis.Rule;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.PolicyPattern;
import org.siliconeconomy.idsintegrationtoolbox.model.input.policy.PolicyInput;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.springframework.web.client.RestClientException;

/**
 * Provides methods for using the examples API of the DSC. This includes obtaining an example
 * policy for a given pattern and validating a policy.
 *
 * @author Ronja Quensel
 */
public interface ExamplesApi {

    /**
     * Gets an example policy for a given policy pattern.
     *
     * @param policyInput the policy pattern.
     * @return an Information Model rule of the specified pattern.
     * @throws RestClientException                 if sending the request fails.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws IOException                         if deserializing the rule fails.
     */
    Rule policy(PolicyInput policyInput) throws RestClientException,
        ApiInteractionUnsuccessfulException, IOException;

    /**
     * Gets the policy pattern represented by a given JSON string.
     *
     * @param policy the JSON string representing a policy.
     * @return the policy pattern represented by the JSON string.
     * @throws RestClientException                 if sending the request fails.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     */
    PolicyPattern policyValidation(String policy) throws RestClientException,
        ApiInteractionUnsuccessfulException;

}
