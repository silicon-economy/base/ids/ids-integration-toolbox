/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.ids;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import de.fraunhofer.iais.eis.InfrastructureComponent;
import de.fraunhofer.iais.eis.Rule;
import org.siliconeconomy.idsintegrationtoolbox.model.input.SubscriptionInput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.AgreementOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.AppOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.InvalidInputException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.NoParsableResponseException;
import org.springframework.web.client.RestClientException;

/**
 * Provides the methods for sending different types of IDS messages.
 *
 * @author Ronja Quensel
 */
public interface MessagesApi {

    /**
     * Sends a DownloadAppMessage to the specified recipient, which downloads an IDS App from the
     * AppStore.
     *
     * @param recipient the recipient of the message.
     * @param appId     the app to download.
     * @return the response of the connector if request successful.
     */
    AppOutput appDownload(URI recipient, URI appId)
        throws RestClientException, ApiInteractionUnsuccessfulException, JsonProcessingException;

    /**
     * Sends a ConnectorUnavailableMessage to the specified recipient.
     *
     * @param recipient the recipient of the message.
     * @throws RestClientException                 if sending the request fails.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     */
    void connectorUnavailable(URI recipient)
        throws RestClientException, ApiInteractionUnsuccessfulException;

    /**
     * Sends a ConnectorUpdateMessage to the specified recipient.
     *
     * @param recipient the recipient of the message.
     * @throws RestClientException                 if sending the request fails.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     */
    void connectorUpdate(URI recipient)
        throws RestClientException, ApiInteractionUnsuccessfulException;

    /**
     * Sends a ContractRequestMessage to the specified recipient. When the respective endpoint
     * is called at the DSC, it will also download the metadata for the resources and artifacts
     * and optionally download the associated data.
     *
     * @param recipient   the recipient of the message.
     * @param resourceIds list of resource IDs that should be requested.
     * @param artifactIds list of artifact IDs that should be requested.
     * @param download    indicates whether the connector should automatically download the data of
     *                    the artifacts.
     * @param rules       Information Model representation of all rules that should be part of the
     *                    contract.
     * @return the response of the connector if request successful
     * @throws RestClientException                 if sending the request fails.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws IOException                         if a rule cannot be parsed.
     * @throws InvalidInputException               if any of the list parameters is null or empty.
     */
    AgreementOutput contractNegotiation(URI recipient, List<URI> resourceIds, List<URI> artifactIds,
                                        boolean download, List<Rule> rules)
        throws RestClientException, ApiInteractionUnsuccessfulException, IOException,
        InvalidInputException;

    /**
     * Sends a DescriptionRequestMessage to the specified recipient.
     *
     * @param recipient the recipient of the message.
     * @return the response of the connector if request successful
     * @throws RestClientException                 if sending the request fails.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws IOException                         if deserializing the response fails.
     */
    InfrastructureComponent descriptionRequest(URI recipient)
        throws ApiInteractionUnsuccessfulException, IOException;

    /**
     * Sends a DescriptionRequestMessage to the specified recipient.
     *
     * @param recipient the recipient of the message.
     * @param elementId the ID of the requested element.
     * @return Information Model object representing the response of the connector if request
     *         successful.
     * @throws RestClientException                 if sending the request fails.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws NoParsableResponseException         if deserializing the response fails.
     */
    Object descriptionRequest(URI recipient, URI elementId) throws RestClientException,
        ApiInteractionUnsuccessfulException, NoParsableResponseException;

    /**
     * Sends a QueryMessage to the specified recipient.
     *
     * @param recipient the recipient of the message.
     * @param query     the query to be sent.
     * @return the response of the connector if request successful.
     * @throws RestClientException                 if sending the request fails.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     */
    String query(URI recipient, String query) throws RestClientException,
        ApiInteractionUnsuccessfulException;

    /**
     * Sends a ResourceUnavailableMessage to the specified recipient.
     *
     * @param recipient  the recipient of the message.
     * @param resourceId the ID of the resource to be updated.
     * @throws RestClientException                 if sending the request fails.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     */
    void resourceUnavailable(URI recipient, URI resourceId)
        throws RestClientException, ApiInteractionUnsuccessfulException;

    /**
     * Sends a ResourceUpdateMessage to the specified recipient.
     *
     * @param recipient  the recipient of the message.
     * @param resourceId the ID of the resource to be updated.
     * @throws RestClientException                 if sending the request fails.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     */
    void resourceUpdate(URI recipient, URI resourceId)
        throws RestClientException, ApiInteractionUnsuccessfulException;

    /**
     * Sends a SearchMessage to the specified recipient, which results in a full text search
     * being performed.
     *
     * @param recipient the recipient of the message.
     * @param body      the search string.
     * @param limit     the total amount of results to fetch.
     * @param offset    the offset of the results, used for pagination.
     * @return the response of the connector if request successful.
     */
    String search(URI recipient, String body, int limit, int offset)
        throws RestClientException, ApiInteractionUnsuccessfulException;

    /**
     * Sends a SubscribeMessage to the specified recipient.
     *
     * @param recipient the recipient of the message.
     * @param input the body including the subscription message.
     * @return the response of the connector if request successful.
     * @throws RestClientException                 if sending the request fails.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws IOException                         if parsing the payload fails.
     */
    String subscribe(URI recipient, SubscriptionInput input)
        throws RestClientException, ApiInteractionUnsuccessfulException, IOException;

    /**
     * Sends a UnsubscribeMessage to the specified recipient.
     *
     * @param recipient the recipient of the message.
     * @param elementId the element to unsubscribe from.
     * @return the response of the connector if request successful.
     */
    String unsubscribe(URI recipient, URI elementId)
        throws RestClientException, ApiInteractionUnsuccessfulException;

    /**
     * Notifies all subscribers.
     *
     * @param elementId the element to notify.
     */
    void notify(URI elementId)
        throws RestClientException, ApiInteractionUnsuccessfulException;

}
