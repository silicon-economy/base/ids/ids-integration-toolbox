/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.ids.implementation;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import org.siliconeconomy.idsintegrationtoolbox.api.ids.IdsApi;
import org.siliconeconomy.idsintegrationtoolbox.api.ids.MessagesApi;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.IdsWorkflowApi;
import org.springframework.stereotype.Service;

/**
 * Implementation of the {@link IdsApi} that exposes the APIs for sending IDS messages and using
 * provided IDS workflows.
 *
 * @author Ronja Quensel
 */
@Service
@RequiredArgsConstructor
@Getter
@Accessors(fluent = true)
public class IdsApiImpl implements IdsApi {

    /** The API for sending IDS messages. */
    private final @NonNull MessagesApi messages;

    /** The API for using IDS workflows. */
    private final @NonNull IdsWorkflowApi workflows;

}
