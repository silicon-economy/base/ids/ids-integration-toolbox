/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.ids;

import org.siliconeconomy.idsintegrationtoolbox.api.workflow.IdsWorkflowApi;

/**
 * Intermediary interface for the IDS API, that delegates to the respective interfaces
 * for sending IDS messages and using provided IDS workflows.
 *
 * @author Ronja Quensel
 */
public interface IdsApi {

    /**
     * The API for sending IDS messages.
     *
     * @return an API implementation.
     */
    MessagesApi messages();

    /**
     * The API for using IDS workflows.
     *
     * @return an API implementation.
     */
    IdsWorkflowApi workflows();

}
