/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.connector.implementation;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import org.siliconeconomy.idsintegrationtoolbox.api.connector.ConnectorApi;
import org.siliconeconomy.idsintegrationtoolbox.api.connector.ConnectorSettingsApi;
import org.siliconeconomy.idsintegrationtoolbox.api.connector.SelfDescriptionApi;
import org.springframework.stereotype.Service;

/**
 * Implementation of the {@link ConnectorApi} that exposes the APIs for obtaining self descriptions
 * and managing the connector settings.
 *
 * @author Ronja Quensel
 */
@Service
@RequiredArgsConstructor
@Getter
@Accessors(fluent = true)
public class ConnectorApiImpl implements ConnectorApi {

    /** The self description API. */
    private final @NonNull SelfDescriptionApi selfDescription;

    /** The connector settings API. */
    private final @NonNull ConnectorSettingsApi settings;

}
