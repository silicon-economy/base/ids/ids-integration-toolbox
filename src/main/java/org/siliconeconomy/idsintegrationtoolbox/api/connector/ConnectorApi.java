/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.connector;

/**
 * Intermediary interface for the DSC's connector API, that delegates to the respective interfaces
 * for obtaining the DSC's self description and managing its settings.
 *
 * @author Ronja Quensel
 */
public interface ConnectorApi {

    /**
     * The API for getting self descriptions.
     *
     * @return an API implementation.
     */
    SelfDescriptionApi selfDescription();

    /**
     * The API for managing connector settings.
     *
     * @return an API implementation.
     */
    ConnectorSettingsApi settings();

}
