/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.connector;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.springframework.web.client.RestClientException;

/**
 * Provides methods for managing the connector settings for contract negotiation and unsupported
 * policy patterns.
 *
 * @author Ronja Quensel
 */
public interface ConnectorSettingsApi {

    /**
     * Sets the contract negotiation status of the connector, i.e. whether or not contract
     * negotiation is enabled.
     *
     * @param status the new policy negotiation status.
     * @return the response of the connector if request successful.
     * @throws RestClientException                 if sending the request fails.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws JsonProcessingException             if parsing the response fails.
     */
    boolean negotiation(boolean status) throws RestClientException,
        ApiInteractionUnsuccessfulException, JsonProcessingException;

    /**
     * Gets the contract negotiation status of the connector, i.e. whether or not contract
     * negotiation is enabled.
     *
     * @return the response of the connector if request successful.
     * @throws RestClientException                 if sending the request fails.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws JsonProcessingException             if parsing the response fails.
     */
    boolean negotiation() throws RestClientException, ApiInteractionUnsuccessfulException,
        JsonProcessingException;

    /**
     * Sets the unsupported patterns status of the connector, i.e. whether or not unknown policy
     * patterns should be ignored.
     *
     * @param status the new unsupported pattern status.
     * @return the response of the connector if request successful.
     * @throws RestClientException                 if sending the request fails.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws JsonProcessingException             if parsing the response fails.
     */
    boolean unsupportedPatterns(boolean status) throws RestClientException,
        ApiInteractionUnsuccessfulException, JsonProcessingException;

    /**
     * Gets the unsupported patterns status of the connector, i.e. whether or not unknown policy
     * patterns should be ignored.
     *
     * @return the response of the connector if request successful.
     * @throws RestClientException                 if sending the request fails.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws JsonProcessingException             if parsing the response fails.
     */
    boolean unsupportedPatterns() throws RestClientException,
        ApiInteractionUnsuccessfulException, JsonProcessingException;

}
