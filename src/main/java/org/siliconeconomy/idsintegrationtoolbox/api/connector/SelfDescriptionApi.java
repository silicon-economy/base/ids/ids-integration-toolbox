/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.api.connector;

import java.io.IOException;

import de.fraunhofer.iais.eis.Connector;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.springframework.web.client.RestClientException;

/**
 * Provides methods for obtaining the public and private self description of the DSC.
 *
 * @author Ronja Quensel
 */
public interface SelfDescriptionApi {

    /**
     * Gets the connector's public self-description.
     *
     * @return the response of the connector if request successful
     * @throws RestClientException if sending the request fails.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws IOException if deserializing the response fails.
     */
    Connector getPublic() throws RestClientException, ApiInteractionUnsuccessfulException,
        IOException;

    /**
     * Gets the connector's private self-description.
     *
     * @return the response of the connector if request successful
     * @throws RestClientException if sending the request fails.
     * @throws ApiInteractionUnsuccessfulException if the response code is not 2xx.
     * @throws IOException if deserializing the response fails.
     */
    Connector getPrivate()  throws RestClientException, ApiInteractionUnsuccessfulException,
        IOException;

}
