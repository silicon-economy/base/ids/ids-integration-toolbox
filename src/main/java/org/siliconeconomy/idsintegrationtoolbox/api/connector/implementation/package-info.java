/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
/**
 * Provides implementations for interfaces of the connector API, that just delegate to other
 * interfaces.
 */

package org.siliconeconomy.idsintegrationtoolbox.api.connector.implementation;
