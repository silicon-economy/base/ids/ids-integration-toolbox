/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.config;

import java.io.IOException;

import lombok.NonNull;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;

/**
 * Performs error handling for the RestTemplate.
 *
 * @author Ronja Quensel
 */
public class RestTemplateErrorHandler implements ResponseErrorHandler {

    /**
     * Always returns false, as errors are handled by the ErrorCodeGenerator interface instead of
     * in the RestTemplate.
     *
     * @param clientHttpResponse the response.
     * @return false.
     * @throws IOException never.
     */
    @Override
    public boolean hasError(final @NonNull ClientHttpResponse clientHttpResponse)
            throws IOException {
        return false;
    }

    /**
     * Intentionally empty, as errors are handled outside the RestTemplate.
     *
     * @param clientHttpResponse the response.
     * @throws IOException never.
     */
    @Override
    public void handleError(final @NonNull ClientHttpResponse clientHttpResponse)
            throws IOException {
        // Intentionally empty.
    }
}
