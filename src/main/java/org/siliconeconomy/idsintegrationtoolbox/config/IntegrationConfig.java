/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.config;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import de.fraunhofer.iais.eis.ids.jsonld.Serializer;
import lombok.NoArgsConstructor;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.ResourceUtils;
import org.springframework.web.client.RestTemplate;


/**
 * Configuration that provides commonly used classes as beans.
 *
 * @author Haydar Qarawlus
 */
@NoArgsConstructor
@Configuration
public class IntegrationConfig {

    /**
     * Location of the truststore.
     */
    @Value("${http.client.ssl.trust-store:}")
    private String trustStoreLocation;

    /**
     * Password of the truststore.
     */
    @Value("${http.client.ssl.trust-store-password:}")
    private String trustStorePassword;

    /**
     * Creates a {@link RestTemplate} bean using the truststore specified by properties.
     *
     * @return the RestTemplate
     * @throws IOException if the truststore file cannot be found.
     * @throws CertificateException if there is a problem with a certificate.
     * @throws NoSuchAlgorithmException if the cryptographic algorithm required for the truststore
     *                                  is not available.
     * @throws KeyStoreException if there is a problem with the truststore.
     * @throws KeyManagementException if there is a problem with the key.
     */
    @Bean
    @Primary
    public RestTemplate restTemplate() throws IOException, CertificateException,
            NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        RestTemplate restTemplate;
        if (trustStoreLocation != null && !trustStoreLocation.isBlank()) {
            final var sslContext = new SSLContextBuilder()
                .loadTrustMaterial(ResourceUtils.getURL(trustStoreLocation),
                    trustStorePassword.toCharArray())
                .build();

            final var socketFactory = new SSLConnectionSocketFactory(sslContext);
            final var httpClient = HttpClients.custom().setSSLSocketFactory(socketFactory).build();
            final var factory = new HttpComponentsClientHttpRequestFactory(httpClient);

            restTemplate = new RestTemplate(factory);
        } else {
            restTemplate = new RestTemplate();
        }

        restTemplate.setErrorHandler(new RestTemplateErrorHandler());
        return restTemplate;
    }

    /**
     * Creates an {@link ObjectMapper} bean with the time module registered as the primary bean
     * for this type.
     *
     * @return the ObjectMapper
     */
    @Bean
    @Primary
    public ObjectMapper overrideObjectMapperBean() {
        return new ObjectMapper().registerModule(new JavaTimeModule());
    }

    /**
     * Creates a {@link Serializer} bean if none exists.
     *
     * @return the Serializer
     */
    @Bean
    @ConditionalOnMissingBean
    public Serializer serializer() {
        return new Serializer();
    }

}
