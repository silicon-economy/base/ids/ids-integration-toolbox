/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
/**
 * Contains classes that perform general application context configuration, like e.g. the
 * instantiation of beans.
 */

package org.siliconeconomy.idsintegrationtoolbox.config;
