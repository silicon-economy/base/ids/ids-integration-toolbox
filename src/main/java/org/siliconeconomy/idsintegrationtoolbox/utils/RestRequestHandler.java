/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.idsintegrationtoolbox.model.RestRequest;
import org.siliconeconomy.idsintegrationtoolbox.model.output.ResourceData;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.FailedToReadResponseException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * Processes and executes pre-built {@link RestRequest}s.
 *
 * @author Haydar Qarawlus
 */
@Service
@RequiredArgsConstructor
@Log4j2
public class RestRequestHandler implements ErrorCodeGenerator {

    /** Sends HTTP requests. */
    private final @NonNull RestTemplate restTemplate;

    /**
     * Sends a pre-built request and returns the response parsed as string.
     *
     * @return the response as a string.
     * @throws RestClientException if the request could not be sent.
     * @throws ApiInteractionUnsuccessfulException if a response code other than 2xx is returned.
     */
    public String sendRequest(final RestRequest restRequest)
        throws RestClientException, ApiInteractionUnsuccessfulException {

        return sendRequest(restRequest, String.class).getBody();
    }

    /**
     * Builds and sends a request based on the given request information and returns a response
     * parsed into specified class.
     *
     * @param restRequest the RestRequest.
     * @param clazz the class to parse the response into.
     * @param <T> the type parameter of the {@link ResponseEntity} to return.
     * @return the request as response entity.
     * @throws ApiInteractionUnsuccessfulException with a status code specific exception as cause,
     *                                             if the status code is not 2xx.
     */
    private <T> ResponseEntity<T> sendRequest(RestRequest restRequest, Class<T> clazz)
        throws ApiInteractionUnsuccessfulException {

        final var headers = createHeaders(restRequest);
        final var url = buildUrl(restRequest);
        final var request = createRequest(restRequest.getBody(), headers);

        final var responseEntity = restTemplate.exchange(url, restRequest.getHttpMethod(), request,
            clazz);

        log.debug(LoggingUtils.RECEIVED_RESPONSE, responseEntity);
        checkHttpCode(responseEntity.getStatusCode());

        return responseEntity;
    }

    /**
     * Sends a pre-built request. The response to the request will be read and returned as
     * {@link ResourceData} holding information about the mime type and the content as raw bytes.
     *
     * @param restRequest the RestRequest.
     * @return the response as {@link ResourceData}.
     * @throws RestClientException if the request could not be sent.
     * @throws FailedToReadResponseException if the content header is missing.
     * @throws ApiInteractionUnsuccessfulException if a response code other than 2xx is returned.
     */
    public ResourceData sendDataRequest(final RestRequest restRequest) throws RestClientException,
        ApiInteractionUnsuccessfulException {

        final var responseEntity = sendRequest(restRequest, byte[].class);

        try {
            return new ResourceData(Objects.requireNonNull(
                responseEntity.getHeaders().getContentType()).toString(),
                responseEntity.getBody());
        } catch (NullPointerException e) {
            throw new FailedToReadResponseException("ContentType is null.");
        }
    }

    /**
     * Creates the headers for the request by either using the in the {@link RestRequest}
     * included ones, or a set of default headers if none are specified from the default headers.
     *
     * @param restRequest the RestRequest.
     * @return an {@link HttpHeaders} instance
     */
    private HttpHeaders createHeaders(final RestRequest restRequest) {
        var headers = new HttpHeaders();
        if (restRequest.getHeaders().isEmpty()) {
            headers = HttpUtils.getDefaultHttpHeaders();
        }

        restRequest.getHeaders().forEach(headers::add);

        if (restRequest.getUsername() != null && restRequest.getPassword() != null) {
            headers.setBasicAuth(restRequest.getUsername(), restRequest.getPassword());
        }

        return headers;
    }

    /**
     * Builds the URL for the request using the base URL, path (segments), query parameters and
     * path variables specified by the {@link RestRequest}. If both endpointPath and
     * endpointPathSegments are set, endpointPath takes precedence.
     *
     * @param restRequest the RestRequest.
     * @return the full request URL.
     */
    private URI buildUrl(final RestRequest restRequest) {
        var uriBuilder = UriComponentsBuilder.fromHttpUrl(restRequest.getBaseUrl());

        if (restRequest.getEndpointPath() != null) {
            uriBuilder = uriBuilder.path(restRequest.getEndpointPath());
        } else {
            uriBuilder =
                uriBuilder.pathSegment(splitPathSegments(restRequest.getEndpointPathSegments()));
        }

        return uriBuilder
            .queryParams(restRequest.getQueryParameters())
            .build(restRequest.getPathVariables());
    }

    /**
     * Creates an {@link HttpEntity} representing the request to be made.
     *
     * @param body the request body. May be null.
     * @param headers the request headers.
     * @return an HttpEntity representing the request.
     */
    private HttpEntity<String> createRequest(final String body, final HttpHeaders headers) {
        HttpEntity<String> httpEntity;

        if (body == null) {
            httpEntity = new HttpEntity<>(headers);
        } else {
            httpEntity = new HttpEntity<>(body, headers);
        }

        return httpEntity;
    }

    /**
     * Splits the path segments into a list when encountering a '/' separator.
     *
     * @param pathSegments the path segments to be split.
     * @return the list of strings after the split
     */
    private String[] splitPathSegments(final String... pathSegments) {
        ArrayList<String> segments = new ArrayList<>();
        for (String pathSegment : pathSegments) {
            segments.addAll(Arrays.asList(pathSegment.split("/")));
        }
        return segments.toArray(new String[0]);
    }
}
