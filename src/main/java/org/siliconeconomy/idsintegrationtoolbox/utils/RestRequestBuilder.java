/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils;

import java.util.HashMap;

import org.siliconeconomy.idsintegrationtoolbox.model.RestRequest;
import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

/**
 * Creates {@link RestRequest}s using the builder pattern.
 *
 * @author Florian Zimmer
 */
public class RestRequestBuilder implements ErrorCodeGenerator {

    /** HTTP method of the request.  */
    private final HttpMethod httpMethod;

    /** Base URL of the request's recipient. */
    private final String baseUrl;

    /** Path to the endpoint to be called, e.g. "/api/connector". */
    private String endpointPath;

    /** Path segments of the endpoint to be called, e.g. ["api", "connector"]. */
    private String[] endpointPathSegments;

    /**  Username for basic authentication. If set, password is also required. */
    private String username;

    /** Password for basic authentication. If set, username is also required. */
    private String password;

    /** Request body to send with the request. */
    private String body;

    /** HTTP headers to send with the request. */
    private final HashMap<String, String> headers = new HashMap<>();

    /** Query parameters to send with the request. */
    private final MultiValueMap<String, String> queryParameters = new LinkedMultiValueMap<>();

    /** Path variables to send with the request. */
    private final HashMap<String, String> pathVariables = new HashMap<>();

    /**
     * Constructs a new RestRequestBuilder with the specified HTTP method, URL and endpoint path.
     *
     * @param httpMethod {@link HttpMethod} of the request.
     * @param baseUrl base URL of the request's recipient.
     * @param endpointPath the path of the endpoint to be called, e.g. "/api/connector".
     */
    public RestRequestBuilder(final HttpMethod httpMethod, final String baseUrl,
                              final String endpointPath) {
        this.httpMethod = httpMethod;
        this.baseUrl = baseUrl;
        this.endpointPath = endpointPath;
    }

    /**
     * Constructs a new RestRequestBuilder with the specified HTTP method, URL and endpoint path
     * segments.
     *
     * @param httpMethod {@link HttpMethod} of the request.
     * @param baseUrl base URL of the request's recipient.
     * @param endpointPathSegments the path of the endpoint to be called, e.g. "api", "connector".
     */
    public RestRequestBuilder(final HttpMethod httpMethod, final String baseUrl,
                              final String... endpointPathSegments) {
        this.httpMethod = httpMethod;
        this.baseUrl = baseUrl;
        this.endpointPathSegments = endpointPathSegments;
    }

    /**
     * Sets the request body. Optional operation.
     *
     * @param body the request body
     * @return RestRequestExecutor to add further properties
     */
    public RestRequestBuilder body(final String body) {
        this.body = body;
        return this;
    }

    /**
     * Sets the username and password for basic authentication. Optional operation.
     *
     * @param username the username.
     * @param password the password.
     * @return RestRequestExecutor to add further properties
     */
    public RestRequestBuilder basicAuth(final String username, final String password) {
        this.username = username;
        this.password = password;
        return this;
    }

    /**
     * Adds additional headers.
     *
     * @param key the name of the header.
     * @param value the value of the header.
     * @return RestRequestExecutor to add further properties
     */
    public RestRequestBuilder header(final String key, final String value) {
        this.headers.put(key, value);
        return this;
    }

    /**
     * Adds additional query parameters.
     *
     * @param key the name of the parameter.
     * @param value the value of the parameter.
     * @return RestRequestExecutor to add further properties
     */
    public RestRequestBuilder queryParameter(final String key, final String value) {
        this.queryParameters.add(key, value);
        return this;
    }

    /**
     * Replaces the placeholders for path variables with actual values. In order for this to have
     * any effect, respective placeholders must be present in the URL or path (segments).
     * Placeholders are identified by curly brackets ({placeholder-name}).
     *
     * <p>
     *  Example:
     *      in https://example.com/{id}/info the placeholder for "id" is to be replaced.
     *      in https://example.com/123/info there is no placeholder to be replaced.
     * </p>
     *
     * <p>
     *  Note: If the URL or path (segments) contain any placeholders, these must be replaced using
     *  this method in order for the request to be successful!
     * </p>
     *
     * @param key the key used in the placeholder.
     * @param value the value to replace the placeholder.
     * @return RestRequestExecutor to add further properties
     */
    public RestRequestBuilder pathVariable(final String key, final String value) {
        this.pathVariables.put(key, value);
        return this;
    }

    /**
     * Builds and returns a RestRequest according to all parameters set by previous method calls
     * on this RestRequestBuilder instance.
     *
     * @return the RestRequest containing all necessary properties.
     */
    public RestRequest build() {
        return new RestRequest(
            httpMethod,
            baseUrl,
            endpointPath,
            endpointPathSegments,
            username,
            password,
            body,
            headers,
            queryParameters,
            pathVariables
        );
    }

}
