/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils;

import java.util.List;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * Provides HTTP related utility methods.
 *
 * @author Marc Peschke
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class HttpUtils {

    /**
     * Response content-type json.
     */
    public static final MediaType JSON = MediaType.valueOf("application/json");

    /**
     * Response content-type json-ld.
     */
    public static final MediaType JSON_LD = MediaType.valueOf("application/ld+json");

    /**
     * Response content-type json hal.
     */
    public static final MediaType HAL = MediaType.valueOf("application/hal+json");


    /**
     * Builds the default HTTP headers to use for communication with the Dataspace Connector.
     *
     * @return an HttpHeaders instance
     **/
    public static HttpHeaders getDefaultHttpHeaders() {
        final var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(List.of(JSON, HAL, JSON_LD));
        return headers;
    }

}
