/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.client;

/**
 * Thrown to indicate that the server responded with status code 408 (Request Timeout).
 *
 * @author Johannes Pieperbeck
 */
public class RequestTimeoutException extends HttpClientException {

    /** The exception message. */
    private static final String MESSAGE = "408 - Request Timeout.";

    /**
     * Constructs a RequestTimeoutException.
     */
    public RequestTimeoutException() {
        super(MESSAGE);
    }

    /**
     * Constructs a RequestTimeoutException with the specified cause.
     *
     * @param cause the cause.
     */
    public RequestTimeoutException(Throwable cause) {
        super(MESSAGE, cause);
    }
}
