/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils.exceptions;

/**
 * Thrown to indicate that a response stream could not be read.
 *
 * @author Ronja Quensel
 */
public class FailedToReadResponseException extends ApiInteractionUnsuccessfulException {

    /**
     * Constructs a FailedToReadResponseException with the specified detail message.
     *
     * @param message the detail message.
     */
    public FailedToReadResponseException(final String message) {
        super(message);
    }

    /**
     * Constructs a FailedToReadResponseException with the specified detail message and cause.
     *
     * @param message the detail message.
     * @param cause the cause.
     */
    public FailedToReadResponseException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
