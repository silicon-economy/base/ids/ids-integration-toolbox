/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.client;

/**
 * Thrown to indicate that the server responded with status code 401 (Unauthorized).
 *
 * @author Johannes Pieperbeck
 */
public class UnauthorizedException extends HttpClientException {

    /** The exception message. */
    private static final String MESSAGE = "401 - Unauthorized.";

    /**
     * Constructs an UnauthorizedException.
     */
    public UnauthorizedException() {
        super(MESSAGE);
    }

    /**
     * Constructs an UnauthorizedException with the specified cause.
     *
     * @param cause the cause.
     */
    public UnauthorizedException(Throwable cause) {
        super(MESSAGE, cause);
    }
}
