/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
/**
 * Contains all custom exceptions related to unexpected HTTP status codes indicating an error on
 * the server side.
 */

package org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.server;
