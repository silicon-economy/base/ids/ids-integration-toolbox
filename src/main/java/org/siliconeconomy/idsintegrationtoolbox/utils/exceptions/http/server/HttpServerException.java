/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.server;

import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.HttpCommunicationException;

/**
 * Thrown to indicate an error on the server side.
 *
 * @author Johannes Pieperbeck
 */
public class HttpServerException extends HttpCommunicationException {

    /**
     * Constructs an HttpServerException with the specified detail message.
     *
     * @param message the detail message.
     */
    public HttpServerException(final String message) {
        super(message);
    }

    /**
     * Constructs an HttpServerException with the specified detail message and cause.
     *
     * @param message the detail message.
     * @param cause the cause.
     */
    public HttpServerException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
