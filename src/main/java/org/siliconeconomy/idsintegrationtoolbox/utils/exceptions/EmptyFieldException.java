/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils.exceptions;

/**
 * Thrown to indicate that a field which should have been extracted does not exist.
 *
 * @author Florian Zimmer
 */
public class EmptyFieldException extends Exception {

    /**
     * Constructs an EmptyFieldException with the specified detail message.
     *
     * @param message the detail message.
     */
    public EmptyFieldException(final String message) {
        super(message);
    }

    /**
     * Constructs an EmptyFieldException with the specified detail message and cause.
     *
     * @param message the detail message.
     * @param cause the cause.
     */
    public EmptyFieldException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
