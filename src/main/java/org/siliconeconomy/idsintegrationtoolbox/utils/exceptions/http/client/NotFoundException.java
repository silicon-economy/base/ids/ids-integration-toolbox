/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.client;

/**
 * Thrown to indicate that the server responded with status code 404 (Not Found).
 *
 * @author Johannes Pieperbeck
 */
public class NotFoundException extends HttpClientException {

    /** The exception message. */
    private static final String MESSAGE = "404 - Not Found.";

    /**
     * Constructs a NotFoundException.
     */
    public NotFoundException() {
        super(MESSAGE);
    }

    /**
     * Constructs a NotFoundException with the specified cause.
     *
     * @param cause the cause.
     */
    public NotFoundException(Throwable cause) {
        super(MESSAGE, cause);
    }
}
