/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http;

/**
 * Thrown to indicate that HTTP communication failed, i.e. a non 2xx status code was returned.
 *
 * @author Johannes Pieperbeck
 */
public class HttpCommunicationException extends Exception {

    /**
     * Constructs an HttpCommunicationException with the specified detail message.
     *
     * @param message the detail message.
     */
    public HttpCommunicationException(String message) {
        super("HTTP communication failed: " + message);
    }

    /**
     * Constructs an HttpCommunicationException with the specified detail message and cause.
     *
     * @param message the detail message.
     * @param cause the cause.
     */
    public HttpCommunicationException(String message, Throwable cause) {
        super(message, cause);
    }
}
