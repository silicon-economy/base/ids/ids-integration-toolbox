/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils.exceptions;

/**
 * Thrown to indicate that the response to a description request could not be parsed to an
 * Information Model object, i.e. an invalid response has been returned.
 *
 * @author Ronja Quensel
 */
public class NoParsableResponseException extends Exception {

    /**
     * Constructs a NoParsableResponseException with the specified detail message.
     *
     * @param message the detail message.
     */
    public NoParsableResponseException(final String message) {
        super(message);
    }

    /**
     * Constructs a NoParsableResponseException with the specified detail message and cause.
     *
     * @param message the detail message.
     * @param cause the cause.
     */
    public NoParsableResponseException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
