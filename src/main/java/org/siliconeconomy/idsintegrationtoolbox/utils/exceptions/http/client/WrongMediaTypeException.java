/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.client;

/**
 * Thrown to indicate that the server responded with status code 415 (Wrong Media Type).
 *
 * @author Johannes Pieperbeck
 */
public class WrongMediaTypeException extends HttpClientException {

    /** The exception message. */
    private static final String MESSAGE = "415 - Wrong Media Type.";

    /**
     * Constructs a WrongMediaTypeException.
     */
    public WrongMediaTypeException() {
        super(MESSAGE);
    }

    /**
     * Constructs a WrongMediaTypeException with the specified cause.
     *
     * @param cause the cause.
     */
    public WrongMediaTypeException(Throwable cause) {
        super(MESSAGE, cause);
    }
}
