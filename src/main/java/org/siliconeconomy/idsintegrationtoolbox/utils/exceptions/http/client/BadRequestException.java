/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.client;

/**
 * Thrown to indicate that the server responded with status code 400 (Bad Request).
 *
 * @author Johannes Pieperbeck
 */
public class BadRequestException extends HttpClientException {

    /** The exception message. */
    private static final String MESSAGE = "400 - Bad Request.";

    /**
     * Constructs a BadRequestException.
     */
    public BadRequestException() {
        super(MESSAGE);
    }

    /**
     * Constructs a BadRequestException with the specified cause.
     *
     * @param cause the cause.
     */
    public BadRequestException(Throwable cause) {
        super(MESSAGE, cause);
    }
}
