/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils.exceptions;

/**
 * Thrown to indicate that no valid EntityInput instance can be created from the supplied
 * parameters.
 *
 * @author Ronja Quensel
 */
public class InputValidationException extends RuntimeException {

    /**
     * Constructs an InputValidationException with the specified detail message.
     *
     * @param message the detail message.
     */
    public InputValidationException(final String message) {
        super(message);
    }

}
