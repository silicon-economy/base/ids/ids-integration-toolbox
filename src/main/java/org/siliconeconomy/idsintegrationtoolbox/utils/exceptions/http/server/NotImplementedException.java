/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.server;

/**
 * Thrown to indicate that the server responded with status code 501 (Not Implemented).
 *
 * @author Johannes Pieperbeck
 */
public class NotImplementedException extends HttpServerException {

    /** The exception message. */
    private static final String MESSAGE = "501 - Not Implemented.";

    /**
     * Constructs a NotImplementedException.
     */
    public NotImplementedException() {
        super(MESSAGE);
    }

    /**
     * Constructs a NotImplementedException with the specified cause.
     *
     * @param cause the cause.
     */
    public NotImplementedException(final Throwable cause) {
        super(MESSAGE, cause);
    }
}
