/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.client;

import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.HttpCommunicationException;

/**
 * Thrown to indicate an error on the client side.
 *
 * @author Johannes Pieperbeck
 */
public class HttpClientException extends HttpCommunicationException {

    /**
     * Constructs an HttpClientException with the specified detail message.
     *
     * @param message the detail message.
     */
    public HttpClientException(final String message) {
        super(message);
    }

    /**
     * Constructs an HttpClientException with the specified detail message and cause.
     *
     * @param message the detail message.
     * @param cause the cause.
     */
    public HttpClientException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
