/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.client;

/**
 * Thrown to indicate that the server responded with status code 417 (Expectation Failed).
 *
 * @author Johannes Pieperbeck
 */
public class ExpectationFailedException extends HttpClientException {

    /** The exception message. */
    private static final String MESSAGE = "417 - Expectation Failed.";

    /**
     * Constructs an ExpectationFailedException.
     */
    public ExpectationFailedException() {
        super(MESSAGE);
    }

    /**
     * Constructs an ExpectationFailedException with the specified cause.
     *
     * @param cause the cause.
     */
    public ExpectationFailedException(Throwable cause) {
        super(MESSAGE, cause);
    }
}
