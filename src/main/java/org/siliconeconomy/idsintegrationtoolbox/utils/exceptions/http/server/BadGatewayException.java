/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.server;

/**
 * Thrown to indicate that the server responded with status code 502 (Bad Gateway).
 *
 * @author Johannes Pieperbeck
 */
public class BadGatewayException extends HttpServerException {

    /** The exception message. */
    private static final String MESSAGE = "502 - Bad Gateway.";

    /**
     * Constructs a BadGatewayException.
     */
    public BadGatewayException() {
        super(MESSAGE);
    }

    /**
     * Constructs a BadGatewayException with the specified cause.
     *
     * @param cause the cause.
     */
    public BadGatewayException(final Throwable cause) {
        super(MESSAGE, cause);
    }
}
