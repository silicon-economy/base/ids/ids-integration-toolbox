/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.server;

/**
 * Thrown to indicate that the server responded with status code 500 (Internal Server Error).
 *
 * @author Johannes Pieperbeck
 */
public class InternalServerErrorException extends HttpServerException {

    /** The exception message. */
    private static final String MESSAGE = "500 - Internal Server Error.";

    /**
     * Constructs an InternalServerErrorException.
     */
    public InternalServerErrorException() {
        super(MESSAGE);
    }

    /**
     * Constructs an InternalServerErrorException with the specified cause.
     *
     * @param cause the cause.
     */
    public InternalServerErrorException(final Throwable cause) {
        super(MESSAGE, cause);
    }
}
