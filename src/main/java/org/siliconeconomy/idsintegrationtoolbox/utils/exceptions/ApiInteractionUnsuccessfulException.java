/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils.exceptions;

/**
 * Thrown to indicate that an API call has been unsuccessful.
 *
 * @author Ronja Quensel
 */
public class ApiInteractionUnsuccessfulException extends Exception {

    /**
     * Constructs an ApiInteractionUnsuccessfulException with the specified detail message.
     *
     * @param message the detail message.
     */
    public ApiInteractionUnsuccessfulException(final String message) {
        super(message);
    }

    /**
     * Constructs an ApiInteractionUnsuccessfulException with the specified detail message and
     * cause.
     *
     * @param message the detail message.
     * @param cause the cause.
     */
    public ApiInteractionUnsuccessfulException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
