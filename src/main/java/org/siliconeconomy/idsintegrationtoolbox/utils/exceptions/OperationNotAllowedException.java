/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils.exceptions;

/**
 * Thrown to indicate the attempt to perform/call an operation that is not allowed. Used to override
 * methods from superclasses that are not supported by a specific implementation.
 *
 * @author Ronja Quensel
 */
public class OperationNotAllowedException extends RuntimeException {

    /**
     * Constructs an OperationNotAllowedException with the specified detail message.
     *
     * @param message the detail message.
     */
    public OperationNotAllowedException(final String message) {
        super(message);
    }

    /**
     * Constructs an OperationNotAllowedException with the specified detail message and cause.
     *
     * @param message the detail message.
     * @param cause the cause.
     */
    public OperationNotAllowedException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
