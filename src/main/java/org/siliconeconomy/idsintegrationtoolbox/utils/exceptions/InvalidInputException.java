/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils.exceptions;

/**
 * Thrown to indicate that invalid input has been supplied to a method.
 *
 * @author Ronja Quensel
 */
public class InvalidInputException extends RuntimeException {

    /**
     * Constructs an InvalidInputException with the specified detail message.
     *
     * @param message the detail message.
     */
    public InvalidInputException(final String message) {
        super(message);
    }

    /**
     * Constructs an InvalidInputException with the specified detail message and cause.
     *
     * @param message the detail message.
     * @param cause the cause.
     */
    public InvalidInputException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
