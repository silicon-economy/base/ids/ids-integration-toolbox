/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.idsintegrationtoolbox.core.DscApiOperator;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

/**
 * Performs an online status check for the wrapped connector upon application start. This is to
 * ensure that the connector is running and reachable, as otherwise the toolbox classes cannot
 * function properly.
 *
 * @author Johannes Pieperbeck
 */
@Component
@RequiredArgsConstructor
@Log4j2
public class HealthChecker extends DscApiOperator {

    /** Milliseconds to wait in between communication attempts with the connector. */
    @Value("${connector.health-check.delay:10000}")
    private int healthCheckDelay;

    /** Number of tries for establishing successful communication with the connector. */
    @Value("${connector.health-check.tries:10000}")
    private int healthCheckTries;

    /** Path for entrypoint for the API. */
    @Value("${connector.api.path.entrypoint:/api}")
    private String entrypoint;

    /** Handles the sending of REST requests. */
    private final @NonNull RestRequestHandler requestHandler;

    /**
     * Checks whether the wrapped connector is reachable using the connection properties from the
     * application.properties file by trying to reach it for a defined number
     * of times.
     *
     * @return true, if the connector is reachable; false otherwise.
     */
    public boolean isConnectorReachable() {
        try {
            testConnection();
            return true;
        } catch (ApiInteractionUnsuccessfulException e) {
            return false;
        }
    }

    /**
     * Verifies the connection to the wrapped connector using the connection properties from the
     * application.properties file by trying to reach it for a defined number of times.
     * Depending on the exitOnError parameter, the method will throw an exception or shut down
     * the application, if the connector can not be reached.
     *
     * @param exitOnError whether to shut down the application if verification fails.
     * @throws ApiInteractionUnsuccessfulException if verification fails and exitOnError = false.
     */
    public void verifyConnection(final boolean exitOnError)
            throws ApiInteractionUnsuccessfulException {
        try {
            testConnection();
        } catch (ApiInteractionUnsuccessfulException e) {
            if (exitOnError) {
                log.error("Connector not reachable. Shutting down application...");
                System.exit(1);
            } else {
                throw e;
            }
        }
    }

    /**
     * Tries to reach the wrapped connector. Therefore the /api path, which returns a list of
     * available API paths and requires basic authentication, is called periodically. The number
     * of tries as well as the amount of time to wait in between tries can be configured via
     * properties. If no 2xx response code is returned in the specified number of tries, an
     * exception is thrown.
     *
     * @throws ApiInteractionUnsuccessfulException if the connector does not return 2xx status code.
     */
    private void testConnection() throws ApiInteractionUnsuccessfulException {
        // Build request for getting the public self description
        final var request = new RestRequestBuilder(HttpMethod.GET, connectorUrl, entrypoint)
            .basicAuth(connectorUsername, connectorPassword)
            .build();

        log.info("Beginning to perform connector health check...");

        // Periodically try to send request until connector is ready to accept requests
        var requestSuccessful = false;
        var errorCounter = 0;
        while (!requestSuccessful) {
            try {
                Thread.sleep(healthCheckDelay);

                log.debug("Sending request to connector...");
                requestHandler.sendRequest(request);

                // If no exception thrown, status code of response was 2xx
                requestSuccessful = true;
                log.info("Successfully performed health check. Connector is ready to accept"
                    + " requests.");
            } catch (Exception e) {
                log.warn("Failed to communicate with connector: {}", e.getMessage());
                errorCounter++;

                // If the configured maximum number of tries has been reached, throw exception
                if (errorCounter >= healthCheckTries) {
                    Thread.currentThread().interrupt();
                    log.error("Failed to communicate with connector {} times.", healthCheckTries);
                    throw new ApiInteractionUnsuccessfulException("Failed to communicate with"
                        + " connector " + healthCheckTries + "times. Connector not reachable.", e);
                }
            }
        }
    }
}
