/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils;

import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.HttpCommunicationException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.client.BadRequestException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.client.ExpectationFailedException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.client.NotFoundException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.client.RequestTimeoutException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.client.UnauthorizedException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.client.WrongMediaTypeException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.server.BadGatewayException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.server.InternalServerErrorException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.server.NotImplementedException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.http.server.ServiceUnavailableException;
import org.springframework.http.HttpStatus;

/**
 * Generates custom exceptions based on HTTP status codes. To be implemented by classes performing
 * HTTP requests.
 *
 * @author Johannes Pieperbeck
 */
public interface ErrorCodeGenerator {

    /**
     * Checks the response status code and throws respective exceptions if the status code is not
     * 2xx.
     *
     * @param status the response status code.
     * @throws ApiInteractionUnsuccessfulException with a status code specific exception as cause,
     *                                             if the status code is not 2xx.
     */
    default void checkHttpCode(final HttpStatus status) throws ApiInteractionUnsuccessfulException {
        if (status.is2xxSuccessful()) {
            return;
        }

        Throwable cause;
        switch (status) {
            case UNAUTHORIZED:
                cause = new UnauthorizedException();
                break;
            case NOT_FOUND:
                cause = new NotFoundException();
                break;
            case EXPECTATION_FAILED:
                cause = new ExpectationFailedException();
                break;
            case UNSUPPORTED_MEDIA_TYPE:
                cause = new WrongMediaTypeException();
                break;
            case BAD_REQUEST:
                cause = new BadRequestException();
                break;
            case REQUEST_TIMEOUT:
                cause = new RequestTimeoutException();
                break;
            case INTERNAL_SERVER_ERROR:
                cause = new InternalServerErrorException();
                break;
            case SERVICE_UNAVAILABLE:
                cause = new ServiceUnavailableException();
                break;
            case BAD_GATEWAY:
                cause = new BadGatewayException();
                break;
            case NOT_IMPLEMENTED:
                cause = new NotImplementedException();
                break;
            default:
                cause = new HttpCommunicationException("Unknown error code: " + status);
        }

        final var message = "Communication with Connector API was unsuccessful.";
        throw new ApiInteractionUnsuccessfulException(message, cause);
    }
}
