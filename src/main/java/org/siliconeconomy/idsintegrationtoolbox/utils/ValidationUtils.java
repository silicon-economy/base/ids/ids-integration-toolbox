/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.InvalidInputException;

/**
 * Provides methods for validating e.g. the input to a method.
 *
 * @author Ronja Quensel
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ValidationUtils {

    /**
     * Checks whether a given object is null and throws an {@link InvalidInputException} with the
     * specified detail message if it is.
     *
     * @param object the object.
     * @param message the message.
     * @throws InvalidInputException if the object is null.
     */
    public static void assertNotNull(final Object object, final String message)
        throws InvalidInputException {
        if (object == null) {
            throw new InvalidInputException(message);
        }
    }

}
