/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils;

import java.io.FileNotFoundException;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.core.io.FileSystemResource;

/**
 * Provides utility methods regarding file handling.
 *
 * @author Haydar Qarawlus
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FileUtils {

    /**
     * Opens the file provided as the path (if exists) and returns a FileSystemResource Object.
     *
     * @param filePath The path of file to be read as FileSystemResource.
     * @return FileSystemResource A Spring FileSystemResource to be used later.
     *
     */
    public static FileSystemResource readFile(final String filePath) throws FileNotFoundException {
        final var fileResource = new FileSystemResource(filePath);
        if (!fileResource.getFile().isFile()) {
            throw new FileNotFoundException("File not found!");
        }
        return fileResource;

    }

}
