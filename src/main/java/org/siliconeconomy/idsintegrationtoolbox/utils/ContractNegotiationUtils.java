/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils;

import static org.siliconeconomy.idsintegrationtoolbox.utils.ValidationUtils.assertNotNull;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import de.fraunhofer.iais.eis.ArtifactImpl;
import de.fraunhofer.iais.eis.ContractOffer;
import de.fraunhofer.iais.eis.Duty;
import de.fraunhofer.iais.eis.DutyBuilder;
import de.fraunhofer.iais.eis.DutyImpl;
import de.fraunhofer.iais.eis.Permission;
import de.fraunhofer.iais.eis.PermissionBuilder;
import de.fraunhofer.iais.eis.PermissionImpl;
import de.fraunhofer.iais.eis.Prohibition;
import de.fraunhofer.iais.eis.ProhibitionBuilder;
import de.fraunhofer.iais.eis.ProhibitionImpl;
import de.fraunhofer.iais.eis.Resource;
import de.fraunhofer.iais.eis.Rule;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.InvalidInputException;

/**
 * Provides utility methods for the contract negotiation workflow.
 *
 * @author Ronja Quensel
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ContractNegotiationUtils {

    private static final String RESOURCE_NULL_MESSAGE = "Resource must not be null!";

    private static final String TARGET_NULL_MESSAGE = "Target must not be null!";

    /**
     * Returns the IDs of all artifacts associated with a given resource.
     *
     * @param resource the resource.
     * @return a list of artifact IDs.
     * @throws InvalidInputException if resource is null.
     */
    public static List<URI> getAllArtifactsForResource(final Resource resource)
        throws InvalidInputException {
        assertNotNull(resource, RESOURCE_NULL_MESSAGE);

        final var artifacts = new ArrayList<URI>();

        final var representations = resource.getRepresentation();
        if (representations == null) {
            return artifacts;
        }

        for (var representation : representations) {
            final var instances = representation.getInstance();
            if (instances == null) {
                continue;
            }
            for (var artifact : instances) {
                if (artifact instanceof ArtifactImpl) {
                    artifacts.add(artifact.getId());
                }
            }
        }

        return artifacts;
    }

    /**
     * Compares a list of artifact IDs with the artifacts of a resource and returns a list of all
     * artifact IDs for which a match could be found.
     *
     * @param resource the resource.
     * @param artifactIds the list of artifact IDs.
     * @return the list of artifact IDs for which a match could be found.
     * @throws InvalidInputException if any of the parameters is null.
     */
    public static List<URI> getArtifactsForResourceById(final Resource resource,
                                                        final List<URI> artifactIds)
        throws InvalidInputException {
        assertNotNull(resource, RESOURCE_NULL_MESSAGE);
        assertNotNull(artifactIds, "ArtifactIds must not be null!");

        final var artifacts = new ArrayList<URI>();

        final var representations = resource.getRepresentation();
        if (representations == null) {
            return artifacts;
        }

        for (var representation : representations) {
            final var instances = representation.getInstance();
            if (instances == null) {
                continue;
            }
            for (var artifact : instances) {
                for (var artifactId : artifactIds) {
                    if (artifact instanceof ArtifactImpl && artifact.getId().equals(artifactId)) {
                        artifacts.add(artifactId);
                    }
                }
            }
        }

        return artifacts;
    }

    /**
     * Searches for a {@link ContractOffer} with an ID from a given list in the contract offers
     * of a resource. Returns the matching contract offer, if there is a match.
     *
     * @param resource the resource.
     * @param contractIds the contract IDs.
     * @return the contract offer with a matching ID or null.
     * @throws InvalidInputException if no contract with one of the given IDs was found or if one
     *                               of the parameters is null.
     */
    public static ContractOffer findContractForResourceById(final Resource resource,
                                                            final List<URI> contractIds)
        throws InvalidInputException {
        assertNotNull(resource, RESOURCE_NULL_MESSAGE);
        assertNotNull(contractIds, "ContractIds must not be null!");

        for (var contract : resource.getContractOffer()) {
            for (var contractId : contractIds) {
                if (contract.getId().equals(contractId)) {
                    return contract;
                }
            }
        }

        throw new InvalidInputException("Could not find contract with matching ID.");
    }

    /**
     * Searches for a {@link ContractOffer} with a given ID in the contract offers of a resource.
     * Returns the matching contract offer, if there is a match.
     *
     * @param resource the resource.
     * @param contractId the contract ID.
     * @return the contract offer with a matching ID.
     * @throws InvalidInputException if no contract with the given ID was found.
     * @throws InvalidInputException if any of the parameters is null.
     */
    public static ContractOffer findContractForResourceById(final Resource resource,
                                                            final URI contractId)
        throws InvalidInputException {
        assertNotNull(resource, RESOURCE_NULL_MESSAGE);

        for (var contract : resource.getContractOffer()) {
            if (contract.getId().equals(contractId)) {
                return contract;
            }
        }

        throw new InvalidInputException("Could not find contract with matching ID.");
    }

    /**
     * Gets all rules for a given {@link ContractOffer}. This includes all permissions, all
     * prohibitions and all duties listed in the contract offer.
     *
     * @param contract the contract offer.
     * @return a list of rules.
     * @throws InvalidInputException if contract is null.
     */
    public static List<Rule> getAllRulesFromContract(final ContractOffer contract)
        throws InvalidInputException {
        assertNotNull(contract, "Contract must not be null!");

        final var rules = new ArrayList<Rule>();

        if (contract.getPermission() != null) {
            rules.addAll(contract.getPermission());
        }

        if (contract.getProhibition() != null) {
            rules.addAll(contract.getProhibition());
        }

        if (contract.getObligation() != null) {
            rules.addAll(contract.getObligation());
        }

        return rules;
    }

    /**
     * Adds a list of targets to a list of rules. Creates a copy of each rule for each target and
     * adds the respective target. All created rules are returned in a list.
     *
     * @param rules the rules.
     * @param targets the target.
     * @return a list of rules, which contains each rule for each target.
     * @throws InvalidInputException if any of the parameters is null.
     */
    public static List<Rule> addTargetToRules(final List<Rule> rules, final List<URI> targets)
        throws InvalidInputException {
        assertNotNull(rules, "Rules must not be null!");
        assertNotNull(targets, "Targets must not be null!");

        final var rulesWithTarget = new ArrayList<Rule>();

        for (var rule : rules) {
            for (var target : targets) {
                rulesWithTarget.add(addTargetToRule(rule, target));
            }
        }

        return rulesWithTarget;
    }

    /**
     * Adds a target to a rule. Internally calls {@link #addTargetToPermission(Permission, URI)},
     * {@link #addTargetToProhibition(Prohibition, URI)} or {@link #addTargetToDuty(Duty, URI)}
     * depending on the type of rule.
     *
     * @param rule the rule.
     * @param target the target.
     * @return a copy of the rule with the specified target.
     * @throws InvalidInputException if any of the parameters is null.
     */
    public static Rule addTargetToRule(final Rule rule, final URI target)
        throws InvalidInputException {
        if (rule instanceof PermissionImpl) {
            return addTargetToPermission((Permission) rule, target);
        } else if (rule instanceof ProhibitionImpl) {
            return addTargetToProhibition((Prohibition) rule, target);
        } else if (rule instanceof DutyImpl) {
            return addTargetToDuty((Duty) rule, target);
        } else {
            throw new IllegalArgumentException("Unknown rule type encountered: " + rule);
        }
    }

    /**
     * Adds a target to a {@link Permission}. As a permission cannot be modified after it's been
     * created, a copy of the permission is created with the specified target.
     *
     * @param permission the permission.
     * @param target the target.
     * @return a copy of the permission with the specified target.
     * @throws InvalidInputException if any of the parameters is null.
     */
    private static Permission addTargetToPermission(final Permission permission, final URI target)
        throws InvalidInputException {
        assertNotNull(permission, "Permission must not be null!");
        assertNotNull(target, TARGET_NULL_MESSAGE);

        return new PermissionBuilder(permission.getId())
            ._target_(target)
            ._action_(permission.getAction())
            ._assignee_(permission.getAssignee())
            ._assigner_(permission.getAssigner())
            ._constraint_(permission.getConstraint())
            ._description_(permission.getDescription())
            ._title_(permission.getTitle())
            ._preDuty_(permission.getPreDuty())
            ._postDuty_(permission.getPostDuty())
            .build();
    }

    /**
     * Adds a target to a {@link Prohibition}. As a prohibition cannot be modified after it's been
     * created, a copy of the prohibition is created with the specified target.
     *
     * @param prohibition the prohibition.
     * @param target the target.
     * @return a copy of the prohibition with the specified target.
     * @throws InvalidInputException if any of the parameters is null.
     */
    private static Prohibition addTargetToProhibition(final Prohibition prohibition,
                                                      final URI target)
        throws InvalidInputException {
        assertNotNull(prohibition, "Prohibition must not be null!");
        assertNotNull(target, TARGET_NULL_MESSAGE);

        return new ProhibitionBuilder(prohibition.getId())
            ._target_(target)
            ._action_(prohibition.getAction())
            ._assignee_(prohibition.getAssignee())
            ._assigner_(prohibition.getAssigner())
            ._constraint_(prohibition.getConstraint())
            ._description_(prohibition.getDescription())
            ._title_(prohibition.getTitle())
            .build();
    }

    /**
     * Adds a target to a {@link Duty}. As a duty cannot be modified after it's been created, a
     * copy of the duty is created with the specified target.
     *
     * @param duty the duty.
     * @param target the target.
     * @return a copy of the duty with the specified target.
     * @throws InvalidInputException if any of the parameters is null.
     */
    private static Duty addTargetToDuty(final Duty duty, final URI target)
        throws InvalidInputException {
        assertNotNull(duty, "Duty must not be null!");
        assertNotNull(target, TARGET_NULL_MESSAGE);

        return new DutyBuilder(duty.getId())
            ._target_(target)
            ._action_(duty.getAction())
            ._assignee_(duty.getAssignee())
            ._assigner_(duty.getAssigner())
            ._constraint_(duty.getConstraint())
            ._description_(duty.getDescription())
            ._title_(duty.getTitle())
            .build();
    }

}
