/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Provides constants commonly used for logging or exception messages across different classes.
 *
 * @author Ronja Quensel
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class LoggingUtils {

    /** Log message to use when logging a response received from the DSC. */
    public static final String RECEIVED_RESPONSE = "Received response: [{}]";

    /** Part of exception messages for OperationNotAllowedExceptions. */
    public static final String NOT_ALLOWED = "not allowed.";

}
