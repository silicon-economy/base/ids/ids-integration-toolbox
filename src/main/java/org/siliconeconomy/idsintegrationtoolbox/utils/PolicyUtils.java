/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils;

import java.net.URI;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import de.fraunhofer.iais.eis.Action;
import de.fraunhofer.iais.eis.BinaryOperator;
import de.fraunhofer.iais.eis.ConstraintBuilder;
import de.fraunhofer.iais.eis.DutyBuilder;
import de.fraunhofer.iais.eis.LeftOperand;
import de.fraunhofer.iais.eis.PermissionBuilder;
import de.fraunhofer.iais.eis.ProhibitionBuilder;
import de.fraunhofer.iais.eis.Rule;
import de.fraunhofer.iais.eis.util.RdfResource;
import de.fraunhofer.iais.eis.util.TypedLiteral;
import de.fraunhofer.iais.eis.util.Util;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;


/**
 * Helper class to generate example policies.
 *
 * @author Haydar Qarawlus
 * */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PolicyUtils {


    private static final DateTimeFormatter dateTimeFormatter =
        DateTimeFormatter.ISO_INSTANT;
    private static final String TIMESTAMP_URI = "xsd:dateTimeStamp";

    /**
     * Builds an ids rule to provide access to the resource.
     *
     * @return The ids rule.
     * @throws de.fraunhofer.iais.eis.util.ConstraintViolationException if rule creation fails.
     */
    public static Rule buildProvideAccessRule(final String title) {
        return new PermissionBuilder()
            ._title_(Util.asList(new TypedLiteral(title)))
            ._description_(Util.asList(new TypedLiteral("provide-access")))
            ._action_(Util.asList(Action.USE))
            .build();
    }

    /**
     * Builds an ids rule to prohibit access to the resource.
     *
     * @return The ids rule.
     * @throws de.fraunhofer.iais.eis.util.ConstraintViolationException if rule creation fails.
     */
    public static Rule buildProhibitAccessRule(final String title) {
        return new ProhibitionBuilder()
            ._title_(Util.asList(new TypedLiteral(title)))
            ._description_(Util.asList(new TypedLiteral("prohibit-access")))
            ._action_(Util.asList(Action.USE))
            .build();
    }

    /**
     * Builds an IDS rule to only provide access for 5 times.
     *
     * @return The ids rule.
     * @throws de.fraunhofer.iais.eis.util.ConstraintViolationException if rule creation fails.
     */
    public static Rule buildNumberTimesUsageRule(final String title, final int numberTimes) {
        return new PermissionBuilder()
            ._title_(Util.asList(new TypedLiteral(title)))
            ._description_(Util.asList(new TypedLiteral("n-times-usage")))
            ._action_(Util.asList(Action.USE))
            ._constraint_(Util.asList(new ConstraintBuilder()
                ._leftOperand_(LeftOperand.COUNT)
                ._operator_(BinaryOperator.LTEQ)
                ._rightOperand_(
                    new RdfResource(Integer.toString(numberTimes), URI.create("xsd:double")))
                .build()))
            .build();
    }

    /**
     * Builds an IDS rule to only provide access for a certain duration.
     *
     * @param duration The duration of the rule. E.g. "PT1M30.5S"
     * @return The ids rule.
     * @throws de.fraunhofer.iais.eis.util.ConstraintViolationException if rule creation fails.
     */
    public static Rule buildDurationUsageRule(final String title, final String duration) {
        return new PermissionBuilder()
            ._title_(Util.asList(new TypedLiteral(title)))
            ._description_(Util.asList(new TypedLiteral("duration-usage")))
            ._action_(Util.asList(Action.USE))
            ._constraint_(Util.asList(new ConstraintBuilder()
                ._leftOperand_(LeftOperand.ELAPSED_TIME)
                ._operator_(BinaryOperator.SHORTER_EQ)
                ._rightOperand_(new RdfResource(duration, URI.create("xsd:duration")))
                .build()))
            .build();
    }

    /**
     * Builds an IDS rule to provide access during an interval.
     *
     * @param title The title of the rule.
     * @param startTime the start time for data usage
     * @param endTime the end time for data usage
     *
     * @return The ids rule.
     * @throws de.fraunhofer.iais.eis.util.ConstraintViolationException if rule creation fails.
     */
    public static Rule buildIntervalUsageRule(final String title,
                                              final ZonedDateTime startTime,
                                              final ZonedDateTime endTime) {
        return new PermissionBuilder()
            ._title_(Util.asList(new TypedLiteral(title)))
            ._description_(Util.asList(new TypedLiteral("usage-during-interval")))
            ._action_(Util.asList(Action.USE))
            ._constraint_(Util.asList(new ConstraintBuilder()
                ._leftOperand_(LeftOperand.POLICY_EVALUATION_TIME)
                ._operator_(BinaryOperator.AFTER)
                ._rightOperand_(new RdfResource(dateTimeFormatter.format(startTime),
                    URI.create(TIMESTAMP_URI)))
                .build(), new ConstraintBuilder()
                ._leftOperand_(LeftOperand.POLICY_EVALUATION_TIME)
                ._operator_(BinaryOperator.BEFORE)
                ._rightOperand_(new RdfResource(dateTimeFormatter.format(endTime),
                    URI.create(TIMESTAMP_URI)))
                .build()))
            .build();
    }

    /**
     * Builds an IDS rule to provide access until the resource is deleted.
     *
     * @param title The title of the rule.
     * @param startTime the start time for data usage
     * @param endTime the end time for data usage
     * @param deleteTime the time at which the data will be deleted
     *
     * @return The ids rule.
     * @throws de.fraunhofer.iais.eis.util.ConstraintViolationException if rule creation fails.
     */
    public static Rule buildUsageUntilDeletionRule(final String title,
                                                   final ZonedDateTime startTime,
                                                   final ZonedDateTime endTime,
                                                   final ZonedDateTime deleteTime) {
        return new PermissionBuilder()
            ._title_(Util.asList(new TypedLiteral(title)))
            ._description_(Util.asList(new TypedLiteral("usage-until-deletion")))
            ._action_(Util.asList(Action.USE))
            ._constraint_(Util.asList(new ConstraintBuilder()
                ._leftOperand_(LeftOperand.POLICY_EVALUATION_TIME)
                ._operator_(BinaryOperator.AFTER)
                ._rightOperand_(new RdfResource(dateTimeFormatter.format(startTime),
                    URI.create(TIMESTAMP_URI)))
                .build(), new ConstraintBuilder()
                ._leftOperand_(LeftOperand.POLICY_EVALUATION_TIME)
                ._operator_(BinaryOperator.BEFORE)
                ._rightOperand_(new RdfResource(dateTimeFormatter.format(endTime),
                    URI.create(TIMESTAMP_URI)))
                .build()))
            ._postDuty_(Util.asList(new DutyBuilder()
                ._action_(Util.asList(Action.DELETE))
                ._constraint_(Util.asList(new ConstraintBuilder()
                    ._leftOperand_(LeftOperand.POLICY_EVALUATION_TIME)
                    ._operator_(BinaryOperator.TEMPORAL_EQUALS)
                    ._rightOperand_(new RdfResource(dateTimeFormatter.format(deleteTime),
                        URI.create(TIMESTAMP_URI)))
                    .build()))
                .build()))
            .build();
    }

    /**
     * Builds an IDS rule that logs all resource usage.
     *
     * @param title The title of the rule.
     *
     * @return The ids rule.
     * @throws de.fraunhofer.iais.eis.util.ConstraintViolationException if rule creation fails.
     */
    public static Rule buildUsageLoggingRule(final String title) {
        return new PermissionBuilder()
            ._title_(Util.asList(new TypedLiteral(title)))
            ._description_(Util.asList(new TypedLiteral("usage-logging")))
            ._action_(Util.asList(Action.USE))
            ._postDuty_(Util.asList(new DutyBuilder()
                ._action_(Util.asList(Action.LOG))
                .build()))
            .build();
    }

    /**
     * Builds an IDS rule to create notification upon resource usage.
     *
     * @param title The title of the rule.
     * @param loggingDestination The participant to notify upon data access.
     *
     * @return The ids rule.
     * @throws de.fraunhofer.iais.eis.util.ConstraintViolationException if rule creation fails.
     */
    public static Rule buildUsageNotificationRule(final String title,
                                                  final URI loggingDestination) {
        return new PermissionBuilder()
            ._title_(Util.asList(new TypedLiteral(title)))
            ._description_(Util.asList(new TypedLiteral("usage-notification")))
            ._action_(Util.asList(Action.USE))
            ._postDuty_(Util.asList(new DutyBuilder()
                ._action_(Util.asList(Action.NOTIFY))
                ._constraint_(Util.asList(new ConstraintBuilder()
                    ._leftOperand_(LeftOperand.ENDPOINT)
                    ._operator_(BinaryOperator.DEFINES_AS)
                    ._rightOperand_(new RdfResource(
                        loggingDestination.toString(), URI.create("xsd:anyURI")))
                    .build()))
                .build()))
            .build();
    }

    /**
     * Builds an IDS rule to restrict usage to a specific connector.
     *
     * @param title The title of the rule.
     * @param targetConnectorUri the connector id to restrict access to.
     *
     * @return The ids rule.
     * @throws de.fraunhofer.iais.eis.util.ConstraintViolationException if rule creation fails.
     */
    public static Rule buildConnectorRestrictedUsageRule(final String title,
                                                         final URI targetConnectorUri) {
        return new PermissionBuilder()
            ._title_(Util.asList(new TypedLiteral(title)))
            ._description_(Util.asList(new TypedLiteral("connector-restriction")))
            ._action_(Util.asList(Action.USE))
            ._constraint_(Util.asList(new ConstraintBuilder()
                ._leftOperand_(LeftOperand.SYSTEM)
                ._operator_(BinaryOperator.SAME_AS)
                ._rightOperand_(
                    new RdfResource(targetConnectorUri.toString(),
                        URI.create("xsd:anyURI")))
                .build()))
            .build();
    }

    /**
     * Builds an IDS rule to restrict usage to a specific connector.
     *
     * @param title The title of the rule.
     * @param profile the connector security profile.
     *
     * @return The ids rule.
     * @throws de.fraunhofer.iais.eis.util.ConstraintViolationException if rule creation fails.
     */
    public static Rule buildSecurityProfileRestrictedUsageRule(final String title,
                                                               final String profile) {
        return new PermissionBuilder()
            ._title_(Util.asList(new TypedLiteral(title)))
            ._description_(Util.asList(new TypedLiteral("security-level-restriction")))
            ._action_(Util.asList(Action.USE))
            ._constraint_(Util.asList(new ConstraintBuilder()
                ._leftOperand_(LeftOperand.SECURITY_LEVEL)
                ._operator_(BinaryOperator.EQUALS)
                ._rightOperand_(
                    new RdfResource("https://w3id.org/idsa/code/" + profile,
                        URI.create("xsd:string")))
                .build()))
            .build();
    }

    /**
     * Removes the surrounding quotes of a policy pattern response.
     *
     * @param policyPatternResponse the string of the policy pattern response.
     * @return the same as the input string without surrounding quotes.
     */
    public static String removePolicyPatternSurroundings(String policyPatternResponse) {
        return policyPatternResponse.replaceAll("(^\")|(\"$)", "");
    }
}
