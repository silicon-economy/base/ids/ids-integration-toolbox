/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.idsintegrationtoolbox.utils;

import java.net.URI;
import java.util.UUID;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.AbstractEntity;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.EntityOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.EmptyFieldException;

/**
 * Provides utility methods for converting and extracting UUIDs.
 *
 * @author Florian Zimmer
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class UuidUtils {

    /**
     * Extracts the UUID of an entity from its EntitySingleOutput.
     *
     * @param output the output.
     * @return the extracted UUID.
     * @throws EmptyFieldException if there is no UUID present in the given output.
     */
    public static UUID getUuidFromOutput(final EntityOutput<? extends AbstractEntity> output)
        throws EmptyFieldException {
        try {
            final var parts = output.getLinks().getSelf().getHref().split("/");
            final var uuid = parts[parts.length - 1];

            return UUID.fromString(uuid);
        } catch (NullPointerException | IllegalArgumentException exception) {
            throw new EmptyFieldException("There is no UUID present in the given output.");
        }
    }

    /**
     * Extracts the UUID of an entity from a URI if the UUID is the last part.
     *
     * @param uri the URI to extract a UUID from.
     * @return the extracted UUID.
     * @throws EmptyFieldException if there is no UUID present in the given output.
     */
    public static UUID getUuidFromUri(final URI uri)
        throws EmptyFieldException {
        try {
            final var parts = uri.toString().split("/");
            final var uuid = parts[parts.length - 1];

            return UUID.fromString(uuid);
        } catch (NullPointerException | IllegalArgumentException exception) {
            throw new EmptyFieldException("There is no UUID present in the last part of the URI.");
        }
    }
}
