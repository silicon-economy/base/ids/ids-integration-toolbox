# Changelog
All notable changes to this project will be documented in this file

## [5.2.3] - 2023-04-13

**Compatible [Dataspace Connector Version: 7.0.3](https://github.com/International-Data-Spaces-Association/DataspaceConnector/releases/tag/v7.0.3)**

### Changed
- Update Package License configuration

## [5.2.2] - 2023-04-06

**Compatible [Dataspace Connector Version: 7.0.3](https://github.com/International-Data-Spaces-Association/DataspaceConnector/releases/tag/v7.0.3)**

### Changed
- Use OLFL1.3 License.

## [5.2.1] - 2022-10-13

**Compatible [Dataspace Connector Version: 7.0.3](https://github.com/International-Data-Spaces-Association/DataspaceConnector/releases/tag/v7.0.3)**

### Changed
- Update dependencies' version to remove vulnerabilities caused by transitive dependencies.

### Fixed
- Bug in `RelationLinker`, that caused trying to link an entity with itself

## [5.2.0] - 2022-10-04

**Compatible [Dataspace Connector Version: 7.0.3](https://github.com/International-Data-Spaces-Association/DataspaceConnector/releases/tag/v7.0.3)**

### Fixed
- Use DSC's add and remove functions for replacing relations
- `ResourceFinder` now searches all catalogs

### Added
- Configure Maven Central deployment
- Builders for all input classes

## [5.1.3] - 2022-07-28

**Compatible [Dataspace Connector Version: 7.0.3](https://github.com/International-Data-Spaces-Association/DataspaceConnector/releases/tag/v7.0.3)**

### Fixed
- Fix resource removal workflow
- Update `README`

### Added
- Add project, developer, license and scm information into `pom.xml`

## [5.1.2] - 2022-07-13

**Compatible [Dataspace Connector Version: 7.0.3](https://github.com/International-Data-Spaces-Association/DataspaceConnector/releases/tag/v7.0.3)**

### Fixed
- Fix logic bugs in resource removal workflow
- Fix wrong relation endpoint in RuleContractsApiOperator

### Added
- Add debug logs in ResourceRemover when removing or unlinking elements

## [5.1.1] - 2022-07-06

**Compatible [Dataspace Connector Version: 7.0.3](https://github.com/International-Data-Spaces-Association/DataspaceConnector/releases/tag/v7.0.3)**

### Fixed
- Fix a bug that caused the resource remover workflow to not function correctly

## [5.1.0] - 2022-06-22

**Compatible [Dataspace Connector Version: 7.0.3](https://github.com/International-Data-Spaces-Association/DataspaceConnector/releases/tag/v7.0.3)**

### Added
- Add a workflow to remove resource and their connected components

### Changed
- API operators now return the full model classes with all properties. Casting of the output
  classes is no longer required

## [5.0.3] - 2022-05-17

**Compatible [Dataspace Connector Version: 7.0.3](https://github.com/International-Data-Spaces-Association/DataspaceConnector/releases/tag/v7.0.3)**

### Fixed
- Fix accessors to protected in abstract classes

### Changed
- Change return types of interfaces in`api/entity` to implementations instead of abstract classes

## [5.0.2] - 2022-05-11

**Compatible [Dataspace Connector Version: 7.0.3](https://github.com/International-Data-Spaces-Association/DataspaceConnector/releases/tag/v7.0.3)**

### Fixed
- Add null check for `routeIds` in `ArtifactDataApiOperator`

## [5.0.1] - 2022-04-01

**Compatible [Dataspace Connector Version: 7.0.3](https://github.com/International-Data-Spaces-Association/DataspaceConnector/releases/tag/v7.0.3)**

### Changed
- Update `Spring Boot` to version `2.6.6`. Addresses `CVE-2022-22965`

## [5.0.0] - 2022-03-30

**Compatible [Dataspace Connector Version: 7.0.3](https://github.com/International-Data-Spaces-Association/DataspaceConnector/releases/tag/v7.0.3)**

### Added
- Add AppApiOperator method to get the AppStore, from which an app was downloaded
- Add `api` package and `DscOperator` class to have a single entry point to all toolbox functionalities
  - Add interfaces for all API operating and workflow classes
  - Create `DscOperator` that has references to all interfaces via chained method calls
- Create workflow for querying the Clearing House
- Add `OfferedResourceBrokerRelationOutput` and `OfferedResourceBrokersRelationApiOperator`

### Changed
- Relocate negotiation and pattern methods to ConnectorApiOperator
- Change name of UsageControlApiOperator to ExamplesApiOperator as the other methods were
  relocated to ConnectorApiOperator
- Change AppEndpointsApiOperator to not allow POST, PUT and DELETE operations
- Rename *xSingleOutput* classes to *xOutput*
- Add Jackson sub-type annotations to `EndpointOutput`
- Change HTTP method for linking endpoints and data sources to `PUT`
- Rename link in `RouteLinks` from *artifacts* to *output*

## [4.0.0] - 2022-03-10

**Compatible [Dataspace Connector Version: 7.0.3](https://github.com/International-Data-Spaces-Association/DataspaceConnector/releases/tag/v7.0.3)**

### Added
- Add ArtifactDataApiOperator routeIds query parameter
- Add DatabaseDataSourceInput and RestDataSourceInput deriving from abstract DataSourceInput
- Add DatabaseDataSourceSingleOutput and RestDataSourceSingleOutput deriving from abstract DataSourceSingleOutput
- Add route support for Artifact and Subscription inputs
- Add abstract methods to output classes to avoid parsing to specific sub-types
- Add subtypes for `EntityInput` and `EntitySingleOutput` that provide a title and a description field
- Add workflow class for broker interactions
- Renaming to 'IDS Integration Toolbox' from 'Wrapper Toolbox'

### Changed
- Change PolicyPattern to include value descriptions
- AppApiOperator now uses the right query parameter name 'type' when applying actions
- Remove ConnectorEndpoint
- Change ConfigurationInput property names
- Update to Java 17
- Replace `interaction library` 4.0.10 with `Infomodel java` 4.2.7 and `Infomodel serializer` 4.2.8
- Remove RouteArtifactsApiOperator as it is not needed with DSC v7
- RelationApiOperator has a new method to retrieve a route's related Artifact
- RouteEndpointsApiOperator uses URIs instead of UUIDs for Endpoint IDs
- Change RouteErrorApiOperator route property
- Toolbox now accepts HAL & JSON-LD MediaTypes
- Change values of ActionType enum
- Move field `remoteId` from `EntitySingleOutput` to respective sub-classes

## [3.0.1] - 2022-02-10

**Compatible [Dataspace Connector Version: 6.5.3](https://github.com/International-Data-Spaces-Association/DataspaceConnector/releases/tag/v6.5.3)**

### Changed
- Update Getting Started guide
- Remove JDBC dependencies

## [3.0.0] - 2022-01-27

**Compatible [Dataspace Connector Version: 6.5.3](https://github.com/International-Data-Spaces-Association/DataspaceConnector/releases/tag/v6.5.3)**

### Added
- New Messages endpoints
- New ActuatorInfo endpoint
- New Endpoint endpoints
- New Configuration endpoints
- Entity inputs now support additional fields
- Add broker entity
- Add authentication entity
- New Resource Finder workflow
- GitLab merge request template
- Subscription entity support
- Add keystore entity
- Add truststore entity
- Add proxy entity
- Add subscription workflow
- Add relation linker workflow
- New method for querying Clearing House log entries
- Add route entity
- Add app entity
- Add Appstore entity
- Add resource creator workflow

### Changed
- Verify that `ContractInput`'s end date is never null
- UsageControl API endpoints handling
- HealthChecker now offers two public methods for checking the connection to the linked DSC:
  1. return boolean indicating result without throwing exception
  2. throw exception or shut down application on error

## [2.0.1] - 2021-09-02

**Compatible [Dataspace Connector Version: 5.2.1](https://github.com/International-Data-Spaces-Association/DataspaceConnector/releases/tag/v5.2.1)**

### Added
- Initial version
