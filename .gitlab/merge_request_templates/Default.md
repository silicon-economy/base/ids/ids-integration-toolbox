## Related Issues

<!-- Issues that are closed by or related to this merge request, e.g. "Closes SE-XXXX" or "Related to SE-XXXX" -->

Closes SE-XXXX


## Acceptance Criteria

<!-- Criteria for the MR to be considered mergeable -->

- [ ] The current branch name has the correct format `TYPE/SE-XXXX-name` (e.g. `feature/SE-1234-add-api-endpoint`). Types:
    - breaking-change
    - feature
    - fix
    - docs
    - dev-ops
    - chore
- [ ] The target branch is a more abstract one (e.g. `breaking-change/...`) or the `main` branch (triggers a new release)
- [ ] The title of the MR summarizes the work
    - [ ] If the MR is still work in progress, the title starts with `Draft:`, otherwise, it is handled as ready for review
- [ ] The Git Pipeline runs successfully
    - License headers are used
    - Developer guidelines are met
    - The SonarQube succeeds
    - All used third party licenses are audited
- [ ] The SonarQube Coverage, Code Smells etc. are checked
- If the target branch is the `main` branch:
  - [ ] The version number is incremented according to [SemVer](https://semver.org/) (Snapshot versions are not merged/released) in the `CHANGELOG.md`
- If the target branch is not the `main` branch:
  - [ ] The version number stays untouched
- [ ] The `CHANGELOG.md` is extended by all changes of this MR (and their reasons)
- [ ] The documentation is up to date and reflects the status quo after merge
- [ ] Only released versions of dependencies are used (i.e. no snapshot versions)
- [ ] Both checkboxes (`Delete source branch when merge request is accepted` and `Squash commits when merge request is accepted`) are checked
- [ ] The MR is assigned to a person
- [ ] The MR milestone is set
- [ ] *Optional: The MR is assigned to a reviewer*


## Merge Workflow Checklist *(only for Reviewers)*

<!-- This checklist is only for reviewers -->

- [ ] All acceptance criteria are fulfilled  (checklist above)
- [ ] The merge request message remains as it is
- [ ] The squash commit message is exchanged by the suggested message below
- [ ] If the target branch is the main branch: A Git tag with the correct version number is created

## Proposed Squash Commit Message

<!--
A proposed message for the eventual squashed commit.
Please stick to the following pattern:

- A short one-line summary (max. 50 characters).
- A blank line.
- A detailed explanation of the changes introduced by this merge request.
  Each line should not exceed 72 characters.
*********1*********2*********3*********4*********5*********6*********7** (<-- Ruler for line width assistance)
-->
```
A short one-line summary (max. 50 characters)

* A more detailed explanation of the changes introduced by this merge
  request.
* Each line should not exceed 72 characters.

Co-authored-by: NAME <EMAIL>
```
<!--
*********1*********2*********3*********4*********5*********6*********7** (<-- Ruler for line width assistance)
-->
