[[Top]]
= Tutorial

We describe the main usage scenarios of the IDS Integration Toolbox.

== Installation

In order to make use of the framework one has to first add the respective dependency to maven through their pom.xml. The maven dependency adjustment should look as follows:

[source]
<dependency>
<groupId>org.siliconeconomy</groupId>
<artifactId>ids-integration-toolbox</artifactId>
<version>4.0.0</version>
</dependency>

After the dependency is added, the IDS Integration Toolbox has to be integrated into the designated Spring Boot Application through package scan. +
Therefore, the following packages have to be included in the main spring application:

[source, java]
@SpringBootApplication(scanBasePackages = {"org.siliconeconomy.idsintegrationtoolbox"})

Finally, a number of values have to be added to the _application.properties_ file in order to configure the framework parameters. The respective properties can be found in the source code at _src/main/resources/example.properties._

After having all of this done, the whole functionality of the framework is prepared to be used. To do so, just use the default Spring autowiring and pull the desired classes into the designated code area. The next section provides a short overview on what classes to expect and how to actually use them.

== Example Usage

In general, the framework acts like a wrapper for all of the Dataspace Connector (DSC) API endpoints. It enables developers to access those endpoints through Java source code with respective methods. Rather than returning JSON strings, it returns instantiated objects. To do so, every section mentioned in the DSC Swagger UI got multiple classes to provide those endpoints, all of them extend the super classes BaseApiOperator and RelationApiOperator, which abstract the general operations a section can offer. The BaseApiOperator offers different methods to mimic CRUD operations for entities, while the RelationApiOperator does this for the relation between entities. For example, the artifact section is split up into the following subclasses: ArtifactApiOperator, a child of BaseApiOperator, ArtifactAgreementApiOperator and ArtifactRepresentationApiOperator, both children of RelationApiOperator.

All operators provided by the framework can be obtained through the DscOperator, so a user does not need to know the names of the respective operators they need. They can simply be obtained by chaining method calls on the DscOperator.

In order to access all artifacts, one would have to first add the DscOperator through autowiring into the designated code area. The DscOperator offers a method called entities(). The interface returned by this call in turn offers method for each entity, so next you can call artifacts() to obtain the operator for artifacts. Next, simply use the getAll() method to receive an EntityMultiOutput which encapsulates the response and holds the requested artifacts as well as some additional information. Finally, just call EntityMultiOutput.getEmbedded() to get all the artifacts included in the response. You can find a more detailed code example below:

*Example of getting all artifacts*

[source, java]
--
@Component
public class FooBar {

    /**
    * Provides the entry point to all framework functionalities.
    */
    private DscOperator dscOperator;

    @Autowired
    public FooBar(DscOperator dscOperator) {

        this.dscOperator = dscOperator;

    }

    /**
    * Fetches all artifacts and returns the first one.
    */
    public ArtifactOutput getFirstArtifact() {

        ArtifactMultiOutput response = (ArtifactMultiOutput) dscOperator.entities().artifacts().getAll();
        return response.getEmbedded().getEntries().get(0);

    }

}
--
Besides providing developers with several integration classes and methods for the endpoints, the framework offers several utility classes as well, which represent different workflows. For example, finding a resource in a connector requires multiple concatenated requests. In order to avoid this, the workflow classes offer more convenience by automating these processes. For instance a HealthChecker class exists, which can be used to automatically check if the DSC is already up and running, or there's a ResourceFindingUtil class which provides the developers with easy to use resource finding methods. In conclusion, it's just an easy way to use common workflows.

Workflow classes can be obtained via the DscOperator in different ways: either by calling workflows() directly on the DscOperator, which will provide access to all workflow classes, or by calling entities().workflows() or ids().workflows(), which will provide access to only the respective workflows.

<<Top, Top>>
